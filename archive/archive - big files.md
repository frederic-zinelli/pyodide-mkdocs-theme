
### Stockage en ligne de fichiers volumineux { #big-files }

Les plateformes d'hébergement du type de GitLab/la Forge EN imposent des limitations quant à la taille des artefacts générés lors de pipelines. Ainsi, il peut arriver qu'un build échoue parce que les fichiers de données (csv, xls, images, vidéos, bdd, ...) que l'on souhaite ensuite utiliser dans les exercices sont trop volumineux.

L'espace alloué au dépôt est, quant à lui, beaucoup plus important.
<br>
Une solution simple est alors d'héberger les fichiers volumineux sur le dépôt, mais en dehors du répertoire de la documentation afin que les fichiers ne soient pas intégrés au build, puis de faire les requêtes de récupérations des fichiers directement sur le dépôt, mais en ciblant les adresses des "fichiers bruts", et non les pages visibles simplement lorsqu'on parcourt le dépôt :

!!! tip "Accéder à des fichiers entreposés sur un dépôt de type GitLab"

    ![Récupérer une adresse de fichier brute sur GitLab](!!gitlab_raw_files_png){: loading=lazy .w30 align=right }

    * {{orange("Le dépôt doit être ___public___.")}}
    * Stocker les fichiers en dehors du repertoire de la documentation.
    * Récupérer l'adresse brute du fichier sur le dépôt (accessible comme montré dans la capture ci-contre).
    * Télécharger le fichier depuis l'environnement Pyodide en utilisant cette adresse.

<br>

___Remarques :___

1. Il est nécessaire de travailler avec les adresses absolues de ces fichiers, puisqu'ils ne sont pas stockés sur le même domaine que le site construit.
1. Il peut être intéressant de [créer une bibliothèque personnalisée](--python-libs), avec le thème, contenant un template pour générer plus facilement les adresses vers le fichiers bruts

    ??? help "Utilisation d'un template vers des fichiers bruts sur un dépôt"

        A titre d'exemple, voici comment on pourrait faire pour récupérer des fichiers hébergés dans un dossier `big-files` qui serait hébergé à la racine du site du thème, [`Pyodide-Mkdocs-Theme`][pmt-repo]{: target=_blank } :

        <br>

        === "Créer la bibliothèque"

            À la racine du projet :

            1. Ajouter un dossier `load_big_files`
            1. Y ajouter un fichier `__init__.py` avec le contenu suivant :

                <br>

                ```python
                REPO     = "https://gitlab.com/frederic-zinelli/pyodide-mkdocs-theme"
                BRANCH   = "main"
                DIR      = "big-files"
                TEMPLATE = "{REPO}/-/raw/{BRANCH}/{DIR}/{sub_path}?ref_type=heads"

                async def copy_big_file(sub_path:str):
                    address = TEMPLATE.format(
                        REPO=REPO,
                        BRANCH=BRANCH,
                        DIR=DIR,
                        sub_path=sub_path,
                    )
                    await copy_from_server(address)
                ```

        === "MAJ du fichier `mkdocs.yml`"

            Enregistrer ensuite la nouvelle bibliothèque dans la section `plugins` du fichier `mkdocs.yml`:

            <br>

            ```yaml
            plugins:
                pyodide_macros:
                    build:
                        {{config_validator("build.python_libs", 1)}}:
                            - load_big_files
            ```

        === "Utilisation dans la documentation"

            La fonction `copy_big_file` peut-être utilisée depuis la section `env` d'un [fichier python](--ide-files) associé à un exercice, comme la fonction `copy_from_server` du thème sur laquelle elle repose :

            <br>

            ```python
            # --- PYODIDE:env --- #
            from load_big_file import copy_big_file

            await copy_big_file("chapitre1/images/joconde.jpg")
            # Crée un fichier joconde.jpg à la racine de l'environnement pyodide.
            ```


!!! danger "Volumineux mais pas trop... !"

    [Comme indiqué dans la partie "contexte"](--files-sys-ctx), garder en tête que {{red("_les resources mémoires sont disponibles en quantités limitées dans l'environnement pyodide_")}}, et ces quantités ne sont pas prévisibles car elles dépendent des machines/OS des utilisateurs.

    Le but ici est plus de proposer une solution pour limiter la taille des artefacts lors des pipelines que de proposer une façon d'utiliser des fichiers monstrueusement gros dans pyodide (ce qui planterait l'environnement...).

