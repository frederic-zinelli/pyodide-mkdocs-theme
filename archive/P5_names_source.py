import ast, sys
from pathlib import Path


P5_NAMES = set('''
ADD                      ALT                      ARROW                    AUDIO
AUTO                     AXES                     BACKSPACE                BASELINE
BEVEL                    BEZIER                   BLEND                    BLUR
BOLD                     BOLDITALIC               BOTTOM                   BURN
CENTER                   CHAR                     CHORD                    CLAMP
CLOSE                    CONTAIN                  CONTROL                  CORNER
CORNERS                  COVER                    CROSS                    CURVE
DARKEST                  DEGREES                  DEG_TO_RAD               DELETE
DIFFERENCE               DILATE                   DODGE                    DOWN_ARROW
ENTER                    ERODE                    ESCAPE                   EXCLUSION
FALLBACK                 FILL                     FLAT                     FLOAT
GRAY                     GRID                     HALF_FLOAT               HALF_PI
HAND                     HARD_LIGHT               HSB                      HSL
IMAGE                    IMMEDIATE                INVERT                   ITALIC
LABEL                    LANDSCAPE                LEFT                     LEFT_ARROW
LIGHTEST                 LINEAR                   LINES                    LINE_LOOP
LINE_STRIP               MIRROR                   MITER                    MOVE
MULTIPLY                 NEAREST                  NORMAL                   OPAQUE
OPEN                     OPTION                   OVERLAY                  P2D
PI                       PIE                      POINTS                   PORTRAIT
POSTERIZE                PROJECT                  QUADRATIC                QUADS
QUAD_STRIP               QUARTER_PI               RADIANS                  RADIUS
RAD_TO_DEG               REMOVE                   REPEAT                   REPLACE
RETURN                   RGB                      RGBA                     RIGHT
RIGHT_ARROW              ROUND                    SCREEN                   SHIFT
SMOOTH                   SOFT_LIGHT               SQUARE                   STROKE
SUBTRACT                 TAB                      TAU                      TESS
TEXT                     TEXTURE                  THRESHOLD                TOP
TRIANGLES                TRIANGLE_FAN             TRIANGLE_STRIP           TWO_PI
UNSIGNED_BYTE            UNSIGNED_INT             UP_ARROW                 VERSION
VIDEO                    WAIT                     WEBGL                    WEBGL2
WORD                     _CTX_MIDDLE              _DEFAULT_FILL            _DEFAULT_LEADMULT
_DEFAULT_STROKE          _DEFAULT_TEXT_FILL       __bool__                 __class__
__defineGetter__         __defineSetter__         __delattr__              __delitem__
__dir__                  __doc__                  __eq__                   __format__
__ge__                   __getattribute__         __getitem__              __getstate__
__gt__                   __hash__                 __init__                 __init_subclass__
__le__                   __lookupGetter__         __lookupSetter__         __lt__
__module__               __ne__                   __new__                  __proto__
__reduce__               __reduce_ex__            __repr__                 __setattr__
__setitem__              __sizeof__               __str__                  __subclasshook__
_accessibleOutputs       _accsBackground          _accsCanvasColors        _accsOutput
_addAccsOutput           _angleMode               _areDownKeys             _assert3d
_axesIcon                _bezierDetail            _checkFileExtension      _colorMaxes
_colorMode               _copyHelper              _createFriendlyGlobalFunctionBinder_createOutput
_curElement              _curveDetail             _decrementPreload        _defaultCanvasSize
_defaultGraphicsCreated  _describeElementHTML     _describeHTML            _downKeys
_draw                    _elements                _events                  _frameRate
_fromDegrees             _fromRadians             _gaussian_previous       _getArea
_getContainer            _getPos                  _getTintedImageCanvas    _glAttributes
_grid                    _handleMotion            _hasMouseInteracted      _helpForMisusedAtTopLevelCode
_incrementPreload        _initializeInstanceVariables_isGlobal                _isSafari
_js_type_flags           _lastFrameTime           _lastRealFrameTime       _lastTargetFrameTime
_lcg                     _lcgSetSeed              _lcg_random_state        _legacyPreloadGenerator
_loadingScreenId         _loop                    _makeFrame               _maxAllowedPixelDimensions
_millisStart             _mouseWheelDeltaY        _normalizeArcAngles      _onblur
_onclick                 _ondblclick              _ondevicemotion          _ondeviceorientation
_ondragend               _ondragover              _onkeydown               _onkeypress
_onkeyup                 _onmousedown             _onmousemove             _onmouseup
_onresize                _ontouchend              _ontouchmove             _ontouchstart
_onwheel                 _pWriters                _pixelDensity            _pmouseWheelDeltaY
_preloadCount            _preloadDone             _preloadMethods          _promisePreloads
_recording               _registeredMethods       _registeredPreloadMethods_renderEllipse
_renderRect              _renderer                _requestAnimId           _rgbColorName
_runIfPreloadsAreDone    _setMouseButton          _setProperty             _setup
_setupDone               _setupPromisePreloads    _start                   _startListener
_styles                  _targetFrameRate         _toDegrees               _toRadians
_updateAccsOutput        _updateGridOutput        _updateMouseCoords       _updateNextMouseCoords
_updatePAccelerations    _updatePRotations        _updateTextMetrics       _updateTextOutput
_updateTouchCoords       _updateWindowSize        _userNode                _wrapElement
_wrapPreload             _wrapPromisePreload      abs                      accelerationX
accelerationY            accelerationZ            acos                     alpha
ambientLight             ambientMaterial          angleMode                append
applyMatrix              arc                      arrayCopy                as_object_map
asin                     atan                     atan2                    background
baseColorShader          baseMaterialShader       baseNormalShader         baseStrokeShader
beginClip                beginContour             beginGeometry            beginShape
bezier                   bezierDetail             bezierPoint              bezierTangent
bezierVertex             blend                    blendMode                blue
boolean                  box                      brightness               buildGeometry
byte                     callRegisteredHooksFor   camera                   canvas
ceil                     char                     circle                   clear
clearDepth               clearStorage             clip                     color
colorMode                concat                   cone                     constrain
constructor              copy                     cos                      createA
createAudio              createButton             createCamera             createCanvas
createCapture            createCheckbox           createColorPicker        createDiv
createElement            createFileInput          createFilterShader       createFramebuffer
createGraphics           createImage              createImg                createInput
createModel              createNumberDict         createP                  createRadio
createSelect             createShader             createSlider             createSpan
createStringDict         createVector             createVideo              createWriter
cursor                   curve                    curveDetail              curvePoint
curveTangent             curveTightness           curveVertex              cylinder
day                      debugMode                degrees                  deltaTime
describe                 describeElement          deviceOrientation        directionalLight
displayDensity           displayHeight            displayWidth             dist
downloadFile             draw                     drawingContext           ellipse
ellipseMode              ellipsoid                emissiveMaterial         encodeAndDownloadGif
endClip                  endContour               endGeometry              endShape
erase                    exitPointerLock          exp                      fill
filter                   float                    floor                    focused
fract                    frameCount               frameRate                freeGeometry
frustum                  fullscreen               get                      getFilterGraphicsLayer
getFrameRate             getItem                  getTargetFrameRate       getURL
getURLParams             getURLPath               green                    gridOutput
hasOwnProperty           height                   hex                      hour
httpDo                   httpGet                  httpPost                 hue
image                    imageLight               imageMode                int
isKeyPressed             isLooping                isPrototypeOf            join
js_id                    key                      keyCode                  keyIsDown
keyIsPressed             lerp                     lerpColor                lightFalloff
lightness                lights                   line                     linePerspective
loadBytes                loadFont                 loadImage                loadJSON
loadModel                loadPixels               loadShader               loadStrings
loadTable                loadXML                  log                      loop
mag                      map                      match                    matchAll
max                      metalness                millis                   min
minute                   model                    month                    mouseButton
mouseIsPressed           mouseX                   mouseY                   movedX
movedY                   nf                       nfc                      nfp
nfs                      noCanvas                 noCursor                 noDebugMode
noErase                  noFill                   noLights                 noLoop
noSmooth                 noStroke                 noTint                   noise
noiseDetail              noiseSeed                norm                     normal
normalMaterial           object_entries           object_keys              object_values
orbitControl             ortho                    pAccelerationX           pAccelerationY
pAccelerationZ           pRotateDirectionX        pRotateDirectionY        pRotateDirectionZ
pRotationX               pRotationY               pRotationZ               paletteLerp
panorama                 perspective              pixelDensity             pixels
plane                    pmouseX                  pmouseY                  point
pointLight               pop                      popMatrix                popStyle
pow                      preload                  print                    propertyIsEnumerable
push                     pushMatrix               pushStyle                pwinMouseX
pwinMouseY               quad                     quadraticVertex          radians
random                   randomGaussian           randomSeed               rect
rectMode                 red                      redraw                   registerMethod
registerPreloadMethod    registerPromisePreload   remove                   removeElements
removeItem               requestPointerLock       resetMatrix              resetShader
resizeCanvas             reverse                  rotate                   rotateX
rotateY                  rotateZ                  rotationX                rotationY
rotationZ                round                    saturation               save
saveCanvas               saveFrames               saveGif                  saveJSON
saveJSONArray            saveJSONObject           saveStrings              saveTable
scale                    second                   select                   selectAll
set                      setAttributes            setCamera                setFrameRate
setMoveThreshold         setShakeThreshold        setup                    shader
shearX                   shearY                   shininess                shorten
shuffle                  sin                      smooth                   sort
specularColor            specularMaterial         sphere                   splice
split                    splitTokens              spotLight                sq
sqrt                     square                   storeItem                str
stroke                   strokeCap                strokeJoin               strokeWeight
subset                   tan                      text                     textAlign
textAscent               textDescent              textFont                 textLeading
textOutput               textSize                 textStyle                textWidth
textWrap                 texture                  textureMode              textureWrap
tint                     toLocaleString           toString                 to_py
torus                    touchend                 touches                  touchstart
translate                triangle                 trim                     turnAxis
typeof                   unchar                   unhex                    unregisterMethod
updatePixels             valueOf                  vertex                   webglVersion
width                    winMouseX                winMouseY                windowHeight
'''.split())

PY_NAMES = {
    *dir(__builtins__),
    *dir(object()),
    *dir(sys.modules),
}

import pkgutil
for module in pkgutil.iter_modules():
    try: PY_NAMES.update(dir(__import__(module.name)))
    except: pass



EXTRAS_NAMES: set[str] = P5_NAMES - PY_NAMES - {s for s in P5_NAMES if s.startswith('_')}
print(f'''
p5: {len(P5_NAMES)}
py: {len(PY_NAMES)}
x:  {len(EXTRAS_NAMES)}
''')


it = (s.ljust(25) for s in sorted(P5_NAMES-EXTRAS_NAMES))
print('\n'.join(map(''.join, zip(it,it,it,it))))
