
Le but de `pyodide-mkdocs-theme` <span markdown style="color:red">_n'est pas_</span> de construire des restrictions ultra strictes. En effet :

D'un point de vue pédagogique :

- Le thème est conçu pour construire des sites permettant aux élèves de s'entraîner, pas pour les évaluer.


D'un point de vue technique :

- Python étant ce qu'il est, il est toujours possible de contourner des restrictions, si on est suffisamment persistant et qu'on en sait suffisamment sur son fonctionnement.
- L'intégralité du runtime étant côté client, même en verrouillant complètement la partie python, l'utilisateur peut toujours mettre en échec des restrictions en travaillant dans la couche javascript.
- De toute façon, le contenu des corrections est visible au bout de quelques clics...

Ces restrictions, même si elles présentent déjà un certain niveau de fiabilité (au moins face à des élèves de lycée), sont donc plus à voir comme des outils pédagogiques permettant de s'assurer qu'un élève ne va pas utiliser telle ou telle chose _par erreur_. Encore une fois, une personne motivée et avec une certaine expérience de python finira par trouver comment contourner les restrictions.