
![alt](!!VIS_REM_example_png){: loading=lazy .w35 align=right }

Le fichier `{exo}_REM.md` contient des informations à afficher avec le code de la correction pour l'IDE en cours. Ces informations sont insérées dans une admonition repliée.

Le fichier `{exo}_VIS_REM.md` est quant à lui inséré après l'admonition repliée, ce qui fait que son contenu est directement visible lorsque la solution devient disponible pour l'utilisateur.


!!! danger "Rappel"
    Le contenu de ces fichiers `REM` doit être __statique__ (pas d'appels de macros).
