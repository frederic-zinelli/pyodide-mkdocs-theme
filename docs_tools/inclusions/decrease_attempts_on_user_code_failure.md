Par défaut, `#!py "editor"` est utilisé, ce qui veut dire que n'importe quelle erreur provoquée par le code de l'éditeur de l'IDE consommera un essai (même une erreur de syntaxe).

!!! danger "Attention aux conditions d'accessibilité des corrections et remarques"

    * Utiliser une valeur autre que `#!py "editor"` implique qu'un utilisateur qui n'arriverait pas à résoudre des problèmes de syntaxe dans le code ne pourra jamais accéder à la correction.

    * Avec `#!py "public"`, il suffit à un utilisateur de désactiver les tests ou même de supprimer tout le contenu de l'éditeur pour pouvoir faire décroître le compteur d'essais à volonté.

    * Avec `#!py "secrets"`, le compteur d'essais ne peut décroître que si le code ne contient pas d'erreurs de syntaxe et passe les tests publics (ce qui est faisable en hard-codant les réponses des tests publics).
