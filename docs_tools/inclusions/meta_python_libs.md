
!!! help inline end w45 no-margin-top "Contenu du `mkdocs.yml`"

    ```yaml
    plugins:
        pyodide_macros:
            build:
                python_libs:
                    - linear_adt
                    - any_tree
                    - ABR
                    - ...
    ```

!!! help no-margin-top "Hiérarchie des fichiers"

    ```
    ...
    ├── chapitre_1_ADT_lin
    │   ├── {{meta(0)}}
    │   ├── exo_stack.md
    │   ...
    ├── chapitre_2_ADT
    │   ├── {{meta(0)}}
    │   ├── exo_tree1.md
    ```

<br>

On peut alors imaginer interdire toutes les bibliothèques personnalisées dans les exercices du chapitre 1 en utilisant pour le fichier `chapitre_1_ADT_lin/{{meta(0)}}`{.blue .bgd} :

```yaml
build:
    python_libs: []
```

De même, on pourrait interdire l'accès aux bibliothèques `any_tree` et `ABR` mais laisser l'accès aux types linéaires dans les exercices du chapitre 2, en utilisant pour le fichier `chapitre_2_ADT/{{meta(0)}}`{.red .bgd} :

```yaml
build:
    python_libs:
        - linear_adt
```