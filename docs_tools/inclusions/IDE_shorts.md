- Un éditeur de code ( ++ctrl+comma++ ou ++f1++ pour l'aide et les options, ++ctrl+space++ pour une autocompletion rudimentaire)
- Un terminal (auto-complétion avec ++tab++, et rappel de l'historique avec ++ctrl+R++ )
- Les boutons permettant d'activer les fonctionnalités de cet IDE

Voir la [page dédiée](--redactors/IDE-details/) aux IDEs pour des explications détaillées.