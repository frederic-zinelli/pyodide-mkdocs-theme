
### Les variables CSS des couleurs { #XXX-css-vars }

Mkdocs-Material ainsi que le thème définissent des variables CSS dont certaines répondent aux modifications de la palette de couleur. Le tableau ci-dessous regroupe les noms des variables en question :

| Variable | | Rôle |
|:-:|:-:|:-|
| `--main-theme`          | {{css_sq('--main-theme')}}          | Couleur du thème lui-même (bandeau, contours des IDEs, ...) |
| `--md-default-fg-color` | {{css_sq('--md-default-fg-color')}} | Couleur actuelle du texte. |
| `--md-default-bg-color` | {{css_sq('--md-default-bg-color')}} | Couleur actuelle du fond. |

<br>

Les codes des différentes couleurs sont (si la palette est toujours celle par défaut) :

| Couleur | Code (hex) | Code (rgb)
|-|:-:|:-:|
| `--main-theme`     | `#4051b5` | `(64, 81, 181)`   |
| Texte en mode nuit | `#bec1c6` | `(190, 193, 198)` |
| Fond en mode nuit  | `#1e2129` | `(30, 33, 41)`    |
| Texte en mode jour | `#212121` | `(33, 33, 33)`    |
| Fond en mode jour  | `#ffffff` | `(255, 255, 255)` |