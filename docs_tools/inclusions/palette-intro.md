
## Gérer la palette de couleurs { #XXX-color-palette }

Les sites construits avec PMT proposent à l'utilisateur les modes jour/nuit, via le bouton : :octicons-sun-24: / :material-weather-night:
<br>Il peut alors être intéressant de connaître le mode actif afin d'adapter le tracé des animations p5, afin de choisir la couleur de fond ou de tracé les plus appropriées.

### La fonction `js.isDark()` { #XXX-js-isDark }

Le thème met à disposition une fonction javascript `isDark()` qui renvoie `#!js true` si le mode nuit est activé. Cette fonction est accessible depuis l'environnement de Pyodide :
