
=== "{{ btn('play') }}++ctrl+s++"

    1. Le message `"{{ Env().lang.run_script }}"` apparaît dans le terminal.
    1. Exécution du code de la section `env`.
    1. Exécution du contenu actuel de l'éditeur.
    1. {{ green("Affichage de la sortie standard.") }}
    1. Si aucune erreur n'a été rencontrée
        * Si l'IDE n'a pas de bouton de validation,`"{{ Lang('success_msg_no_tests') }}"` est affiché.
        * Sinon, le message `"{{ Lang('editor_code') }}: {{ Lang('success_msg') }}"` est affiché dans le terminal, suivi d'un message rappelant de faire une validation.
    1. Exécution du code de la section `post`.

    <div style="display:grid; width:100%;">
        <div class="row-of-imgs">
            ![img](!!terminal_messages_png){: loading=lazy }
        </div>
    </div>


=== "{{ btn('check') }}++ctrl+enter++"

    1. Le message `"{{ Env().lang.run_script }}"` apparaît dans le terminal.
    1. Exécution du code de la section `env`.
    1. Exécution du contenu actuel de l'éditeur.
    1. {{ green("Affichage de la sortie standard.") }}
    1. Si pas d'erreur jusque là, validation avec :
        - Exécution de la version originale des tests publics (section `tests`), au cas où l'utilisateur les aurait modifiés ou désactivés dans l'éditeur.
        - Exécution des tests de la section `secrets`.
    1. {{ orange("Pas d'affichage de la sortie standard, si elle est désactivée pendant la validation.") }} (ce qui est le [réglage par défaut](--pyodide_macros_ides_deactivate_stdout_for_secrets)).
    1. Si une erreur a été rencontrée, le compteur d'essais est diminué de 1 (pour modifier ce comportement, [voir ici](--pyodide_macros_ides_decrease_attempts_on_user_code_failure)).
    1. Si le compteur d'essais est arrivé à zéro ou si l'utilisateur a passé tous les tests avec succès, une admonition apparaît sous l'IDE, contenant la correction si elle existe (section `corr`) et les éventuelles remarques (`{exo}_REM.md` / Ce comportement est modulé et modifiable selon les contenus existant et différentes options ou arguments, comme par exemple l'argument [`MODE`](--IDE-MODE) des IDEs).
    1. Exécution du code de la section `post`.

    <div style="display:grid; width:100%;">
        <div class="row-of-imgs">
            ![img](!!terminal_success_failure_png){: loading=lazy }
        </div>
    </div>


    ??? tip "Changement des conditions de décomptes des essais"

        Il est possible de contrôler finement à partir de quelle étape des validations une erreur sera considérer comme comptant pour un essai consommé.
        <br>Ceci se fait en modifiant l'option `{{ config_validator("ides.decrease_attempts_on_user_code_failure") }}` dans la configuration du plugin, ou dans les [méta-données](--meta-pmt-yml-files). Cette option prend le nom de l'étape à partir de laquelle une erreur sera considérée comme comptant pour un essai.

        ```yaml
        plugins:
            - pyodide_macros:
                ides:
                    {{ config_validator("ides.decrease_attempts_on_user_code_failure",1)}}: "secrets"
        ```

        --8<-- "docs_tools/inclusions/decrease_attempts_on_user_code_failure.md"


    ??? warning "Réglage du niveau de feedback donné à l'utilisateur, durant les tests de validation"

        Il est en fait possible de changer le comportement du thème, concernant l'affichage ou non de la sortie standard et le formatage automatique des messages d'erreurs, lors des tests de validation.

        Ceci peut se faire avec les options suivantes du fichier `mkdocs.yml` (ou via les fichier [{{meta()}}](--custom/metadata/)) :

        ```yaml
        plugins:
            - pyodide_macros:
                ides:
                    {{ config_validator("ides.deactivate_stdout_for_secrets",1)}}: true             # défaut: true
                    {{ config_validator("ides.show_only_assertion_errors_for_secrets",1)}}: true    # défaut: false
        ```

        <br>

        * L'option {{ config_link('ides.deactivate_stdout_for_secrets', 1) }} permet de réactiver l'affichage de la sortie standard dans les tests de validation, en passant la valeur à `false`.

        * L'option {{ config_link('ides.show_only_assertion_errors_for_secrets', 1) }} permet quant à elle de réduire au minimum les informations accessibles à l'utilisateur lors d'une erreur. Si cette option est passée à true :

            - {{red("La traceback est totalement supprimée.")}}
            - Si l'erreur n'est pas de type `AssertionError`, {{red("seul le type d'erreur est affiché")}} dans la console (ex: `IndexError has been raised.`).

                !!! danger "No feedback = _BAD_ feedback !"

                    Ceci peut permettre d'éviter que l'utilisateur n'ait accès à des informations sur l'environnement de tests (les fonctions de tests "custom", en l'occurrence), mais garder en tête que ce réglage rend le débogage très compliqué, voire impossible, côté utilisateur.

                    Une solution plus souple, dans ce type de cas, est d'attraper les erreurs depuis les fonctions de tests et de décider ce que vous en faites à ce moment-là.


=== "Synthèse IDE"

    Voici un récapitulatif du déroulement des exécutions des différentes sections, avec les embranchements logiques utilisés dans le code pour les tests publics (bouton "play") et les validations (bouton "check").

    {{div_svg("docs/assets/play-check-runtime.svg", kls="w65", padding=1.5, center=True)}}

=== "Terminaux"

    Le terminal d'un IDE suit le même mode d'exécution que les [terminaux isolés](--redactors/terminaux/), dont voici le résumé :

    {{div_svg("docs/assets/term-runtime.svg", kls="w40", center=True)}}

    Les [particularités des commandes multilignes](--terms-runtime-details) sont décrites dans la page dédiée aux terminaux.

