1. Créer un dossier à la racine du projet.<br><br>

1. Dans le ficher `mkdocs.yml`, ajouter le nom du dossier de macros personnalisées dans la configuration du plugin :

    ```yaml title="Utiliser un package pour les macros personnalisées"
    plugins:
        - pyodide_macros:
            module_name: package_name
    ```

    <br>

1. Ajouter un fichier `__init__.py` dans ce dossier (ceci transforme le dossier en package python).<br><br>

1. Ajouter une fonction `define_env(env:PyodideMacrosPlugin)` à ce fichier `__init__.py`.
<br>Toutes les macros doivent être définies depuis l'intérieur de cette fonction, comme pour le fichier `main.py`.

<br>
<br>

Les fonctions définissant les macros peuvent aussi être déclarées dans d'autres fichiers de la bibliothèque, puis importées pour être enregistrées en tant que macros.

Il y a de nombreuses approches possibles pour réaliser ceci. En voici une ci-dessous :

* Import des fonctions depuis le fichier `__init__.py`
* Enregistrées en tant que macros en executant `env.macro(function_importée)` depuis l'intérieur de la fonction `define_env`.

```python title="Exemple de fichier __init__.py"
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin
from . import my_file1, my_file2

def define_env(env:PyodideMacrosPlugin):

    env.macro(my_file1.macro1)      # my_file1 contient une fonction "macro1"
    env.macro(my_file1.macro2)
    ...

    # Ou créer les macros directement ici (mais le package n'a alors plus d'intérêt...)
    @env.macro
    def macroX(...):
        ...
```

<br>

Si une de ces macros nécessite d'accéder à la variable `env`, on peut ruser de différentes façons :

=== "Méthode 1"

    La façon de procéder la plus naturelle, en combinant des fichiers contenant une fonction `define_env(env)`, comme un fichier `main.py` isolé le ferait :

        └── `macros_perso`
            ├── __init__.py
            ├── module1.py
            └── module2.py

    !!! note inline w45 no-margin-top "`module1.py`"

        ```python
        def define_env(env):

            @env.macro
            def my_macro():
                ...
                return ...
        ```

    !!! note w45 no-margin-top "`__init__.py`"

        ```python
        from . import module1, module2

        def define_env(env):

            for module in (module1, module2):
                module.define_env(env)
        ```

=== "Méthode 2"

    Une alternative, moins naturelle, utilisant des "functions factories". L'intérêt pourrait être de voir les noms de toutes les macros personnalisées depuis le fichier `__init__.py`.

    La structure de fichiers est la même:

        └── `macros_perso`
            ├── __init__.py
            └── module1.py

    !!! note inline w45 no-margin-top "`module.py`"

        ```python
        def macro1(env):
            def macro1():
                # impérativement le même nom !
                return ... # avec env
            return macro1 # la fonction interne
        ```

    !!! note w45 no-margin-top "`__init__.py`"

        ```python
        from .module import macro1

        def define_env(env):

            env.macro( macro1(env) )
        ```
