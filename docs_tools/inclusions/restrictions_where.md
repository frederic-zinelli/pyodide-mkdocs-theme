| Codes | Méthodes<br>+<br>Mots clefs | Fonctions<br>+<br>Imports |
|:-|:-:|:-:|
| Code de l'IDE<br>(`code+tests`) | {{ qcm_svg("single missed") }} | {{ qcm_svg("single missed") }} |
| Validations<br>(`tests+secrets`) | {{ qcm_svg("single unchecked") }} | {{ qcm_svg("single missed") }} |
| Commande terminal | {{ qcm_svg("single missed") }} | {{ qcm_svg("single missed") }} |
