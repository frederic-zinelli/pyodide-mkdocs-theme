<br>

!!! tip "Exemple: connaître le réglage de la palette de couleur"

    Le code python ci-dessous est lancé dès le démarrage de la page, en utilisant la macro [`{% raw %}{{ run(...) }}{% endraw %}`](--redactors/run_macro/).
    <br>Modifier le réglage de la palette de couleur met à jour le texte dans l'encadré suivant :

    <br>

    {{ figure('empty_palette', admo_kind="", inner_text="(Attendre que l'environnement démarre...)") }}

    {{ run('exemples/palette') }}

    <br>

    {{ py('exemples/palette') }}


    !!! warning "Chrome & `serve`"

        Durant le développement, si Chrome est utilisé pour afficher les pages, l'exemple ci-dessus ne fonctionne pas au cause de certaines limitations du navigateur. Cela fonctionne cependant sur le site construit.

<br>
