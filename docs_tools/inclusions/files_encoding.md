
??? tip "Gérer l'encodage des fichiers textes"

    Le passage des données entre la couche javascript et la couche python à travers pyodide comporte certaines limitations.
    <br>Aussi, s'il est nécessaire d'avoir un contrôle fin de l'encodage utilisé pour lire ou écrire un fichier de données, il faut procéder comme suit :

    * Pour téléverser :

        1. Télécharger le fichier sous forme de `#!py bytes`
        1. Convertir les `#!py bytes` avec l'encodage désiré dans pyodide.

    * Pour télécharger :

        1. Convertir dans pyodide la chaîne de caractères en `#!py bytes`.
        1. Passer le contenu sous cette forme à la couche JS qui enregistrera le fichier sur le disque de l'utilisateur.
