La logique des interdictions fonctionne bien du moment que le contexte d'exécution est restreint.<br>
En l'occurrence, les restrictions appliquées à un IDE sont valables pour son éditeur et son terminal.

En revanche tout autre IDE ou terminal apparaissant dans la même page est un autre point d'entrée vers l'environnement `pyodide` qui, rappelons-le, _est commun à toute la page_.
<br>En conséquence, si un IDE ou un terminal comporte des restrictions, tous les IDEs et terminaux isolés de la page doivent avoir les mêmes.

Les [fichiers {{meta()}}](--meta-pmt-yml-files) ou les métadonnées dans les [entêtes des fichiers markdown](--pages-meta) peuvent être mis à profit pour régler ce type de paramètres pour un groupe de fichiers ou une page entière.
