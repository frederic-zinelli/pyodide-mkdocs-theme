
Voici les différentes options de formatage disponibles.

!!! help inline end w45 "Remarques"

    - La couleur par défaut des terminaux est celle définie par la palette de couleur du site (dans le fichier `mkdocs.yml:theme.palette`).
    - Les couleurs utilisées dans les exemples ci-contre ne sont pas exactement les mêmes que celles utilisées dans les terminaux, mais permettent d'avoir une bonne idée du rendu qui sera obtenu.

| Nom | Rendu |
|-|-|
| `#!py "error"`   | {{ red("__Rouge + gras__")                  }} |
| `#!py "info"`    | {{ gray("_Gris + italique_")                }} |
| `#!py "italic"`  | {{ primary("_Italique_")                    }} |
| `#!py "none"`    | {{ primary("Défaut")                        }} |
| `#!py "stress"`  | {{ primary("__Gras__")                      }} |
| `#!py "success"` | {{ green("___Vert + gras + italique___")    }} |
| `#!py "warning"` | {{ orange("___Orange + gras + italique___") }} |
