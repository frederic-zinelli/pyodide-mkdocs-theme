def tests():
    for n in range(100):
        # Il est conseillé d'utiliser une fonction pour éviter que des
        # variables des tests se retrouvent dans l'environnement global.
        val = est_pair(n)
        exp = n%2 == 0

        msg = f"est_pair({n})"                           # Minimum vital
        msg = f"est_pair({n}): valeur renvoyée {val}"    # Conseillé
        msg = f"est_pair({n}): {val} devrait être {exp}" # La totale

        assert val == exp, msg

tests()         # Ne pas oublier d'appeler la fonction de tests... ! x)
