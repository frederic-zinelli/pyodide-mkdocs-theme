![Tester tous les IDEs de la documentation](!!tests_ides_panel_png){: loading=lazy .w35 align=left }

* À partir de la version 2.3.0, le thème propose de [générer automatiquement une page pour tester tous les IDEs du site](--redactors/IDE-tests-page/), permettant notamment de vérifier que le code présent dans la section `corr` d'un IDE passe bien les tests de validations.
<br>Cette fonctionnalité vient avec diverses options permettant d'affiner la façon de tester un IDE (ne pas faire le test, le faire mais considérer une erreur comme un succès, ...).

    Ceci permet de tester les codes dans le même environnement que l'utilisateur, de manière semi-automatisée.
