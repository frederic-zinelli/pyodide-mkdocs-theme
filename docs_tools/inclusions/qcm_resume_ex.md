{{ multi_qcm(
    [
        "On peut utiliser du TeX: $F_n$ | Blabla... (réponses corrects: texte & !!!)",
        [
            "Là aussi on peut mettre du TeX: $F_{n+1}$ ",
            "!!!",
            "...",
            "...",
        ],
        [1,2]
    ],
    [
        "Choix multiples possibles, mais une seule bonne réponse :super-vilain: (réponse correct: \"On peut aussi...\")",
        [
            "Déchiffrer illégitimement le message",
            "Chiffrer le message",
            "Dissimuler le message dans un support",
            """

            On peut aussi mettre de contenus plus complexes, du moment que...:

            * c'est de la syntaxe markdown valide et que...
            * les niveaux d'indentation...
                * dans le contenu...
                * sont cohérents.

            _(Noter que la première ligne vide de cette chaîne de caractère est automatiquement supprimée)_
            """,
        ],
        [4],
        {'multi': True},
    ],
    [
        "Blabla... (réponse correct: code python)",
        [
            "Alice a besoin de la clé privée de Bob",
            "Alice a besoin de la clé publique de Bob",
            "Bob a besoin de la clé privée d'Alice",
            """
            ```python title=''
            # On peut aussi insérer des blocs de code
            def func():
                return 26
            ```
            """,
        ],
        [4]
    ],
    qcm_title = "Exemple fonctionnel de QCM avec mélange automatique",
    multi = False,
    shuffle = True,
) }}