
{{ multi_qcm(
    [
    """
    The following code was entered:
    ```python title=''
    n = 8
    while n > 1:
        n = n//2
    ```

    What is the value of `n` after the code is executed?
    """,
        [
            "0",
            "1",
            "2",
            "4",
        ],
        [2]
    ],
    [
        "Which machine will execute a JavaScript program included in an HTML page?",
        [
            "The user's machine where the web browser is running.",
            "The user's machine or the server, whichever is more available.",
            "The user's machine or the server, depending on the confidentiality of the data handled.",
            "The web server where the HTML page is stored."
        ],
        [1],
    ],
    [
        """
        Tick all the correct answers:
        ```python title=''
        furniture = ['table', 'chest of drawers', 'wardrobe', 'cupboard', 'sideboard']
        ```
        """,
        [
            "`#!py furniture[1]` is `#!py table`",
            "`#!py furniture[1]` is `#!py chest of drawers`",
            "`#!py furniture[4]` is `#!py sideboard`",
            "`#!py furniture[5]` is `#!py sideboard`",
        ],
        [2, 3],
        {'multi':True}
    ],
    multi = False,
    qcm_title = "A multiple-choice quiz with automatic shuffling of the questions (button at the bottom to restart)",
    DEBUG = False,
    shuffle = True,
    description = "_(An additional description can be added at the beginning of the admonition...)_\n{style=\"color:orange\"}"
) }}
