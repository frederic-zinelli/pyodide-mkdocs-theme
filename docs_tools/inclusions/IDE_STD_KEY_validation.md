Lors de la construction du site, une erreur est levée si un IDE est rencontré avec toutes ces conditions réunies :

* Des tests de validation
* La sortie standard est désactivée durant les validations
* Un argument `STD_KEY` qui est _"falsy"_
* La chaîne `#!py "terminal_message("` est trouvée dans l'une des sections `tests` ou `secrets`.

!!! danger "Ne pas utiliser `terminal_message` dans les sections `tests`"
    Il est à noter que la fonction `terminal_message` ne devrait normalement jamais être utilisée dans la section `tests`, car son contenu apparaît dans l'éditeur et est également utilisé dans les tests de validation. Or, pour pouvoir l'utiliser lors d'une validation, la bonne valeur pour l'argument `@key` doit être utilisée, ce qui veut dire que cette valeur apparaîtra aussi dans l'éditeur...