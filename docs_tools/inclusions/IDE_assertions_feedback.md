[pyodide-mkdocs][pyodide-mkdocs]{: target=_blank } pratiquait une sorte d'introspection du code pour reconstruire un message en injectant automatiquement des informations/bouts de codes provenant de différents endroits. Il reconstruisait notamment les valeurs utilisées pour les arguments.

La logique employée posait de nombreux problèmes et le comportement n'était pas modulable, ce qui rendait les tests secrets "pas très secrets". Au final, il a été décidé de supprimer cette fonctionnalité et d'en faire une version plus robuste, mais aussi beaucoup plus simple.

Par défaut, le thème va reconstruire des messages mais ils seront probablement insuffisants pour donner un feedback digne de ce nom aux utilisateurs, si le code n'a pas été rédigé en prévision de cela.<br>Concrètement :

=== "Feedback utile"

    ```python
    assert user_func(42) == [
        [0,1,2],
        [1,2,3],
        [3,4,5],
    ]
    ```

    Si cette assertion échoue, le code entier de l'assertion sera ajoutée au message :

    ```pycons
    ...
    AssertionError: assert user_func(42) == [
        [0,1,2],
        [1,2,3],
        [3,4,5],
    ]
    >>>
    ```

=== "Feedback inexploitable"

    ```python
    actual = user_func(42)
    expected = [
        [0,1,2],
        [1,2,3],
        [3,4,5],
    ]
    assert actual == expected
    ```

    Si cette assertion échoue, la même chose se produit, mais cette fois le message ne contient aucune information utile pour l'utilisateur !

    ```pycons
    ...
    AssertionError: assert actual == expected
    >>>
    ```
