"""
Hook functions that are called from the devops orchestration steps, and that must be tweaked
depending on each project specificities.
"""


# pylint: disable=unused-wildcard-import, wildcard-import, missing-function-docstring


from myutils.code_helpers import *
from myutils.projects import *
from myutils.system import *
from myutils.terminal import *
from myutils.dev_ops_scripts import *
from myutils.dev_package.models import *


from python_devops.config import *
from python_devops.release import (
    update_files_from_material_async,
    set_js_logger_to,
    set_is_pipeline_to,
    make_sure_mkdocs_yml_is_correctly_configured_for_release,
)
from python_devops.system_updates.langs_and_scripts import handle_Lang_related_logic_and_files_and_PMT_python_scripts







# Redefine the hooks below, to fit the current project needs:
#------------------------------------------------------------


# skip = {
#     'README.md',
#     '.coverage',
#     '.git',
#     '.gitignore',
#     '.pytest_cache',
#     '.python-version',
#     '.venv',
#     '.vscode',
#     '.VSCodeCounter',
#     'node_modules',
#     'coverage',
#     'dist',
#     'project_devops_hooks.py',
#     'tests',
#     '__pycache__'
# }

# def toggle_hook(args:SysDevArgs):
#     """ Stand alone step. To use to toggle ON/OFF lines or blocks of code in the project
#
#         TOKENS:     "!"   =>  Unique line token
#                     ">"   =>  Multiline opening token
#                     "<"   =>  Multiline closing token
#     """
#     python.explore_drive_and_toggle(
#         'src',
#         lambda name: True,
#         lambda name: name not in skip,
#         *args.todo,
#     )



get_project_version_hook  = poetry.get_project_version_from_toml
set_project_version_hook  = poetry.set_toml_version_to

js_logger_was = None


def pre_release_operations():
    global js_logger_was

    js_logger_was = set_js_logger_to(False)     # needed for clean_post_release_hook call
    update_files_from_material_async()
    export_repo_url_from_yml_to_maestro_base()
    handle_Lang_related_logic_and_files_and_PMT_python_scripts()






@docstring_teller
def export_repo_url_from_yml_to_maestro_base():
    """
    Update the pmt_url in BaseMaestro if needed
    """
    repo_url = CodeModifier(MKDOCS).extract_between('repo_url:', '\n').strip("'\" ")
    maestro  = CodeModifier(MAESTRO)
    pmt_url  = maestro.extract_between('pmt_url:str', "\n", archive=True).strip(("'\" ="))

    if pmt_url != repo_url:
        maestro.replace_current(f" = '{ repo_url }'\n").dump()
        print('Updated to: ', repo_url)







def apply_hook(args:DevopsArgs):
    """ Stand alone step, that can be used to add any kind of task you would want to
        trigger through the use of the dev commands
    """
    pre_release_operations()
    print('----------')
    clean_post_release_hook(args)









# def update_hook(args:DevopsArgs):
#     """ Generic update operations to run before a build (typically) """


def setup_release_hook(args:DevopsArgs):
    """ Specific operations to apply just before a build for a release.

        NOTE: args.version is automatically updated according to the sys arg release
        argument before this call
    """
    poetry.update_toml_project_version(args)
    poetry.reinstall_project(args)

    make_sure_mkdocs_yml_is_correctly_configured_for_release()
    pre_release_operations()




def build_hook(args:DevopsArgs):
    """ Build the project itself """

    python.fill_module_init_with_readme_file(args)

    current_version = poetry.get_project_version_from_toml(args)
    python.update_version_module_file_with_project_version(args, current_version)

    generic.remove_dist_directory(args)
    poetry.building_project(args, 'both')

    if args.release:
        generic.copy_last_build_into_archived_versions_directory(args)





# def deploy_core_hook(args:DevopsArgs):
#     """ Basic "core" deployment steps (generally used for local deployment tasks) """


# def clean_up_hook(args:DevopsArgs):
#     """ to revert some of the previous operations before distant/git deployment """





def deploy_git_hook(args:DevopsArgs):
    git.project_deployment(args)




def release_post_hook(args:DevopsArgs):
    """
    Deployment for built packages.
    Done last, so any failing step before will forbid the publication.
    """
    os_system('poetry publish')




def clean_post_release_hook(_:DevopsArgs):

    print("\n\nSet back is_pipeline to false.")
    set_is_pipeline_to('false')

    print("\nPut back in place the no_undefined logistic in the PyodideSectionsRunner class:")
    os_system('.venv/bin/python python_devops/system_updates/release_post_in_venv.py')

    if js_logger_was is not None:
        print(f"Set back js_logger to { js_logger_was }.")
        set_js_logger_to(js_logger_was)





if __name__=='__main__':
    # Allow to apply some logic directly from a console file call, if needed
    pass
    # handle_languages_and_scripts()
