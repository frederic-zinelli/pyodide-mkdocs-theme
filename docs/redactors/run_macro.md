
Cette macro est l'équivalent d'un {% raw %}`{{ py_btn(..., AUTO_RUN=True) }}`{% endraw %}, mais sans le bouton visible dans la page.

Elle peut s'avérer utile pour initialiser une [animation `p5`](--custom/matplotlib/) au chargement de la page, [créer un graphique](--custom/matplotlib/), ...




## Exemple

!!! note "Code markdown"

    ```markdown
    {% raw %}
    {{ figure(
        admo_title="Animation est lancée au chargement de la page...",
        inner_text="...et qui apparaît une fois l'environnement pyodide lancé.",
    ) }}

    {{ run("run_p5") }}
    {% endraw %}
    ```

!!! note inline end w55 "Fichier `run_p5.py`"

    {{ py("env_examples/run_p5") }}

{{ figure(
    admo_title = "Animation est lancée au chargement de la page...",
    inner_text = "...et qui apparaît une fois l'environnement pyodide lancé.",
    admo_class = "tip h20",
) }}

{{ run("env_examples/run_p5") }}

!!! tip py_mk_hidden "2e appel à run sur un fichier et une div différents"

    {{ run("env_examples/run_p5_2") }}

    {{ figure("figure2", admo_kind=None) }}



## Signature

{{ macro_signature('run') }}


## Arguments

{{ macro_args_table('run', with_headers=True, with_text=True) }}


- Voir la [page des IDEs](--redactors/IDE-details/) concernant la structure des fichiers associés à l'argument `py_name`.

- Seules les sections `env` et `ignore` de ces fichiers peuvent être utilisées avec la macro `run`. Il est rappelé que la section `env` est exécutée en mode [`async`](--async), ce qui permet plus de souplesse.

- Si une autre section ou des fichiers `{exo}_REM.md` ou `{exo}_VIS_REM.md` sont trouvés, {{red("__une erreur est levée__")}}.
