# --- PYODIDE:code --- #
# Utiliser le bouton de validation !
NON_BLOCKING = True     # Passer à False pour voir le comportement en mode synchrone
DELTA_T = 0.100         # Temps d'attente entre deux "tests"

# --- PYODIDE:corr --- #
NON_BLOCKING = False
DELTA_T = 0.010

# --- PYODIDE:secrets --- #
@auto_run
async def _():
    import js, time
    std_key = 'ok'

    async def something_long_here(that, delta_t):
        terminal_message(std_key, that, new_line=False)

        for _ in range(10):
            time.sleep(delta_t)     # "quelque chose de long à calculer" (bloquant)
            terminal_message(std_key, ".", new_line=False)

            if NON_BLOCKING:        # (défini dans l'éditeur)
                await js.sleep()    # Pause non bloquante => MAJ affichage du terminal

        terminal_message(std_key, "")   # (retour à la ligne final)


    await something_long_here("Premiers tests : ", DELTA_T)
    await something_long_here("D'autres tests : ", DELTA_T * 2)