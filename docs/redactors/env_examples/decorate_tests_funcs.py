# --- PYODIDE:code --- #

test = lambda f: f()

@test
def passing():
    print('passed')        # Simule des tests réussis

print(passing)

@test
def failing():
    assert False, 'oups...'

# Entrer `failing` dans le terminal donne NameError
