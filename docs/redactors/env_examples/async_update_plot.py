# --- PYODIDE:env --- #
import matplotlib.pyplot as plt
fig = PyodidePlot('plot_fig')

# --- PYODIDE:secrets --- #
import js, math
from random import choice, random

funcs = [
    lambda x,a: a * abs(x)**.5,
    lambda x,a: a * x**2,
    lambda x,a: a * x,
    lambda x,a: a * x**3,
    lambda x,a: a * math.log(abs(x) or 0.01),
]

@auto_run
async def _():
    for _ in range(5):
        fig.target()
        func = choice(funcs)
        a = random()
        xs = range(-10,10)
        ys = [ func(x,a) for x in xs]
        plt.plot(xs, ys, '-')
        plt.show()

        await js.sleep(500)  # Attends 500ms (non bloquant)
