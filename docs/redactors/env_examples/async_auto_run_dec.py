# --- PYODIDE:env --- #
SUCCESS = True
# --- PYODIDE:code --- #
# Utiliser le bouton de validation !
SUCCESS = False

# --- PYODIDE:secrets --- #

key = 'ok'
VAL = 0
terminal_message(key, f"Code sync... ({VAL=})", )


@auto_run
async def delayed():
    terminal_message(key, f"Fonction ASYNC décorée exécutée à la fin... ({VAL=})")

VAL += 1

@auto_run
async def delayed2():
    terminal_message(key, "...mais toujours avant que les tests ne soient finis ou interrompus.")
    assert SUCCESS, "Changer SUCCESS dans l'éditeur..."

@auto_run
async def delayed3():
    terminal_message(key, "Ce message n'apparaît que si delayed2 n'a pas levé d'erreur.")


async def now_async():
    terminal_message(key, f"Fonction async exécutée dans le flux normal... ({VAL=})")
await now_async()

VAL += 1

@auto_run
def now_again():
    terminal_message(
        key, f"Une fonction sync reste exécutée dans le flux normal ({VAL=})"
    )

VAL += 1
terminal_message(key, f"""Code sync... ({VAL=})\n---""")
VAL += 1
