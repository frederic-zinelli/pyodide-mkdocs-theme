# ---PYODIDE:code --- #
import sys

print('abcd')
print(42)
txt = sys.stdout.getvalue()
print('---')
print(repr(txt))

sys.stdout.truncate(0)
sys.stdout.seek(0)
print('reset!')
txt = sys.stdout.getvalue()
print('---')
print(repr(txt))


# ---PYODIDE:secrets --- #

print('rien dans le terminal')
print('mais visible dans sys.stdout')

assert "visible dans sys.stdout" in sys.stdout.getvalue()