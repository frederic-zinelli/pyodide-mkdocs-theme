
![un IDE](!!theme_ide_ex_png){: loading=lazy .w30 align=right }

!!! tip "But de cette page"

    Voici une procédure minimale pour utiliser un IDE ou un terminal reposant sur des fichiers python dans une page de la documentation.

    Cette page décrit les éléments nécessaire pour les IDEs, mais la procédure est [la même pour les terminaux, à quelques détails près](--macro-terminal).

    La page [discutant en détail des IDEs](--redactors/IDE-details/), décrit l'intégralité des fonctionnalités disponibles, ainsi que les [organisations alternatives des fichiers](--ide-files-organization).


!!! note "Anciens formats de fichiers utilisés dans [`pyodide-mkdocs`][pyodide-mkdocs]{: target=_blank }"

    Les informations données ci-dessous correspondent au mode de travail qu'il serait préférable d'adopter, mais les IDEs restent rétrocompatibles avec les fichiers de `pyodide-mkdocs` qui utilisait notamment 3 types de fichiers pythons différents (`{exo}.py`, `{exo}_corr.py` et `{exo}_test.py`).

    Voir la page de [mise à jour depuis pyodide-mkdocs](--maj_pyodide_mkdocs/) vers le thème pour connaître les changements nécessaires lors de la mise à jour.


<br>

{{sep()}}

## Exemple { #IDE-QG-example }

{{ IDE('../exemple/exo') }}


## Interface & fonctionnalités { #IDE-QG-GUI }

Une description rapide du fonctionnement des IDEs et de leurs fonctionnalités est donnée dans la page d'[aide pour les utilisateurs](--users-IDEs).

{{sep()}}



## Organisation des fichiers {{anchor_redirect(id='organisation-des-fichiers')}} { #quick-guide-files }

Pour pouvoir insérer un IDE dans le fichier `docs/exercices/est_pair/index.md`, l'approche la plus simple est d'organiser les fichiers comme suit :

```
    ├── docs
    │   ├── exercices
    │   │   ├── est_pair
    │   │   │   ├── index.md
    │   │   │   ├── exo.py
    │   │   │   ├── exo_REM.md
    │   │   │   └── exo_VIS_REM.md
```

_(Nota: certains fichiers sont optionnels / toute autre hiérarchie est envisageable, seul le contenu du dossier `est_pair` importe, dans cet exemple)_

<br>

| Fichier | Rôle |
|-|-|
| `index.md` |  Contient l'énoncé de l'exercice et accueil l'IDE.<br>Vous pouvez utiliser un autre nom de fichier si vous voulez. |
| `{exo}.py` | Contient les différents codes python nécessaires pour l'IDE.<br>Nom de fichier à choisir par vos soins. |
| `{exo}_REM.md`<br>(_optionnel_) | Remarques qui seront affichées avec la correction, dans une admonition repliée, lorsque la solution sera révélée.<br><ul><li>Le début du nom doit correspondre au fichier python (sans extension).</li><li>Le fichier REM ne doit pas contenir d'appels de macros.</li></ul> |
| `{exo}_VIS_REM.md`<br>(_optionnel_) {{width(13)}} | Remarques qui seront affichées après la correction et le fichier REM, en dehors de l'admonition (et donc visibles tout de suite, une fois la solution révélée).<br><ul><li>Le début du nom doit correspondre au fichier python (sans extension).</li><li>Le fichier VIS_REM ne doit pas contenir d'appels de macros.</li></ul> |

!!! warning ""
    Les anciennes organisations des fichiers sont toujours utilisables. Voir à ce sujet la [section détaillée](--ide-files) concernant les IDEs.




## Le fichier `index.md`

Placez-y l'énoncé de l'exercice et toutes les informations que vous jugerez utiles, sous forme de syntaxe markdown compatibles avec les extensions et plugins renseignés dans le fichier `mkdocs.yml`.

Les IDEs y sont ajoutés avec l'appel de la macro `IDE`, en lui fournissant le nom du fichier python (sans extension) :

```markdown
[...énoncé, avec tout le markdown qui vous fait envie...]

{% raw %}{{ IDE('exo') }}{% endraw %}
```





!!! tip inline end w50 margin-top-h2
    Il existe d'autres façons d'organiser les codes python, en utilisant des fichiers séparés (organisation obsolète, utilisée pour [pyodide-mkdocs][pyodide-mkdocs]{: target=_blank }). Voir la page des [détails des IDEs](--ide-files).

## Le fichier `exo.py` { #IDE-exo-py }


### Contenu

La structure classique du fichier est celle ci-dessous.

Il est également possible d'afficher ce contenu dans un terminal ou de créer un fichier avec ce contenu en utilisant un des [scripts du thème](--scripts/) :

```
python -m pyodide_mkdocs_theme --py
```

```
python -m pyodide_mkdocs_theme --py --file "path/exo.py"
```

<br>

Voici donc un exemple de contenu pour un fichier python associé à un IDE :

{{md_include("docs_tools/inclusions/IDE_exo_py.md")}}

### Validité

--8<-- "docs_tools/inclusions/IDE_exo_py_validity.md"
{{ md_include("docs_tools/inclusions/IDE_config_checks.md") }}




## Les fichiers `REM.md` et `VIS_REM.md` {{anchor_redirect(id="exo_remmd-et-exo_vis_remmd")}} { #rem_md-vis_rem_md-files }

![VIS_REM - exemple](!!VIS_REM_example_png){: loading=lazy .w35 align=right }

Ces fichiers sont spécifiques aux IDEs.
<nr>La seule contrainte particulière les concernant est qu'ils ne doivent pas contenir d'appels de macros ni de graphes `mermaid`. Il est par contre possible d'y utiliser des syntaxes Latex.

Lorsque l'utilisateur passe tous les tests ou consomme tous les essais disponibles, le contenu du fichier REM est affiché dans une admonition repliée (contenant également le code de la correction quand elle existe).

Le contenu de VIS_REM est quant à lui affiché au mêmes moments mais en-dessous de l'admonition repliée, de manière à ce que son contenu soit tout de suite visible par l'utilisateur.
{{pmt_note("L'admonition de la partie \"VIS_REM\" dans l'image ci-contre n'est pas ajoutée automatiquement : elle est définie dans le fichier VIS_REM lui-même.")}}

<br>

---

<br>

... _Et c'est tout !_