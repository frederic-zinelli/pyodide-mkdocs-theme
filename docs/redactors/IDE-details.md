---
ides:
    forbid_secrets_without_corr_or_REMs: false
---

# Les IDEs


![un IDE](!!theme_ide_ex_png){: loading=lazy .w45 align=right }

Les IDEs proposés par `pyodide-mkdocs-theme` sont un environnement clef en main avec :

* Un éditeur de code, très similaire à ce qu'on peut trouver dans des éditeurs de type VSC.
* Un terminal, qui fonctionne de la même manière qu'une console interactive python.
* Un jeu de boutons et fonctionnalités permettant de faire tourner l'ensemble, avec pyodide travaillant en sous-main.


!!! note "Rappel RGPD"

    L'intégralité de l'environnement tourne dans le navigateur, côté client. Il n'y a aucune donnée renvoyée au serveur !











## Vue d'ensemble { #ide-overview }



### Contenus / sections { #ide-sections }

Pour fonctionner, un IDE a besoin de différents snippets/sections python fournis par le rédacteur via un ou plusieurs fichiers pythons. Ces fichiers sont spécifiques à un exercice/IDE.

<br>

Les différents codes python sont identifiés par des noms de sections, chaque section ayant un rôle spécifique :

| Section {{width(8)}} | Exécution | Rôle |
|:-|:-|:-|
| `env`| [`async`](--async) | Code d'environnement : "setup".<br>Non visible par l'utilisateur, ce code permet de définir des choses nécessaires au code de l'utilisateur ou aux tests, comme une classe à utiliser, des fonctions de tests "custom", ... |
| `code` | `sync` | Code initial proposé à l'utilisateur. |
| `corr` | `sync` | Solution permettant de passer tous les tests.<br>L'utilisateur ne peut pas voir ce code, sauf une fois que la solution a été révélée, soit après avoir réussi lui-même l'exercice, soit après avoir consommé tous les essais disponibles.<br>Ce code n'est en fait jamais exécuté dans l'IDE lui-même, mais il est possible de l'exécuter à la place du code de l'éditeur, pour [tester le code de correction durant le développement en local](--IDEs-test-corr). |
| `tests` | `sync` | Code original des tests publics.<br>Visible sous le contenu de la section `code`, dans l'éditeur. |
| `secrets` | [`async`](--async) | Code des tests cachés, lors des validations.<br>L'utilisateur ne peut pas voir le contenu de ces tests.<br>{{orange("_Ne pas y recopier le contenu de la section `tests` !_")}} (voir [le déroulement de la validation](--runtime-pyodide)) |
| `post` | [`async`](--async) | Code d'environnement : "teardown".<br>Non visible par l'utilisateur, ce code permet d'appliquer des éventuelles étapes de nettoyage de l'environnement.<br>{{red("_À ne pas utiliser pour des tests !_")}} |
| `ignore` |  | Un type de section spécial, qui permet de stocker dans les fichiers du contenu qui doit être ignoré pour le site construit.<br>La section `ignore` est la seule qui peut être présente plusieurs fois dans le fichier.<br>Ces sections sont utiles pour laisser des commentaires pour les rédacteurs. |


* Toutes les sections sont optionnelles.
* La section `ignore` peut être utilisée autant de fois qu'on le souhaite dans le fichier.
* Les autres sections ne doivent être utilisées qu'une seule fois.



<br><br>



En dehors des codes python, un IDE peut aussi utiliser deux autres fichiers markdown, `{exo}_REM.md` et `{exo}_VIS_REM.md`, contenant des remarques que le rédacteur voudrait joindre à la correction (ici, `{exo}` est le nom du fichier python contenant les codes nécessaire, sans l'extension `.py`. Voir plus bas).

![alt](!!VIS_REM_example_png){: loading=lazy .w35 align=right }

Les contenus de ces deux fichiers sont affichés sous l'IDE lorsque l'utilisateur passe tous les tests ou consomme tous les essais disponibles. La différence essentielle entre les deux est que :

- `{exo}_REM.md` est affiché avec la correction (section `corr`), dans une admonition repliée. Ceci afin que l'utilisateur n'ai pas la solution visible de suite, s'il souhaite poursuivre ses essais.
- `{exo}_VIS_REM.md` est affiché sous l'admonition repliée, et est donc tout de suite visible. C'est l'endroit idéal pour donner par exemple un lien pour faire l'exercice suivant.

En termes de types de contenus, ces fichiers ne doivent pas contenir d'appels de macros ni de graphes `mermaid`, mais il est possible d'y utiliser des syntaxes Latex.


<br><br><br>



??? tip "Changer le commentaire séparant le code utilisateur des tests publics, dans l'éditeur"

    Par défaut, la chaîne `"\n# Tests\n"` est utilisée comme séparateur.
    <br>Il est possible de la modifier, en utilisant la [personnalisation des messages](--custom/messages/), en modifiant l'entrée `"tests"`.



??? warning "Contraintes sur l'écriture de commentaires dans les tests publics (section `tests`)"

    Le bouton `###` dans le coin supérieur droit de chaque éditeur ainsi que le raccourci ++ctrl+i++ permettent d'activer ou désactiver le code contenu dans les tests publics (v. [l'aide utilisateur](--users-IDEs)).

    Afin de garantir le bon fonctionnement de cet outil, il est impératif que tout commentaire écrit par vos soins dans les tests publics ait un espace après le `#` :

    ```python title="Bien commenter ses tests publics"
    # Tests

    # Ceci est un commentaire et le restera même après un Ctrl+I

    assert True     # Ctrl+I désactiverait cette ligne et activerait la suivante.
    #assert False   # Ceci n'est pas un commentaire, mais du code désactivé.
    ```


??? tip "L'existence d'une section `secrets` impacte certains éléments de l'interface"

    * Le bouton de validation n'est visible que si du contenu `secrets` est présent.

    * L'indication du nombre d'essais disponibles, affiché en bas à droite de l'IDE devient $\infty$ s'il n'y a pas de contenu `secrets` :

    <div style="display:grid; width:100%;">
        <div class="row-of-imgs w80">
            <div class="c1 w90">
                <img src="!!simple_IDE_png" />
                <div class="img-legend" style="margin-top:0.3em">Avec une section `secrets`</div>
            </div>
            <div class="c2 w90">
                <img src="!!terminal_messages_png" />
                <div class="img-legend" style="margin-top:0.3em">Sans section `secrets`</div>
            </div>
        </div>
    </div>


??? warning "Validité des contenus"

    {{ md_include("docs_tools/inclusions/IDE_config_checks.md") }}



??? tip "À propos de la section `env`"

    Toute première à être exécutée, cette section permet de mettre en place des ressources pour l'exercice. Par exemple :

    - ajout de fonctions ou de classes qui pourraient servir à l'utilisateur, ou aux tests
    - remplacement de fonctions dans le scope global (si cela est fait dans le but d'imposer des restrictions sur le code utilisé, [préférer l'argument `SANS` des macros `IDE`](--IDE-SANS) proposé par le thème),
    - ...

    Elle est par ailleurs lancée en utilisant le [mode asynchrone](--async) de pyodide, ce qui permet également :

    - [d'accéder à un fichier sur un serveur](--async),
    - gérer des fichiers dans l'environnement de Pyodide
    - ...

    <br>Concernant les erreurs levées lors de l'exécution de la section `env` :

    Il est possible d'y faire des tests _avant_ que le code de l'utilisateur n'ait été exécuté (et donc avant qu'il ait une chance d'altérer l'environnement, pour l'exécution en cours).

    * Si `AssertionError` est levée alors que l'utilisateur lançait une validation, un essai est décompté si l'option {{ config_link('ides.decrease_attempts_on_user_code_failure', 1) }} est à `#!py "editor"` (c'est le cas par défaut).
    <br>Dans cette situation, le code de l'utilisateur et les tests ne seront pas exécutés, mais la section `post` le sera.

    * Si une autre erreur est levée, les exécutions sont complètement stoppées.<br>Ceci est considéré comme une {{red("___situation anormale___")}}, donc aucune des sections suivantes ne sera exécutée, pas même la section `post`. Le compteur d'essais n'est pas non plus mis à jour si cela arrive durant une validation.

        !!! warning "Protéger le code `env` contre les erreurs !"

            Si vous mettez en place de la logistique consommatrice de ressources, qu'il faut impérativement gérer à la fin des exécutions, il faut que vous soyez sûr que l'exécution du code de la section `env` ne lèvera pas d'erreur.

            À noter que le problème ne se pose que lorsque la section elle-même est exécutée, et pas lorsque du code qui y a été défini lève une erreur quand il est appelé plus tard : il n'y a aucun problème avec une fonction définie dans `env` qui crasherait lors d'un appel par l'utilisateur, par exemple (ce n'est plus la section `env` qui est en cours d'exécution, à ce moment-là).


??? tip "À propos de la section `post`"

    Cette section est exécutée même si le code utilisateur ou les tests ont levé une erreur quelconque.

    Elle est par ailleurs lancée en utilisant le [mode asynchrone](--async) de pyodide, et permet donc typiquement de disposer de certaines ressources qui ont été mises en place depuis la section `env`:

    - gestion de fichiers,
    - remplacement de fonctions dans le scope global,
    - ...

    Il est cependant à noter que :

    * Cette section n'est pas disponible avec l'organisation des fichiers originale de [pyodide-mkdocs][pyodide-mkdocs]{: target=_blank }, utilisant jusqu'à 3 fichiers python.
    * Cette section ne sera {{red("_PAS_ exécutée si la section `env`{.red} a levé une erreur autre que ")}} `AssertionError`{.red}.
    * {{red("_Ne pas faire de tests depuis cette section._")}} La section `post` est effectivement exécutée _après_ la logique de validation du code de l'utilisateur.





### Créer un IDE

La déclaration d'un IDE dans une page de la documentation se fait par simple appel d'une des macros `IDE` ou `IDEv`, en lui passant le nom du fichier python à utiliser (sans extension).

```markdown
[...énoncé, avec tout le markdown qui vous fait envie...]
{% raw %}
{{ IDE('exo') }}
{% endraw %}
```

La macro se charge de récupérer tous les contenus nécessaires et de les insérer là où approprié.

<br>

 Il y a en fait de nombreuses façons différentes d'organiser les fichiers et leurs contenus : voir [l'argument `py_name`](--IDE-py_name), plus bas dans la page, et la section sur [l'organisation des fichiers sur le disque](--ide-files).




### Déroulement des exécutions {: #runtime-pyodide }


!!! tip "Installation automatiques des bibliothèques python manquantes, lors des exécutions"

    Exécuter dans pyodide "une [section](--ide-sections) de code python" ou une commande du terminal implique toujours les étapes suivantes :

    1. Recherche des éventuels bibliothèques manquantes pour exécuter le code en question.
    1. Installation des bibliothèques manquantes (saufs restrictions sur les packages) .
    1. Exécution du code lui-même.

    Voir ici les détails sur le [fonctionnement des bibliothèques](--custom/python_libs/) dans l'environnement du thème.

<br>

Voici une description des grandes lignes des exécutions réalisées sur le site construit par [mkdocs][mkdocs]{: target=_blank }, lorsque l'utilisateur appuie sur les boutons suivants d'un IDE :


{{ md_include("docs_tools/inclusions/runtime.md") }}



## Fichiers pour un IDE { #ide-files }

En utilisant les désignations suivantes :

- Le fichier markdown dans lequel une macro `IDE` ou `IDEv` est appelée est le fichier {{source()}}.
- Les contenus nécessaires au fonctionnement de cet IDE sont stockés dans des fichiers python dits {{annexes()}}.
- La chaîne `{exo}` (à choisir par vos soins) est un préfixe commun à tous les fichiers annexes.

<br>

Les macros `IDE` et `IDEv` du [thème Pyodide-Mkdocs][pmt-pypi]{: target=_blank } supportent différentes façons d'organiser ces fichiers et leur contenu.<br>
Quelle que soit l'organisation choisie, tous les fichiers {{ annexes() }} utilisés doivent toujours se trouver dans un même dossier (qui n'est pas forcément celui du fichier {{source()}} !).

| Fichier {{width(13)}} | Désignation |
|-|-|
| `{...}.md` | Fichier {{ source() }}, depuis lequel les macros sont appelées. |
| `{exo}.py` | Fichier {{ annexe() }}, dit {{ olive('"principal"') }}. |
| `{exo}_REM.md` | Fichier {{ annexe() }} de remarques, affiché avec le code de la correction (dans une admonition repliée). |
| `{exo}_VIS_REM.md` | Fichier {{ annexe() }} de remarques affichées sous l'admonition repliées donc visibles dès que la solution est révélée. |
| `{exo}_corr.py`<br>`{exo}_test.py` | [ _OBSOLÈTES_ ] Fichiers {{ annexes() }}, originellement utilisés [pour le projet pyodide-mkdocs original][pyodide-mkdocs], mais encore utilisables avec le thème, si vous n'optez pas pour l'organisation avec un fichier python unique. |




### Organisation des contenus { #ide-files-content }

Voici une description de quels contenus placer dans quels fichiers,

Sont présentées ici :

* L'organisation idoine liée au thème (version n'utilisant qu'un seul fichier python). C'est l'organisation conseillée.
* L'organisation hérité du projet original, [pyodide-mkdocs][pyodide-mkdocs]{: target=_blank }, utilisant 3 fichiers python. Cette organisation est toujours supportée, mais certaines des fonctionnalités proposées par le thème ne seront pas disponibles avec cette organisation des fichiers.

<br>

=== "Version "thème" (1 fichier `.py`)"

    === "`{exo}.py`"

        Tous les codes pythons sont regroupés dans un unique fichier `{exo}.py`, chaque section y étant annoncée avec un séparateur spécifique :

        ```
        # ------ PYODIDE:{section} ------ #
        ```

        Dans ces séparateurs :

        - Les espaces sont optionnels (quel que soit leur nombre et leur placement).
        - Les `#` au début et à la fin sont obligatoires.
        - Il faut au moins un trait d'union de chaque côté du texte central.

        <br>

        !!! warning "Validité des contenus"

            --8<-- "docs_tools/inclusions/IDE_exo_py_validity.md"
            {{ md_include("docs_tools/inclusions/IDE_config_checks.md") }}

        <br>

        ??? help "Exemple de code python pour un fichier `{exo}.py`"

            Sur une machine où le thème est installé, le contenu de cet exemple est également accessible via un terminal en utilisant la commande :

            ```
            python -m pyodide_mkdocs_theme --py
            ```

            <br>

            {{md_include("docs_tools/inclusions/IDE_exo_py.md")}}


    === "`{exo}_REM.md` et `{exo}_VIS_REM.md`"

        --8<-- "docs_tools/inclusions/IDE_REMs.md"




=== "Version "pyodide-mkdocs" (3 fichiers `.py`)"

    === "`{exo}.py`"

        _Équivalent aux sections `env`, `code` et `tests`._

        ---

        Il doit contenir, dans cet ordre:

        * Une éventuelle section `HDR` (_équivalent `env`_) en l'insérant entre {{ orange("_deux_") }} séparateurs comme décrit ci-dessous.
        * Le code utilisateur (_équivalent `code`_).
        * Le [`TestsToken`](--les-éditeurs) (insensible à la casse ou aux espaces), si des tests publics sont donnés.
        * Le code des tests publics (_équivalent `tests`_).

        <br>

        Le séparateur pour la section HDR a la forme suivante :

        ```
        # ------ HDR ------ #
        ```

        Ce séparateur suit les mêmes règles que les séparateurs de sections du [fichier python unique du thème](--exopy){: target=_blank } :

        - Les espaces sont optionnels (quel que soit leur nombre).
        - Les `#` au début et à la fin sont obligatoires.
        - il faut au moins un trait d'union entre un `#` et le texte central.

        <br>

        ???+ help "Exemple de code python pour un fichier `{exo}.py`"

            ```python
            # --- HDR --- #

            def helper(x):
                """ For whatever you need """

            # --- HDR --- #

            def est_pair(n):
                ...

            # Tests

            assert est_pair(3) is False
            assert est_pair(24) is True
            ```

        !!! tip "Séparateur alternatif"
            Il est en fait possible d'utiliser `ENV` à la place de `HDR` dans le séparateur :

            ```
            # ------ ENV ------ #
            ```



    === "`{exo}_test.py`"

        _Équivalent à la section `secrets`._

        ---

        Contient les tests secrets pour l'IDE.

        <br>

        ???+ help "Exemple de code python pour un fichier `{exo}_test.py`"

            ```python
            ---8<--- "docs_tools/inclusions/is_even_tests_example.py"
            ```


    === "`{exo}_corr.py`"

        _Équivalent à la section `corr`._

        ---

        Contient la correction pour l'IDE.

        <br>

        ???+ help "Exemple de code python pour un fichier `{exo}_corr.py`"

            ```python
            def est_pair(n):
                return not n%2
            ```


    === "`{exo}_REM.md` et `{exo}_VIS_REM.md`"

        --8<-- "docs_tools/inclusions/IDE_REMs.md"

    <br>

    !!! warning "Attention"

        * Le mode [**benchmark** de pyodide-mkdocs][benchmark] n'est plus supporté.

        {{ md_include("docs_tools/inclusions/IDE_config_checks.md") }}

    <br>

    (Rappels : `secrets` = `{exo}_test.py`, `corr` = `{exo}_corr.py`)




### Organisation des fichiers { #ide-files-organization }


Tous les fichiers {{ annexes() }} pour un IDE donné doivent être stockés au même endroit, mais pas obligatoirement dans le dossier du fichier markdown {{ source() }} (celui depuis lequel les macros `IDE` sont appelées).

Par ailleurs, la macro est insensible à la configuration des noms pages sur le site final/construit : vous pouvez nommer le fichier {{ source() }} comme bon vous semble (`sujet.md`, `{exo}.md`, `index.md`, ou autres...).


<br>

Voici trois façons différentes d'organiser les fichiers, qui sont compatibles avec le thème :

=== "Mode "par exercice""

    C'est l'organisation la plus simple, avec le ou les fichiers python dans le meme dossier que le fichier markdown source.

    - Fichier {{source()}} : `docs/.../paire_ou_pas/index.md`
    - Appel de macro : `{% raw %}{{ IDE('exo') }}{% endraw %}`

    === "Version "thème""

        ```
        ...
        ├── paire_ou_pas
        │   ├── index.md
        │   ├── exo.py
        │   ├── exo_REM.md
        │   └── exo_VIS_REM.md
        ```

    === "Ancienne version"

        ```
        ...
        ├── paire_ou_pas
        │   ├── index.md
        │   ├── exo.py
        │   ├── exo_corr.py
        │   ├── exo_test.py
        │   ├── exo_REM.md
        │   └── exo_VIS_REM.md
        ```


=== "Mode "par exercice + dossiers""

    Le premier argument de la macro est en fait un chemin relatif depuis le répertoire du fichier markdown source.

    Donc si la page contient un grand nombre d'IDEs, il peut être intéressant d'utiliser un sous-dossier pour les fichiers d'un IDE.

    - Fichier {{source()}} : `docs/.../prog_dynamique/index.md`
    - Appels de macro : `{% raw %}{{ IDE('partie1/exo') }} ... {{ IDE('partie2/exo') }}{% endraw %}`

    === "Version "thème""

        ```
        ...
        ├── prog_dynamique
        │   ├── index.md
        │   ├── partie1
        │   │   ├── exo.py
        │   │   ├── exo_REM.md
        │   │   └── exo_VIS_REM.md
        │   ├── partie2
        │   │   ├── exo.py
        │   │   ├── exo_REM.md
        │   │   └── exo_VIS_REM.md
        ...
        ```

    === "Ancienne version"

        ```
        ...
        ├── prog_dynamique
        │   ├── index.md
        │   ├── partie1
        │   │   ├── exo.py
        │   │   ├── exo_corr.py
        │   │   ├── exo_test.py
        │   │   ├── exo_REM.md
        │   │   └── exo_VIS_REM.md
        │   ├── partie2
        │   │   ├── exo.py
        │   │   ├── exo_corr.py
        │   │   ├── exo_test.py
        │   │   ├── exo_REM.md
        │   │   └── exo_VIS_REM.md
        ...
        ```

=== "Mode "par chapitre""

    Les deux cas précédents partent plutôt du principe que chaque fichier source dispose de son propre dossier.

    On peut aussi envisager de travailler avec différents exercices (fichiers markdowns {{sources()}}), tous contenus dans le même dossier, avec leurs fichiers {{annexes()}} dans des sous-dossiers eux-mêmes contenus dans un dossier `scripts`.

    Concrètement, les appels de macros seraient de la forme :

    - `{% raw %}{{ IDE('exo1') }}{% endraw %}` dans le fichier `chapitre1.md`
    - `{% raw %}{{ IDE('exo1') }}{% endraw %}` dans le fichier `chapitre2.md`

    <br>

    La version générique correspondant à cette organisation est donc à voir comme suit :

    - Considérant un ficher {{source()}} `.../XXX.md`.
    - Dans le même dossier, on doit trouver des sous-dossers : `.../scripts/XXX/` contenant les fichiers relatifs aux IDEs (au moins un fichier `{exo}.py` par IDE).
    - Dans le fichier {{source()}}, les appels de macros sont faits avec {% raw %}`{{ IDE('exo') }}`{% endraw %}.


    <br>

    === "Version "thème""

        ```
        docs
        ├── chapitre1.md     => corps du chapitre que vous souhaitez écrire
        ├── chapitre2.md
        ├── chapitre3.md
        ├── scripts
        │   ├── chapitre1    => dossier contenant les exercices à intégrer à vos IDEs
        │   │   ├── exo1.py
        │   │   ├── exo1_REM.md
        │   │   ├── exo1_VIS_REM.md
        │   │   ├── exo2.py
        │   │   ├── exo2_REM.md
        │   │   ├── exo2_VIS_REM.md
        │   │   ├── exo3.py
        │   │   ├── exo3_REM.md
        │   │   └── exo3_VIS_REM.md
        │   ├── chapitre2
        │   │   ├── exo1.py
        │   │   ├── ...
        ...
        ```

    === "Ancienne version"

        ```
        docs
        ├── chapitre1.md     => corps du chapitre que vous souhaitez écrire
        ├── chapitre2.md
        ├── chapitre3.md
        ├── scripts
        │   ├── chapitre1    => dossier contenant les exercices à intégrer à vos IDEs
        │   │   ├── exo1.py
        │   │   ├── exo1_corr.py
        │   │   ├── exo1_test.py
        │   │   ├── exo1_REM.md
        │   │   ├── exo1_VIS_REM.md
        │   │   ├── exo2.py
        │   │   ├── exo2_corr.py
        │   │   ├── exo2_test.py
        │   │   ├── exo2_REM.md
        │   │   ├── exo2_VIS_REM.md
        │   │   ├── exo3.py
        │   │   ├── exo3_corr.py
        │   │   ├── exo3_test.py
        │   │   ├── exo3_REM.md
        │   │   └── exo3_VIS_REM.md
        │   ├── chapitre2
        │   │   ├── exo1.py
        │   │   ├── ...
        ...
        ```


### Mélanges de fichiers PMT et Pyodide-MkDocs { #melanges-de-fichiers }

Si le fichier {{ annexe("principal") }} `{exo}.py` est reconnu comme un fichier "thème" (donc avec des séparateurs de sections `# --- PYODIDE:{section} --- #`), les éventuelles fichiers `{exo}_corr.py` ou `{exo}_test.py` seront tout bonnement ignorés.




### Modifications vs `localStorage`

Les sites construits via le thème enregistrent les codes utilisateurs dans le `localStorage` associé au site, afin que les utilisateurs puissent retrouver leurs modifications lorsqu'ils se reconnectent au site depuis le même couple machine/navigateur.

L'enregistrement de leur code est effectué dans les cas suivants :

* Lorsqu'une validation ou les tests publics sont exécutés.
* Lorsque l'enregistrement est explicitement demandé par l'utilisateur (via le bouton sous l'IDE).
* Toutes les 30 frappes de caractères dans l'éditeur.

<br>

Cette sauvegarde peut parfois poser des problèmes lorsque l'auteur du site fait une mise à jour des tests publics ou du code proposé initialement à l'utilisateur (sections `code` et `tests`).

Afin que l'utilisateur ne puisse pas se retrouver avec un code qui ne correspond plus aux spécifications actuelles du problème sans le savoir, le thème garde une trace du contenu initial attendu pour un éditeur. S'il constate que la version référencée dans le `localStorage` est différente du code que l'utilisateur obtiendrait actuellement après un `reset`, un message est automatiquement affiché dans le terminal au chargement de la page, indiquant à l'utilisateur qu'il devrait copier ses modifications puis réinitialiser l'IDE.

<br>

!!! tip "Remarques"

    * Le message n'apparaît que tant qu'aucune action utilisateur ne déclenche de nouvelle sauvegarde dans le `localStorage`.
    * Réinitialiser l'IDE (bouton `reset`) met également à jour le `localStorage`, mais avec la version initiale du code de l'éditeur.
    * Cette fonctionnalité a été introduite en version `2.5.0` du thème.
    <br>Les informations nécessaires pour décider d'afficher ou non le message demandant de réinitialiser l'IDE n'existaient pas avant cela, et la transition se fait automatiquement, côté utilisateur.
    <br>Il a par contre été décidé que lors de cette transition, le code actuellement stocké dans le `localStorage` serait considéré comme "à jour", afin de ne pas avoir tous les IDEs du site demandant à l'utilisateur de faire la mise à jour (95% d'entre eux étant probablement des faux positifs).














## {{anchor_redirect(id="la-macro-ide")}}Les macros `IDE` et `IDEv` { #ide-macro }


Voici les specifications des arguments des macros `IDE` ou `IDEv`.




### Signature

La signature complète pour un appel aux macros `IDE` ou `IDEv` est du type :

{{ macro_signature('IDE') }}

<br>

??? tip "L'astérisque seule dans `IDE(..., *, ...)` ?"

    Cette astérisque fait qu'il n'est pas possible d'utiliser les arguments autres que `py_name` sans renseigner le nom de l'argument en plus de la valeur. Tous les arguments suivant sont donc _arguments nommés_.


{{md_include("docs_tools/inclusions/config_defaults_info.md")}}



<span id="IDE-py-name" backward-compatibility><span>

### `py_name` {: #IDE-py_name}

{{ macro_args_table('IDE', only='py_name') }}

Chemin relatif au dossier contenant le ficher markdown {{ orange('source') }} permettant d'accéder au fichier python {{ annexe('principal') }} pour l'IDE.

* Si l'argument n'est pas renseigné ou est une chaîne vide, l'IDE sera créé vide (ex : [bac à sable](--bac_a_sable/)).
* Le chemin ne donne que le préfixe commun des fichiers {{annexes()}} pour cet IDE, et il faut donc {{red("omettre l'extension")}} : si le fichier {{annexe("principal")}} est `.../exercice.py`, l'argument `py_name` doit être  `".../exercice"`.
* Une {{red('erreur')}} est levée si un chemin est donné mais qu'aucun fichier python ne peut être trouvé pour les différentes [organisations de fichiers supportées](--ide-files-organization).






### `ID` {: #IDE-ID }

{{ macro_args_table('IDE', only='ID') }}

Pour pouvoir sauvegarder, télécharger et téléverser des codes dans ou depuis les IDEs, l'`id` html de chaque IDE doit être unique. Or, ceux-ci sont construits à partir de la localisation du fichier python principal sur le disque.

Le thème vérifie notamment l'unicité des `id` générés pour l'intégralité du site et lève une erreur si le même fichier python (argument `py_name`) est utilisé plusieurs fois dans différents IDEs.

L'argument `ID` de la macro `IDE` permet donc de différencier deux IDEs utilisant le même fichier python, afin de garantir l'unicité des ids html.

Exemple :

```
Fichier 1: docs/index.md
    {% raw %} {{ IDE('exemples/ex1', ID=1)}} {% endraw %}

Fichier 2: docs/aide.md
    {% raw %} {{ IDE('exemples/ex1', ID=2)}} {% endraw %}
```






### `SANS` {: #IDE-SANS }

{{ macro_args_table('IDE', only='SANS') }}

Voici le détail des syntaxes et contraintes sur l'utilisation de l'arguent `SANS` :

* Les identifiants utilisés dans l'argument `SANS` peuvent êtres séparés par des espaces, des virgules ou des points-virgules.
* Les fonctions builtins et les modules sont simplement renseignés an donnant leur identifiant (pour les modules, renseigner le nom du "top niveau": `matplotlib`, et non `matplotlib.pyplot`).
* Les restrictions d'accès à des attributs ou des méthodes sont l'identifiant de l'attribut ou la méthode, préfixé avec un point.
* Les mots clefs et opérateurs doivent être renseignés en dernier, après avoir ajouté le séparateur `"{{AST()}}"` : `SANS="... AST: mot_clefs + ** // ..."`.

    | Syntaxe  {{width(10)}} | Ce qui est interdit |
    |-|-|
    | `identifiant` | [Interdit la fonction builtin](--restrictions-functions) correspondant, ou si aucune fonction n'est trouvée, [interdit le module/package](--restrictions-packages) correspondant.{{pmt_note("Les imports sont couverts quelle que soit la méthode utilisée : `import`{.pmt_note}, `from`{.pmt_note}, aliasing, ...")}} |
    | `.attribut`<br>`.method` | [Interdit les accès d'attributs](--restrictions-methods) correspondant à ce nom lève une erreur s'ils sont trouvés dans le code. |
    | `keyword` ou `operator` | [Interdit des opérateurs et des mots clefs](--restrictions-keywords) ou assimilés.{{pmt_note("Ceux-ci sont différenciés des identifiants de fonctions ou des modules en les renseignant après le séparateur `{{ AST() }}`{.pmt_note}.")}} |


Exemple d'utilisation :

```
{% raw %}{{ IDE('exo', SANS="sorted, max ; .find .index AST: for") }}{% endraw %}
```

Cet appel de macro interdit :

- Les fonctions `sorted` et `max`.
- Les appels de méthodes `.find` et `.index`.
- Tous types de boucles `for` (boucles normales et comprehensions).

{{pmt_note("Des informations plus détaillées sur les interdictions et notamment sur les restrictions de mots-clefs et opérateurs sont disponibles dans la [page détaillant les exécutions](--restrictions).")}}

<br>

??? tip "Sections/code appliquant les différents types de restrictions"

    {{ md_include("docs_tools/inclusions/restrictions_where.md") }}

    Cela signifie que l'on ne peut normalement pas utiliser les éléments interdits dans les tests `secrets` pour vérifier les réponses de l'utilisateur (il existe en fait un moyen... Voir la section des utilisations avancées).


{{ kws_exclusions() }}


??? warning "But recherché avec les restrictions"

    {{ md_include("docs_tools/inclusions/restrictions_goals.md") }}


??? warning "Efficacité des interdictions"

    {{ md_include("docs_tools/inclusions/restrictions_efficiency.md") }}







### `WHITE` {: #IDE-WHITE }

{{ macro_args_table('IDE', only='WHITE') }}

??? help "_Cet argument est normalement {{ orange('**obsolète**') }}._"

    Permet de déclarer un ensemble de modules/packages à préimporter avant que les restrictions d'imports ne soient mises en place.


    La syntaxe est la même que celle de l'argument [`SANS`](--IDE-SANS): des identifiants séparés par des espaces, des virgules ou des points-virgules.

    Exemple: `{% raw %}{{ IDE(..., SANS="sys", WHITE="math") }}{% endraw %}`

    !!! tip "Le problème que `WHITE` essaie de résoudre..."

        Certains modules de "bas niveau" sont utilisés pour importer d'autres modules, même dans des cas inattendus (ex: `math` qui importe `sys`). Si le module sys est interdit (_ce qui est une très mauvaise idée !_), il peut devenir nécessaire de préimporter d'autres modules que l'utilisateur pourrait avoir besoin d'utiliser dans son code.

        Voir la page concernant les [restrictions](--restrictions) pour plus de détails.

    !!! warning "`WHITE` n'a pas pour but de remplacer du code python !"

        Cet argument est un ersatz d'une version antérieure du thème.

        Il a été conservé dans l'éventualité où un rédacteur se retrouverait tout de même confronté à un problème d'import interdit pour de mauvaises raisons, mais des changements intervenus plus tard dans le développement ont normalement rendu cet argument obsolète : réaliser les préimports depuis la section `env` devrait suffire à éviter ce type de problèmes.


    !!! warning "Efficacité des interdictions"

        {{ md_include("docs_tools/inclusions/restrictions_efficiency.md") }}






### `REC_LIMIT` {: #IDE-REC_LIMIT }

{{ macro_args_table('IDE', only='REC_LIMIT') }}

Si cet argument est utilisé avec une valeur positive, la profondeur de récursion sera limitée et une erreur sera levée si elle est atteinte durant les exécutions.

??? tip "Limitations, particularités, conseils pour cet argument..."

    * Ne pas oublier que ce réglage affecte la stack, qui est globale. Ce qui veut dire que le premier appel de fonction de l'utilisateur n'est __pas__ à une profondeur 0 ou 1.

    * Pour cette raison, une erreur est levée par la macro si une valeur inférieure à {{ rec_limit() }} est utilisée pour `REC_LIMIT`, car l'environnement lui-même ne pourrait pas faire tourner le code de l'utilisateur et les tests.

    * Côté utilisateur, la fonction `sys.setrecursionlimit` est désactivée lorsque cette fonctionnalité est utilisée.

        - Le procédé utilisé est le même que pour les restrictions de code.
        - Ce réglage affecte donc également les différents types de tests et le terminal de l'IDE.

    * Ne surtout pas activer cette fonctionnalité si vous utilisez des structures de données récursives, avec des implantations pour `__str__` ou `__repr__` : l'utilisateur se retrouverait probablement avec un crash dû à la restriction à des moments inopportuns :

        - Sur un `print` (même si la sortie standard n'est pas affichée !)
        - Lors de la construction d'un message d'erreur affichant une structure de données.





### `MERMAID` { #IDE-MERMAID }

{{ macro_args_table('IDE', only='MERMAID') }}

Le rôle de cet argument est un peu particulier : son but est de signaler au thème que cette page devra intégrer la logistique javascript et pyodide pour construire dynamiquement des graphes `mermaid` dans la page.

Ce "signalement" est en fait global à la page entière de la documentation, et il n'est donc pas nécessaire de l'utiliser pour chaque IDE d'une page : {{green("__une seule fois par page est suffisant__")}}.

Voir la page dédiée à [l'utilisation dynamique de `mermaid`](--custom/mermaid/) dans pyodide pour plus d'informations.





### `AUTO_RUN` {: #IDE-AUTO_RUN }

{{ macro_args_table('IDE', only='AUTO_RUN') }}





### `LOGS` {: #IDE-LOGS }

{{ macro_args_table('IDE', only='LOGS') }}

Lors d'une assertion échouée durant une validation, si le code de l'assertion n'a aucun message d'erreur, le thème peut en construire un automatiquement. Selon le type d'exercices que vous rédigez, ou si l'exercice provient d'un ancien site utilisant [pyodide-mkdocs][pyodide-mkdocs]{: target=_blank } vous pourriez souhaiter que les messages d'erreur automatiques soient construits ou non, pour ces assertions sans messages.

<br>

!!! warning "Migration depuis `pyodide-mkdocs` : ___BREAKING CHANGE___"

    ---8<--- "docs_tools/inclusions/IDE_assertions_feedback.md"

    {{ orange("_Notez que ce changement de comportement par rapport à `pyodide-mkdocs` concerne aussi les tests publics._") }}





### `MAX` {: #IDE-MAX}

{{ macro_args_table('IDE', only='MAX') }}

* En l'absence de correction et de fichiers de remarques, le compteur d'essais sera automatiquement passé à $\infty/\infty$.
* Il est possible d'imposer un nombre d'essais infini en passant `#!py 1000` ou `#!py "+"` en argument.
<br>Ceci impliquerait que la seule façon pour l'utilisateur de voir la solution et/ou les remarques serait de passer tous les tests avec succès.


??? danger "Erreur levée pour les contenus `corr/REM` avec des compteurs d'essais à l'$\infty$"
    Par défaut, le thème considère que du contenu `corr` ou des remarques (visibles ou non) qui seraient "cachés" par un compteur d'essais réglé à l'$\infty$ est une situation non désirée. Une erreur est donc levée durant le build si la situation est rencontrée.

    Si c'est effectivement le but recherché, il faut alors modifier l'option {{ config_link("ides.forbid_corr_and_REMs_with_infinite_attempts") }} du plugin, soit via le fichier `mkdocs.yml`,  soit via la configuration des métadonnées ([fichiers `{{meta()}}` ou entêtes de pages](--custom/metadata/)) :

    ```yaml
    plugins:
        pyodide_macros:
            ides:
                forbid_corr_and_REMs_with_infinite_attempts: false  # (défaut : true)
    ```




### `MODE` {: #IDE-MODE }

{{ macro_args_table('IDE', only='MODE') }}

Une erreur est levée si une valeur est passée en argument alors qu'elle ne correspond à aucun `MODE` existant.

!!! danger "L'utilisation de l'argument `MODE`supprime les routines de validation des données des IDEs"

    Si les profiles sont utilisés, toutes les vérifications faites habituellement par le thème lorsqu'il construit l'IDE sont supprimées.

    Ceci concerne notamment les vérifications liées aux options suivantes du plugin :

    * {{ config_link('ides.forbid_corr_and_REMs_with_infinite_attempts', 1) }}
    * {{ config_link('ides.forbid_hidden_corr_and_REMs_without_secrets', 1) }}
    * {{ config_link('ides.forbid_secrets_without_corr_or_REMs', 1) }}








### `MAX_SIZE` {: #IDE-MAX_SIZE }

{{ macro_args_table('IDE', only='MAX_SIZE') }}

Permet de définir le nombre maximal de lignes de la fenêtre de l'éditeur : si le code comporte plus de lignes, des glissières apparaîtront.








### `MIN_SIZE` {: #IDE-MIN_SIZE }

{{ macro_args_table('IDE', only='MIN_SIZE') }}

Permet de définir le nombre minimal de lignes de la fenêtre de l'éditeur.





### `TERM_H` {: #IDE-TERM_H }


{{ macro_args_table('IDE', only='TERM_H') }}

Remarques :

* Le réglage n'est pas très précis et peut devenir erroné selon les règles CSS surchargées par vos soins.
* Cet argument est ignoré pour les macros `IDEv`.





### `TEST` {: #IDE-TEST }

{{ macro_args_table('IDE', only='TEST') }}

Voir la page dédiée pour plus d'information sur les [tests automatiques des IDEs de la documentation](--redactors/IDE-tests-page/).





### `TWO_COLS` {: #IDE-TWO_COLS }

{{ macro_args_table('IDE', only='TWO_COLS') }}

Comme tous les autres arguments de macros, il peut être défini au niveau des `meta` (fichiers, entêtes, mkdocs.yml).
<br>À noter que si plusieurs IDEs ont ce réglage à True dans la même page, il n'y a aucune garantie sur celui qui sera effectivement en mode "deux colonnes" après chargement de la page.





### `STD_KEY` {: #IDE-STD_KEY }

{{ macro_args_table('IDE', only='STD_KEY') }}

--8<-- "docs_tools/inclusions/IDE_STD_KEY_validation.md"




### `EXPORT` {: #IDE-EXPORT }

{{ macro_args_table('IDE', only='EXPORT') }}


Les IDEs marqués avec `#!py EXPORT=True` se voient ajouté un bouton {{btn('zip', in_tag='span')}} permettant de télécharger une archive `.zip` avec les contenus de tous les éditeurs marqués dans la page en cours.

Le but de cette fonctionnalité est multiple :

* Permettre aux utilisateurs de télécharger en une fois tous les contenus des éditeurs (marqués) de la page, pour garder une trace de leurs codes.
* Générer en un clic une archive que l'enseignant peut ensuite récupérer (voir plus bas pour ce qui concerne les noms de fichiers donnés aux archives)
* Il est en fait possible de charger dans la page du site le contenu d'un fichier zip en faisant un glissé-déposé de l'archive sur le bouton de création du fichier zip de l'un des IDEs marqués de la page. Cela permet de tester rapidement les codes d'un élève ou groupe d'élèves. Cette fonctionnalité n'est pas décrite dans la documentation des utilisateurs.

___Gestion des noms de fichiers des archives zip :___

* Par défaut, le nom de l'archive zip est créé à partir de l'adresse de la page sur le site construit (en excluant la racine du site).
* Il est possible à l'auteur d'ajouter un préfixe de son choix aux noms des archives, en renseignant l'option {{config_link('ides.export_zip_prefix')}}, dans les métadonnées de la page.
* Si l'enseignant envisage de récupérer les archives zip des élèves, il est également possible de forcer les utilisateurs à renseigner leur nom au moment de créer l'archive zip (ou tout autre chose pouvant servir d'identifiant). Pour cela configurer dans les métadonnées l'option {{config_link('ides.export_zip_with_names', val="true")}}.

Le nom complet des archives est généré selon le modèle suivant, selon les éléments activés via les options de configuration : `PREFIX-NAMES-DEFAULT`.







{{ IDE_tests_to_details() }}
