# Outil de création de QCMs { #qcm-builder }

Cet outil permet de :

- Créer un QCM.
- Télécharger le fichier JSON équivalent à partir du formulaire rempli.
- Copier le code de déclaration JSON dans le presse-papier pour modifier un fichier existant.
- Téléverser un fichier JSON de QCM existant, pour pouvoir le modifier.
- Il simplifie _drastiquement_ la déclaration des contenus markdown, en évitant toutes les complications liées aux [déclarations faites directement dans les appels de macro](--qcms-pieges-a-eviter) `multi_qcm`.

Une fois le fichier créé, il suffit de le déposer dans le dossier voulu, puis d'ajouter un appel à la macro `multi_qcm` avec les arguments appropriés dans la page markdown où l'on souhaite ajouter le QCM.

<br SPLIT_TOKEN>

Les champs laissés à `unset` ne sont pas inclus dans la déclaration Json finale, ce qui permet de laisser les réglages globaux s'appliquer quand nécessaire.

* Les arguments de [la macro `multi_qcm`](--qcm-macro)
* Les [informations de configuration via les méta-données ou les entêtes de pages](--custom/metadata/).

<br><br>

{{ qcm_builder() }}

<br><br><br>
