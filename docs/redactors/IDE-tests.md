---
ides:
    forbid_secrets_without_corr_or_REMs: false
---


# Écrire des tests {{anchor_redirect(id="ecrire-des-tests")}} { #tests-writing-tests }


La façon de rédiger les tests peut _drastiquement_ changer le confort de l'utilisateur, lorsqu'il essaie de résoudre un problème.

En particulier, si  la sortie standard est désactivée durant les tests secrets, il est critique que les assertions donnent bien le niveau d'information souhaité par le rédacteur, sans quoi l'utilisateur peut se retrouver sans aucune information utile, et donc très peu de chances de trouver comment modifier son code.



## Stratégies {{anchor_redirect(id="strategies")}} { #tests-strategies }

De manière générale, les "stratégies" suivantes sont conseillées :

1. Mettre tous les cas particuliers dans les tests publics.

1. Mettre suffisamment de tests publics (section `tests`) pour couvrir au moins une fois tous les types de cas présents dans les tests de la section `secrets`.

1. {{red("Les assertions sans message d'erreur sont à proscrire dans les tests secrets.")}}
<br>Si vous comptez utiliser la fonctionnalité de génération automatique des messages d'erreurs des macros `IDE` (voir [l'argument `LOGS`](--IDE-LOGS) et sa configuration globale), il faut ___à minima___ que les valeurs des arguments soient définies dans le code de chaque assertion.

1. On peut adapter le niveau de feedback des messages d'erreur selon le but recherché et le contexte de l'exercice.<br>Typiquement :

    - Un message donnant uniquement le ou les arguments peut être considéré comme le strict minimum acceptable.

        ```python
        assert func(arg) == attendu, f"func({arg})"
        ```

    - Un message donnant en plus la réponse de l'utilisateur est un confort ajouté certain. Cela ne change rien au niveau d'information accessible à l'utilisateur, mais cela lui évite d'avoir à créer lui-même le test public correspondant pour savoir ce que sa fonction a renvoyé.

        ```python
        val = func(arg)
        assert val == attendu, f"func({arg}): a renvoyé {val}"
        ```

    - Un message présentant en plus la réponse attendue est la "Rolls" du message d'assertion (mais n'est pas toujours souhaitable pour des tests "secrets").

        ```python
        val = func(arg)
        assert val == attendu, f"func({arg}): {val} devrait être {attendu}"
        ```

    ??? tip "Ne pas appeler plusieurs fois la fonction de l'utilisateur pour un même test"

        Il est _vivement_ déconseillé d'appeler plusieurs fois la fonction de l'utilisateur pour le même test. Cela peut rendre le code plus difficile à déboguer.

        === "{{ red('Pas bien') }}"
            ```python
            assert func(arg) == attendu, f"func({arg}): {func(arg)} devrait être {attendu}"
            ```

        === "{{ green('Bien') }}"
            ```python
            val = func(arg)
            assert val == attendu, f"func({arg}): {val} devrait être {attendu}"
            ```

1. Si des tests aléatoires sont implantés, il est _INDISPENSABLE_ de leur associer un niveau de feedback élevé (voir ci-dessus) :

    Gardez en tête que des tests aléatoires _vont_ générer des cas particuliers pour les fonctions alambiquées de vos utilisateurs, que vous n'avez aucune chance de prévoir dans les tests publics. Les insuffisances du feedback peuvent alors transformer l'exercice que vous aurez passé des heures à peaufiner en véritable cauchemar pour vos élèves, ce qui serait tout de même dommage...










## Aspects techniques {{anchor_redirect(id="aspects-techniques")}} { #tests-technique }

_^^Problématique :^^_

Gardez en mémoire que l'environnement Pyodide est commun à toute la page, ou jusqu'à un éventuel rechargement de celle-ci. Cela implique que de nombreux effets de bords peuvent se présenter, dont les utilisateurs n'auront pas conscience.

En particulier toute variable ou fonction définie dans les tests est visible depuis la fonction de l'utilisateur. Ceci signifie que l'environnement de la seconde exécution n'est souvent pas le même que celui de la première exécution des tests.

Concrètement, cela peut amener à des erreurs très dures à tracer côté utilisateur, car les comportements ne sont pas toujours reproductibles. On a par exemple vu des codes lever ou pas `NameError`, selon l'ordre d'utilisation des boutons de l'IDE après un chargement de page...

<br>

_^^Solution :^^_

* Il est vivement conseillé d'écrire les tests dans une fonction afin que leur contenu ne puisse pas "leaker" dans l'environnement de l'utilisateur.
* Ne pas oublier d'appeler la fonction des tests après l'avoir écrite... :wink:
* Comme les fonctions restent définies dans l'environnement global, après avoir lancé une première validation, un utilisateur malin pourrait exécuter des fonctions définies dans la section `secrets` {{ annotate("Le nom des fonctions est visible dans les stacktraces des erreurs, ou encore, accessible via `dir()` dans le terminal.") }}. Ajouter des appels à ces fonctions dans les tests publics, dans l'éditeur de l'IDE, permettrait de contourner le compteur d'essais. <br>Dans le contexte du thème, cela n'est sans doute pas un problème (il n'y a rien à gagner, après tout... !), mais si vous voulez l'empêcher, il vous faudra supprimer la fonction de l'environnement en utilisant `del` comme montré  dans le second exemple ci-dessous.

[Le décorateur `@auto_run`](--tests-auto_run) proposé par le thème facilite tout ceci, en gérant la définition et le lancement des fonctions englobant les tests pour le rédacteur.

<br>


=== "Conseillé: `@auto_run`"

    Voir plus bas les [explications détaillées sur le décorateur `@auto_run`](--tests-auto_run).

    ```python
    # Exécute automatiquement la fonction et empêche sa définition dans l'environnement:
    @auto_run
    def tests():
        # Il est conseillé d'utiliser une fonction pour éviter que des
        # variables des tests se retrouvent dans l'environnement global.
        for n in range(100):
            val = est_pair(n)
            exp = n%2 == 0

            msg = f"est_pair({n})"                           # Minimum vital
            msg = f"est_pair({n}): valeur renvoyée {val}"    # Conseillé
            msg = f"est_pair({n}): {val} devrait être {exp}" # La totale

            assert val == exp, msg
    ```


=== "Exemple simple"

    ```python title="De bons tests secrets..."
    ---8<--- "docs_tools/inclusions/is_even_tests_example.py"
    ```

=== "Exemple complexe (effacer les traces)"

    !!! tip "Voir l'outil `@auto_run`, plus bas dans la page"

        Le thème met à disposition un décorateur faisant exactement tout cela, et gérant également le nettoyage régulier de l'environnement.

    Effacer les fonctions de tests peut devenir assez fastidieux, notamment de par le fait que si une erreur est levée, le code suivant la fonction de test n'est pas exécuté, sauf à utiliser des blocs `try/except` ou assimilés...

    Il existe une solution très rapide qui permet :

    1. D'être certain de ne pas oublier d'appeler les fonctions de tests.
    1. D'être certain que la fonction de tests ne sera pas disponible du côté de l'utilisateur, et ce quelle que soit le déroulement des tests (succès/erreur).
    1. Ne fournit aucun élément exploitable à l'utilisateur pour investiguer le contenu des tests.

    === "La méthode..."

        {{ section("env_examples/decorate_tests_funcs", "code")}}


    === "Pourquoi cela marche ?"

        `test` repose sur la syntaxe des décorateurs, mais n'en est en fait pas vraiment un : `test` appelle directement la fonction `f` passée en argument, au lieu de renvoyer une fonction comme un décorateur normal.

        {{green("Ceci fait que toute fonction décorée avec `test`{.green} est une fonction de test exécutée.")}}

        <br>

        Par ailleurs, la variable du scope global correspondant au nom d'une fonction décorée se voit en fait associée ce que l'appel au décorateur renvoie. Ici, `test` renvoie le résultat de `f()`, qui est la fonction décorée, mais les fonctions de tests ne renvoient rien, et `test` renvoie donc `None`, si aucune erreur n'est levée.

        {{green("Une fonction décorée avec `test`{.green} n'est donc en fait jamais assignée à la variable correspondant dans le scope globale, et ne sera donc jamais accessible à l'utilisateur.")}}

    <br>

    ![résultat](!!tests_decorator_png){: loading=lazy .w35 align=right }

    Après les exécutions :

    - `test` est définie et accessible à l'utilisateur, mais n'a strictement aucun intérêt pour lui.
    - `passing` est également définie, mais est en fait `None` au lieu d'être la fonction `passing`.
    - `failing` n'est pas définie car la fonction a levé une erreur avant que le décorateur n'affecte la variable dans le scope global.

    Il est possible de constater tout cela avec l'IDE ci-dessous :

    {{ IDE('env_examples/decorate_tests_funcs', ID=1, TEST=Case(
        code=1, fail=1, all_in_std=["passed", "None", "AssertionError: oups..."]
    )) }}


<br>

Vous pouvez appliquer la même stratégie aux tests publics, mais cela surcharge notablement le code et n'est donc probablement pas souhaitable.<br>
D'autant plus que pour ces tests, si les assertions sont écrites avec toutes les données "en dur", la construction automatique des messages d'erreur donne presque tout le feedback nécessaire à l'utilisateur, tout en faisant gagner du temps au rédacteur (le seul élément non affiché étant alors la valeur renvoyée par la fonction de l'utilisateur).

```python
# Suffisant pour des tests publics, mais pas forcément pour des tests secrets !
assert est_pair(3) is False
assert est_pair(24) is True
...
```








## {{anchor_redirect(id="outil-auto-run")}}L'outil `@auto_run` { #tests-auto_run }


### But { #tests-auto_run-goal }

À partir de la version 2.0, le thème propose un décorateur qui __permet d'exécuter automatiquement la fonction qu'il décore, et empêche de la réutiliser par la suite__.

Ceci est particulièrement utile pour écrire des fonctions devant exécuter certaines tâches en gardant leur contenu isolé de l'environnement global, comme des fonctions de tests pour les sections `secrets` des IDEs, par exemple.

<br>

=== "Avec décorateur"

    ```python
    @auto_run
    def test():
        """ tests dans un scope isolé """
        assert func() == 42
    ```

    * Le code executant la fonction (le décorateur) est écrit au plus proche de la déclaration de fonction, ce qui évite d'oublier l'appel de fonction dans le code, quand la fonction devient très longue.
    * La façon de fonctionner du décorateur fait que la fonction décorée n'est en fait jamais affectée dans l'environnement, et l'utilisateur ne pourra pas y accéder lors d'une exécution ultérieure.

=== "Comportement équivalent sans le décorateur"

    ```python
    def test():
        """ tests dans un scope isolé """
        assert func() == 42

    try:
        test()
    finally:
        test=None
    ```

    * Obtenir le même comportement sans le décorateur est très laborieux, car il faut garantir l'effacement de la fonction, si `test()` lève une erreur.
    * Si le code de la fonction est long, on ne voit pas de suite si la fonction est bien appelée ou pas.



### Spécifications { #tests-auto_run-specs }

* Le décorateur exécute automatiquement la fonction décorée.
* {{red("__Le décorateur renvoie `None`{.red}__")}}, donc la fonction d'origine n'est jamais disponible dans le scope de l'utilisateur après exécution.
* Le décorateur est redéfini à chaque exécution lancée par l'utilisateur, de manière à fiabiliser son utilisation.
* Une fonction `func` décorée va laisser une variable `func=None` dans l'environnement. Le thème supprime automatiquement ces variables, à différents moments des exécutions :

    - juste {{orange("_avant_")}} d'exécuter le code ou la commande de l'utilisateur,
    - à la toute fin d'une exécution, {{orange("_après_")}} `post_term` et `post`.

* Il est possible de déclencher manuellement le nettoyage de l'environnement en appelant la méthode `auto_run.clean()`, si besoin.

<br>

!!! warning "Ne pas utiliser le décorateur dans les tests publics"

    L'utilisateur n'a pas besoin de connaître l'existence du décorateur : il pourrait sinon interagir avec lui et potentiellement changer le comportement des exécutions, s'il sait comment faire.


<br>
<br>

Voici une mise en évidence du comportement des fonctions décorées, et des variables présentes dans l'environnement après exécutions.

{{ IDE_py(
    'env_examples/auto_run_ex', ID=1, TERM_H=25, MODE='no_reveal', TEST=Case(
        code=1, fail=1, all_in_std=[
            "passing non définie",
            "failing non définie",
            "KeyError",
            "passing None",
            "failing non définie",
        ]
    ),
    before='!!!', admo_kls='tip inline end w50'
) }}


On observe que :

- Les variables `passing` et `failing` ne sont jamais définies lors de l'exécution de la section `code`, même après la première validation.
- Après l'exécution de la section `secrets` (dans `post`) :
    - `passing` existe mais vaut `None`.
    - `failing` a levé une erreur ce qui a empêché son affectation. Elle n'est donc pas définie dans l'environnement.
- Après l'exécution de `post`, `passing` est supprimée automatiquement de l'environnement.



### `@auto_run` vs `async` { #tests-auto_run-async }

À partir de la version `4.2.0` du thème, le décorateur `@auto_run` peut être utilisé sur des fonctions asynchrones. Il y a cependant quelques subtilités concernant l'ordre d'exécution des différents codes dont il faut alors être conscient.

<br>

Le fonctionnement du décorateur pour les fonctions asynchrones est le suivant :

1. La section en cours est exécutée normalement.
1. Les fonctions asynchrones décorées ne sont __PAS__ appelées directement mais sont stockées.
1. Une fois tout le code de la section exécuté, les fonctions asynchrones sont exécutées dans l'ordre où elles ont été stockées.

___Important :___

* La première erreur rencontrée stoppe les exécutions (que ce soit durant la phase d'exécution du code de la section elle-même ou durant l'exécution des fonctions asynchrones décorées).
* Les fonctions asynchrones sont toutes exécutées avant que d'éventuelles restrictions ne soient supprimées, donc toutes les restrictions s'appliquent à ces fonctions de la façon habituelle.

<br>

Concrètement, il faut donc se méfier des codes contenant des fonctions asynchrones décorées avec d'autres codes synchrones qui modifieraient des états globaux. En effet, les fonctions asynchrones décorées ne verraient que l'état final des différentes variables globales, comme le montre l'exemple ci-dessous :


{{ IDE_py('env_examples/async_auto_run_dec', ID=1, STD_KEY='ok', TERM_H=20, before='!!!', TEST=Case(
    description="Vérifie l'ordre des exécutions des fonctions async vs utilisation de @auto_run",
    subcases=[
        Case(
            code = 0,
            description = "Decorated async last, in order",
            all_in_std = [
                "Code sync... \\(VAL=0\\)\n"
                "Fonction async exécutée dans le flux normal... \\(VAL=1\\)\n"
                "Une fonction sync reste exécutée dans le flux normal \\(VAL=2\\)\n"
                "Code sync... \\(VAL=3\\)\n"
                "---\n"
                "Fonction ASYNC décorée exécutée à la fin... \\(VAL=4\\)\n"
                "...mais toujours avant que les tests ne soient finis ou interrompus.\n"
                "Ce message n'apparaît que si delayed2 n'a pas levé d'erreur.\n"
            ]
        ),
        Case(
            code = 1,
            description = "Decorated async last, stopped after error",
            in_error_msg = "AssertionError: Changer SUCCESS dans l'éditeur...",
            all_in_std = [
                "Code sync... \\(VAL=0\\)\n"
                "Fonction async exécutée dans le flux normal... \\(VAL=1\\)\n"
                "Une fonction sync reste exécutée dans le flux normal \\(VAL=2\\)\n"
                "Code sync... \\(VAL=3\\)\n"
                "---\n"
                "Fonction ASYNC décorée exécutée à la fin... \\(VAL=4\\)\n"
                "...mais toujours avant que les tests ne soient finis ou interrompus.\n"
            ],
            none_in_std=["Ce message n'apparaît que si delayed2 n'a pas levé d'erreur."]
        ),
    ])
)}}

<br>

À la lecture du code, on pourrait imaginer que `VAL` prenne la valeur `#!py 0` dans le message de la fonction `delayed`, mais c'est en fait la valeur `#!py VAL=4` qui est affichée.









## Feedback : `terminal_message` { #tests-terminal_message }


Si l'option {{ config_link("ides.deactivate_stdout_for_secrets") }} est activée, la fonction `#!py print` est désactivée durant les tests de validation.
<br>Si cela permet de sécuriser un peu le contenu de la section `secrets`, cela présente un énorme désavantage : il devient impossible de donner un quelconque feedback à l'utilisateur, ou de mettre en évidence un découpage des tests en différentes parties, s'ils sont nombreux.

À partir de la version `2.5.0`, le thème met à disposition dans l'environnement pyodide une fonction `terminal_message`, qui permet d'afficher du contenu directement dans le terminal, potentiellement formaté, et donc sans passer par la sortie standard de python ni être impacté par la désactivation de la sortie standard.




### Signature { #tests-terminal_message-signature }

En dehors du premier argument, `@key`, la fonction `terminal_message` est très proche de la fonction `print` d'origine.

À partir de la version `4.2.3`, la signature est la suivante :

```python
def terminal_message(key, *msg:Any, format:str=None, sep=' ', end=None, new_line=True):
    """
    Display the given message directly into the terminal, without using the python stdout.
    This allows to give informations to the user even if the stdout is deactivated during
    a validation.

    @key:      Value to pass to allow the use of the function when the stdout is deactivated.
    @*msg:     Any number of elements to display in the terminal.
    @format:   One of the predefined formatting options for the terminal:
               "error", "warning", "info", "italic", "stress", "success", "none" (default)
    @sep=' ':  Equivalent to `print(..., sep=...)`
    @end=None: Close to `print(..., end=...)`. If not used, the @new_line argument will apply.
               Otherwise, @new_line will be False, and @end will control the end of the
               displayed message.
    @new_line: _Legacy behavior_: this argument is now useless and can be replaced with the
               use of `end`. If False, no new line character is added after the @msg content.
    @returns:  None
    """
```


??? help "Signature pour les versions antérieures de PMT"

    ```python
    def terminal_message(key, msg:str, format:str="none", new_line=True):
        """
        Affiche @msg directement dans le terminal de l'IDE en cours, sans passer par la sortie
        standard de python/pyodide.
        Le message est formaté avec le réglage désigné par `@format`.

        @key:      Valeur autorisant l'utilisation de la fonction, lorsque la sortie standard est
                   désactivée. La valeur attendue est définie par l'argument `STD_KEY` de la macro IDE.
        @msg:      Message à afficher. Potentiellement multilignes.
        @format:   Nom du formatage à appliqué au message.
        @new_line: Si False, ne rajoute pas `\n` à la fin de @msg.
        @throws:  `ValueError` si la sortie standard est désactivée et @key n'est pas la valeur attendue
        @returns:  None
        """
    ```


### Contrats & comportements

1. ___L'argument `@key`___

    Cet argument, bien qu'ennuyeux à l'usage, est nécessaire pour empêcher un utilisateur de se servir de la fonction `terminal_message` en lieu et place de `#!py print` lorsque la sortie standard est désactivée.

    Il n'est en fait utilisé que lors des validations, et à condition que la sortie standard soit désactivée (option {{ config_validator("ides.deactivate_stdout_for_secrets", val="true") }}). Dans les autres cas, il est possible d'utiliser n'importe quelle valeur, et la fonction `terminal_message` se comporte alors plus ou moins comme `print`.

    <br>

1. ___Définition de la valeur cible pour `@key`___

    C'est [l'argument `STD_KEY`](--IDE-STD_KEY) des macros `IDE` qui permet de choisir quelle sera la valeur autorisant l'utilisation de la fonction `terminal_message`. Comme toutes les valeurs passées via les macros, cet argument peut également être défini via les [fichiers de métadonnées ou les entêtes de pages markdown](--custom/metadata/).

    <br>

1. ___Validation de l'argument `{% raw %}{{ IDE(..., STD_KEY=...)}}{% endraw %}`___

    --8<-- "docs_tools/inclusions/IDE_STD_KEY_validation.md"

    <br>

1. ___L'argument `@format`___

    {{ md_include("docs_tools/inclusions/terminals_formatting.md") }}











## Profiter de l'exécution `async` de la section `secrets` { #tests-secrets-async }

À partir de la version `4.2.0`, les sections `secrets` sont exécutées en mode `async`.


!!! tip "Intérêts d'utiliser des appels asynchrones dans la section `secrets`"

    Outre la possibilité de réaliser des appels asynchrones pour faire des requêtes ou autres, l'un des buts de passer cette section en exécution asynchrone est de pouvoir rendre visible à l'utilisateur toute modification de la page réalisée durant les exécutions.

    Avec un mode asynchrone, il est en effet possible de mettre l'environnement pyodide "en pause", de manière non bloquante via l'appel asynchrone `await js.sleep()`, ceci permettant à l'horloge du navigateur d'avancer et de mettre à jour le contenu de la page. L'environnement pyodide reprend ensuite le cours des exécutions normalement.

<br>

L'exécution asynchrone s'avère donc particulièrement intéressante pour pouvoir :

- Afficher des informations dans un terminal lorsque les tests sont longs, pour signaler à l'utilisateur où il en est (utilisation de [`terminal_message`](--tests-terminal_message)).

- Mettre à jour des graphiques ou des animations p5 dans la page au cours des tests.





### Informer l'utilisateur { #tests-async-secrets-feedback }

L'exemple ci-dessous simule une exécution lente des tests de validation, en montrant comment afficher des informations dans le terminal au fur et à mesure, avec `terminal_message`.


{{ IDE_py('env_examples/async_long_secrets', STD_KEY='ok', ID=1, TERM_H=12, before='!!!',
    TEST=Case(
        description = "Just check that the terminal content is what is expected (blocking)",
        all_in_std  = ["\nPremiers tests : ..........\nD'autres tests : ..........\n", "Tests secrets"],
    )
) }}



### Mettre à jour la page { #tests-async-secrets-p5 }


De la même façon, il est aussi possible de mettre à jour des éléments de la page autres que le terminal, alors même que la validation est toujours en cours.
<br>L'exemple ci-dessous remplace le tracé matplotlib d'une fonction choisie aléatoirement.

{{ IDE_py(
    'env_examples/async_update_plot', ID=1, before='!!!',
    TEST = Case(description="Just check it runs without errors"),
) }}

{{ figure('plot_fig') }}











## Tester l'utilisation de `print` { #tests-test-print }


Il est possible d'intercepter facilement le contenu de la sortie standard, pour tester l'utilisation de la fonction `print` par l'utilisateur.

Durant les exécutions, `sys.stdout` est en fait une instance de `io.String`, qui récupère tous les contenus affichés dans la console pour la [section](--ide-sections) en cours. Cet objet n'est cependant pas utilisé par le thème pour afficher les messages dans les terminaux, et un rédacteur peut donc l'utiliser à sa guise pour réaliser des tests.


```python title="Accéder au contenu de la sortie standard"
import sys

txt = sys.stdout.getvalue()
```

Le code ci-dessus renvoie l'intégralité de tout ce qui a été transmis à la sortie standard via des appels à la fonction `print`, depuis le début de la section en cours d'exécution.

Comme cet objet n'est en fait pas utilisé par le thème, il est possible de le modifier pour tester plus facilement les contenus transmis à la sortie standard :

```python title="Vider la sortie standard avant le prochain appel à print"
sys.stdout.truncate(0)
sys.stdout.seek(0)
```
<br>

Le contenu de cet objet est également mis à jour lors de l'utilisation de `print` alors que la sortie standard dans le terminal est désactivée, ce qui permet de réaliser des tests même lors des validations.

<br>

??? help "Exemple"

    {{ IDE_py('env_examples/stdout_capture', ID=1, TERM_H=12, after="!!!", TEST=Case(
        code = 1,
        all_in_std = ["abcd\n42\n---\n'abcd\\\\n42\\\\n'\nreset!\n---\n'reset!\\\\n'\n"],
    )) }}








## Tester la correction et les tests {{anchor_redirect(id="test-corr")}} { #tests-test-corr }

![Tester la correction](!!test_corr_png){: loading=lazy .w45 align=right }

* {{orange("Durant le développement en local")}}, via `mkdocs serve`, {{green("__les IDEs disposant d'un contenu pour la")}} [section `corr`](--ide-sections)__ se voient automatiquement ajoutés un nouveau bouton qui permet d'exécuter une validation, mais avec le contenu de la correction en lieu et place du contenu de l'éditeur de code. Cela permet de :

    - Tester le code de la correction, dans le même contexte que le code de l'utilisateur.
    - Tester aussi le bon fonctionnement des tests publics et secrets, puisque les tests des sections `tests` et `secrets` sont tous deux exécutés lors des validations (voir les informations sur le [déroulement des exécutions](--runtime)).

    Ce bouton n'est jamais présent dans le site construit avec la commande `mkdocs build`.

<br>

{{ md_include("docs_tools/inclusions/pages-tests-disclaimer.md") }}

<br>
<br>








![Tester la correction](!!show_corr_REMs_png){: loading=lazy .w40 .margin-top-h2 align=right }

## Voir les contenus `corr` & `REM`s {{anchor_redirect(id="corr-REM-reveal")}} { #tests-corr-REM-reveal }

De manière similaire, et toujours {{orange("durant le développement en local")}}, via `mkdocs serve`, {{green("__les IDEs disposant d'un contenu `corr`{.green} ou de fichiers `{exo}_REM.md`{.green} ou `{exo}_VIS_REM.md`{.green}__")}} se voient automatiquement ajoutés un autre bouton permettant de révéler le contenu caché sans exécuter les tests.

Ce bouton n'est jamais présent dans le site construit avec la commande `mkdocs build`.
