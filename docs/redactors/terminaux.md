

# Les terminaux {: #macro-terminal }

Les terminaux sont en gros des "IDEs sans éditeur".

Le but premier des terminaux isolés n'est pas forcément d'utiliser toute la logistique qui va avec les IDEs (notamment en ce qui concerne les codes python), mais il est tout de même possible de leur appliquer les mêmes réglages, à quelques détails près.

La macro `terminal` fonctionne donc dans les grandes lignes de la même manière que la macro [`IDE`](--ide-macro).

<br>

??? help "Bugs d'affichage connus pour les terminaux (isolés ou avec IDEs)"

    !!! tip inline end w35 margin-top-h4 "Spécifique"
        - Plateformes : Windows 11 + Chrome
        - Après avoir déplié une admonition (`??? tip ...`)

    * Si le terminal est créé dans une admonition repliée, il peut arriver qu'après avoir déplié celle-ci, le terminal affiche le feedback en mettant ___un seul caractère par ligne___.

        Il n'y a pas de solution connue à ce bug (interne au terminal), mais l'utilisateur peut contourner le problème en redimensionnant la fenêtre, d'une façon ou d'une autre :

        - Changer le mode de la fenêtre (agrandie / fenêtrée / plein écran).
        - Ouvrir puis fermer la console du navigateur ( <kbd>F12</kbd> ).

    ---

    !!! tip inline end w35 no-margin-top "Multi-plateformes"
        - Plateformes : toutes
        - Après avoir déplié une admonition (`??? tip ...`)
        - Après avoir changé d'onglet, dans des panneaux coulissants (`=== "tabbed"`)

    * Il peut également arriver que le terminal semble rester vide (sans même les chevrons à gauche de la ligne de saisie).
    <br>Ceci est un bug provenant d'interactions malheureuses entre des codes de différentes sources (`material`, `pymdown-extensions` et `jQuery.terminal`) et ne peut donc pas être corrigé. Une correction du symptôme a par contre été mise en place, et il suffit de cliquer sur la zone du terminal ou de lancer les exécutions pour que le contenu du terminal apparaisse.






## Performances d'affichage

Les terminaux jQuery deviennent ___extrêmement lents___ si beaucoup de contenu y est affiché. Cela devient particulièrement visible lorsque la taille de la fenêtre du navigateur est modifiée.

![terminal truncated stdout](!!truncated_stdout_png){: loading=lazy .w35 align=right }

Pour cette raison, le [bouton dans le coin supérieur droit](--user-terminal) de chaque terminal permet de régler si la sortie standard et les messages d'erreurs sont automatiquement tronqués ou non. Il est vivement conseillé de garder ce réglage activé.

Il est également possible de configurer ce comportement via l'option {{ config_link('terms.stdout_cut_off') }} du plugin :

```yaml
plugins:
    ...
    - pyodide_macros:
        terms:
            {{ config_validator("terms.cut_feedback",1)}}: true     # (valeur par défaut)
```

<br>

Un autre réglage, au niveau du plugin lui-même, permet par ailleurs de limiter le nombre de lignes restant affichées à tout instant dans les terminaux.
<br>C'est l'option {{ config_link('terms.stdout_cut_off') }} :

```yaml
plugins:
    ...
    - pyodide_macros:
        terms:
            {{ config_validator("terms.stdout_cut_off",1)}}: 200     # (valeur par défaut)
```

Par défaut, 200 lignes au plus sont affichées, ce qui devrait suffire pour éviter les cas les plus courants de chutes de performances.

Ne cependant pas oublier qu'un plaisantin entrant quelque chose comme `"a" * 1_000_000` dans le terminal se retrouvera avec une unique ligne mais une page de navigateur inutilisable car le terminal contiendra trop de caractères...
<br>L'option pour tronquer la sortie standard, {{ config_link('terms.cut_feedback') }}, évite ce type de problèmes.






## Signature

```
{% raw %}{{ terminal() }}{% endraw %}
```
<br>
{{ terminal() }}

<br>


{{ macro_signature('terminal') }}

<br>

{{ macro_args_table('terminal', with_headers=True, with_text=True) }}


!!! help "Informations sur les fichiers pythons associés"

    Voir la [page des IDEs](--redactors/IDE-details/) concernant la structure des fichiers associés à l'argument `py_name`.


!!! help "Remarques sur l'argument `FILL`"

    * Il n'est utilisable {{orange("que pour des terminaux isolés")}}.

    * Il est possible d'utiliser des commandes multilignes, mais elle seront affichées bizarrement dans la console car elles ne présenteront pas le "prompt" en début de ligne (`...`) et apparaîtront donc "mal indentées".

    * La commande de l'argument `FILL` est insérée dans l'historique du terminal et peut donc y être retrouvée si besoin.


??? note "L'ancien argument `SIZE`"

    Une version antérieure du thème proposait un argument `SIZE`, qui est maintenant remplacé par `TERM_H`.<br>
    <br>Il était toujours utilisable jusqu'à la version 3.0 du thème. À partir de cette version, l'argument lève une erreur s'il est utilisé, mais il est possible de transformer cette erreur en warning en modifiant l'option de configuration du plugin `pyodide_macros`: {{ config_link("build.deprecation_level", val="warn") }}.







## Fichiers python & exécutions {{anchor_redirect(id='fichier-python-executions')}} { #terms-runtime-details }

{{div_svg("docs/assets/term-runtime.svg", kls="w30 inline end")}}

- Les sections `code`{.red}, `tests`{.red}, `secrets`{.red}, `corr`{.red}, ainsi que les fichiers `{exo}_REM.md`{.red} et `{exo}_VIS_REM.md`{.red} sont {{red("___interdits pour les terminaux isolés___")}} et une erreur est levée si l'une de ces données est trouvée.<br><br>

- Les sections `env`{.green}, `env_term`{.green}, `post_term`{.green} et `post`{.green} (ou `ignore`{.green}...) {{green('peuvent être utilisées')}}, avec un fonctionnement similaire à celui des IDEs.<br><br>

- Les exécutions globales des commandes entrées dans des terminaux se déroulent comme dans la figure ci-contre. Il est à noter que :

    * Les sections `env` et `post`, si elles existent, ne sont exécutées que si elles ne l'ont jamais été auparavant
    * La commande entrée dans le terminal est exécutée en mode [`async`](--async), ce qui permet d'utiliser `await ...` depuis un terminal.

<br>

??? danger "Particularité des commandes multilignes"

    Du fait de la façon dont le terminal jQuery est articulé avec la console python sous-jacente, les sections `env_term` et `post_term` doivent être exécutées à chaque fois que l'utilisateur appuie sur la touche entrée, afin de garantir que les restrictions de code, quand elles existent, seront _effectivement_ appliquées au moment de l'exécution de la commande dans le moteur python.

    Cela implique que les sections `env_term` ou `post_term` sont également exécutées _entre_ deux lignes successives d'une commande multiligne, que l'utilisateur n'a pas encore fini de taper...

    Et donc, il faut absolument {{orange("__éviter d'avoir des sections `env_term`{.orange} ou `post_term`{.orange} qui affichent des choses dans le terminal__")}}, car ces contenus apparaîtraient au beau milieu de la commande multilignes tapée par l'utilisateur, comme dans cet exemple :

    ```
    >>> """début commande...
    Print depuis env_term !!
    ... ...suite commande...
    Print depuis env_term !!
    ... fin de commande !"""
    Print depuis env_term !!
    'début commande...\n...suite commande...\nfin de commande !'
    >>>
    ```

    <br>

    La [variable globale `__USER_CMD__`](--user_code) présente une problématique similaire, pour les commandes multilignes.
    <br>Étant vue dans la section `env_term`, elle peut donc présenter du code incomplet tant que toute la commande n'est pas finalisée.

    À noter également que la commande n'est réellement exécutée que lorsque celle-ci est complète (ou erronée). Concrètement :

    | Dans le terminal {{width(17,center=1)}} | Executions {{width(10,center=1)}} | `#!py __USER_CMD__`, vue depuis `env_term` ou `post_term` |
    |:-:|:-:|:-|
    | `"""début commande...` | `env_term`<br>`post_term` | `#!py '"""début commande...'` |
    | `...suite commande...` | `env_term`<br>`post_term` | `#!py '"""début commande...\n...suite commande...'` |
    | `...fin de commande !"""` | `env_term`<br>`cmd dans pyodide`{.orange}<br>`post_term` | `#!py '"""début commande...\n...suite commande...\nfin de commande !"""'` |


## `__USER_CMD__`

À chaque lancement du terminal, une [variable globale `__USER_CMD__`](--user_code) (cachée, mais accessible) est mise à jour dans l'environnement avec le contenu de la commande en cours (potentiellement incomplète : voir l'encadré ci-dessus à propos des commandes multilignes).

* Cette variable, si utilisée, devrait l'être depuis la section `env_term`.
* Les problématiques autour de l'utilisation de `__USER_CMD__` pour imposer des restrictions sont exactement les mêmes que celles concernant la [variable globale `__USER_CODE__`](--user_code) pour les éditeurs des IDEs.




## Exemple

Voici un exemple permettant de voir comment les différentes sections se comportent lors des exécutions, et utilisant divers arguments des terminaux.

<br>

Le terminal plus bas étant créé avec l'appel de macro suivant :

```
{% raw %}{{ terminal('exo', SANS="sorted .count", REC_LIMIT=42, TERM_H=12, FILL="inf_rec()") }}{% endraw %}
```

Et le contenu du fichier `exo.py` est le suivant :

{{ py('env_examples/exo_term') }}

<br>

Dans le terminal ci-dessous :

* Essayer d'y utiliser `inf_rec()`, pour voir l'effet de la limite de récursion.
* Essayer d'y utiliser `sorted()`, ou `[].count(1)` pour voir l'effet des [restrictions](--restrictions).

{{ terminal('env_examples/exo_term', SANS="sorted .count", REC_LIMIT=42, TERM_H=12, FILL="inf_rec()") }}
