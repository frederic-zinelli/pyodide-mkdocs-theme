# QCMs & QCSs

![alt](!!qcm_example_png){: loading=lazy .w35 align=right }

<br><br>

Le thème propose une macro `multi_qcm`, qui permet de construire des groupes de questions :

- À choix simples ou multiples
- Avec évaluation automatique des résultats
- Avec ou sans affichage de la correction
- Avec ou sans mélange automatique des questions et/ou de leurs choix.

<br>

Les qcms sont "rejouables" à volonté et ont donc valeur d'évaluations formative.







## Structure et fonctionnement

{{ md_include('docs_tools/inclusions/qcms.md') }}

<br>

??? help "Code utilisé pour déclarer ce qcm"

    === "Via fichier `.json`"

        En utilisant [l'outil de création de fichiers JSON pour QCMs](--qcm-builder), le fichier markdown contient l'appel de macro suivant, avec le fichier `qcm_exemple.json` déposé dans le même dossier :

        ```markdown
        {% raw %}{{ multi_qcm('qcm_exemple.json') }}{% endraw %}
        ```

        Le contenu du fichier `json` est :

        ```json
        {
          "questions": [
            [
              "On a saisi le code suivant :\n```python title=\"\"\nn = 8\nwhile n > 1:\n    n = n//2\n```\n\nQue vaut `n` après l'exécution du code ?",
              [
                "0",
                "1",
                "2",
                "4"
              ],
              [2]
            ],
            [
              "Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?",
              [
                "La machine de l’utilisateur sur laquelle s’exécute le navigateur web.",
                "La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.",
                "La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.",
                "Le serveur web sur lequel est stockée la page HTML."
              ],
              [1],
              {"multi":true}
            ],
            [
              "Cocher toutes les bonnes réponses, avec :\n```python title=''\nmeubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']\n```",
              [
                "`#!py meubles[1]` vaut `#!py Table`",
                "`#!py meubles[1]` vaut `#!py Commode`",
                "`#!py meubles[4]` vaut `#!py Buffet`",
                "`#!py meubles[5]` vaut `#!py Buffet`"
              ],
              [2,3]
            ]
          ],
          "description": "_(Une description additionnelle peut être ajoutée au début de l'admonition...)_\n{ style=\"color:orange\" }",
          "multi": false,
          "shuffle": true,
          "qcm_title": "Un QCM avec mélange automatique des questions (bouton en bas pour recommencer)"
        }

        ```

    === "Via un appel de macro direct"

        !!! danger ""
            Il est vivement déconseillé de passer par l'appel de macro direct, qui pose de nombreuses difficultés à l'usage.
            <br>Les déclarations utilisant [l'outil de création de fichier json pour QCM](--qcm-builder) sont beaucoup plus simples d'utilisation.

        ```markdown
        --8<-- "docs_tools/inclusions/qcms_working_ex.md"
        ```






## {{anchor_redirect(id="la-macro-multi_qcm")}}La macro `multi_qcm` { #qcm-macro }

### Arguments

La signature complète de la macro est la suivante :

{{ macro_signature('multi_qcm') }}

<br>
Détails:

{{ macro_args_table('multi_qcm', with_headers=True, width1=13) }}





### Données pour une question { #qcm_question }

??? warning "Préférer l'outil de création de fichier `json` pour QCMs (voir section suivante)"

    _Il n'est pas utile de lire le contenu de cette section si l'outil de création de QCM est utilisé, si ce n'est pour avoir quelques informations supplémentaires concernant les données renseignées pour chaque question._

    <br>

    ---

    <br>

    Il est possible de définir autant d'arguments `*questions` que nécessaire, chaque question étant une liste python de 3 ou 4 éléments.

    Un exemple avec deux questions :

    ```markdown
    multi_qcm(
        ["Fait-il beau ?",          ['Oui', 'Non', "J'ai pas ouvert les volets"], [1], {"multi": False}],
        ["Fait-il vraiment beau ?", ['Oui', 'Non', "J'ai pas ouvert les volets"], [1,3]],
    )
    ```

    <br>Les différents éléments de chaque liste sont, respectivement :

    | Type {{width(11)}} | Rôle |
    |:-|:-|
    | `str` | Intitulé de la question. Cela peut être une simple phrase, un bloc de code, ou n'importe quoi de plus complexe, à condition d'utiliser une [chaîne multilignes compatible](--multiline_qcms). |
    | `List[str]` | Ensemble des choix possibles pour cette question.<br>Là aussi, une simple phrase, un bloc de code, ou une [chaîne multilignes compatible](--multiline_qcms). |
    | `List[int]` | Indique les numéros des items corrects. {{red("_Attention_ : ce ne sont pas des indices, donc démarrent à 1 !")}} |
    | `Dict[str,Any]` | `[Optionnel]` Si fourni, un dictionnaire qui contient des options spécifiques à cette question.<br>À l'heure actuelle, deux options sont utilisables: {{ ul_li([
        "`#!py 'multi': bool`, qui permet de [désambiguer les questions à choix multiples ou simples](--multi_or_single_qcm).",
        "`#!py 'shuffle': bool`, qui indique si les items de cette question doivent être mélangés automatiquement ou pas (équivalent de l'argument `shuffle_items`, mais pour les items de cette question uniquement).",
    ]) }} |



{{ qcm_builder_to_qcms() }}





### Fichiers `.json` { #qcms-json }

À partir de la version `2.4.0` du thème, il est possible d'utiliser des fichiers `.json` pour déclarer des QCMs.
<br>Ceci permet notamment de s'affranchir des pièges liés à la syntaxe Jinja. L'écriture du fichier json introduit évidemment d'autres contraintes (spécifiquement concernant les contenus multilignes, qui sont plus délicats à écrire), mais celles-ci peuvent être très facilement éliminées en mettant à profit [l'outil de création de fichiers json pour QCMs](--qcm-builder) du thème.

<br>

Lorsque des fichiers `.json` sont utilisés :

* Dans le fichier markdown, l'argument `*questions` de l'appel de macro doit être uniquement un chemin relatif vers le fichier `.json` cible, contenant à minima toutes les informations sur les questions.

* Les autres arguments nommés de la macro peuvent toujours être utilisés pour remplacer les valeurs correspondantes ou manquantes dans le fichier `.json`.

* Les validations de données décrites plus bas dans cette page s'appliquent toujours aux déclarations utilisant les fichiers `.json`, ceux-ci n'étant qu'une nouvelle alternative pour définir les arguments de la macro `multi_qcm`.

* En termes de précédence, les données provenant des fichiers `.json` s'intercalent entre les [données de configuration des macros](--custom/metadata/) et les arguments de l'appel de macro lui-même (sources, par priorité croissante) :

    <div markdown style="text-align:center">$mkdocs.yml \lt {{meta(0)}} \lt Entêtes~markdown \lt JSON \lt arguments~macro$</div>

<br>

Les points suivants ne s'appliquent que si le fichier `json` est créé à la main : ces contraintes seront toujours respectées si le fichier est créé via [l'outil de création de QCMs](--qcm-builder).

* Le fichier `.json` doit contenir un unique objet dont les propriétés doivent être des [noms d'arguments de la macro `multi_qcm`](--qcm-macro). Seule la propriété `#!json "questions"` est obligatoire, toutes les autres sont optionnelles.

* La valeur associée à la propriété `#!json "questions"` doit être un tableau contenant les données pour chaque question, sous la forme de l'équivalent JSON des [structures décrites plus haut](--qcm_question) (nota: les contenus multilignes doivent donc être rédigés en utilisant `\n` pour les retours à la ligne).

<br>


??? help "Exemple de QCM créé avec un fichier `json`"

    === "Données sources"

        En imaginant la hiérarchie de fichiers suivante :

        ```
        ...
        └── dossier
            ├── index.md
            └── qcms
                ├── qcm1.json
                ...
        ```

        <br>

        L'appel de macro utilisé dans le fichier `index.md` est :

            {% raw %}{{multi_qcm('qcms/qcm1.json', shuffle_questions=True)}}{% endraw %}

        <br>

        Avec le contenu suivant pour le fichier `qcm1.json` :

        ```json
        --8<-- "docs/redactors/env_examples/qcm1.json"
        ```

    === "Résultat"

        {{multi_qcm('env_examples/qcm1.json', shuffle_questions=True)}}





### Vérifications & comportements généraux


#### 1. Validité des données

`BuildError` est levée dans les cas suivants :

- Une question a des réponses correctes en doublons : `[1,1,4]`.
- Une question a des réponses correctes invalides : `[6]` pour seulement 3 choix, ou `[0,-1]` (rappel: les réponses sont données avec des numéros de choix, pas des indices).
- Une question n'a aucune réponse correcte renseignée et est par ailleurs à choix unique (`#!py multi=False`).

    !!! help "Questions à choix multiples sans réponses correctes"

        Formellement, une question à choix multiples pourrait n'avoir que des réponses fausses (même si cela implique que quelqu'un ne répondant pas à la question se verra crédité d'une réponse correcte).

        Par défaut, le thème n'accepte pas ce type de situation et lève une erreur. Il est cependant possible d'autoriser cette situation en passant l'option de configuration {{ config_link('qcms.forbid_no_correct_answers_with_multi') }} à `#!yaml false`.



#### 2. QCMs ou QCSs { #multi_or_single_qcm }

Le code considère automatiquement que toute question ayant deux réponses correctes ou plus est une question à choix multiples. Dans les autres cas, il faut préciser explicitement le type de comportement à utiliser.
<br>Ceci peut être fait de différentes façons.

Chacun des réglages qui suivent est prioritaire sur les suivants, ce qui permet une assez bonne flexibilité lors de la rédaction des questions :

1. Réglage pour une question en particulier.

    ```markdown
    {% raw %}{{{% endraw %} multi_qcm(
        ...,
        [
            "...",              # Intitulé de la question
            [...],              # Liste de choix possibles (List[str])
            [1],                # Réponse correcte (une seule => ambiguë)
            {"multi": True}     # <<< ICI ; seulement cette question aura plusieurs choix possibles
        ],
        ...
    ) {% raw %}}}{% endraw %}
    ```

    <br>

1. Réglage pour toutes les questions d'un QCM, avec l'argument de la macro.

    ```markdown
    {% raw %}{{{% endraw %} multi_qcm(
        [...],                  # Question 1
        [...],                  # Question 2
        ...,
        multi = True,           # <<< ICI ; Toutes les questions du qcm auront plusieurs choix possibles
    ) {% raw %}}}{% endraw %}
    ```

    <br>

1. Réglages globaux : fichiers {{meta()}}.

    ```yaml
    args:
        multi_qcm:
            {{ config_validator("args.multi_qcm.multi", 1) }}: true
    ```

    Voir la page concernant [la configuration et les métadonnées](--custom/metadata/).

    <br>

1. Réglages globaux : `mkdocs.yml` :

    ```yaml
    plugins:
        pyodide_macros:
            args:
                multi_qcm:
                    {{ config_validator("args.multi_qcm.multi", 1) }}: true  # ou false (défaut: false)
    ```















## {{anchor_redirect("pieges-a-eviter")}}Déclaration manuelles: les pièges { #qcms-pieges-a-eviter }

!!! danger ""
    Cette section ___ne concerne pas___ les QCMs créés en utilisant l'[outil de génération de fichiers JSON](--qcm-builder).

    Seules les déclarations de QCMs directement via l'appel de macro sont soumis à ces problèmes.

<br>

Lorsque les déclarations de QCMs sont faites directement dans l'appel de macro, cela revient à dire que :

* Du "code python" écrit dans un fichier markdown (en fait, un template Jinja) est interprété une première fois, ...
* ... avant d'être passé en arguments à la macro `multi_qcm`...
* ... qui génère alors du contenu markdown...
* ... qui sera ensuite converti en html par mkdocs.

De ce fait, il y a un certain nombre de pièges à éviter lorsqu'on utilise des chaînes de caractères, et plus encore des chaînes multilignes.


### 1. LaTex { data-toc-label="" }

{{orange("Les caractères d'échappement, `" + back_slash() + "`{.orange}, dans les opérateurs LaTex doivent être eux-mêmes échappés")}}, quand ils sont écrits dans les chaînes de caractères utilisées en arguments des macros.

Exemples :

| Appel de macro {{width(17)}}| Rendu {{width(6)}} | Remarques |
|-|-|-|
| `"... $a \times b$ ..."` | {{red("_Erroné!_")}} | ... car `\t` est interprété comme une tabulation. |
| `"... $a \\times b$ ..."` | $a \times b$ |  |
| `"... $a \neq b$ ..."` | {{red("_Erroné!_")}} | ... car `\n` est interprété comme une nouvelle ligne. |
| `"... $a \\neq b$ ..."` | $a \neq b$ |  |
| `"... ${{back_slash()}}infty$ ..."` | $\infty$ | Correct mais génère `DeprecationWarning`{.orange} |
| `"... $\\infty$ ..."` | $\infty$ | |
| `"... ${{back_slash()}}mathbb{N}$ ..."` | $\mathbb{N}$ | Correct mais génère `DeprecationWarning`{.orange} |
| `"... $\\mathbb{N}$ ..."` | $\mathbb{N}$ | |


!!! warning "r-strings & f-strings"
    Dans le contexte des appels de macros, {{ red("**les`r-strings`{.red} de même que les `f-strings`{.red} ne fonctionnent pas**") }} : `r"$a \times b$"` lèverait une erreur.



### 2. Chaînes multilignes { #multiline_qcms data-toc-label="" }

#### Le problème de la syntaxe

!!! danger "Chaînes multilignes dans les templates Jinja"

    Le code tapé dans les appels de macro suit en fait la syntaxe des templates Jinja, qui n'est pas tout à fait équivalente à de la syntaxe python. En l'occurrence :

    * Les syntaxes pour les chaînes multilignes n'existent pas, techniquement...
    * ... mais toutes les chaînes sont en fait multilignes par défaut, quand écrites dans un template !

    Vous pouvez donc utiliser les "triple quotes" de python, en tant que rappel visuel, mais il vous faudra systématiquement échapper les délimiteurs "simples" apparaissant dans la chaîne car l'interpréteur Jinja analyse en fait ces chaînes de la façon suivante :

    - `{% raw %}{{ """...""" }}{% endraw %}` est interprété comme `{% raw %}{{ "" + "..." + "" }}{% endraw %}`.
    - `{% raw %}{{ '''...''' }}{% endraw %}` est interprété comme `{% raw %}{{ '' + '...' + '' }}{% endraw %}`.

    <br>

    Ces quatre déclarations donnent donc des résultats identiques :

    === "`"""..."""`"

        ```
        {% raw %}
        {{ func("""
          Hoho...
          C'est une chaîne \"multilignes\" !
        """)}}
        {% endraw %}
        ```

    === "`"..."`"

        ```
        {% raw %}
        {{ func("
          Hoho...
          C'est une chaîne \"multilignes\" !
        ")}}
        {% endraw %}
        ```

    === "`'''...'''`"

        ```
        {% raw %}
        {{ func('''
          Hoho...
          C\'est une chaîne "multilignes" !
          ''')}}
        {% endraw %}
        ```

    === "`'...'`"

        ```
        {% raw %}
        {{ func('
          Hoho...
          C\'est une chaîne "multilignes" !
        ')}}
        {% endraw %}
        ```

<br>

#### Gestion des indentations

Concernant les niveaux d'indentation utilisés dans une `chaine` "multilignes", la macro leur applique successivement `chaine.strip`, puis `textwrap.dedent`, avant d'utiliser le contenu résultant.

Par conséquent, vous pouvez utiliser n'importe quel niveau de base, du moment que tout est cohérent pour une même chaîne et que vous n'allez pas sur la gauche du début de l'appel de macro `multi_qcm` :

<br>

=== "valide"

    ````markdown
    {% raw %}{{{% endraw %} multi_qcm(
        [
            """
            ceci...
            ...est valide.

            On peut aussi mettre des choses

            - plus \"complexes\"

                mais ce n'est pas une obligation

            ```python
            #ou des blocs de code
            def somme(...):
                ...
            ```
            """,
            ...
        ],
        ...
    ) {% raw %}}}{% endraw %}
    ````

=== "valide"

    ````markdown
    {% raw %}{{{% endraw %} multi_qcm(
        ["""

    Vous pouvez aussi supprimer complètement l'indentation, si c'est plus facile
    pour rédiger, mais il faut tout de même respecter l'indentation de base de
    l'appel de la macro, c'est-à-dire qu'il faut être aligné avec ou sur la droite
    du début de l'appel à {% raw %}`{{ multi_qcm(...`{% endraw %}.

    ```python
    # ça marchera encore...
    def somme(...):
        ...
    ```
    Notez que les espaces vides au début ou à la fin ne comptent pas : seules les
    indentations dans la chaîne importent.


    """,
            ...
        ],
        ...
    ) {% raw %}}}{% endraw %}
    ````

=== "invalide"

    ```markdown
    {% raw %}{{{% endraw %} multi_qcm(
        [
            """ Ceci générera par contre...

                ...un markdown invalide car l'indentation n'est pas cohérente
                avec la toute première ligne de la chaîne.
                """,
            ...
        ],
        ...
    ) {% raw %}}}{% endraw %}
    ```

=== "invalide"

    ````markdown
    !!! tip "L'indentation interne est à gauche de l'appel à `multi_qcm`"

        {% raw %}{{{% endraw %} multi_qcm(
            [ """
    Trop à gauche !
    ```python
    #ou des blocs de code
    def somme(...):
        ...
    ```
    """,
            ...
            ],
            ...
        ) {% raw %}}}{% endraw %}
    ````
