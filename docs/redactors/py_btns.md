

Les boutons sont en gros des "IDEs sans éditeur et sans terminal". Ou des terminaux sans terminal...

Ils permettent de lancer un code python arbitraire dans la page (en général, pour utiliser le résultat et modifier le contenu de la page depuis le code python).


## Exemples

{{ md_include("docs_tools/inclusions/py_btns_examples.md") }}



## Signature

{{ macro_signature('py_btn') }}


## Arguments

{{ macro_args_table('py_btn', with_headers=True, with_text=True) }}


- Voir la [page des IDEs](--redactors/IDE-details/) concernant la structure des fichiers associés à l'argument `py_name`.

- Seules les sections `env` et `ignore` de ces fichiers peuvent être utilisées avec la macro `py_btn`. Il est rappelé que la section `env` est exécutée en mode [`async`](--async), ce qui permet plus de souplesse.

- Si une autre section ou des fichiers `{exo}_REM.md` ou `{exo}_VIS_REM.md` sont trouvés, {{red("__une erreur est levée__")}}.


Un bouton ira en général de paire avec une fonctionnalité pour modifier quelque chose dans la page. <br>Cette modification peut être faite via le code python, en utilisant les objets proxy proposés par [Pyodide][Pyodide]{: target=_blank }, et qui permettent d'interagir avec la couche JS/html de la page. La macro {% raw %}`{{ figure() }}`{% endraw %} peut aussi avoir son utilité ici pour ajouter un élément à la page html qui devra être ensuite modifier par le code : voir par exemple les utilisations avec [`matplotlib`](--custom/matplotlib/) ou [`mermaid`](--custom/mermaid/).