# --- PYODIDE:env --- #
import js, p5

W,H = 200, 40
is_night = not js.isDark()  # Négation pour forcer la mise à jour initiale

def setup():
    p5.createCanvas(W,H)
    p5.textSize(18)
    p5.textAlign(p5.CENTER, p5.CENTER)

def draw():
    global is_night

    if is_night != js.isDark():
        is_night = not is_night
        bg, fg, state = (50,255,'NUIT') if is_night else (200,0,'jour')
        p5.background(bg)
        p5.fill(fg)
        p5.text(f"Mode { state } actif !", W/2, H/2)

p5.run(setup, draw, target="empty_palette")