# --- PYODIDE:code --- #
import p5

W,H,D = 200, 200, 50

def setup_with_bgd(color):
    def setup():
        p5.createCanvas(W,H)
        p5.background(color)
    return setup

def draw():
    """ Les positions sont liées au canevas en cours """
    x,y = p5.mouseX, p5.mouseY
    if -D <= x < W+D and -D <= y < H+D:
        p5.circle(x, y, D)

p5.run(setup_with_bgd(20), draw, target="div1")
p5.run(setup_with_bgd(90), draw, target="div2", stop_others=False)
""" `stop_others=False` pour laisser la 1ère animation tourner. """