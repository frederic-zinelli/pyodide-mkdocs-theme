# --- PYODIDE:code --- #
import p5

W = H = 200
A, B = W//3, W*2//3

def setup():
    p5.createCanvas(W,H)
    p5.background(50)
    p5.stroke(255)
    p5.line(A,0, A,H)
    p5.line(B,0, B,H)


def mouseDragged(e):
    if p5.mouseX < A:
        # tiers de gauche
        terminal_message(None, p5.mouseX, "dans le terminal !")

    elif p5.mouseX < B:
        # section centrale
        raise ValueError('Toujours dans la console')

    else:
        # tiers de droite
        print(p5.mouseX, "dans la console")


p5.run(setup, mouseDragged=mouseDragged, target="drag")
print(
    'Cliquer sur la figure et déplacer la souris... '
    '(F12 pour ouvrir la console)'
)
