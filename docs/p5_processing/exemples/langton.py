# --- PYODIDE:code --- #

import p5

NB_CASES = 25           # nombre de cases du quadrillage par ligne ou colonne
FRAMERATE = 15          # nombre d'images par seconde
NB_ETAPES_MAX = 1000    # Nombre maxmale d'images affichées
SIZE = 500              # Taille de la grille (pixels)

#------------------------------------------------------------------------------------

def grille_initiale(n):
    '''
    renvoie une grille carrée de taille n * n, ne contenant que des zéros
    '''
    grille = [[0] * n for i in range(n)]
    return grille


def dessiner_grille_vide(nb_cases, dim_case):
    '''
    dessine la grille initiale : grille de nb_cases lignes et nb_cases colonnes
    Chaque case est un carré de dimension dim_case.
    '''
    p5.strokeWeight(1)  # épaisseur du tracé
    p5.stroke(180)      # couleur de tracé (gris)
    p5.fill(255)        # couleur de remplissage (blanc)
    for i in range(nb_cases):
        for j in range(nb_cases):
            p5.square(i * dim_case, j * dim_case, dim_case)


def fourmi_initiale(n):
    '''
    renvoie le dictionnaire dont les clés sont 'ligne', 'colonne' et 'direction'
    pour une fourmi située au centre d'une grille de taille n * n, orientée vers
    le nord.
    '''
    return {'ligne':n // 2, 'colonne':n // 2, 'direction':3}


def diriger_fourmi(fourmi):
    '''
    fourmi est un tableau de 3 valeurs représentant la position (ligne, colonne)
    et la direction de la fourmi.
    diriger_fourmi modifie la direction de la fourmi en fonction de la couleur
    de la case sur laquelle elle se trouve.
    '''
    if  grille[fourmi['ligne']][fourmi['colonne']] == 0:    # couleur case courante ?
        fourmi['direction'] = (fourmi['direction'] + 1) % 4 # tourne à droite
    else:
        fourmi['direction'] = (fourmi['direction'] - 1) % 4 # tourne à gauche


def bouger_fourmi(fourmi):
    '''
    fourmi est un tableau de 3 valeurs représentant la position (ligne, colonne)
    et la direction de la fourmi.
    bouger_fourmi modifie la position de la fourmi en fonction de sa direction.
    '''
    if fourmi['direction'] == 0:                # direction vers l'est
        fourmi['colonne'] = fourmi['colonne']+1 # déplacement d'une colonne vers l'est
    elif fourmi['direction'] == 1:              # direction vers le sud
        fourmi['ligne'] = fourmi['ligne']+1     # déplacement d'une ligne vers le sud
    elif fourmi['direction'] == 2:              # direction vers l'ouest
        fourmi['colonne'] = fourmi['colonne']-1 # déplacement d'une colonne vers l'ouest
    else:                                       # direction vers le nord
        fourmi['ligne'] = fourmi['ligne']-1     # déplacement d'une ligne vers le nord


def tracer_case(ligne, colonne, couleur, dim_case):
    '''
    tracer_case(ligne, colonne, couleur, dim_case) trace à la position
    (ligne, colonne) une case de la couleur spécifiée, dans une grille
    dont les cases ont pour dimension dim_case.
    '''
    p5.stroke(180)
    p5.fill(couleur)
    p5.square(colonne * dim_case, ligne * dim_case, dim_case)


def inverser_couleur(ligne, colonne, dim_case):
    '''
    inverser_couleur(ligne, colonne, dim_case) modifie la couleur associée à la
    case repérée par ses indices de ligne et colonne dans la variable grille
    et appelle la fonction tracer_case pour afficher cette modification à l'écran
    '''
    if  grille[ligne][colonne] == 0: # test sur couleur de la case
        grille[ligne][colonne] = 1   # inversion de couleur
        couleur = 0                  # niveau de gris ou nom de la couleur
    else:
        grille[ligne][colonne] = 0   # inversion de couleur
        couleur = 255                # niveau de gris ou nom de la couleur
    tracer_case(ligne, colonne, couleur, dim_case)


def est_dans_grille(ligne, colonne, grille):
    '''
    est_dans_grille(ligne, colonne, grille) renvoie True si les indices de ligne
    et colonne correspondent à une case de grille et False sinon.
    '''
    if 0 <= ligne and ligne < len(grille) and 0 <= colonne and colonne < len(grille):
        return True
    else:
        p5.noLoop()
        return False


def tester_nb_etapes(nb_etapes_max):
    '''
    tester_nb_etapes(nb_etapes_max) interrompt le programme si le nombre
    d'étapes réalisées est supérieur à nb_etapes_max
    '''
    if p5.frameCount >= nb_etapes_max:
        p5.noLoop()


def dessiner_fourmi(dim_case):
    '''
    dessine une "fourmi" dans une case de dimension dim_case
    l'origine du repère est le centre de la case
    '''
    pas = dim_case // 8
    # les pattes et antennes
    p5.line(0, 2*pas, 0, -2*pas)
    p5.line(-2*pas, -2*pas, -pas // 3,  0)
    p5.line(-2*pas,  2*pas, -pas // 3,  0)
    p5.line( 2*pas, -2*pas,  pas // 3,  0)
    p5.line( 2*pas,  2*pas,  pas // 3,  0)
    p5.line(2 * pas, 0, 3 * pas, -pas)
    p5.line(2 * pas, 0, 3 * pas,  pas)
    # le corps
    p5.noStroke()
    p5.ellipse(0,  0, dim_case //3, pas)
    p5.ellipse(-2 * pas,  0, 2 * pas, dim_case // 6)
    p5.ellipse(2 * pas, 0, dim_case // 6, dim_case // 6 )


def tracer_fourmi(fourmi, dim_case):
    '''
    Trace une "fourmi" dans une case de dimension dim_case, à la position et dans
    la direction contenues dans le dictionnaire fourmi.
    fourmi est un dictionnaire dont les clés sont `ligne`, `colonne` et`direction`.
    '''
    p5.fill(220, 30, 30)
    p5.stroke('red')
    p5.push()
    p5.translate(
        fourmi['colonne'] * dim_case + dim_case // 2,
        fourmi['ligne']   * dim_case + dim_case // 2
    )
    p5.rotate(fourmi['direction'] * p5.PI/2)
    dessiner_fourmi(dim_case)
    p5.pop()


def afficher_nb_etapes(x, y):
    '''
    affiche le nombre frameCount à la position (x, y)
    '''
    p5.textSize(36)
    p5.fill(255)
    p5.noStroke()
    p5.rect(
        x,
        y - p5.textAscent(p5.frameCount),
        p5.textWidth(p5.frameCount),
        2 * p5.textAscent(p5.frameCount)
    )
    p5.fill(0)
    p5.text(p5.frameCount, x, y)

#------------------------------------------------------------------------------------

DIM_CASE = SIZE // NB_CASES    # dimension d'une case
grille = grille_initiale(NB_CASES)
fourmi = fourmi_initiale(NB_CASES)


def setup():
    p5.createCanvas(SIZE, SIZE)
    dessiner_grille_vide(NB_CASES, DIM_CASE)
    tracer_fourmi(fourmi, DIM_CASE)
    p5.frameRate(FRAMERATE)

def draw():
    diriger_fourmi(fourmi)
    inverser_couleur(fourmi['ligne'], fourmi['colonne'], DIM_CASE)
    bouger_fourmi(fourmi)
    if est_dans_grille(fourmi['ligne'], fourmi['colonne'], grille):
        tracer_fourmi(fourmi, DIM_CASE)
    tester_nb_etapes(NB_ETAPES_MAX)
    afficher_nb_etapes(DIM_CASE, NB_CASES * DIM_CASE)

#------------------------------------------------------------------------------------

p5.run(setup, draw, target='langton')
