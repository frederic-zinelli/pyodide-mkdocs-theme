# --- PYODIDE:code --- #
import p5

Point = tuple[float,float]


def milieu(a:Point, b:Point) -> Point :
    """Renvoie les coordonnées du milieu de [ab]"""
    return (a[0] + b[0]) / 2, (a[1] + b[1]) / 2

def distance(a:Point, b:Point) -> float :
    """ Calcule la longueur de [ab] """
    return ((a[0] - b[0])**2 + (a[1] - b[1])**2)**0.5

def bezier_rec(a:Point, b:Point, c:Point, profondeur:int, lst:list[float]):
    """Construit la liste des points INTERIEURS de la courbe
    de Bézier de points de contrôle a, b et c, en les ajoutant à la liste lst.
    """
    if profondeur > 0:
        d, e = milieu(a, b), milieu(b, c)
        f = milieu(d, e)
        bezier_rec(a, d, f, profondeur - 1, lst)
        lst.append(f)
        bezier_rec(f, e, c, profondeur - 1, lst)



class Bezier(p5.Sketch):

    LARGEUR = 600
    HAUTEUR = 350
    COULEUR_POINT = (0, 0, 0)
    BLANC = (255, 255, 255)
    ROUGE = (255, 0, 0)
    RAYON_POINT_BASE = 15
    RAYON_POINT = 5
    PROFONDEUR = 5

    def __init__(self):
        self.i_selection = None
        self.noms = 'ABC'
        self.points = [
            (self.LARGEUR // 5,     self.HAUTEUR * 4 // 5),
            (self.LARGEUR // 2,     self.HAUTEUR // 5),
            (self.LARGEUR * 4 // 5, self.HAUTEUR * 4 // 5),
        ]

    @p5.hook('setup')
    def initialisation(self):
        """ Initialise et configure le canevas """
        self.p5.createCanvas(self.LARGEUR, self.HAUTEUR)
        self.p5.fill(*self.COULEUR_POINT)
        self.dessin()
        self.p5.loop()

    def dessin(self):
        """ Dessine toute la figure (seulement quand nécessaire) """
        # Le fond
        self.p5.background(*self.BLANC)

        # Les points de base + les textes
        for nom, (x,y) in zip(self.noms, self.points):
            self.p5.circle(x, y, self.RAYON_POINT_BASE)
            self.p5.text(nom, x + 10, y)

        # les lignes de base
        for i in range(2):
            self.p5.line(*self.points[i], *self.points[i+1])

        # la courbe de bézier
        courbe = [self.points[0]]
        bezier_rec(*self.points, self.PROFONDEUR, courbe)
        courbe.append(self.points[-1])
        for x,y in courbe:
            self.p5.circle(x, y, self.RAYON_POINT)

    @p5.hook
    def mousePressed(self, _evt):
        """ Identifie si le clic est sur un des points de base """
        for i_point, pnt in enumerate(self.points):
            if distance(pnt, (self.p5.mouseX, self.p5.mouseY)) <= self.RAYON_POINT_BASE:
                self.i_selection = i_point
                break

    @p5.hook
    def mouseReleased(self, _evt):
        """ Relâche un point sélectionné """
        self.i_selection = None
        self.dessin()            # évite un éventuel retard d'1 frame de l'affichage

    @p5.hook
    def mouseDragged(self, _evt):
        """ Déplacement d'un point, si sélectionné """
        if self.i_selection is not None:
            self.points[self.i_selection] = (self.p5.mouseX, self.p5.mouseY)
            self.dessin()


Bezier().run('bezier', stop_others=False)
