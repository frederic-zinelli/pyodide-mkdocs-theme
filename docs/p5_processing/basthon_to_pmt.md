


# Conversion d'activités Basthon/Capytale { #basthon-p5 }

Le fonctionnement de p5 avec PMT est très proche de celui sur Basthon / Capytale, les différences étant essentiellement syntaxiques. Mais les codes des animations p5 sont très souvent particulièrement longs, ce qui les rend fastidieux à convertir manuellement.


## Utilisation du script

Le thème propose donc un script qui se charge de faire la conversion pour l'utilisateur.

<br>

!!! tip "Script pour convertir une activité p5 de Basthon vers le thème"

    Il repose sur les arguments suivants :

    * `-B, --basthonP5`, pour indiquer les chemins vers les fichiers à convertir.
    * `-i, --id` (par défaut, `figure1`), pour indiquer l'id html de la figure dans laquelle créer l'animation.
    * `--tail` (par défaut, `#!py "_pmt"`), qui permet de définir comment modifier le nom du fichier source pour obtenir le nom du fichier cible (systématiquement créé dans le même dossier que le fichier d'origine).

    <br>

    Par exemple :

    ```
    python -m pyodide_mkdocs_theme --basthonP5 docs/en_travaux/alien/exo1.py --id alien1
    ```

    Cet appel va convertir le fichier indiqué, en précisant qu'il faut créer la figure dans l'élément de la page d'id `#!py "alien1"`, et le fichier créé sera `docs/en_travaux/alien/exo1_pmt.py`.

    <br>

    ??? note "Astuce de workflow, avec l'argument `--tail`"

        Si les fichiers sources ont un suffixe commun, un entier négatif peut être passé à l'argument `--tail`, qui indique alors le nombre de caractères à supprimer à la fin du nom de fichier :

        <br>

        ```
        python -m pyodide_mkdocs_theme --tail -4 -B docs/en_travaux/alien/exo1_src.py docs/en_travaux/alien/exo2_src.py docs/en_travaux/alien/exo3_src.py
        ```

        Cette commande convertit les trois fichiers `exoN_src.py` en `exoN.py`.


    <br>

<br>

Le fonctionnement du script est le suivant :


## Suppositions

* Il n'y a qu'une cible/animation p5 par fichier (argument `--id`)
* Le fichier utilise les noms de fonctions habituels pour p5: `setup`, `draw` et `preload`, `start`, `stop`.
* La fonction `run(...)` est utilisée de manière standard. Si elle est utilisée plusieurs fois, elle utilise les mêmes arguments à chaque fois (voir point suivant).
* Aucune de ces fonctions n'est "aliasée" ni redéfinie avec un comportement différent par l'utilisateur.


## Transformations

1. Tous les noms identifiés de manière non ambiguë comme faisant partie de p5.js se voient convertis en `p5.nomChose`.
1. Les appels aux fonctions `start` et `stop` sont remplacés par `p5.loop` et `p5.noLoop`.
1. Les appels à la fonction `print` sont remplacés par des appels à [`terminal_message`](--tests-terminal_message), en adaptant l'appel pour passer `None` en premier argument (l'animation lèvera donc une erreur si elle est exécutée durant une validation alors que la sortie standard est désactivée. Voir l'aide à propos de la fonction [`terminal_message`](--tests-terminal_message)).
1. Les fonctions `setup`, `draw` et `preload` sont laissées comme telles.
1. Le ou les appels à la fonction `run(...)` sont automatiquement convertis en analysant le contenu du fichier pour savoir quelles fonctions doivent être passées en argument, et en ajoutant l'argument `target` pour le thème.
1. Tout import `#!py from p5 import *` est remplacé par `#!py import p5`.
1. Le script affiche alors des informations dans la console, indiquant notamment :

    * Le nom du fichier traité + le nom final.
    * Le nombre et les localisations des appels `p5.run(...)` dans le fichier final, pour pouvoir les vérifier/modifier si besoin.
    * Les noms considérés comme ambigus et leurs localisations dans le fichier final, que l'utilisateur devra gérer à la main.

<br>

La liste des noms considérés comme faisant partie de `p5.js` peut-être trouvée dans [ce fichier][pmt-p5-script], ainsi que la liste des noms considérés comme ambigus, plus bas dans le fichier (`DUNNO_WHAT_TO_DO`).
