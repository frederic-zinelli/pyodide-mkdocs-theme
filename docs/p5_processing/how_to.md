---
args:
    IDE:
        TERM_H: 2
        TEST: skip
---


# Utilisation de `p5.js` dans Pyodide-MkDocs-Theme

À partir de la version 3.0, le thème propose le support pour l'utilisation de [`p5.js`][p5js]{: target=_blank }, qui permet d'intégrer des animations interactives dans la page web.







## Vue d'ensemble { #p5-overview }


### `p5` dans PMT vs PyPI ? { #p5-PMT-PyPI }

La première chose à garder en tête est que {{red("le module `p5`{.red} utilisé dans `pyodide-mkdocs-theme`{.red} n'a ___strictement rien à voir___ avec la bibliothèque du même nom disponible [sur `PyPI`](https://pypi.org/project/p5/){: target=_blank }")}} (nota: par ailleurs, ce projet n'est plus maintenu depuis plusieurs années).

Le module utilisé ici interface donc directement les codes python avec la bibliothèque originale en javascript, [p5js][p5js]{: target=_blank }. C'est donc directement dans la [documentation originale][p5-reference]{: target=_blank } qu'il faut aller chercher d'éventuelles informations sur l'utilisation des différentes fonctionnalités.

Les différences et l'articulation entre le module de PMT et la bibliothèque JavaScript d'origine sont entre autres discutées plus bas, dans la section à propos de [la fonction `p5.run`](--p5-run).



### Idée générale { #p5-generalities }

Le principe de fonctionnement de `p5` sur un site construit avec PMT est donc le suivant :

1. Un conteneur html (une `<div>` avec un `id` unique) est inséré dans le fichier markdown source, grâce à la macro `{% raw %}{{ figure(...) }}{% endraw %}`. Cet élément accueillera le canevas de l'animation.
1. Une macro `IDE`, `terminal`, `py_btn` ou `run` utilisant un fichier python est également ajoutée au fichier markdown.
1. Le contenu du fichier python est ensuite écrit, en y important `p5` et en définissant les fonctions typiques de `p5.js`. À minima, la fonction `setup()`, mais on peut y définir [n'importe quelle fonction de la bibliothèque JavaScript][p5-reference]{: target=_blank } (`draw`, `preload`, `mouseDragged`, ...).
1. L'animation est ensuite créée en faisant un appel du type [`p5.run(setup, draw, target=div_id)`](--p5-run) dans le fichier python.



### {{anchor_redirect("exemple-simple")}} Exemple simple { #p5-simple-example }

Avec le code markdown suivant, on obtient le résultat ci-dessous (nota: le fichier python ne contient que le code visible dans l'IDE) :

{% raw %}
```markdown
{{ IDE('exo_1') }}

{{ figure("figure_p5", admo_title="Passer la souris au-dessus du canevas") }}
```
{% endraw %}

<br>

{{ IDE('exemples/exo_1', MAX_SIZE=36, TEST=Case(
    code=1, description="Just to make sure it runs... (draw raises an unrelated error on next test cleanup)")
) }}

{{ figure("figure_p5", admo_title="Passer la souris au-dessus du canevas") }}



### {{anchor_redirect("contrats")}} Contrats { #p5-contracts }


!!! danger "_Le module `p5` du thème doit être utilisé en tant qu'espace de noms !_"

    Afin de simplifier l'implantation et d'éviter certaines limitations de l'utilisation de p5 dans [Basthon][Basthon] (dont est inspiré le port pour le thème), il a été décidé d'imposer l'utilisation du module python `p5` du thème en tant qu'espace de noms, pour accéder aux fonctionnalités de `p5.js`.
    <br>Voir [dans une page suivante](--basthon-p5) pour convertir les codes d'activités p5 créées pour Basthon vers des versions utilisables dans PMT.

    <br>

    !!! danger inline end w45 no-margin-top "Ne marche pas !"

        ```python
        from p5 import *

        def setup():
            createCanvas(200,200)
            background(50)

        def draw():
            circle(mouseX, mouseY, 50)

        run(setup, draw, target="figure1")
        ```

    !!! tip  w45 "OK"

        ```python
        import p5

        def setup():
            p5.createCanvas(200,200)
            p5.background(50)

        def draw():
            p5.circle(p5.mouseX, p5.mouseY, 50)

        p5.run(setup, draw, target="figure1")
        ```

<br>

!!! danger "Erreur levée sur les imports invalides"

    {{red("Les codes contenant `from p5 import`{.red} lèvent systématiquement une erreur")}} lors de leur exécution, afin de garantir le bon fonctionnement de l'ensemble.











## Détails { #p5-details }



### {{anchor_redirect("figure-cible-par-defaut-id")}} Figure cible par défaut (`id`) { #p5-default-figure-id }

Comme les autres outils du thème, le module `p5` utilise la valeur de [l'argument par défaut `div_id`](--redactors/figures/) de la macro `figure` si l'argument `target` n'est pas renseigné dans l'appel à `#!py p5.run`. Ces deux appels sont donc équivalents :

```python
p5.run(setup, draw)
p5.run(setup, draw, target="figure1")
```

<br>

!!! tip "Rappel: configurer la valeur par défaut de l'argument {% raw %}`{{ figure(div_id) }}`{% endraw %}"

    Il est rappelé que la valeur par défaut pour l'argument `div_id` peut être configuré via le fichier mkdocs.yml ou encore les fichiers de méta-données ou les entêtes de pages markdown.

    [Voir la page dédiée](--custom/metadata/) pour plus d'informations.




###  {{anchor_redirect(id="boutons-startstop")}}  Boutons stop/start/step { #p5-buttons }

Il est possible d'ajouter automatiquement des boutons `stop`, `start` et `step` à côté du canevas.

* Le bouton `stop` permet d'interrompre le déroulement de l'animation.
* Le bouton `start` la redémarre depuis là où elle a été arrêtée.
* Le bouton `step` avance d'une image dans l'animation.

Ils sont insérés dans la page grâce à l'argument `p5_buttons` de [la macro {% raw %}`{{ figure(...) }}`{% endraw %}](--redactors/figures/).

Cet argument peut prendre les valeurs `#!py "left"`, `#!py "right"` , `#!py "top"` ou `#!py "bottom"`, qui conditionnent de quel côté du canevas les boutons seront ajoutés.

<br>

{% raw %}
```markdown
{{ figure(
    "langton", p5_buttons='left',
    admo_title = "La fourmi de Langton (code proposé par Nathalie Weibel)",
    inner_text = "Lancer le code de l'IDE ci-dessous...",
) }}

{{ IDE('langton', MAX_SIZE=45, TERM_H=2) }}
```
{% endraw %}

<br>

{{ figure(
    "langton", p5_buttons='left',
    admo_title="La fourmi de Langton (code proposé par Nathalie Weibel)",
    inner_text="Lancer le code de l'IDE ci-dessous...",
) }}

{{ IDE('exemples/langton', MAX_SIZE=45) }}




### {{anchor_redirect(id="plusieurs-animations")}} Plusieurs animations { #p5-multi-anim }

Il est possible de tracer différentes animations dans la même page, voire même depuis le même code python, simplement en ciblant une autre figure dans la page avec l'argument `target` de la fonction `p5.run`.

!!! danger "Attention aux états partagés"

    S'il est simple de faire cohabiter des rendus fixes ou des animations ne faisant que répondre aux actions de la souris sur le canevas, il y a cependant de très nombreux pièges à éviter, quand on cherche à faire cohabiter des animations plus complexes. Spécifiquement, lorsque l'on cherche à garder une trace de données entre des images successives de chaque animation.

    Les exemples ci-dessous sont extrêmement simplistes et [évitent tous les écueils potentiels](--p5_processing/troubles/).

<br>

Afin de limiter les problèmes potentiels évoqués ci-dessus, le comportement par défaut de la fonction [`p5.run`](--p5-run) est d'arrêter toutes les animations en cours avant d'en créer une nouvelle.

Si plusieurs animations actives en même temps dans la page sont souhaitées, il faut ajouter l'argument nommé `stop_others=False` à l'appel à `p5.run` :

<br>

{% raw %}
```markdown
{{ IDE('exo_2') }}

{{ figure('div1', admo_title="Passer la souris au-dessus du canevas (div1)") }}

{{ figure('div2', admo_title="Passer la souris au-dessus du canevas (div2)") }}
```
{% endraw %}

<br>

{{ IDE('exemples/exo_2') }}

<br>

{{ figure('div1', admo_title="Passer la souris au-dessus du canevas (div1)", admo_class="tip inline end w45") }}

{{ figure('div2', admo_title="Passer la souris au-dessus du canevas (div2)", admo_class="tip no-margin-top w45") }}





### `p5.run(...)` : spécifications { #p5-run }

La signature détaillée de la fonction `p5.run` est la suivante :

```python
run(
    setup:   Callable[[],None],
    draw:    Callable[[],None]=None,
    preload: Callable[[],None]=None,
    *,
    target:  str=None,
    stop_others: bool=True,
     **routines:Callable
) -> None
```

<br>

| Arguments {{width(11)}} | Explications |
|:-|:-|
| `setup` | La fonction setup ne prend pas d'arguments. Correspond à la fonction `p5` équivalente. |
| `draw`<br>`preload` | Ces fonctions sont optionnelles et ne prennent pas d'arguments. Correspondent aux fonctions `p5` équivalentes. |
| `target` | Argument nommé, qui désigne l'id html de la figure où l'animation doit être tracée.<br>Si non renseigné, c'est la valeur de l'argument par défaut pour la macro {% raw %}`{{ figure() }}`{% endraw %} qui est utilisée. |
| `stop_others` | Si laissé à `#!py True`, toutes les animations en cours seront stoppées avant de mettre en place la nouvelle animation (cela limite les problèmes liés à des animations concurrentes). |
| `**routines` | Il est possible d'ajouter tous types de routines p5 souhaitées (`mouseDragged`, ...), en les passant sous forme d'arguments nommés. |


!!! tip "Concernant les arguments nommés, `**routines`"

    * Les noms utilisés doivent correspondre aux noms des fonctions originales de `p5.js`, donc en `camelCase`.

    * Python n'étant pas Javascript, les fonctions originales qui peuvent être déclarées avec différents nombres d'arguments se verront toujours passées tous les arguments possibles, dans pyodide.

    * La documentation sur [les évènements disponibles dans `p5.js` est ici][p5-events]{: target=_blank }.

    Exemple:

    ```python
    def mouse_dragged():
        """
        Cette version lève:
            TypeError: mouseDragged() takes 0 positional arguments but 1 was given
        """

    def mouse_dragged(event):
        """ Celle-ci fonctionne """

    p5.run(..., mouseDragged=mouse_dragged)   # Argument en camelCase !
    ```





### Feedback durant les animations { #p5-feedback }

Les animations p5 se déroulent "en dehors" de la logistique normale d'exécution de PMT :

1. Un IDE, terminal, bouton, ... crée une animation p5.
1. Le code de cet "élément" se termine.
1. La couche JS prend le relais et met en place l'animation dans la page, puis démarre sa boucle d'évènements.

<br>

À ce stade, il faut être vigilant, surtout en phase de développement/debugging d'une animation car :

* Toutes les erreurs levées depuis les fonctions python seront levées dans la console du navigateur.
* Tous les messages affichés depuis python via un `print` seront affichés dans la console du navigateur.
* Il reste possible de rediriger les affichages vers le terminal ou l'IDE ayant créé l'animation en utilisant la fonction `terminal_message(None, ...)` au lieu de `print` (rappel : [nécessité de passer un 1er argument](--tests-terminal_message) quelconque).

    !!! warning "Ne jamais utiliser `terminal_message` depuis une macro `run` ou `py_btn`"

        Ces éléments n'ayant pas de zone de feedback dans la page, les flux sont alors redirigés vers `window.alert`. Ce qui rendrait la page complètement inutilisable...

??? help "Exemple: redirection (ou pas...) des erreurs et feedbacks"

    Ici, lorsque le bouton de gauche de la souris est maintenu enfoncé et que l'on la déplace au-dessus du canevas:

    * Dans le premier tiers, un message est transmis au terminal (via `terminal_message`)
    * Dans la partie centrale, une erreur est levée : celle-ci n'est ___PAS___ visible dans le terminal. Il faut ouvrir la console du navigateur ( ++f12++ ) pour voir le message.
    * Dans le tiers de droite, la fonction `print` est utilisée : le message n'est ___PAS___ visible dans le terminal. Il faut ouvrir la console du navigateur ( ++f12++ ) pour le voir.

    {{ IDE("exemples/draggable_feedback") }}

    {{ figure("drag", admo_title="Cliquer sur le canevas, maintenir et déplacer la souris horizontalement...") }}








{{ md_include_palette("p5") }}
