

# {{anchor_redirect("problemes-courants-autour-des-animations-p5")}} Problèmes courants autour des animations `p5` { #p5-troubles }

## {{anchor_redirect("les-rechargements-de-pages")}} Animations "mélangées" ? { #p5-reload }

Lorsque la page est rechargée alors qu'une animation est en train de se dérouler, il peut arriver que les évènements de l'animation en cours ne soient pas éliminés. Lorsque la nouvelle animation est lancée après chargement de la page, on peut alors avoir deux boucles d'évènements mettant à jour le même canevas en même temps. Ceci peut résulter en une animation qui semble "flasher", en alternant les images des deux boucles d'évènement à l'écran.

Ce problème se pose beaucoup plus fréquemment lors du développement en local, avec `mkdocs serve`, lorsque c'est le serveur qui déclenche la mise à jour de la page. Mais il peut être rencontré occasionnellement sur le site construit, en ligne.

!!! tip "Solution, en cas d'animations qui font n'importe quoi..."

    À supposer qu'il y ait d'autres onglets actuellement ouverts dans le navigateur :

    1. ++ctrl+w++ pour fermer l'onglet en cours du navigateur.
    1. ++ctrl+shift+t++ pour rouvrir le dernier onglet fermé.


## Problèmes avec les animations concurrentes ? { #p5-concurrent }

L'articulation de la bibliothèque `p5` Python du thème avec sa version originale en JavaScript nécessite une certaine "gymnastique interne". Si celle-ci reste invisible à l'utilisateur, elle peut amener à des problèmes pas toujours évident à cerner... Un des points critiques à ce sujet est notamment : dans quels cas avoir plusieurs animations dans la même page pose problème ou non ?

<br>

1. __États globaux, dans la partie python ?__

    Si plusieurs animations sont lancées dans la même page, il faut en premier lieu se rappeler que l'environnement python est le même pour chacune d'elles.

    Les fonctions `setup`, `draw`, ... redéfinies ne poseront pas de problème (grâce à la "gymnastique interne"...).

    Par contre, tout état global partagé par plusieurs animations va être écrasé par le dernier code exécuté (typiquement, des variables définies dans l'espace global). Dans ce cas, les animations précédentes utiliseront toutes:

    - Leurs propres fonctions d'évènements (`draw`, `mouseDragged`, ...),
    - ...mais avec l'état des données de la dernière animation lancée.

    Ce qui peut s'avérer pour le moins perturbant à l'usage.

    <br>

1. __Animations par actions de l'utilisateur ou automatiques ?__

    Les animations concurrentes qui mettent en jeu des interactions de l'utilisateur avec le canevas ne poseront normalement pas de problèmes (une fois que les éventuels états globaux partagés ont été éliminés).
    <br>En effet, les évènements en jeu étant liés aux canevas, la "gymnastique interne" de `p5` dans PMT utilisera systématiquement le bon objet.

    Par contre, les animations automatiques tournant en parallèle, c'est-à-dire évoluant sans que l'utilisateur n'intervienne, _peuvent_ poser des problèmes : l'animation de l'une pourrait à un moment se retrouver à utiliser l'objet de l'autre. Les résultats sont alors totalement imprévisibles.

<br>

!!! tip "La solution : `p5.Sketch`"

    Si vous êtes face à l'un de ces problèmes, la solution évidente sera d'utiliser [la classe `p5.Sketch`](--p5_processing/p5_sketch/) proposée par le thème.

    Cette classe permet en effet de s'affranchir totalement et facilement de tout état global pour une animation, au prix d'une modification de la syntaxe du code (on s'éloigne donc un peu plus de la syntaxe d'origine de `p5.js`).



## {{anchor_redirect("tester-des-ides-contenant-des-animations")}} Tester des IDEs contenant des animations ? { #p5-vs-tests }

Il est déconseillé de [tester](--redactors/IDE-tests-page/) les IDEs générant des animations p5. Formellement, les animations y fonctionnent, mais :

* Elles sont insérées en bas de la page car il n'y a pas d'élément [`figure`](--redactors/figures/) pour les accueillir.
* Si une boucle d'évènements est en cours, le déclenchement du test suivant provoquera une erreur (visible uniquement dans la console du navigateur) car l'environnement est alors nettoyé et la définition de `p5` en est supprimée alors que les évènements de l'animation (fonction `draw`) sont toujours en cours d'exécution.
<br>Cette erreur n'a aucun impact sur les tests eux-mêmes.

La solution la plus simple est donc d'utiliser l'argument `TEST='skip'` ou `TEST='human'` pour ces IDEs.







