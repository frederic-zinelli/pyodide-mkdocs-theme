

# Utilisations avancées : la classe `Sketch` { #p5-sketch-class }


L'utilisation de `p5.js` décrite jusqu'à présent repose sur un module python qui profite du mode de travail "par instances" de la bibliothèques JavaScript, pour simuler son mode de fonctionnement "global" du côté des utilisateurs dans pyodide (c'est-à-dire que les fonctions `setup`, `draw`, ... sont déclarées au niveau de base du module).

Comme discuté dans les pages précédentes, ce mode hybride couvre la plupart des cas. Mais il peut provoquer des problèmes lorsque l'on cherche à mettre plusieurs animations dans la même page, ou pour éviter de partager des états globaux.

Pour palier à ce type de problèmes, le thème offre une syntaxe alternative reposant intégralement sur de la programmation objet. Celle-ci met complètement à profit le mode "par instances" de p5, et permet ainsi de travailler avec des objets et une logique parfaitement encapsulés.
<br>Les codes résultants semblent plus logiques et moins "magiques", par certains côtés, mais ils s'éloignent donc sensiblement des syntaxes habituelles de `p5.js`.



## Idée générale { #sketch-generalities }

1. Créer une classe qui étend la classe `p5.Sketch`. {{pmt_note("La classe parente comporte toute la logistique pour mettre en place les liens entre la classe enfant et `p5.js`{.pmt_note}.")}}

1. Le constructeur de la classe peut être implanté à la convenance du rédacteur, pour initialiser chaque animations avec des paramètres différents, selon ses besoins.

1. Les fonctions essentielles à définir (`setup`, `draw`, gestionnaires d'évènements, ...) sont alors des méthodes de la classe qui doivent être décorées avec `@p5.hook`. {{pmt_note("Le décorateur permet de marquer ces méthodes pour que la classe parente puisse savoir sans ambiguïté quelle méthode est une fonction p5 et laquelle ne l'est pas.")}}

1. Les méthodes décorées avec `@p5.hook` doivent prendre l'argument self, suivi des mêmes arguments que les fonctions d'origine dans `p5`.

1. {{orange("___Les fonctions et variables provenant de `p5`{.orange} doivent maintenant être extraites/utilisées avec la syntaxe `self.p5.xxx`{.green} au lieu de `p5.xxx`{.red}___")}}. {{pmt_note("C'est ce qui permet de garantir l'encapsulation pour chaque animation.")}}

1. Créer une instance, puis appeler [sa méthode `run(...)`](--p5-sketch-run), en lui passant l'id html de la figure où placer l'animation via l'argument `target`.{{pmt_note("Il n'est plus nécessaire de passer les fonctions/méthodes en argument, puisque la classe parente connaît déjà les méthodes à utiliser grâce au [décorateur `@p5.hook`](--p5-sketch-hook)")}}





## Exemples { #sketch-examples }

### Animations concurrentes

Voici une mise en application concrète, avec deux animations de "gouttes tombantes", pouvant se dérouler en parallèle. L'encapsulation avec les instances permet d'éviter les interactions malheureuses entre les animations :

<br>

{{ IDE("exemples/class_version", MAX_SIZE=50, TEST='skip') }}

{{ figure('class2', admo_title='class2', admo_class="tip inline end w45") }}

{{ figure('class1', admo_title='class1', admo_class="tip no-margin-top w45") }}


### Animation complexe

Construction dynamique d'une courbe de Bézier, avec réactivité à la souris.{{pmt_note("(sur une proposition de Nicolas Revéret)")}}

<br>

{{ IDE("exemples/bezier", MAX_SIZE=50, TEST='skip') }}

{{ figure('bezier', admo_title='Courbe de Bézier récursive') }}






## Contrats { #sketch-contracts }

Quelques points d'attentions :


###  Constructeur  { #p5-sketch-init }

Il n'est pas nécessaire d'appeler la `super()` méthode, car la classe parente ne l'implante pas.

L'utilisateur peut donc utiliser la méthode `__init__` à sa convenance, pour initialiser proprement une animation.

<br>

!!! danger "`self.p5` n'est pas utilisable depuis le constructeur"

    La propriété `self.p5` est définie seulement après que l'appel à la méthode `run` ait été effectué.
    Cela implique qu'il n'est pas possible de l'utiliser depuis le constructeur ou dans une méthode appelée depuis ele constructeur, à moins d'appeler la méthode `run` auparavant, depuis le constructeur lui-même.



### Méthodes à ne pas écraser  { #p5-sketch-no-override }

La classe enfant ne doit surtout pas implanter les méthodes suivantes :

- `run`
- `start`
- `step`
- `stop`
- `remove`
- `__call__`


### La méthode `run` { #p5-sketch-run }

Sa signature est la suivante :

```python
sketch.run(
    target: Optional[str] = None,
    stop_others: bool = True,
    **_
) -> None
```

<br>

Elle prend deux arguments, tous deux optionnels :

| Argument {{width(10)}} | Défaut | Rôle |
|-|-|-|
| `target` | `#!py None` | Id html de la figure où tracer l'animation.<br>Comme pour [la fonction `p5.run`](--p5-run), si la valeur est `#!py None`, c'est la valeur par défaut de l'argument `div_id` de [la macro `{% raw %}{{figure()}}{% endraw %}`](--redactors/figures/) qui est utilisé. |
| `stop_others` | `#!py True` | Si laissé à `#!py True`, toutes les animations en cours seront stoppées avant de mettre en place celle pour le présent appel. |

Les arguments nommés restant (c'est-à-dire, `**_`) ne devraient jamais être utilisés.{{pmt_note("(C'est un détail d'implantation lié à la machinerie interne du module `p5`{.pmt_note} de PMT)")}}.

<br>

!!! danger "Les instances ne peuvent appeler `run` qu'une seule fois"

    Si un second appel est tenté sur une même instance, `#!py ValueError` est levée.

    Ceci permet de garantir que l'animation est toujours créée avec un état initial "propre", tout en gardant l'interface de création des objets (`__init__`) séparée de celle du lancement des animations (`run`). L'utilisateur n'a ainsi pas à se préoccuper d'appeler les `super` méthodes (...et ne risque donc pas d'oublier ces appels).



### Le décorateur `@p5.hook` { #p5-sketch-hook }

Il permet donc d'indiquer à la classe parente, `p5.Sketch`, quelles sont les méthodes qui correspondent à des fonctions à utiliser pour/via le module original de `p5.js`, comme par exemple les fonctions `setup`, `draw`, `preload`, les gestionnaires d'évènements comme `mousePressed`, ...

Les méthodes décorées devraient respecter les noms des fonctions équivalentes dans la bibliothèque [`p5.js`][p5-reference]{: target=_blank } d'origine, c'est-à-dire qu'ils devraient être écrits en `camelCase`.

Les personnes qui se sentiraient des poussées d'urticaire à cette idée ont la possibilité de donner un nom arbitraire à chaque méthode, et de passer un argument au décorateur `p5.hook`. Cet argument sera alors le nom de la fonction d'origine dans `p5.js`.

<br>

Concrètement, la déclaration suivante :

```python
class Animation(p5.Sketch):

    @p5.hook
    def setup(self):
        ...

    @p5.hook
    def mouseClicked(self, event):
        ...
```

<br>

Peut être transformée en... :

```python
class Animation(p5.Sketch):

    @p5.hook('setup')
    def preparation(self):
        ...

    @p5.hook('mouseClicked')
    def celui_qui_ecoute_a_l_oreille_des_souris(self, event):
        ...
```

<br>
