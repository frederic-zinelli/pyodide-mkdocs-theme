---
ides:
    deactivate_stdout_for_secrets: false
    forbid_secrets_without_corr_or_REMs: false
---


## Vérifie les conditions de mise à jour du nombre d'essais sur des erreurs à différents stades des validations

Vérifie également que les tests publics ne modifient pas les nombre d'essais


### `decrease_attempts_on_user_code_failure: "editor"`

{{ IDE_py(
    'exo', MAX=45, TERM_H=15, ID=1,
    TEST=Case(
        decrease_attempts_on_user_code_failure = 'editor',
        fail=1,
        set_max_and_hide = 5,
        delta_attempts = -1,
        subcases=[
            Case(run_play=1, delta_attempts=0, fail=False),
            Case(title="KeyError in env",        delta_attempts=0),
            Case(title="AssertionError in env"),
            Case(title="KeyError in corr"),
            Case(title="AssertionError in corr"),
            Case(title="KeyError in tests"),
            Case(title="AssertionError in tests"),
            Case(title="KeyError in secrets"),
            Case(title="AssertionError in secrets"),
            Case(title="KeyError in post",       delta_attempts=0),
            Case(title="AssertionError in post", delta_attempts=0),
    ])
) }}


### `decrease_attempts_on_user_code_failure: "public"`

{{ IDE_py(
    'exo', MAX=45, TERM_H=15, ID=2,
    TEST=Case(
        decrease_attempts_on_user_code_failure='public',
        set_max_and_hide = 5,
        fail=1,
        delta_attempts = 0,
        subcases=[
            Case(run_play=1, delta_attempts=0, fail=False),
            Case(title="KeyError in env"),
            Case(title="AssertionError in env"),
            Case(title="KeyError in corr"),
            Case(title="AssertionError in corr"),
            Case(title="KeyError in tests",         delta_attempts=-1),
            Case(title="AssertionError in tests",   delta_attempts=-1),
            Case(title="KeyError in secrets",       delta_attempts=-1),
            Case(title="AssertionError in secrets", delta_attempts=-1),
            Case(title="KeyError in post"),
            Case(title="AssertionError in post"),
    ])
) }}



### `decrease_attempts_on_user_code_failure: "secrets"`

{{ IDE_py(
    'exo', MAX=45, TERM_H=15, ID=3,
    TEST=Case(
        decrease_attempts_on_user_code_failure='secrets',
        set_max_and_hide = 5,
        fail=1,
        delta_attempts = 0,
        subcases=[
            Case(run_play=1, fail=False),
            Case(title="KeyError in env"),
            Case(title="AssertionError in env"),
            Case(title="KeyError in corr"),
            Case(title="AssertionError in corr"),
            Case(title="KeyError in tests"),
            Case(title="AssertionError in tests"),
            Case(title="KeyError in secrets",       delta_attempts = -1),
            Case(title="AssertionError in secrets", delta_attempts = -1),
            Case(title="KeyError in post"),
            Case(title="AssertionError in post"),
    ])
) }}




## Vérifie les comportements après la révélation des corr/REMs


{{ IDE_py(
    'fail_pass', MAX=5,
    TEST=Case(
        description="Pass then failures do not change the number of attempts",
        subcases=[
            Case.ERROR_NO_REVEAL.with_(code=1, no_clear=False),
            Case.REVEAL_SUCCESS,
            Case.ERROR_NO_REVEAL.with_(code=1, delta_attempts=0),
            Case.ERROR_NO_REVEAL.with_(code=1, delta_attempts=0),
            Case(
                description="subsequent success doesn't use reveal messages",
                delta_attempts=0,
                none_in_std=[
                    Lang('fail_head'),
                    Lang('success_head'),
                    Lang('success_head_extra'),
                    Lang('success_tail')
                ],
            )
    ])
) }}





## Quand validation sans rien à révéler, utilise constamment "bravo"...


{{ IDE_py(
    'no_corr',
    TEST=Case(
        description="When nothing to reveal, all successes should show 'bravo'",
        subcases=[
            Case.ERROR_NO_REVEAL.with_(
                no_clear=False, delta_attempts=0,
                assertions = '!corrRemsMask hasCheckBtn',
            ),
            Case.SUCCESS_NOTHING_TO_REVEAL,
            Case.ERROR_NO_REVEAL.with_(
                delta_attempts=0,
                assertions = '!corrRemsMask hasCheckBtn',
            ),
            Case.SUCCESS_NOTHING_TO_REVEAL,
    ])
) }}
