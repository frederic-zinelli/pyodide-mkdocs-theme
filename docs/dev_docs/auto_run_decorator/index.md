
## `auto_run` decorator

{{ IDE('auto_run_decorator', STD_KEY="nope", TEST=Case(
    all_in_std=[
        'start', 'end', 'A\nB\nC',
        'yup', 'can run secret async', 'end secrets', 'async secrets',
        'post1', 'postA'
    ]
)) }}


{{ IDE('async_not_run_on_errors', STD_KEY="nope", TEST=Case(
    fail=1, subcases=[
        Case(
            title = "Error in async step stop subsequent async executions",
            all_in_std=['sync1', 'sync2', 'AA', "AssertionError: async fail!", 'Ran post check'],
            none_in_std=['BB', "auto_run coros should be empty"],
        ),
        Case(
            title = "Error in sync step clears out coroutines",
            all_in_std=['sync1', "KeyError", 'Ran post check'],
            none_in_std=['sync2', 'AA', "AssertionError: async fail!",'BB', "auto_run coros should be empty"],
        ),
    ]
)) }}
