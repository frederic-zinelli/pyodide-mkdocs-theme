# --- PYODIDE:env --- #

print("start")
@auto_run
async def _(): print("A")

@auto_run
async def _(): print("B")

@auto_run
async def _(): print("C")
print("end")

n_coros = len(auto_run.coros)
assert n_coros == 3, f"Coroutines should be stored (found {n_coros=})."
assert '_' in auto_run.run, f"Functions name should be stored but found {auto_run.run}"

# --- PYODIDE:corr --- #

assert not auto_run.coros, "no coroutines should still be stored."
assert not auto_run.run, "no functions name should still be stored."

@auto_run
def check(): print('yup')

@auto_run
def ran(): pass
assert ran is None, f"ran should be None but was {ran!r}"

auto_run.clean()
assert not(set(globals()) & {'ran','check'}), "ran and check should have been removed from the user's scope"


n_coros = len(auto_run.coros)
assert not n_coros, f"No coroutines should be stored (found {n_coros=})."
assert not auto_run.run, f"Functions name should be empty but found {auto_run.run}"

# --- PYODIDE:secrets --- #

assert not auto_run.coros, "no coroutines should still be stored."
assert not auto_run.run, "no functions name should still be stored."

async def ok(): terminal_message('nope', 'can run secret async')
await ok()


@auto_run
async def _(): terminal_message('nope', "async secrets")
terminal_message('nope', "end secrets")

# --- PYODIDE:post --- #

@auto_run
async def _(): print("postA")
print("post1")

n_coros = len(auto_run.coros)
assert n_coros == 1, f"Coroutines should be stored (found {n_coros=})."
assert '_' in auto_run.run, f"Functions name should be stored but found {auto_run.run}"