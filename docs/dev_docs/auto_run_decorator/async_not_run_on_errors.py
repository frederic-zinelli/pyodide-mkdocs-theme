
# --- PYODIDE:corr --- #
from py_libs import *
auto_N(globals())


# --- PYODIDE:secrets --- #
key = 'nope'

@auto_run
async def ok(): terminal_message(key, 'AA')

@auto_run
async def ok(): assert False, "async fail!"

terminal_message(key, 'sync1')
if do_it(): pass

if do_it(): raise KeyError()

terminal_message(key, 'sync2')

@auto_run
async def ok(): terminal_message(key, 'BB')

# --- PYODIDE:post --- #
assert not len(auto_run.coros), "auto_run coros should be empty"
print('Ran post check')