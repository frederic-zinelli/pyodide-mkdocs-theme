# --- PYODIDE:env -- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code -- #

apply_Nth(
    lambda: str(2),
    lambda: any([]),
    lambda: isinstance(1, bool),
    lambda: type(2),
    lambda: print('ok!')
)
