# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code --- #

def f1(): import numpy
def f2(): import numpy as np
def f3(): from numpy import array
def f4(): from numpy.random import bit_generator
def f5(): __import__('numpy')

apply_Nth(f1, f2, f3, f4, f5)

not_defined('numpy', "numpy shouldn't be in scope even if installed")


# --- PYODIDE:corr --- #

import math
not_defined('numpy', "numpy shouldn't be in scope even if installed")
