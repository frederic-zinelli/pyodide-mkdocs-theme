# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code --- #

def f1(): import heapq
def f2(): import heapq as hq
def f3(): from heapq import heapify
def f4(): __import__('heapq')

apply_Nth(f1, f2, f3, f4)

not_defined('heapq', "heapq shouldn't be in scope even if installed")

# --- PYODIDE:corr --- #

import math
not_defined('heapq', "heapq shouldn't be in scope even if installed")
