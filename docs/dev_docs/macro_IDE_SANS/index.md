---
title: Macro IDE exclusions
ides:
    forbid_hidden_corr_and_REMs_without_secrets: false
    forbid_corr_and_REMs_with_infinite_attempts: false
    forbid_secrets_without_corr_or_REMs: false
args:
    IDE:
        EXPORT: true
---

# Vérifie que les différents arguments de la macro IDE sont utilisés correctement

!!! warning "Exclusions posant problèmes"
    * quand une erreur est levée, son formatage se fait encore dans l'environnement avec les restrictions
    => il ne faut pas utiliser un truc susceptible d'être interdit dans le code pyodide des exclusions !
    * `SANS='str type isinstance any'`

    {{ IDE_py(
        'str', SANS='str any isinstance type',
        TEST = Case(code=1, subcases=[
            Case(in_error_msg="FORBIDDEN: don't use str"),
            Case(in_error_msg="FORBIDDEN: don't use any"),
            Case(in_error_msg="FORBIDDEN: don't use isinstance"),
            Case(in_error_msg="FORBIDDEN: don't use type"),
            Case(
                title="Also works from the terminal",
                term_cmd="str(42)", in_error_msg="FORBIDDEN: don't use str",
            ),
        ])
    ) }}


=== "REC_LIMIT+SANS=funcs"

    !!! tip "Ce qui est testé"
        * `REC_LIMIT=50, SANS = "exec eval min max reversed"`
        * eval call fails
        * exec call not used => OK
        * pas de `white_list` => numpy import fails
        * les erreurs levée dans le code ne doivent pas poser de problèmes avec pyodide

    {{ IDE_py(
        'exo', TERM_H=17, REC_LIMIT=50, SANS="exec eval min max reversed",
        TEST = Case(code=1, subcases=[
            Case(description="Not running forbidden things shouldn't case troubles"),
            Case(description="Recursive call not reaching the recursion limit should pass"),
            Case(in_error_msg="RecursionError"),
            Case(in_error_msg="FORBIDDEN: don't use eval", description="`eval` is forbidden"),
            Case(in_error_msg="FORBIDDEN: don't use min", description="`min` is forbidden, even aliasing the function"),
            Case(title="From terminal", term_cmd="''"),
            Case(title="From terminal", term_cmd="rec(20)"),
            Case(title="From terminal", term_cmd="rec()",  in_error_msg="RecursionError"),
            Case(title="From terminal", term_cmd="todo()", in_error_msg="FORBIDDEN: don't use eval"),
            Case(title="From terminal", term_cmd="f()",    in_error_msg="FORBIDDEN: don't use min"),
        ])
    ) }}


=== "SANS=method/attr/ops"

    !!! tip "Ce qui est testé"
        * `SANS=".count ._private AST: ** -"`
        * Les interdiction de méthodes marchent, dunders/protected comprises

    {{ IDE_py(
        'method_ops', SANS=".count ._private AST: ** -",
        TEST = Case(description="Forbidden methods are spotted", code=True, subcases=[
            Case(in_error_msg="FORBIDDEN: don't use **, -, ._private, .count"),
            Case(in_error_msg="FORBIDDEN: don't use .count", term_cmd="[1,2,3].count(2)"),
        ])
    ) }}



=== "SANS=numpy"

    !!! tip "Ce qui est testé"
        * `SANS="numpy"`
        * Interdit un module non encore installé
        * toutes formes d'imports de numpy sont interdites
        * autres modules accessibles (maths)

    {{ IDE_py(
        'import_numpy', SANS="numpy", TERM_H=20, TEST=Case(
        code=1, in_error_msg="FORBIDDEN",
        description="Forbid all imports syntaxes and installed modules never in scope",
        subcases=[
            Case(description='import numpy'),
            Case(description='import numpy as np'),
            Case(description='from numpy import array'),
            Case(description='from numpy.random import bit_generator'),
            Case(description='__import__("numpy")'),
            Case(description='Still works normally', code=0, in_error_msg=""),

            Case(title="From terminal", term_cmd='import numpy'),
            Case(title="From terminal", term_cmd='import numpy as np'),
            Case(title="From terminal", term_cmd='from numpy import array'),
            Case(title="From terminal", term_cmd='from numpy.random import bit_generator'),
            Case(title="From terminal", term_cmd='__import__("numpy")'),
            Case(title="from terminal: still works", term_cmd='import math ; N=5', in_error_msg=""),
            Case(title="Ensure still not in scope", code=0, in_error_msg="")
        ])
    )}}


=== "SANS=kws"

    {{ IDE_py(
        'import_kws', SANS="AST: True False None or and else async_with def f_str while try raise_from", TEST=Case(
        code=1, in_error_msg="FORBIDDEN: don't use and, async with, def, else, f-string, False, None, or, raise from, True",
        subcases=[
            Case(title="Forbid keywords appropriately"),
            Case(title="from terminal", term_cmd='True or False',
                 in_error_msg="FORBIDDEN: don't use False, or, True",
            ),
        ])
    )}}


=== "Ast check vs valid"

    {{ IDE_py(
        'import_ast_validations', SANS=".count AST: True False None or and else async_with def f_str while try raise_from", TEST=Case(code=1, description="No ast exclusions applied on secrets or tests")
    )}}


=== "for loops"

    {{ IDE_py(
        'for_loops', SANS="AST: for_inline for_comp", TEST=Case(
            title="Check normal loops against comprehensions", subcases=[
            Case(code=1, in_error_msg="for inline", not_in_error_msg="for comp"),
            Case(code=0, in_error_msg="for comp", not_in_error_msg="for inline"),
        ])
    )}}


=== "SANS=heapq"

    !!! tip "Ce qui est testé"
        * `SANS="heapq"`
        * Interdit un module de la bibliothèque standard

    {{ IDE_py(
        'import_heap', SANS="heapq", TEST=Case(
        code=1, in_error_msg="FORBIDDEN",
        description="Forbid all imports syntaxes and installed modules never in scope",
        subcases=[
            Case(description='import heapq'),
            Case(description='import heapq as hq'),
            Case(description='from heapq import heapify'),
            Case(description='__import__("heapq")'),
            Case(description='Still works normally', code=0, in_error_msg=""),

            Case(title="from terminal", term_cmd='import heapq'),
            Case(title="from terminal", term_cmd='import heapq as hq'),
            Case(title="from terminal", term_cmd='from heapq import heapify'),
            Case(title="from terminal", term_cmd='__import__("heapq")'),
            Case(title="from terminal: still works", term_cmd='import math ; N=4', in_error_msg=""),
            Case(title="Ensure still not in scope", code=0, in_error_msg="")
        ])
    )}}

=== "SANS=libxyz"

    !!! tip "Ce qui est testé"
        * `SANS="libxyz"`
        * Interdit une bibliothèque custom

    {{ IDE_py(
        'import_py_libs', SANS="libxyz", TEST=Case(code=1, in_error_msg="FORBIDDEN")
    )}}


=== "WHITE='numpy'"

    !!! tip "Ce qui est testé"
        * `SANS="numpy"`
        * L'installation de numpy marche via WHITE
        * Installé une seule fois
        * malgré tout impossible d'importer explicitement dans `code`
        * mais module disponible (le dernier test doit planter)

    {{ IDE_py('import_numpy', ID=2, SANS="numpy", TERM_H=20, WHITE='numpy', TEST=Case(
        code=1, in_error_msg="FORBIDDEN",
        description="WHITE module doesn't change the forbidden imports, even if already in scope",
        subcases=[
            Case(description='import numpy'),
            Case(description='import numpy as np'),
            Case(description='from numpy import array'),
            Case(description='from numpy.random import bit_generator'),
            Case(description='__import__("numpy")'),
            Case(title='WHITE does import in scope', code=0, in_error_msg="AssertionError: 'numpy'"),

            Case(title="From terminal", term_cmd='import numpy'),
            Case(title="From terminal", term_cmd='import numpy as np'),
            Case(title="From terminal", term_cmd='from numpy import array'),
            Case(title="From terminal", term_cmd='from numpy.random import bit_generator'),
            Case(title="From terminal", term_cmd='__import__("numpy")'),
            Case(title="from terminal: still works", term_cmd='import math ; N=5', in_error_msg=""),
            Case(title='WHITE does import in scope', code=0, in_error_msg="AssertionError: 'numpy'"),
        ])
    )}}


=== "SANS='pydantic'"

    !!! tip "Ce qui est testé"
        * N'installe pas un module interdit
        * mais installe tout de même un autre module (bs4)

    {{ IDE_py('import_bs4_pydantic', SANS="pydantic", TEST=Case(
        code=1, description="Does not install forbidden module, but still install others"
    ))}}



=== "SANS='pydantic', env as WHITE"

    !!! tip "Ce qui est testé"
        * N'installe pas un module interdit
        * mais installe tout de même un autre module (bs4)

    {{ IDE_py('import_env_pydantic', SANS="pydantic", TEST=Case(
        code=1, description="Import of forbidden stuff in env provides fine tuning",
        subcases=[
            Case(description="Can import from env without leaking"),
            Case(description="Still not importable, even if the code doigng the import is in env",
                 in_error_msg="FORBIDDEN: don't use pydantic"),
            Case(description="Still not in scope"),
            Case(description="Module actually imported in env."),
        ]
    ))}}



### This title should be visible