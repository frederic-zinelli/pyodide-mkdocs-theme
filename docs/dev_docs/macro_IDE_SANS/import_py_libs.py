# --- PYODIDE:code --- #

import os
from py_libs import *

auto_N(globals())

def check_no_file():
    assert os.listdir()==['py_libs'], "No file should have been created (aside of py_libs.py)"

apply_Nth(
    lambda: __import__('libxyz'),
    check_no_file,
)
