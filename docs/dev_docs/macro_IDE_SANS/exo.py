# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())


# --- PYODIDE:code --- #
def todo():
    return eval('32')       # definition is harmless

def rec(limit=80, n=0):
    return rec(limit, n+1) if n<limit else 1

eval                    # No string false positive
a = lambda: eval        # Not using
a()
f = min                 # no aliasing troubles
exec


apply_Nth(
    "",                 # pass
    lambda: rec(20),    # pass
    rec,                # raise
    todo,               # raise
    f,                  # raise
)
