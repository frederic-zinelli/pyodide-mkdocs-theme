# --- PYODIDE:code --- #

from py_libs import *
auto_N(globals())


# --- PYODIDE:tests --- #
# Ast restrictions shouldn't apply on validations

[].count(41)

if 0 and (f'this' or 'that' and True):
    raise KeyError() from None

async def meh():
    async with duh() as e:
        pass

for _ in ():
    pass
else:
    None

a = None
b = False



# --- PYODIDE:secrets --- #
# Ast restrictions shouldn't apply on validations

[].count(41)

if 0 and (f'this' or 'that' and True):
    raise KeyError() from None

async def meh():
    async with duh() as e:
        pass

for _ in ():
    pass
else:
    None

a = None
b = False