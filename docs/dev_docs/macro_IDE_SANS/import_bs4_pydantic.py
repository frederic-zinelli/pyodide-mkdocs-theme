# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

# --- PYODIDE:code --- #
not_defined('pydantic')
not_defined('bs4')
import bs4
truthy(bs4)
not_importable('pydantic')
not_defined('pydantic')