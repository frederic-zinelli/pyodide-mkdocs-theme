# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

def import_in_env_func():
    import pydantic
    return lambda: pydantic

pydantic_getter = import_in_env_func()

# --- PYODIDE:code --- #
# pydantic imported from env (inside a function)
apply_Nth(
    lambda: not_defined('pydantic', 'imported, but not in scope'),
    import_in_env_func,
    lambda: not_defined('pydantic', "still not in scope..."),
    lambda: truthy(pydantic_getter()),                        # ...but available in env
)