---
build:
    python_libs: ['other_py_libs/libxyz']
---

## Imports of various python_libs

{{ IDE('py_libs', ID=2, TEST=Case(
    title        = "Tests revolving around py_libs imports",
    code         = 1,
    clear_libs   = ["py_libs"],
    in_error_msg = "ModuleNotFoundError",
)) }}


{{ IDE('libxyz_lib2', ID=2, TEST=Case(
    title      = "Lib initially in sub directory",
    code       = 1,
    clear_libs = ["libxyz", "libxyz.lib2"],
    all_in_std = ["meh..."]
)) }}


## Testing slightly wrong imports of py_libs from terminal

{{ IDE(TEST=Case(
    code  = 1,
    fail  = 1,
    title = "Test wrong py_libs imports",
    all_in_std = ["FORBIDDEN: Import of \\w+ is forbidden for security reasons.\\n+Did you mean\\? +import py_libs"],
    subcases = [
        Case(term_cmd="import pylibs"),
        Case(term_cmd="import pylib"),
        Case(term_cmd="import py_lib"),
        Case(term_cmd="import py_libs", all_in_std=[], in_error_msg="ModuleNotFoundError: No module named 'py_libs'"),
    ]
)) }}
