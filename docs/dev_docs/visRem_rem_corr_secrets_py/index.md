---
ide_py_admo: '!!!'
ides:
    forbid_hidden_corr_and_REMs_without_secrets: false
    forbid_secrets_without_corr_or_REMs: false
    forbid_corr_and_REMs_with_infinite_attempts: false
---

## _RESET THE LOCALE STORAGE FIRST_ { .red }


Tab names: `ABCDE` as `VIS_REM, REM, corr, secrets, py_file`




=== "00000"

    !!! tip "ce qu'on devrait voir"
        * un IDE vide
        * les tests publics marchent
        * pas de bouton `Valider`
        * pas de corr btn

    {{ IDE(MAX=1, TEST=Case(
        assertions  = "!has_check_btn !has_corr_btn !has_reveal_btn !has_counter",
        run_play    = True,
        none_in_std = [Lang('success_head'), Lang('success_tail')],
        all_in_std  = [Lang('success_msg_no_tests')],
    )) }}



=== "00001"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide
        * pas de bouton de validation
        * Pas de compteur d'essais, car rien à révéler
        * les tests publics marchent
        * pas de correction ni remarque
        * pas de corr btn

    {{ IDE_py('exo00001', TEST=Case(
        assertions   = "!has_check_btn !has_corr_btn !has_reveal_btn !has_counter",
        run_play     = True,
        code         = True,
        none_in_std  = [Lang('success_head'), Lang('success_tail')],
    )) }}



=== "00011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide
        * avec tous les boutons, y compris celui de validation
        * Pas de compteur d'essais, car rien à révéler
        * les tests publics marchent
        * la validation marche
        * pas de correction ni remarque
        * En renvoyant `True` lors des validations, le message `Bravo!` doit apparaître à chaque fois.
        * pas de corr btn
        * doit générer un message d'erreur dans la console lors du build avec la config par défaut du plugin (`ides.forbid_corr_and_REMs_with_infinite_attempts`)

    {{ IDE_py('exo00011', MAX=1, TEST=Case(subcases=[

        Case.ERROR_NO_REVEAL.with_(
            assertions     = "has_check_btn !has_corr_btn !has_reveal_btn !has_counter",
            delta_attempts = 0,
            no_clear       = False,
        ),
        Case.SUCCESS_NOTHING_TO_REVEAL,
        Case.SUCCESS_NOTHING_TO_REVEAL,

    ])) }}



=== "00101, MAX=1"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, pas de bouton check
        * les tests publics marchent
        * doit générer un message d'erreur dans la console lors du build, avec la config par défaut du plugin (`ides.forbid_hidden_corr_and_REMs_without_secrets`)
        * MAX=1 (arg), mais pas de compteur d'essais (car pas de secrets)
        * corr btn visible

    {{ IDE_py('exo00101', MAX=2, TEST=Case(
        no_clear   = False,
        assertions = "!has_check_btn has_corr_btn has_reveal_btn !has_counter",
    )) }}



=== "00111"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * correction visible, mais pas de remarque
        * MAX = 2
        * corr btn visible

    {{ IDE_py('exo00111', MAX=2, TEST=Case(subcases=[

        Case.ERROR_NO_REVEAL.with_(
            no_clear       = False,
            assertions     = "has_check_btn has_corr_btn has_reveal_btn has_counter",
        ),
        Case.REVEAL_FAIL.with_(
            set_max_and_hide = 1,
            none_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_rem'), ],
            all_in_std = [Lang('fail_head'), Lang('reveal_corr'), Lang('fail_tail')],
        ),
        Case.SUCCESS_AFTER_REVEAL.with_(delta_attempts=1),
        Case.REVEAL_SUCCESS.with_(
            set_max_and_hide = 2,
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr')],
            none_in_std = [Lang('fail_head'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}




=== "00111, MAX=$\infty$"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * doit générer un message d'erreur dans la console lors du build avec la config par défaut du plugin (`ides.forbid_corr_and_REMs_with_infinite_attempts`)
        * MAX = $\infty$
        * corr btn visible


    {{ IDE_py('exo00111', MAX="+", ID=1, TEST=Case(subcases=[

        Case.ERROR_NO_REVEAL.with_(
            no_clear       = False,
            assertions     = "has_check_btn has_corr_btn has_reveal_btn has_counter",
            delta_attempts = 0,
        ),
        Case.ERROR_NO_REVEAL.with_(delta_attempts = 0),
        Case.REVEAL_SUCCESS.with_(
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr')],
            none_in_std = [Lang('fail_head'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}



=== "01011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * la remarque visible, mais pas de correction
        * MAX = 1
        * pas de corr btn
        * On validation: `Dommage ! Les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo01011', MAX=1, TEST=Case(subcases=[

        Case.REVEAL_FAIL.with_(
            no_clear         = False,
            all_in_std       = [Lang('fail_head'), Lang('reveal_rem'), Lang('fail_tail',2)],
            none_in_std      = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr'), ],
        ),
        Case.SUCCESS_AFTER_REVEAL.with_(delta_attempts=0),
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),
        Case.REVEAL_SUCCESS.with_(
            set_max_and_hide = 1,
            none_in_std = [Lang('fail_head'), Lang('reveal_corr')],
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}



=== "01111"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * correction et remarque visibles
        * MAX = 1
        * corr btn visible
        * On validation: `Dommage ! Le corrigé et les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo01111', MAX=1, TEST=Case(subcases=[

        Case.REVEAL_FAIL.with_(
            no_clear         = False,
            all_in_std       = [Lang('fail_head'), Lang('reveal_corr'), Lang('reveal_rem'), Lang('fail_tail',2)],
            none_in_std      = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), ],
        ),
        Case.SUCCESS_AFTER_REVEAL.with_(delta_attempts=0),
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),
        Case.REVEAL_SUCCESS.with_(
            set_max_and_hide = 1,
            none_in_std = [Lang('fail_head')],
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}



=== "10011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * VIS_REM visibles
        * message dans le terminal tient compte de VIS_REM seulement
        * MAX = 1
        * pas de corr btn
        * On validation: `Dommage ! Les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo10011', MAX=1, TEST=Case(subcases=[

        Case.REVEAL_FAIL.with_(
            no_clear         = False,
            all_in_std       = [Lang('fail_head'), Lang('reveal_rem'), Lang('fail_tail',2)],
            none_in_std      = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr'), ],
        ),
        Case.SUCCESS_AFTER_REVEAL.with_(delta_attempts=0),
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),
        Case.REVEAL_SUCCESS.with_(
            set_max_and_hide = 1,
            none_in_std = [Lang('fail_head'), Lang('reveal_corr')],
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}



=== "10111"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * correction et VIS_REM visibles
        * message dans le terminal tient compte de VIS_REM
        * MAX = 1
        * corr btn
        * On validation: `Dommage !Le corrigé et les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo10111', MAX=1, TEST=Case(subcases=[

        Case.REVEAL_FAIL.with_(
            no_clear         = False,
            all_in_std       = [Lang('fail_head'), Lang('reveal_corr'), Lang('reveal_rem'), Lang('fail_tail',2)],
            none_in_std      = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), ],
        ),
        Case.SUCCESS_AFTER_REVEAL.with_(delta_attempts=0),
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),
        Case.REVEAL_SUCCESS.with_(
            set_max_and_hide = 1,
            none_in_std = [Lang('fail_head')],
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}


=== "11011"

    !!! tip "ce qu'on devrait voir"
        * IDE fonctionnel, non vide, avec tous les boutons et le compteur d'essais
        * les tests publics marchent
        * REM et VIS_REM visibles
        * messages dans le terminal et l'admonition corrects
        * MAX = 1
        * pas de corr btn
        * On validation: `Dommage ! Les commentaires sont maintenant disponibles.`

    {{ IDE_py('exo11011', MAX=1, TEST=Case(subcases=[

        Case.REVEAL_FAIL.with_(
            no_clear         = False,
            all_in_std       = [Lang('fail_head'), Lang('reveal_rem'), Lang('fail_tail',2)],
            none_in_std      = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_corr'), ],
        ),
        Case.SUCCESS_AFTER_REVEAL.with_(delta_attempts=0),
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),
        Case.REVEAL_SUCCESS.with_(
            set_max_and_hide = 1,
            none_in_std = [Lang('fail_head'), Lang('reveal_corr')],
            all_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('success_tail'), Lang('reveal_rem')],
        ),
        Case.SUCCESS_AFTER_REVEAL,
        Case.ERROR_NO_REVEAL.with_(delta_attempts=0),

    ]))}}



### This title should be visible
