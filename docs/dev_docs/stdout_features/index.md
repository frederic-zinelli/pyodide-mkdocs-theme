
# Testing `print` & `terminal_message` articulations


## Test `print`

{{ IDE('stdout_print', TEST=Case(
    description = "test behaviors from code editor",
    code = 1,
    all_in_std = [
        "axb\n"
        "a bxduh\n"
        "hein! {None: 423} 43 44 45 46\n"
    ]
))}}

{{ IDE('stdout_print', ID=1, TEST=Case(
    description = "test behaviors from terminal",
    code = 1,
    subcases = [
        Case(
            title="`sep` is usable",
            term_cmd="print('a','b', sep='x')",
            all_in_std=["axb"],
        ),
        Case(
            title="`end` is usable",
            term_cmd="print('a','b', end='x') ; print('duh')",
            all_in_std=["a bxduh\n"],
        ),
        Case(
            title="Work with non string arguments, in any number",
            term_cmd="print('hein!', {None:423}, 43, 44, 45, 46)",
            all_in_std=["hein! {None: 423} 43 44 45 46\n"],
        ),
    ]
))}}




## Test `terminal_message`


{{ IDE('stdout_terminal_message', TEST=Case(
    description = "Test all arguments",
    all_in_std  = ["axb\na bduh_box_hein! \\{None: 423\\} 43 44 45 46\n"],
    subcases = [
        Case(
            code = 1,
            title = "From code editor"
        ),
        Case(
            title = "from terminal",
            term_cmd = (
                "terminal_message(None, 'a','b', sep='x') ; "
                "terminal_message(None, 'a','b', new_line=False) ; "
                "terminal_message(None, 'duh', end='_box_', new_line=True) ; "
                "terminal_message(None, 'hein!', {None:423}, 43, 44, 45, 46)"
            ),
        ),
    ]
))}}



{{ IDE( TEST=Case(
    description = "Check additional behaviors",
    subcases = [
        Case(
            title = "Check failure message en `format`",
            term_cmd = "terminal_message(0, 45, format='kjhjkh')",
            in_error_msg = "ValueError: format='kjhjkh' is not a valid value for terminal_message",
        ),
        Case(
            title = "Check automatic feedback truncation (`print`)",
            term_cmd = "print('e'*2000)",
            all_in_std = ["Message truncated"],
        ),
        Case(
            title = "Check automatic feedback truncation (`terminal_message`)",
            term_cmd = "terminal_message(0, 'e'*2000)",
            all_in_std = ["Message truncated"],
        ),
    ]
))}}
