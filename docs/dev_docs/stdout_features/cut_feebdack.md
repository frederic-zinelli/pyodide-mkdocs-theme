---
terms:
    cut_feedback: false
---


{{ IDE( TEST=Case(
    description = "Check additional behaviors",
    subcases = [
        Case(
            title = "Check failure message en `format`",
            term_cmd = "terminal_message(0, 45, format='kjhjkh')",
            in_error_msg = "ValueError: format='kjhjkh' is not a valid value for terminal_message",
        ),
        Case(
            title = "Check automatic feedback truncation (`print`)",
            term_cmd = "print('e'*2000)",
            none_in_std = ["Message truncated"],
        ),
        Case(
            title = "Check automatic feedback truncation (`terminal_message`)",
            term_cmd = "terminal_message(0, 'e'*2000)",
            none_in_std = ["Message truncated"],
        ),
    ]
))}}
