---
ides:
    forbid_secrets_without_corr_or_REMs: false
---


## BUT

Tester le comportement de l'extraction automatique des messages d'erreur, en vérifiant au passage que les override/merge de configs via les meta fonctionnent correctement.


{{ meta_tests_config(
    args_IDE_LOGS                           = True,
    args_IDE_MAX                            = 5,
    python_libs                             = ['py_libs','other_py_libs/libxyz'],
    deactivate_stdout_for_secrets           = True,
    decrease_attempts_on_user_code_failure  = "editor",
    show_only_assertion_errors_for_secrets  = False,
    testing_load_buttons                    = True,
    testing_empty_section_fallback          = "",
    editor_font_size                        = 15,
) }}



## Pour les tests secrets

!!! tip "AssertionError sur les tests de validation"
    * public: comportement normal quel que soit le réglage
    * secrets:
        - True: construit le message d'AssertionError automatiquement
        - False: pas de message

=== "LOGS=True (always)"
    {{ IDE_py('exo_fail', ID=1, MAX=15, LOGS=True, after="!!!", TEST=Case(subcases=[

        Case(run_play=1, in_error_msg="AssertionError: ce message est tjrs visible"),
        Case(run_play=1, in_error_msg="AssertionError:\nassert N!=2  # ce commentaire devrait tjrs être extrait"),
        Case(run_play=1, in_error_msg="assert [\n    \"tjrs visible aussi, extraction multilignes ok.\"\n] and N!=3"),
        Case(run_play=1, in_error_msg="assert (\n    [\n    \"tjrs visible\",\n    \"marche avec un formatage bizarre.\"\n] and N!=4\n    )"),

        Case(in_error_msg="AssertionError: ce message est tjrs visible"),
        Case(in_error_msg="AssertionError:\nassert N!=6  # ce commentaire devrait être extrait seulement si LOGS est True"),
        Case(in_error_msg="assert [\n    \"extraction multilignes ok. Visible si LOGS=True\"\n] and N!=7"),
        Case(in_error_msg="assert (\n    [\n    \"tjrs visible\",\n    \"marche avec un formatage bizarre.\"\n] and N!=8\n    )"),

    ])) }}

=== "LOGS=False (never)"
    {{ IDE_py('exo_fail', ID=2, MAX=15, LOGS=False, after="!!!", TEST=Case(subcases=[

        Case(run_play=1, in_error_msg="AssertionError: ce message est tjrs visible"),
        Case(run_play=1, in_error_msg="AssertionError:\nassert N!=2  # ce commentaire devrait tjrs être extrait"),
        Case(run_play=1, in_error_msg="assert [\n    \"tjrs visible aussi, extraction multilignes ok.\"\n] and N!=3"),
        Case(run_play=1, in_error_msg="assert (\n    [\n    \"tjrs visible\",\n    \"marche avec un formatage bizarre.\"\n] and N!=4\n    )"),

        Case(in_error_msg="AssertionError: ce message est tjrs visible"),
        Case(in_error_msg="AssertionError", not_in_error_msg="# ce commentaire devrait être extrait seulement si LOGS est True"),
        Case(in_error_msg="AssertionError", not_in_error_msg="extraction multilignes ok. Visible si LOGS=True"),
        Case(in_error_msg="AssertionError", not_in_error_msg="marche avec un formatage bizarre."),

    ])) }}

---

<br>

!!! tip "Effet de `deactivate_stdout_for_secrets`"

    `ides.deactivate_stdout_for_secrets = {{ Env().deactivate_stdout_for_secrets }}`

    `True`, donc, sur ++ctrl+enter++ :

    | section | message| visible |
    |-|-|-|
    | `code` | `#!py 'YEAH!'` | {{ qcm_svg("single correct") }} |
    | `tests` | `#!py 'public'` | {{ qcm_svg("single correct") }} |
    | `secrets` | `#!py 'secrets...'` | {{ qcm_svg("single incorrect") }} |

{{ IDE_py('exo_stdout', ID=1, before="!!!", admo_kls="tip inline end w45", TEST=Case(

    subcases=[
        Case(run_play=1, all_in_std=["YEAH!", "public"], none_in_std=["secrets!!!"]),
        Case(run_play=0, all_in_std=["YEAH!", "public"], none_in_std=["secrets!!!"]),
    ])
) }}



---

<br>

!!! tip "Effet de `decrease_attempts_on_user_code_failure`"

    `ides.decrease_attempts_on_user_code_failure = {{ Env().decrease_attempts_on_user_code_failure }}`

    `True`, donc, sur ++ctrl+enter++ :

    | message | visible |
    |-|-|
    | `#!py `M=1` | `decrease`{.red} |
    | `#!py `M=2` | `decrease`{.red} |
    | `#!py `M=3` | `decrease`{.red} |
    | `#!py `M=4` | `decrease`{.red} |
    | `#!py `M=5` | `success`{.green} |
    | `#!py `M>5` | `constant` |


{{ IDE_py('exo_decrease', ID=42, before="!!!", MAX=15, admo_kls="tip inline end w45", TEST=Case(
    description="Test the presence or absence of assertion messages, depending on the section running and the value of decrease_attempts_on_user_code_failure='editor'",
    subcases=[
        Case(delta_attempts=-1, in_error_msg="error in env"),
        Case(delta_attempts=-1, in_error_msg="error in code/corr"),
        Case(delta_attempts=-1, in_error_msg="error in tests"),
        Case(delta_attempts=-1, in_error_msg="error in secrets"),
        Case(delta_attempts= 0),
        Case(delta_attempts= 0, in_error_msg="success on 5, else error"),
        Case(delta_attempts= 0, run_play=1),
    ])
) }}



---

<br>

!!! tip "Effet de `show_only_assertion_errors_for_secrets`"

    `ides.show_only_assertion_errors_for_secrets = {{ Env().show_only_assertion_errors_for_secrets }}`

    `False`, donc :

    - `tests`:
        - `AssertionError: X = 1, message!`
        - `KeyError: 'X = 2, message!'`

    - `secrets`:
        - `AssertionError: X = 3, message!`
        - `KeyError: 'X = 4, message!'`


{{ IDE_py('exo_trace', before="!!!", admo_kls="tip inline end w45", TEST=Case(subcases=[

    Case(run_play=1, all_in_std=['Traceback'], in_error_msg="AssertionError: X = 1, message!"),
    Case(run_play=1, all_in_std=['Traceback'], in_error_msg="KeyError: 'X = 2, message!'"),
    Case( all_in_std=['Traceback'], in_error_msg="AssertionError: X = 3, message!"),
    Case( all_in_std=['Traceback'], in_error_msg="KeyError: 'X = 4, message!'"),

])) }}



### This title should be visible