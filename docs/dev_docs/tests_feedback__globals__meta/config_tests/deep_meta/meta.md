
# Using another `.meta.pmt.yml` (reversing previous meta)

{{ meta_tests_config(
    args_IDE_LOGS                           = False,
    args_IDE_MAX                            = 1000,
    python_libs                             = ['other_py_libs/libxyz'],
    deactivate_stdout_for_secrets           = True,
    decrease_attempts_on_user_code_failure  = "secrets",
    show_only_assertion_errors_for_secrets  = True,
    editor_font_size                        = 15,
) }}

<br>

Contenu de `docs/dev_docs/tests_feedback__globals__meta/config_tests/deep_meta/.meta.pmt.yml`:

```yaml
--8<-- "docs/dev_docs/tests_feedback__globals__meta/config_tests/deep_meta/.meta.pmt.yml"
```


Contenu de `docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml`:

```yaml
--8<-- "docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml"
```
