---
args:
  IDE:
    LOGS: false
    MAX: 45
ides:
  deactivate_stdout_for_secrets: true
  decrease_attempts_on_user_code_failure: "public"
  show_only_assertion_errors_for_secrets: false
  editor_font_size: 12    # hardcoded in dumps tests
testing:
    load_buttons: False
    empty_section_fallback: human
---

# Using headers (reversing meta file)

{{ meta_tests_config(
    args_IDE_LOGS                           = False,
    args_IDE_MAX                            = 45,
    python_libs                             = ['py_libs'],
    deactivate_stdout_for_secrets           = True,
    decrease_attempts_on_user_code_failure  = "public",
    show_only_assertion_errors_for_secrets  = False,
    testing_load_buttons                    = False,
    testing_empty_section_fallback          = "human",
    editor_font_size                        = 12,
) }}


Contenu entêtes:

```yaml
args:
  IDE:
    LOGS: false
    MAX: 45
ides:
  deactivate_stdout_for_secrets: true
  decrease_attempts_on_user_code_failure: "public"
  show_only_assertion_errors_for_secrets: false
  editor_font_size: 12    # hardcoded in dumps tests
testing:
    load_buttons: False
    empty_section_fallback: human
```

Contenu de `docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml`:

```yaml
--8<-- "docs/dev_docs/tests_feedback__globals__meta/config_tests/.meta.pmt.yml"
```

---

<br>

!!! tip "Effet de `decrease_attempts_on_user_code_failure`"

    `ides.decrease_attempts_on_user_code_failure = {{ Env().decrease_attempts_on_user_code_failure }}`

    `True`, donc, sur ++ctrl+enter++ :

    | message| visible |
    |-|-|
    | `#!py `M=1` | `constant`{.red} |
    | `#!py `M=2` | `constant`{.red} |
    | `#!py `M=3` | `decrease`{.red} |
    | `#!py `M=4` | `decrease`{.red} |
    | `#!py `M=5` | `success`{.green} |
    | `#!py `M>5` | `constant` |


{{ IDE_py('../exo_decrease', ID=22, before="!!!", MAX=15, admo_kls="tip inline end w45",  TEST=Case(
    description="Test the presence or absence of assertion messages, depending on the section running and the value of decrease_attempts_on_user_code_failure='editor'",
    subcases=[
        Case(delta_attempts= 0, in_error_msg="error in env"),
        Case(delta_attempts= 0, in_error_msg="error in code/corr"),
        Case(delta_attempts=-1, in_error_msg="error in tests"),
        Case(delta_attempts=-1, in_error_msg="error in secrets"),
        Case(delta_attempts= 0),
        Case(delta_attempts= 0, in_error_msg="success on 5, else error"),
        Case(delta_attempts=0, run_play=1),
    ])
) }}