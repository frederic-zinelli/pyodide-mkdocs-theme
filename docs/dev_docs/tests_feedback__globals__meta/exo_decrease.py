# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals(),'M')
assert M!=1, "error in env"

# --- PYODIDE:code --- #
print(f'{M = }')
assert M!=2, "error in code/corr"

# --- PYODIDE:tests --- #
assert M!=3, "error in tests (validation)"
# --- PYODIDE:secrets --- #
assert M!=4, "error in secrets"
assert M<=5, "success on 5, else error"

# --- PYODIDE:corr --- #
print(f'{M = }')
assert M!=2, "error in code/corr"