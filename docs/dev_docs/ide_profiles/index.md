---
ides:
    deactivate_stdout_for_secrets: false
    decrease_attempts_on_user_code_failure: editor
---


_Note: stdout visible in secrets !_

---

## `MODE = IdeMode.no_reveal`

{{ IDE_py(
    'exo', MODE='no_reveal', MAX=22, TEST=Case(
        description="`no_reveal` profile never reveals corr/REMs...",
        keep_profile=True,
        reveal_corr_rems=False,
        set_max_and_hide=1,
    ).times(4, with_={
        (1,4): Case(
            title="passing",
            delta_attempts=0,
            all_in_std=[Lang('success_head')],
            none_in_std=[Lang('success_tail')],
        ),
        (2,3): Case(
            title="failing",
            fail=1,
            delta_attempts=-1,
            none_in_std=[Lang('fail_head'),Lang('success_head'),Lang('success_tail')],
        ),
    }),
    before='!!!', admo_kls='tip w45 inline end',
)}}

!!! tip "Ce qui est testé"

    * Compteur $\infty$
    * Bouton de validation présent
    * {{red("__Pas de révélation même sur un succès__")}}
    * `corr_btn` présent et fonctionnel {{red("avec révélation !")}}



## `MODE = IdeMode.no_valid`

{{ IDE_py(
    'exo', MODE='no_valid', ID=2, TEST=Case(
        code         = 1,
        run_play     = 1,
        keep_profile = 1,
        assertions   = '!hasCheckBtn corrRemsMask',
        none_in_std  = [ Lang('fail_head'), Lang('success_head'), ],
    ),
    before='!!!', admo_kls='tip w45 inline end',
)}}


!!! tip "Ce qui est testé"

    * Pas de compteur
    * Bouton de validation ABSENT et ++ctrl+enter++ non fonctionnel
    * `corr_btn` présent et fonctionnel {{red("avec révélation !")}}




## `MODE = IdeMode.delayed_reveal`

{{ IDE_py("delayed", MAX=3, MODE="delayed_reveal", TEST=Case(

    subcases=[
        Case.DELAYED_NO_REVEAL,
        Case.DELAYED_NO_REVEAL.with_(
            no_clear = True,
            set_max_and_hide = 1,
            reveal_corr_rems = True,
            all_in_std = [Lang('success_tail')],
            none_in_std = [Lang('success_head'), Lang('success_head_extra'), Lang('fail_head')],
        )
    ]),
    before='!!!', admo_kls='tip w45 inline end',
)}}

!!! tip "Ce qui est testé"

    * no error message in the console (build)
    * no error messages in the terminal
    * no "bravo" message in the terminal
    * corr/REMs revealed after N tries
