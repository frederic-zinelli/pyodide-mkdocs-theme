# --- PYODIDE:env --- #

target  = 'file.txt'
tmp     = 'tmp'
tmp2    = 'tmp2/duh'
renamed = 'renamed.txt'

await copy_from_server(target)
await copy_from_server(target, tmp)
await copy_from_server(target, tmp2, renamed)

print('Invalid url throws URLError')
from urllib.error import URLError
try:
    await copy_from_server('skfgjksjdgkjsdhfgjhsdgh.txt')
    assert False, "Invalid address should have led to URLError..."
except URLError:
    pass

# --- PYODIDE:code --- #

from pathlib import Path

t1 = Path(target)
t2 = t1.parent / tmp / target
t3 = t1.parent / tmp2 / renamed

for p in (t1,t2,t3):
    print('check', p)
    assert p.is_file(), f"{p} should be a file"
    exp = 'duh?'
    assert p.read_text(encoding='utf-8') == exp, f"{p} should contain {exp}"
    p.unlink()
