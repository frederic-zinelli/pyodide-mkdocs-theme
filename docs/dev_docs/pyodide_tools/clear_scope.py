# --- PYODIDE:code --- #

clear_scope(['min'])

memory  = {
    'Any', 'Callable', 'GLOBS', 'StdOutAsserter', 'VAR', '__hack_std_import_attempt',
    'apply_Nth', 'assertions_mechanisms', 'auto_N', 'do_it', 'local_N', 'not_defined',
    'not_importable', 'seq', 'sequential_tests', 'should_raise', 'sys', 'test_auto_run',
    'test_stdout', 'truthy', 'v'
}
remains = set(dir())
should_be_gone = memory & remains
assert not should_be_gone, f"should_be_gone: {sorted(should_be_gone)}"