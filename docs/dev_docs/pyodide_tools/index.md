---
terms:
    cut_feedback: false
args:
    IDE:
        TEST: code
---

# Test some `0_genericPythonSnippets-pyodide` tools



## `@as_buitlin`

{{ IDE("as_builtin") }}



## `@wraps_buitlin`

{{ IDE("wraps_builtin", TEST=Case(code=1, subcases=[

    Case(description="Check some basic functionalities of the decorator, on various functions"),

    Case(
        description="Check: repr(print)", term_cmd="print", all_in_std=["<built-in function print>"]
    ),
    Case(
        description="Check: help(print) reuses the (kinda) original help msg",
        term_cmd="help(print)",
        all_in_std=[
            "Python Library Documentation: built-in function print in module builtins",
            "print", "args, sep=' ', end='", "n', file=None, flush=False",
            "Prints the values to a stream, or to sys.stdout by default.",
        ]),

    Case(
        description="Check: repr(input)", term_cmd="input", all_in_std=["<built-in function input>"]
    ),
    Case(
        description="Check: help(input) reuses the (kinda) original help msg",
        term_cmd="help(input)",
        all_in_std=[
            "Python Library Documentation: built-in function input in module builtins",
            "input\\(prompt='', /\\)",
            "Read a string from standard input.",
            "The trailing newline is stripped.",
        ]),

    Case(
        description="Check: repr(help)", term_cmd="help", all_in_std=["<function help>"]
    ),
    Case(
        description="Check: help(help) uses custom doc message",
        term_cmd="help(help)",
        all_in_std=[
            "Python Library Documentation: function help in module __main__",
            "help\\(object_or_function\\)",
            "Replace the original help function",
        ]),

    Case(
        description="Check: repr(terminal_message)", term_cmd="terminal_message", all_in_std=["<function terminal_message>"]
    ),
    Case(
        description="Check: help(terminal_message) uses custom docstring message",
        term_cmd="help(terminal_message)",
        all_in_std=[
            "Python Library Documentation: function terminal_message in module __main__",
            "terminal_message\\(key, \\*msg: Any, format: str = None, sep: str = ' ', end: str = None, new_line: bool = True",
            "Display the given message directly into the terminal, without using the python stdout.",
            "@key", "@*msg", "@format", "@sep", "@end", "@new_line",
        ]),

])) }}


## `version`

{{ IDE('version', TEST=Case(code=1, subcases=[
    Case(title="Normal usage"),
    Case(title="Also works from terminal", term_cmd="version()", all_in_std=["pyodide-mkdocs-theme v"]),
])) }}


## `__move_forward__`

{{ IDE('forward', SANS="min", TEST=Case(code=1, subcases=[
    Case(title="Normal usage"),
    Case(title="reassigning from terminal fails", term_cmd="min=__move_forward__('min')", in_error_msg="FORBIDDEN: don't use min"),
])) }}



## `clear_scope`

{{ IDE('clear_scope', SANS="min") }}


## `copy_from_server`


{{ IDE('copy_from_server') }}
