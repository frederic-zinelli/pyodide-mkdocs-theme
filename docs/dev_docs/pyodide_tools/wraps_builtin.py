# --- PYODIDE:code --- #

from py_libs import *

@wraps_builtin
def meh(): pass

assert callable(__builtins__.meh), "@wraps_builtin without arguments"
del __builtins__.meh


@wraps_builtin(name="duh")
def meh(x=42, *, niet=None, **kw):
    """ Use wrapped doc """

assert callable(__builtins__.duh), "@wraps_builtin(name='duh')"
truthy(duh)

test_stdout(
    lambda: print(duh),
    "<function duh>"
)
test_stdout(
    lambda: help(duh),
    "Python Library Documentation: function meh in module __main__",
    "meh(x=42, *, niet=None, **kw)",
    "Use wrapped doc"
)

del __builtins__.duh
