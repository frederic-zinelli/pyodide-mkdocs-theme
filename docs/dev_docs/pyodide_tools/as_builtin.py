# --- PYODIDE:code --- #

from py_libs import *

@as_builtin
def meh(): pass

assert callable(__builtins__.meh), "The decorated function should be stored under its own name"


as_builtin("duh")(meh)

assert callable(__builtins__.duh), "Should be able to use another name to declare a function in __builtins__"
assert __builtins__.duh is __builtins__.meh

del __builtins__.duh, __builtins__.meh