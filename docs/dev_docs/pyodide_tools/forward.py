# --- PYODIDE:code --- #

from py_libs import *

should_raise(
    lambda: min(1,4),
    ExclusionError,
    "The min function should be forbidden"
)
v = __move_forward__('min')(1,4)
assert v == 1
