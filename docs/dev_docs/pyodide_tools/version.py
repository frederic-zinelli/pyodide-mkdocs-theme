# --- PYODIDE:code --- #

from py_libs import *
import re

assert callable(version), "pyodide `version` function should be defined"

v       = version(True)
pattern = r"pyodide-mkdocs-theme v\d+[.]\d+[.]\d+"

assert isinstance(v, str), "version() should return a string"

assert re.fullmatch(pattern, v), "version() should match "+pattern