# - PYODIDE:code - #

from py_libs import *
auto_N(globals())


def rec(): rec()

def key(): raise KeyError('meh...')

def mutual_rec(): mut1()
def mut1(): mut2()
def mut2(): mut3()
def mut3(): mutual_rec()


apply_Nth(
    lambda: aaa,        # NameError
    key,                # KeyError
    rec,                # RecursionError
    mutual_rec,         # RecursionError
)