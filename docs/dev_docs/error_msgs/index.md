# Testing error messages feedback and co

{{ IDE('err', TEST=Case(
    description="Testing error messages contents", code=1, fail=1,
    subcases=[
        Case(in_error_msg="NameError", none_in_std=['File "/lib/python', "PythonError"]),
        Case(in_error_msg="KeyError: 'meh...'"),
        Case(in_error_msg="RecursionError"),
        Case(
            all_in_std=["Traceback", "Message truncated", "RecursionError"],
            title="testing mutual recursion (& repr_shorten)",
        ),

        Case(term_cmd="aaa", in_error_msg="NameError", none_in_std=['File "/lib/python', "PythonError"]),
        Case(term_cmd="key()", in_error_msg="KeyError: 'meh...'"),
        Case(term_cmd="rec()", all_in_std=["Traceback", "Previous line repeated", "RecursionError"]),
        Case(
            term_cmd="mutual_rec()",
            all_in_std=["Traceback", "Message truncated", "RecursionError"],
            title="testing mutual recursion (& repr_shorten)",
        ),
    ])
)}}