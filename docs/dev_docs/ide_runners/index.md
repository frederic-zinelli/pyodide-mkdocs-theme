---
ides:
    deactivate_stdout_for_secrets: false
---




## Testing IDE `AUTO_RUN` argument


{{ IDE("auto_run_arg", AUTO_RUN=True, TEST=Case(
    description="Vérifie qu'AUTO_RUN lance les bonnes sections/routines",
    auto_run=1, subcases=[
        Case(all_in_std=["ran corr"], none_in_std=["ran term"], title="Lanece une validation"),
        Case(all_in_std=["ran term"], none_in_std=["ran corr"], term_cmd="'ran term'", title="Lance le temrinal"),
    ]
))}}



## Testing IDE Corr button


{{ IDE("check_corr", TEST=Case(
    description = "Vérifie validationCorr",
    run_corr    = 1,
    all_in_std  = ["--corr--","--tests--","--secrets--"],
))}}




## Vérifie les articulations des sections selon les erreurs levées et les config de "run"

!!! tip "Ce qui est testé"

    * Utiliser ++ctrl+enter++ uniquement.
    * Teste quelles sections sont effectivement exécutées ou pas, en faisant des asserts sur le contenu de la stdout => semi automatisé: il n'y a qu'(à faire Ctrl+Enter jusqu' voir X=8 dans un ValueError)
    * des sécurités sont posées de façons à ce qu'une section qui n'est pas sensée s'exécuter lève une erreur (sans doute plus très utile vu comment j'ai upgradé les tests sur la stdout, mais bon...)

    Les erreurs levées sont les suivantes selon la valeur de `X`:

    1. aucune
    1. `env` : `KeyError`{.red}
    1. `env` : `AssertionError`{.orange}
    1. `code` : `KeyError`{.red}
    1. `code` : `AssertionError`{.orange}
    1. `secrets` : `KeyError`{.red}
    1. `secrets` : `AssertionError`{.orange}
    1. `post` : `KeyError`{.red}
    1. `post` : `AssertionError`{.orange}

<br>

### Validation, sur la section `code`

{{ IDE_py('sections', MAX=45, TERM_H=15, TEST=Case(
    code=1, description="Vérifie l'articulation des exécutions de sections (user code)",
    subcases=[
        Case(
            description = "Normal behavior: env -> code -> secrets -> post",
            none_in_std = ["--env_term--", "--post_term--"],
            all_in_std  = ["--env--", "--code--", "--secrets--", "--post--"],
        ),
        Case(
            description  = "KeyError in env: NOTHING done!",
            in_error_msg = "KeyError: 'RAISING! from env'",
            none_in_std  = ["--[a-z_]+--"],
        ),
        Case(
            description  = "AssertionError in env: then post only",
            in_error_msg = "AssertionError: from env",
            none_in_std  = ["--(env|code|secrets)--"],
            all_in_std   = ["--post--"],
        ),
        Case(
            description  = "KeyError in code: env -> post",
            in_error_msg = "KeyError: 'RAISING! from code'",
            none_in_std  = ["--(code|secrets)--"],
            all_in_std   = ["--env--","--post--"],
        ),
        Case(
            description  = "AssertionError in code: env -> post",
            in_error_msg = "AssertionError: from code",
            none_in_std  = ["--(code|secrets)--"],
            all_in_std   = ["--env--","--post--"],
        ),
        Case(
            description  = "KeyError in secrets: env -> code -> post",
            in_error_msg = "KeyError: 'RAISING! from secrets'",
            none_in_std  = ["--secrets--"],
            all_in_std   = ["--env--","--code--","--post--"],
        ),
        Case(
            description  = "AssertionError in secrets: env -> code -> post",
            in_error_msg = "AssertionError: from secrets",
            none_in_std  = ["--secrets--"],
            all_in_std   = ["--env--","--code--","--post--"],
        ),
        Case(
            description  = "KeyError in post: env -> code -> secrets",
            in_error_msg = "KeyError: 'RAISING! from post'",
            none_in_std  = ["--post--"],
            all_in_std   = ["--env--","--code--","--secrets--"],
        ),
        Case(
            description  = "AssertionError in post: env -> code -> secrets",
            in_error_msg = "AssertionError: from post",
            none_in_std  = ["--post--"],
            all_in_std   = ["--env--","--code--","--secrets--"],
        ),
    ]
)) }}


### Validation, sur la section `corr`

{{ IDE_py('sections', ID=1, MAX=45, TERM_H=15, TEST=Case(
    description="Vérifie l'articulation des exécutions de sections (corr)",
    subcases=[
        Case(
            description = "Normal behavior: env -> corr -> secrets -> post",
            none_in_std = ["--env_term--", "--post_term--"],
            all_in_std  = ["--env--", "--corr--", "--secrets--", "--post--"],
        ),
        Case(
            description  = "KeyError in env: NOTHING done!",
            in_error_msg = "KeyError: 'RAISING! from env'",
            none_in_std  = ["--[a-z_]+--"],
        ),
        Case(
            description  = "AssertionError in env: then post only",
            in_error_msg = "AssertionError: from env",
            none_in_std  = ["--(env|code|corr|tests|secrets)--"],
            all_in_std   = ["--post--"],
        ),
        Case(
            description  = "KeyError in corr: env -> post",
            in_error_msg = "KeyError: 'RAISING! from corr'",
            none_in_std  = ["--(corr|tests|secrets)--"],
            all_in_std   = ["--env--","--post--"],
        ),
        Case(
            description  = "AssertionError in corr: env -> post",
            in_error_msg = "AssertionError: from corr",
            none_in_std  = ["--(corr|tests|secrets)--"],
            all_in_std   = ["--env--","--post--"],
        ),
        Case(
            description  = "KeyError in tests: env -> corr -> post",
            in_error_msg = "KeyError: 'RAISING! from tests'",
            none_in_std  = ["--(tests|secrets)--"],
            all_in_std   = ["--env--","--corr--","--post--"],
        ),
        Case(
            description  = "AssertionError in tests: env -> corr -> post",
            in_error_msg = "AssertionError: from tests",
            none_in_std  = ["--(tests|secrets)--"],
            all_in_std   = ["--env--","--corr--","--post--"],
        ),
        Case(
            description  = "KeyError in secrets: env -> corr -> post",
            in_error_msg = "KeyError: 'RAISING! from secrets'",
            none_in_std  = ["--secrets--"],
            all_in_std   = ["--env--","--corr--","--tests--","--post--"],
        ),
        Case(
            description  = "AssertionError in secrets: env -> corr -> post",
            in_error_msg = "AssertionError: from secrets",
            none_in_std  = ["--secrets--"],
            all_in_std   = ["--env--","--corr--","--tests--","--post--"],
        ),
        Case(
            description  = "KeyError in post: env -> corr -> secrets",
            in_error_msg = "KeyError: 'RAISING! from post'",
            none_in_std  = ["--post--"],
            all_in_std   = ["--env--","--corr--","--tests--","--secrets--"],
        ),
        Case(
            description  = "AssertionError in post: env -> corr -> secrets",
            in_error_msg = "AssertionError: from post",
            none_in_std  = ["--post--"],
            all_in_std   = ["--env--","--corr--","--tests--","--secrets--"],
        ),
    ]
)) }}




### Commandes du terminal


{{ IDE_py('sections', ID=2, MAX=45, TERM_H=15, TEST=Case(
    description = "Vérifie les sections exécutées avec les commandes du terminal",
    term_cmd    = "'--cmd--'",
    all_in_std  = ["--env_term--", "--cmd--", "--post_term--"],
    none_in_std = ["--env--","--post--"],
    subcases=[
        Case(
            title = "Initial run",
            all_in_std  = ["--env--", "--env_term--", "--cmd--", "--post_term--", "--post--"],
            none_in_std = [],
        ),
        Case(title="Subsequent runs do not run env/post"),
        Case(title="Subsequent runs do not run env/post"),
    ]
)) }}




## Checking CONFIG.running


{{ IDE_py('running', TEST=Case(
    description="",
    subcases=[
        Case(title="Editor only", code=1, run_play=1, ),
        Case(title="Validation"),
        Case(title="Corr button", run_corr=1),
        Case(title="Terminal",  term_cmd="assert 'Command' in running ; check_running()"),
    ]
)) }}