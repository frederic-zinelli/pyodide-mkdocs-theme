
# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())

print('Case', N)
if do_it(): pass
if do_it(): raise KeyError(f"RAISING! from env")
if do_it(): assert False, "from env"

test_public = False
print('--env--')

# --- PYODIDE:env_term --- #
print('--env_term--')

# --- PYODIDE:code --- #
if do_it(): raise KeyError(f"RAISING! from code")
if do_it(): assert False, "from code"
test_public = False
print('--code--')

# --- PYODIDE:corr --- #
if do_it(): raise KeyError(f"RAISING! from corr")
if do_it(): assert False, "from corr"
test_public = True
print('--corr--')

# --- PYODIDE:tests --- #
# Those will run only in the second validation step, IF running the
# `corr` section instead of the `code` one.
#
# WARNING: in test_ides, this content is NOT part of the first validation step (editor),
#          on the contrary of a manual execution in the dev_docs page.
if test_public:
    if do_it(): raise KeyError(f"RAISING! from tests")
    if do_it(): assert False, "from tests"
    print('--tests--')

# --- PYODIDE:secrets --- #
if do_it(): raise KeyError(f"RAISING! from secrets")
if do_it(): assert False, "from secrets"
print('--secrets--')

# --- PYODIDE:post_term --- #
print('--post_term--')

# --- PYODIDE:post --- #
if do_it(): raise KeyError(f"RAISING! from post")
if do_it(): assert False, "from post"
print('--post--')
