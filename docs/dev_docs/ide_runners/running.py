# --- PYODIDE:env --- #
import js
running = js.config().running

def check_running():
    now = js.config().running
    assert now == running, f'{now!r} should still be {running!r}'

# --- PYODIDE:env_term --- #
running = js.config().running

# --- PYODIDE:code --- #
assert 'Play' in running
check_running()

# --- PYODIDE:corr --- #
assert 'Validate' in running
check_running()

# --- PYODIDE:tests --- #
check_running()

# --- PYODIDE:secrets --- #
check_running()

# --- PYODIDE:post_term --- #
check_running()

# --- PYODIDE:post --- #
check_running()