# --- PYODIDE:corr --- #
print("--corr--")
import js
assert 'ValidateCorr' in js.config().running

# --- PYODIDE:tests --- #
print("--tests--")
assert 'ValidateCorr' in js.config().running

# --- PYODIDE:secrets --- #
print("--secrets--")
assert 'ValidateCorr' in js.config().running

# --- PYODIDE:code --- #
raise ValueError("Should never raise this")