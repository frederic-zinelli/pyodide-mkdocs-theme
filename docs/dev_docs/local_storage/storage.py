# - PYODIDE:code - #

from py_libs import *
auto_N(globals())
from pyodide.ffi import JsException



if do_it():
    key      = 'duracell'
    old_soul = get_storage(key)
    exp      = 'Always keep that...'
    assert old_soul==exp, f"""Redactors localStorage additions should survive:
storage.duracell: {old_soul!r} should be {exp!r}.
You need to execute the following code in the IDE of the current page:

    set_storage('{ key }', '{ exp }')

NOTE: localStorage is tide to the id of the editor in the page, not the id of the tested IDE.
"""

if do_it(): set_storage('bla', 42)

if do_it(): equal(get_storage('bla'), 42, "set_storage('bla', 42) didn't work")

if do_it(): set_storage('bla', -3)

if do_it(): equal(
    get_storage('bla'), -3,
    "set_storage() should succeed at updating and existing key ('bla')"
)

if do_it():
    equal(get_storage('meh'), None, "Undefined keys access should return None")




if do_it():
    act = keys_storage()
    assert isinstance(act, list), f"keys_storage() should return a list, but was: {act}"

if do_it():
    equal(set(keys_storage()), set('code done hash name zip bla duracell'.split()))



exp = get_storage('done')

if do_it():
    set_storage('done', 42) # JsException, "Should raise JsException, attempting to override a PMT key ('done')",


if do_it():
    equal(
        get_storage('done'), exp,
        "localStorage.done shouldn't have changed after a failed update"
    )


if do_it():
    del_storage('bla')
    equal(get_storage('bla'), None, "Should have deleted the 'bla' key")

if do_it():
    del_storage('done') # JsException, "Cannot delete PMT keys"


if do_it():
    del_storage('duh') # should do nothing when the key doesn't exist
    equal(get_storage('duh'), None, "Deleting an non existent key shouldn't define it")


if do_it(): set_storage('bla', 42)      # setup for terminal tests
if do_it(): pass # term_cmd
if do_it(): pass # term_cmd
if do_it(): pass # term_cmd
if do_it(): pass # term_cmd
if do_it(): assert get_storage('bla') == 42, "'bla' is still set to 42"
