# Testing localStorage tools used from Pyodide

{{ IDE('storage', TEST=Case(code=1, subcases=[

    Case(description = "get_storage('duracell'): user content fields should persist."),
    Case(description = "set_storage('bla', 42) doesn't raise."),
    Case(description = "get_storage('bla') extracts 42."),
    Case(description = "set_storage('bla', -3) can modify an existing key."),
    Case(description = "get_storage('bla') now extracts -3."),
    Case(description = "get_storage('any') returns None (undefined key)"),

    Case(description = "keys_storage() returns a list"),
    Case(description = "keys_storage() returns: code, done, hash, name, zip and bla"),

    Case(title       = "set_storage('done',42)", in_error_msg="pyodide.ffi.JsException: PythonError"),
    Case(description = "get_storage('done'): the value didn't change"),
    Case(description = "del_storage('bla') can delete personal key"),
    Case(title       = "del_storage('done')", in_error_msg="pyodide.ffi.JsException: PythonError"),
    Case(description = "del_storage('duh'): non existing keys do not cause problems"),

    Case(description = "set_storage('bla', 42): prepare terminal tests.", code=1),

    Case(
        description  = "get_storage not usable from terminal",
        term_cmd     = 'get_storage("done")',
        in_error_msg = "pyodide.ffi.JsException: PythonError"
    ),
    Case(
        description  = "set_storage not usable from terminal",
        term_cmd     = 'set_storage("bla",21)',
        in_error_msg = "pyodide.ffi.JsException: PythonError"
    ),
    Case(
        description  = "del_storage not usable from terminal",
        term_cmd     = 'del_storage("bla")',
        in_error_msg = "pyodide.ffi.JsException: PythonError"
    ),
    Case(
        description  = "keys_storage not usable from terminal",
        term_cmd     = 'keys_storage()',
        in_error_msg = "pyodide.ffi.JsException: PythonError"
    ),
    Case(description = "get_storage('bla') still set to 42."),
]))}}
