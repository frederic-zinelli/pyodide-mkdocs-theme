---
ides:
    forbid_hidden_corr_and_REMs_without_secrets: false
    forbid_corr_and_REMs_with_infinite_attempts: false
---

## IDE et IDEv

!!! tip "Ce qui est testé"
    * Les 2 layouts devraient fonctionner (visuellement)
    * L'argument {{red("ID n'est pas nécessaire")}}

{% raw %} {{ IDE('exo', MAX=1) }} {% endraw %}
{{ IDE('exo', MAX=1, TEST=Case(
    title="Check the section is found", all_in_std=['Found it!'],
)) }}

{{sep()}}

{% raw %} {{ IDEv('exo', MAX=1) }} {% endraw %}
{{ IDEv('exo', MAX=1, TEST=Case(
    title="Check the section is found", all_in_std=['Found it!'],
)) }}


## ID arguments

!!! tip "Ce qui est testé"
    * L'argument {{red("ID n'est pas nécessaire")}} si le chemin relatif vers le même fichier est différent.
    * L'argument ID permet d'utiliser le même fichier plusieurs fois dans la même page.

{% raw %} {{ IDE('../layout_and_ID_tests/exo', MAX=1) }} {% endraw %}
{{ IDE('../layout_and_ID_tests/exo', MAX=1, TEST=Case(
    title="Check the section is found", all_in_std=['Found it!'],
)) }}

{{sep()}}

{% raw %} {{ IDE('exo', MAX=1, ID=1) }} {% endraw %}

{{ IDE('exo', MAX=1, ID=1, TEST=Case(
    title="Check the section is found", all_in_std=['Found it!'],
)) }}


## This title should be visible
