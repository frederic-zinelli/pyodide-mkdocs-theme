Il est aussi possible de n'utiliser qu'une seule fonction :

```python
def transposition(grille):
    # le nb de lignes de la grille sera
    # le nb de colonnes de la transposée
    n = len(grille)

    # le nb de colonnes de la grille sera
    # le nb de lignes de la transposée
    m = len(grille[0])

    t_n, t_m = m, n
    t_grille = [[0 for j in range(t_m)] for i in range(t_n)]
    for i in range(n):
        for j in range(m):
            t_i, t_j = j, i
            t_grille[t_i][t_j] = grille[i][j]
    return t_grille
```

Les éléments mathjax doivent aussi être formatés après affichage des corrections et remarques:


$$\begin{bmatrix}
0 & \mathbf{1} & 1 & 1\\
1 & \mathbf{0} & 1 & 0\\
0 & \mathbf{0} & 0 & 1\\
\end{bmatrix}$$

Et même dans des admonitions...

!!! tip
    $$\begin{bmatrix}
    0 & 1 & 0\\
    \mathbf{1} & \mathbf{0} & \mathbf{0}\\
    1 & 1 & 0\\
    1 & 0 & 1\\
    \end{bmatrix}$$
