---
author: Franck Chambon
hide:
    - navigation
    - toc
title: Formatage (latex, macro code, ...)
tags:
    - à trous
    - en travaux
    - grille
---

# Essais MathJax & macro.py

## MathJax ([version sur CodEx](https://codex.forge.apps.education.fr/en_travaux/transposition/))

!!! tip "ce qu'on devrait voir"
    * les équations et matrices formatées correctement, avec ou sans admonitions
    * recharger la page ou revenir ici après être allé ailleurs formate tjrs proprement le tout
    * une fois les remarques affichées, il faut qu'elles soient formatées avec mathjax

---

Colored mathjax: $\color{red}{x} + \color{blue}{y}$

* Si $n$ est strictement inférieur à $2$, ... ;
* ... compris entre $2$ et $\sqrt{n}$ ...;
* ... $n$ est premier.

$$\begin{bmatrix}
0 & \mathbf{1} & 1 & 1\\
1 & \mathbf{0} & 1 & 0\\
0 & \mathbf{0} & 0 & 1\\
\end{bmatrix}$$

!!! tip
    * Si $n$ est strictement inférieur à $2$, ... ;
    * ... compris entre $2$ et $\sqrt{n}$ ...;
    * ... $n$ est premier.

    $$\begin{bmatrix}
    0 & \mathbf{1} & 1 & 1\\
    1 & \mathbf{0} & 1 & 0\\
    0 & \mathbf{0} & 0 & 1\\
    \end{bmatrix}$$


---

## Macro `py` & code tags formatting


!!! tip "Ce qu'on devrait voir"
    * les parties HDR et tests du fichier `exo.py` ne devraient pas être affichées
    * on doit voir les 3 fonctions
    * Les tests ne doivent pas être visibles
    * la taille de police doit rester cohérente avec le reste (notamment les code-spans)

* Code spans: bool vs `bool` | True vs `#!py True` ou False vs `#!py False`
* 3 &lt; 4 vs `#!py 3 < 4`
* "a" in "chien" vs `#!py "a" in "chien"`
* prop_1, prop_2 vs `prop_1`, `prop_2`

{{ py('relative/rel_py') }}

!!! tip "Marche aussi dans des admonitions?"

    Nota: utilise `MODE="revealed"`

    * Code spans: bool vs `bool` | True vs `#!py True` ou False vs `#!py False`
    * 3 &lt; 4 vs `#!py 3 < 4`
    * "a" in "chien" vs `#!py "a" in "chien"`
    * prop_1, prop_2 vs `prop_1`, `prop_2`


{{ IDE_py('exo', MAX=1, MAX_SIZE=20, MODE='revealed', TEST=Case(
    skip=1, description="Just there to check mathjax rendering inside REMs"
)) }}


### This title should be visible
