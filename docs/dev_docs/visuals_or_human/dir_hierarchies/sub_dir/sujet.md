
---
title: IDE in index.md file (non root)
hide:
    - navigation
    - toc
---


Comme la page d'accueil:

Vérifie s'il est possible de mettre un IDE depuis un fichier `index.md` au lieu de `sujet.md`.

!!! tip "ce qu'on devrait voir"
    * l'IDE et les boutons devraient être visibles
    * l'IDE devait contenir du code

{{ IDE('index_ex/exo', TEST=Case(
    code=1, all_in_std=['Found it!'], assertions='corrRemsMask !corrContent secretTests',
    title="From sujet.md (also find VIS_REM)"
)) }}

!!! info "Ce qui est testé & remarques"
    * Les fichiers pythons de l'IDE sont dans le sous-dosser `./index_ex/`
    * ceci est un test préliminaire pour préparer la bascule `sujet.md` -> `index.md`. Il faudra également vérifier son bon fonctionnement une fois basculé.

### This title should show up
