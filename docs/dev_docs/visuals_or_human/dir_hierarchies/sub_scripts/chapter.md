---
hide:
    - navigation
    - toc
title: Alternative hdd + no tests failure
tags:
    - à trous
    - en travaux
    - programmation orientée objet
---

!!! tip "Ce qui est testé"
    * Les macros arrivent à trouver les fichiers dans `./scripts/nom_exo...`
    * Durant le build, un message d'erreur doit apparaître comme quoi un IDE avec un fichier de REM est utilisé, mais sans fichier de tests (ce qui veut dire que pas de bouton de validation, donc pas possible de voir la remarque).

    Message attendu: `exercices/sub_scripts/: a correction or remark file exists but there is no "exo_test.py" file.`


Here they are...


=== "Theme way"

    {{ IDE("exo", TEST=Case(
        code=True, fail="True", all_in_std=['AssertionError'],
        assertions="!corrRemsMask !corrContent publicTests",
        title="./scripts/chapter/exo.py"
    )) }}


    !!! tip "Teste d'autres macros"

        `py('exo')` :

        {{ py('exo') }}

        `section('exo', 'code')` :
        {{ section('exo', 'code') }}


=== "Old way"

    {{ IDE("exo_old_way", TEST=Case(
        code=True, fail="True", all_in_std=['AssertionError'],
        assertions="corrRemsMask !corrContent envContent publicTests secretTests"
    )) }}


    !!! tip "Teste d'autres macros"

        `py('exo_old_way')` :

        {{ py('exo_old_way') }}

        `section('exo_old_way', 'env')` :
        {{ section('exo_old_way', 'env') }}



### This title should appear
