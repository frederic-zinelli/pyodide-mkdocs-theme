# --- PYODIDE:env --- #

FAIL_ENV_T = 0
FAIL_POST_T = 0

print('env: call fun() -> 42')
def fun(): return 42
def a(): a()

# --- PYODIDE:post --- #
print('post...')

# --- PYODIDE:env_term --- #
print('env_term')
if FAIL_ENV_T:  raise ValueError()
# --- PYODIDE:post_term --- #
print('post_term')
if FAIL_POST_T: raise ValueError()

# --- PYODIDE:code --- #
raise Exception("Code section should never have been run!!")
