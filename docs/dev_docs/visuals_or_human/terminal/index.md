---
title: Macro terminal()
---

# utilisation de la macro `terminal()`

(testing autofocus on active terminals)

2

5

5

5

5

5
2

5

5

5

5

5
2

5

5

5

5

5
2

5

5

5

5

5


!!! tip "ce qu'on devrait voir"
    * le terminal marche aussi bien dans une admonition qu'en dehors
    * possibilité d'avoir plusieurs terminaux dans la même page
    * les deux terminaux sont sensés "communiquer" (peu d'intérêt, mais bon...)
    * les terminaux ne prennent pas le focus et ne font pas scroller la page après chargement de pyodide
    * appuyer sur Enter seulement ne provoque pas d'erreur
    * l'argument SIZE contrôle la taille du terminal (hauteur)
    * une erreur python est formatée de la même façon que depuis l'éditeur
    * `FILL` argument works
    * try imports from terminal


{{ terminal(TERM_H=6) }}

!!! tip "Dans une admonition"

    {{ terminal(TERM_H=12, FILL="{}[42] # Formatted KeyError?") }}


<br>

Multiline FILL:

{{ terminal(TERM_H=12, FILL="print('hello')\n#second\na=42 #next line is editable\na") }}




!!! tip "ce qu'on devrait voir"
    * command in tabbed terminals should display result in the same terminal
    * `FILL` contents should be restored (if lost...) when clicking on a terminal after changing a tab somewhere, if the terminal never got run before.

=== "tabbed (with `.py`)?"

    `terminal('exo', SANS="sorted .count", REC_LIMIT=42, TERM_H=12, FILL="fun()")`

    * `FILL` argument works
    * `REC_LIMIT` argument works (use `a()`)
    * `env` and `post` run once only
    * `env_term` and `post_term` run every time
    * `code` section is never run (would raise, otherwise)
    * Check what happens when errors raised in various places
        - `a()` in cmd
        - `FAIL_POST_T=1` in post_term
        - `FAIL_ENV_T=1` in env_term (to test LAST)

    {{ terminal_py('exo', SANS="sorted .count", REC_LIMIT=42, TERM_H=12, FILL="fun()") }}


=== "tabbed++"

    {{ terminal() }}

=== "tabbed--"

    {{ terminal() }}


{{sep()}}

!!! tip "Ce qui est testé"
    1. v = 'anything'` in the terminal
    2. assertion error must be raised
    3. running the IDE with `v` as code must raise NameError

{{ IDE_py('../../../custom/exemples/user_cmd', ID=1, MAX_SIZE=3, TERM_H=8, TEST='skip', before='!!!') }}

### This title should be visible