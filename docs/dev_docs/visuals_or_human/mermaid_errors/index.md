
!!! tip "Ce qui est testé"
    Missing `MERMAID=True` agument used at least once in the page should lead to error messages

{{figure()}}

{{py_btn('btn')}}

`py_btn` should open a `window.alert`

<br>

---

<br>

{{ figure('term-fig-id') }}

{{ terminal('../../../custom/exemples/mermaid_term', FILL='generate()', TERM_H=3) }}

<br>

---

<br>

{{ IDE('mermaid_wrong', TERM_H=3, TEST=Case(
    skip=1, description="Error cannot be tested through automated testing (IdeTester has MERMAID=True)."
)) }}

Note: this cannot be tested in the `test_ides` page, because the IDE has the MERMAID argument.
