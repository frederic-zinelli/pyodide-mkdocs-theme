# Import exo.py complet


!!! tip "ce qu'on devrait voir"
    * le code de la partie HDR du fichier ne doit pas apparaître dans l'IDE (testé depuis env)
    * Passe les tests publics
    * Fail validation avec `code`
    * Fail validation avec `corr`


{{ IDE(
    'exo', MAX=3,
    TEST=Case(subcases=[
        Case(
            code=1, fail=1,
            all_in_std=["Found HDR as env section", "AssertionError: Found secret tests"],
            assertions="corrRemsMask",
        ),
        Case(in_error_msg="AssertionError: Found public tests: pass code but not corr"),
    ])
) }}


### This title should be visible