# --- HDR --- #

print('Found HDR as env section')
this_is_hdr = 42

code = __USER_CODE__.split('# Tests')[0]
assert 'this_is_hdr' not in code, "La section HDR ne doit pas être visible dans l'IDE"

# --- HDR --- #

def func():
    return 42

# Tests

assert func() == this_is_hdr, "Found public tests: pass code but not corr"