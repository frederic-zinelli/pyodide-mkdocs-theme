---
ides:
    deactivate_stdout_for_secrets: false
---

## download csv data

{{ IDE_py('csv', TEST='code') }}

## Test `copy_from_server` tool

{{ IDE_py('copy_from_server', TEST='code') }}


## uploaders

### Upload async - text - multi

{{ IDE_py("upload-async", TEST=Case(code=1, human=1), before='!!!') }}


### Upload async - bytes

{{ IDE_py("upload_bytes", TEST=Case(code=1, human=1), before='!!!') }}


### Upload async - img

{{ IDE_py("upload_img", TEST=Case(code=1, human=1), before='!!!') }}

{{ figure("img-target2") }}


### Upload sync

{{ IDE_py("upload", TEST=Case(code=1, human=1), before='!!!') }}


### Downloads

Voir [la page de la docs](--pyodide-download)