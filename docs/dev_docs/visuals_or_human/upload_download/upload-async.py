# --- PYODIDE:env --- #
print("env!")

def upload(content:str, name):
    print("This will show up in the terminal!")
    print(name)
    return content[:30]+' [...]'

data = await pyodide_uploader_async(upload, with_name=True, multi=True)
# available NOW!

# --- PYODIDE:code --- #
print("From code, data is a tuple:", *data, sep='\n    ')


# --- PYODIDE:post --- #
print("post!")
