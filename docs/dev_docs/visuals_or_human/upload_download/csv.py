# --- PYODIDE:env --- #
from py_libs import *

animaux = []

from js import fetch
url_fichier = "zoo_traduction.csv"
reponse = await fetch(url_fichier)
contenu = await reponse.text()

for ligne in contenu.strip().splitlines()[1:]:
    valeurs = ligne.strip().split(",")
    convert = list(map(bool,valeurs[1:]))
    convert[-4] = int(valeurs[-4])
    valeurs[1:] = convert
    animaux.append(valeurs)
print('Async file extraction successful...')


# --- PYODIDE:code --- #

print('loaded:', url_fichier)
truthy('animaux')
assert animaux, "not empty"