# --- PYODIDE:env --- #
from py_libs import *
from pathlib import Path

animaux = []

url_fichier = "zoo_traduction.csv"
await copy_from_server(url_fichier)
contenu = Path(url_fichier).read_text(encoding='utf-8')

for ligne in contenu.strip().splitlines()[1:]:
    valeurs = ligne.strip().split(",")
    convert = list(map(bool,valeurs[1:]))
    convert[-4] = int(valeurs[-4])
    valeurs[1:] = convert
    animaux.append(valeurs)
print('Copy from server successful...')


# --- PYODIDE:code --- #
x = [
    ['tortue', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['caméléon', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['iguane', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['lézard', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['gecko', True, True, True, True, True, True,True, True, True, True, True, True, 4, True, True, True],
    ['python', True, True, True, True, True, True, True, True,True, True, True, True, 0, True, True, True],
    ['boa', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['vipère', True, True, True, True, True, True, True, True, True, True, True, True, 0,True, True, True],
    ['crocodile', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['alligator', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['gavial', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['skink',True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['anchois', True, True,True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['limande', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['flétan', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['maquereau', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['barracuda', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['makaire', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['truite', True, True, True, True, True, True, True, True, True, True, True, True, 0,True, True, True],
    ['salamandre', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['sirenidae', True, True, True, True, True, True, True, True, True, True, True, True, 2, True, True, True],
    ['grenouille arboricole', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['grenouille à fléchettes empoisonnées', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['vers luisant', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['crapaud', True, True, True, True, True, True, True, True, True, True, True, True, 4, True, True, True],
    ['moustique', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['frelon', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['criquet', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['scarabé', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['papillon', True, True, True, True,True, True, True, True, True, True, True, True, 6, True, True, True],
    ['blatte', True, True, True, True, True, True,True, True, True, True, True, True, 6, True, True, True],
    ['cafard', True, True, True, True, True, True, True, True,True, True, True, True, 6, True, True, True],
    ['mante religieuse', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['libellule', True, True, True, True, True, True, True, True, True, True,True, True, 6, True, True, True],
    ['puceron', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['cigale', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['fourmi', True, True, True, True, True, True, True, True, True, True, True, True, 6, True, True, True],
    ['coquille Saint-Jacques', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['araignée', True, True, True, True, True, True, True, True, True, True, True, True, 8, True, True, True],
    ['escargot', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['vers à soie',True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['méduse', True, True,True, True, True, True, True, True, True, True, True, True, 0, True, True, True],
    ['sèche', True, True, True, True, True, True, True, True, True, True, True, True, 0, True, True, True]
]

print('loaded:', url_fichier)
truthy('animaux')
assert animaux, "not empty"