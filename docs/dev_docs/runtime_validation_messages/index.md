---
ides:
    deactivate_stdout_for_secrets: false
    forbid_secrets_without_corr_or_REMs: false
---


# Vérifie la structure des messages relatifs aux tests et quand ils sont affichés


__Tous les tests de cette page sont prévus pour être utilisés via la page globale de test des IDEs.__


## Test publics

### No corr/rems/secrets

{{ IDE_py(
    '',
    TEST=Case(
        title="No corr/REMs/secrets: no special message on play",
        run_play = 1,
        assertions = "!corrRemsMask !hasCheckBtn",
        reveal_corr_rems = False,
        none_in_std = [
            Lang('unforgettable'),
            Lang('success_msg'),
            Lang('editor_code'),
        ],
        all_in_std= [
            Lang('run_script'),
            Lang('success_msg_no_tests')
        ],
    )
) }}



### No corr/rems with secrets

{{ IDE_py( 'exo_no_corr',
    TEST=Case(
        title="play with secrets => 'Essayer les validations'",
        run_play=1,
        code=1,
        reveal_corr_rems=False,
        assertions  = "!corrRemsMask hasCheckBtn",
        none_in_std = [Lang('success_msg_no_tests')],
        all_in_std  = [
            Lang('run_script'),
            Lang('editor_code'),
            Lang('success_msg'),
            Lang('unforgettable')
        ],
    )
) }}





## Validations

### With corr/rems/secrets: reveal when no more attempts or correct

WARNING: set_max_and_hide= in between each test.

{{ IDE_py(
    'exo', MAX=1, TERM_H=15, ID=1,
    TEST = Case.REVEAL_FAIL.with_(
        description="Solution is revealed on success or failure (MAX=1, editor)",
        decrease_attempts_on_user_code_failure='editor',
        set_max_and_hide=1,
    ).times(11,
        with_={
            1: Case.REVEAL_SUCCESS.with_(no_clear=False),
            2: Case.ERROR_NO_REVEAL.with_(delta_attempts=0, all_in_std=[]),
            (10,11): Case.ERROR_IN_POST_REVEAL,
        })
) }}





### Same, but with `decrease_attempts_on_user_code_failure: "public"`

WARNING: set_max_and_hide= in between each test.

{{ IDE_py(
    'exo', MAX=1, TERM_H=15, ID=2,
    TEST = Case.REVEAL_FAIL.with_(
        description="Solution is revealed on success or failure (MAX=1, public)",
        decrease_attempts_on_user_code_failure='public',
        set_max_and_hide=1,
    ).times(11,
        with_={
            1:          Case.REVEAL_SUCCESS.with_(no_clear=False),
            (2,3,4,5):  Case.ERROR_NO_REVEAL.with_(delta_attempts=0, all_in_std=[]),
            (10,11):    Case.ERROR_IN_POST_REVEAL,
        })
) }}



### Same, but with `decrease_attempts_on_user_code_failure: "secrets"`

WARNING: set_max_and_hide= in between each test.

{{ IDE_py(
    'exo', MAX=1, TERM_H=15, ID=3,
    TEST = Case.ERROR_NO_REVEAL.with_(
        delta_attempts=0,
        description="Solution is revealed on success or failure (MAX=1, secrets)",
        decrease_attempts_on_user_code_failure='secrets',
        set_max_and_hide=1,
    ).times(11,
        with_={
            1:       Case.REVEAL_SUCCESS.with_(no_clear=False),
            (8,9):   Case.REVEAL_FAIL,
            (10,11): Case.ERROR_IN_POST_REVEAL,
        })
) }}