
# --- PYODIDE:env --- #
from py_libs import *
auto_N(globals())
print('Case', N)
if do_it(): pass
if do_it(): raise KeyError(f"RAISING! from env")
if do_it(): assert False, "from env"
print('--env--')


# --- PYODIDE:corr --- #
if do_it(): raise KeyError(f"RAISING! from corr")
if do_it(): assert False, "from corr"
print('--corr--')

# --- PYODIDE:tests --- #
# WARNING: in test_ides, this content is NOT part of the first validation step (editor),
#          on the contrary of a manual execution in the dev_docs page.
if do_it(): raise KeyError(f"RAISING! from tests")
if do_it(): assert False, "from tests"
print('public tests...')

# --- PYODIDE:secrets --- #
if do_it(): raise KeyError(f"RAISING! from secrets")
if do_it(): assert False, "from secrets"
print('--secrets--')

# --- PYODIDE:post --- #
if do_it(): raise KeyError(f"RAISING! from post")
if do_it(): assert False, "from post"
print('--post--')
