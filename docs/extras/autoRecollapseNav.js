
/** Automatically collapse any previously expanded nav section, when clicking on a new one.
 * */
$('label.md-nav__link').on('click',function(){
  const alreadyOpened = $(this).next().attr('aria-expanded')=='true'
  const that = this
  if(!alreadyOpened) $('label.md-nav__link').each(function(){
    if(this!==that && $(this).next().attr('aria-expanded')=='true'){
            setTimeout(_=>$(this).trigger('click'), 150)
    }
  })
})