import {
  buttonWithTooltip,
  buildJqCheckBox,
  buildJqText,
  buildJqTextArea,
  buildJqSelect,
  downloader,
  uploader,
  makeIdeJqButton,
} from "functools";





// SVG codes are coming from mkdocs-material:
const PLUS_SVG  = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="white"><path d="M20 14h-6v6h-4v-6H4v-4h6V4h4v6h6z"/></svg>'
const MINUS_SVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="white"><path d="M19 13H5v-2h14z"/></svg>'
// const UP_SVG    = ''
// const DOWN_SVG  = ''


let uuid = 0

const BOOLS =  Object.freeze(['unset', false, true])

const config = {
  skipDefaults: true,
  filename: 'qcm.json'
}


const TOOLTIP = {
  description: "Description optionnelle, ajoutée avant les questions, dans l'admonition (si utilisée).",
  hide: "Si vrai, la correction du QCM n'est pas affichée, seul le nombre de points est calculé.",
  multi: "Si utilisé, définit comment sera considérée une question ayant une seule bonne réponse (choix multiple ou simple).",
  shuffle: "Si vrai, les questions et les choix de chaque question sont mélangés à chaque réinitialisation.",
  shuffle_questions: "Si vrai, seules les questions sont mélangées à chaque réinitialisation.",
  shuffle_items: "Si vrai, seuls les choix de chaque question sont mélangés à chaque réinitialisation.",
  admo_kind: "Définit le type de l'admonition utilisée pour englober le qcm (null: pas d'admonition / défaut: !!!).",
  admo_class: "Pour choisir la ou les classes html appliquées à l'admonition (défaut: tip).",
  qcm_title: "Pour définir le titre de l'admonition autour du qcm (défaut: Question, singulier ou pluriel, selon le nombre de questions).",
  tag_list_of_qs: "Si défini, permet de forcer le type de puces utilisé pour les intitulés de questions (défaut: null <=> automatique).",
  DEBUG: "Si vrai, le code markdown généré pour ce qcm sera affiché dans la console durant le build.",
}



const buildDefaultQcmObject=()=>({...defaults, questions: []})

const defaults = Object.freeze({
  // questions:      [],    // Not defined here on purpose (see qcmToMacroData)
  description:       '',
  hide:              'unset',
  multi:             'unset',
  shuffle:           'unset',
  shuffle_questions: 'unset',
  shuffle_items:     'unset',
  admo_kind:         'unset',
  admo_class:        '',
  qcm_title:         '',
  tag_list_of_qs:    'unset',
  DEBUG:             'unset',
})

const questionDefaults = Object.freeze({
  description: "",
  items: [],
  multi: 'unset',
  shuffle: 'unset',
})

const itemDefaults = Object.freeze({
  correct: false,
  txt: ""
})





/**The "global" live object (shared with all functions)
 * */
const qcm = buildDefaultQcmObject()










/**Build the complete JSON string declaration representing the current state of the qcm object,
 * using an automatic indent of 2 spaces to make it humanly readable (mostly...).
 */
const toQcmJsonString=()=>{


  /**Strip the last built line from its comma (JSON objects/arrays requirement)
   * */
  const removeComma=()=>{
    const last  = lines.length-1
    lines[last] = lines[last].slice(0,-1)
  }


  /**Convert the top layer of the wcm objects, then the array of questions.
   * */
  const questionsToJson=(qsArr)=>{
    for(const [q,items,corrects,dct] of qsArr){
      lines.push('    [',
                 `      ${ JSON.stringify(q) },`,
                 '      [',)
      for(const item of items) lines.push(
                 `        ${ JSON.stringify(item) },`
      )
      if(qsArr.length) removeComma()
      lines.push('      ],')
      lines.push(`      ${ JSON.stringify(corrects) },`)
      if(dct) lines.push(`      ${ JSON.stringify(dct) },`)
      removeComma()
      lines.push('    ],')
    }
    removeComma()
  }


  /**Convert the qcm.questions item and its content
   * */
  const qcmDataToJson=()=>{
    lines.push(
      '{',
      '  "questions": [')
    questionsToJson(data.questions)
    lines.push('  ],')

    for(const k in data) if(k!='questions'){
      lines.push(`  ${JSON.stringify(k)}: ${JSON.stringify(data[k])},`)
    }
    removeComma()
    lines.push('}')

    return lines.join('\n') + '\n'
  }


  const data    = qcmToMacroData()
  const lines   = []
  const content = qcmDataToJson()
  return content
}








/**Convert the qcm global object to match the structure of the `multi_qcm` marco arguments,
 * Removing items that are matching the default values if config.skipDefaults is true.
 * */
const qcmToMacroData=()=>{

  /**Convert the object of one question of the qcm to the equivalent array for the `questions`
   * macro call argument.
   * */
  const questionItemsToMacroArray=(question)=>{
    const arr = [
      question.description,
      question.items.map(item=>item.txt),
      [],
    ]

    question.items.forEach((item,i)=>{
      if(item.correct) arr[2].push(i+1)
    })

    const dct = {}
    let keep  = false
    for(const k of ['shuffle','multi']) if(question[k]!=='unset'){
      dct[k] = question[k]=='true'
      keep = true
    }
    if(keep) arr.push(dct)
    return arr
  }

  const macroData = {}
  Object.entries(qcm).forEach(([prop,val])=>{
    if(val!=='unset' && val!==""){
      if(val==='true' || val==='false') val = val==='true'
      else if(val==='null') val = null
      macroData[prop] = val
    }
  })
  macroData.questions = qcm.questions.map(questionItemsToMacroArray)
  return macroData
}










/**Reset the GUI and define all the elements in the given QCM JSON object.
 * The source object has to be compliant with the multi_qcm macro arguments
 * structure (hence, the questions are arrays of arrays and will be converted
 * appropriately on the fly).
 * */
const applyQcmData=(obj)=>{

  // Cancel current state
  questionsHolder.children().off().remove()
  qcm.questions.length = 0

  // Update the UI and the related item of the qcm object:
  for(const prop in obj){

    const val         = obj[prop]
    const valid       = prop in defaults
    const isQuestions = prop=='questions'

    if(isQuestions || !valid) continue

    qcm[prop] = obj[prop]
    const jUi = $(`#${ prop }-input`)
    jUi.val(val+"")
  }

  // Handle the creation of the questions and their choices:
  for(const [description, items, corrects, dct] of obj.questions??[]){
    const question={
      description,
      items:   items.map((item,i)=>({txt:item, correct:corrects.includes(i+1)})),
      multi:   (dct && dct.multi) ?? questionDefaults.multi,
      shuffle: (dct && dct.shuffle) ?? questionDefaults.shuffle
    }
    questionsHolder.append(
      createJqQuestion(qcm, question, true)
    )
  }
}









/**Build bare divs from the given strings.
 * @returns: - A jQuery <div> object if only one argument.
 *           - An array of jQuery <div> objects if several arguments.
 * */
const jDiv=(...elements)=>{
  const out = elements.map( s => $(`<div>${ s }</div>`) )
  return elements.length==1 ? out[0] : out
}




const nestingGrid=(topElement, ...levels)=>{
  const {
    tag,          // Tag name of the top element
    attrs,        // Extra attributes as strings (layout classes are automatically added)
    horizontal,   // Is horizontal grid (means the number of columns will be set automatically)
    gap,          // grid-gap (as a number of pixels). Applied if defined only (override the default)

    // --- Impact only horizontal stuff:
    lastAuto,     // If true, the last column width will be set to auto instead of max-content

  } = topElement

  const obj = $(`<${tag} ${ attrs }></${tag}>`)
  obj.addClass('qcm-grid')
     .addClass(horizontal ? 'horizontal':'vertical')

  if(gap!==undefined) obj.css('grid-gap', gap+'px')

    for(const {appendTo, nested} of levels){

      const target = !appendTo ? obj : obj.find(appendTo)
      target.append(nested)

      const isHorz = target.hasClass('horizontal')
      const hasLeg = isHorz && target.children('legend').length > 0
      const nCols  = isHorz && nested.length - hasLeg - (lastAuto||0)

      if(isHorz && nCols > 0){
        const template =`repeat(${ nCols }, max-content)${ lastAuto?" auto":"" }`
        target.css('grid-template-columns', template)
      }
    }
  return obj
}






const description = buildJqTextArea(
  qcm, 'description', {
    tagId: "qcm-description",
    resize:'vertical',
    placeholder: "(introduction du qcm)",
    tipWidth:15, tipClass:'top', tipText: TOOLTIP.description
  }
)

const optionsConfig = {
  tagClass:   'qcm-grid horizontal inline-qcm',
  inputClass: 'qcm-select-width',
  tipClass:   'right',
  tipWidth:   15,
}


const globalOptions = nestingGrid(
  {
    tag: 'fieldset',
    attrs: 'class="global-qcm-options r2 c1 full-height full-width"'
  },{
    appendTo: "", nested: [
      '<legend>Options globales</legend>',
      nestingGrid({tag: 'div', attrs: 'class="global-qcm-options fit-height"'}),
    ]
  },{
    appendTo: "div.global-qcm-options", nested: [
      ...`
        hide
        multi
        shuffle
        shuffle_questions
        shuffle_items
        DEBUG
      `.trim().split(/\s+/)
       .map( prop => buildJqSelect(qcm, prop, BOOLS, {...optionsConfig, tipText: TOOLTIP[prop]}) ),
       buildJqSelect(qcm, 'admo_kind', ['unset', '!!!', '???', '???+', null], {...optionsConfig, tipText: TOOLTIP.admo_kind}),
       buildJqSelect(qcm, 'tag_list_of_qs', ['unset', null, 'ol', 'ul'], {...optionsConfig, tipText: TOOLTIP.tag_list_of_qs}),
       buildJqText(qcm, 'admo_class', {placeholder: 'tip',     tipWidth:15, tipClass:'right', tipText: TOOLTIP.admo_class}),
       buildJqText(qcm, 'qcm_title', {placeholder: 'Question', tipWidth:15, tipClass:'right', tipText: TOOLTIP.qcm_title}),
    ]
  },
)



const questionsArea = nestingGrid(
  {tag: 'fieldset', attrs: 'class="qcm-questions full-size r2 c2"'},
  {
    appendTo: "",
    nested: [
      '<legend>Questions</legend>',
      nestingGrid({tag: 'div', attrs: 'class="qcm-questions fit-height full-width"'}),
    ]
  },
  {
    appendTo: "div.qcm-questions",
    nested: [
      nestingGrid({
        tag: 'div', horizontal: true,
        attrs: 'class="add-qcm-question fit-width"',
      }),
      nestingGrid({tag: 'div', attrs: 'id="qcm-question-holder"'}),
    ]
  },
  {
    appendTo: "div.add-qcm-question", nested:[
      jDiv("Ajouter une question :"),
      $(buttonWithTooltip(
        {tagClass: "qcm-builder-btn", tipText: "Ajouter une question", tipWidth: 0},
        PLUS_SVG
      )).on('click', function(){ questionsHolder.append(createJqQuestion(qcm)) }),
    ]
  }
)

const questionsHolder = questionsArea.find("#qcm-question-holder")


const exportationsBtns = [

  makeIdeJqButton('check', {
    tagId: "copy-json",
    tipText: "Copier le code Json dans le presse-papier.",
    shift: 50,
    tipWidth: 15,
  }).on('click',async ()=>{
                  const content = toQcmJsonString()
                  await navigator.clipboard.writeText(content)
                }),

  makeIdeJqButton('download', {
    tagId: "export-json",
    tipText: "Télécharger le fichier JSON correspondant.",
    shift: 50,
    tipWidth: 15,
  }).on('click',()=>{
                  const content = toQcmJsonString()
                  downloader(content, config.filename, "application/json")
                }),

  makeIdeJqButton('upload', {
    tagId: "upload-json",
    tipText: "Charger le contenu d'un fichier JSON de QCM",
    shift: 50,
    tipWidth: 15,
  }).on('click',()=>{ uploader(jsonTxt=>{
                        const macroArgs = JSON.parse(jsonTxt)
                        applyQcmData(macroArgs)
                      })
                    }),

  makeIdeJqButton('restart', {
    tagId: "restart-json",
    tipText: "Effacer le contenu actuel",
    shift: 50,
    tipWidth: 15,
  }).on('click', ()=>applyQcmData( buildDefaultQcmObject() )),

]



const exportBtns = nestingGrid(
  {
    tag: 'fieldset',
    attrs: 'id="export-qcm" class="full-width r3"',
    horizontal: true,
  },
  {
    appendTo: "",
    nested: [
      '<legend>Exports</legend>',
      nestingGrid({tag: 'div', attrs: 'class="qcm-config"'}),
      nestingGrid({tag: 'div', attrs: 'class="qcm-export-btns"', horizontal: true, gap: 0}),
    ]
  },
  {
    appendTo: "div.qcm-config",
    nested: [
      // buildJqCheckBox(config, 'skipDefaults', {label: 'Ignorer les valeurs par défaut'}),
      buildJqText(config, 'filename'),
    ]
  },
  {
    appendTo: "div.qcm-export-btns",
    nested: exportationsBtns,
  }
)






// Build the whole thing...
$("#qcm-builder-tool").addClass("qcm-grid full-width")
  .append(
    "<legend>Création d'un fichier pour QCM (Json)</legend>",
    description,
    globalOptions,
    questionsArea,
    exportBtns,
  )






const createJqQuestion=(qcm, data={}, loading=false, idx)=>{
  const qId = ++uuid
  const itemAdderId = `add-qcm-item-${ qId }`

  const question = {...questionDefaults, items:[], ...data}

  idx ??= qcm.questions.length
  qcm.questions.splice(idx, 0, question)


  const jQuestion = nestingGrid(
    {tag: 'fieldset', attrs: 'class="qcm-question"'},
    {nested: [
        '<legend>Question</legend>',
        nestingGrid({tag: 'div', attrs: 'class="qcm-question-options"', horizontal: true}),
        buildJqTextArea(
          question, 'description',
          {tagId:"question-desc"+qId, resize:'vertical', placeholder:"(intitulé de question)"},
        ),
        nestingGrid({tag: 'fieldset', attrs: 'class="qcm-question-items"'}),
    ]},
    {
      appendTo: ".qcm-question-options",
      nested: [
        $(buttonWithTooltip({
            tagId: `del-question-${ qId }`,
            tagClass: "qcm-builder-btn",
            tipText: "Supprimer cette question",
            tipWidth: 0,
          }, MINUS_SVG)).on('click', function(){
            const i = qcm.questions.indexOf(question)
            qcm.questions.splice(i, 1)
            jQuestion.off().remove()
          }),
        ...jDiv('Supprimer', '|'),
        buildJqSelect(question, 'multi',   BOOLS, {...optionsConfig, inputId: `multi-${   qId }`}),
        buildJqSelect(question, 'shuffle', BOOLS, {...optionsConfig, inputId: `shuffle-${   qId }`}),
        // buildJqCheckBox(question, 'multi',   {inputId: `multi-${   qId }`}),
        // buildJqCheckBox(question, 'shuffle', {inputId: `shuffle-${ qId }`}),
      ]
    },
    {
      appendTo: ".qcm-question-items",
      nested: [
        '<legend>Options</legend>',
        nestingGrid(
          {tag: 'div', horizontal: true},
          {nested:[
            ...jDiv('Supprimer', '|', 'Correct', '|', 'Intitulé', '||', 'Nouvelle option :'),
            $(buttonWithTooltip(
                {tagId: itemAdderId, tagClass: "qcm-builder-btn", tipText: "Ajouter un choix", tipWidth: 0,},
                PLUS_SVG
              )).on('click', function(){
                itemsHolder.append(createOption(question.items))
              })
          ]}
        ),
      ]
    }
  )

  const itemAdder   = jQuestion.find("#"+itemAdderId)
  const itemsHolder = jQuestion.find(".qcm-question-items")

  if(!loading){
    itemAdder.trigger('click')
  }else{
    for(const item of question.items){
      itemsHolder.append(createOption(question.items, item))
    }
  }
  return jQuestion
}




const createOption=(itemsArr, item)=>{
  if(!item){
    item = {...itemDefaults}
    itemsArr.push(item)
  }

  const iId = ++uuid

  const jItem = nestingGrid(
    {tag: 'div', attrs: 'class="qcm-item"', horizontal:true, lastAuto: true},
    {nested: [
      $(buttonWithTooltip({
            tagId: `suppress-qcm-item-${ iId }`,
            tagClass: "qcm-builder-btn",
            tipText: "Supprimer ce choix",
            tipWidth: 0,
          }, MINUS_SVG
        )).on('click', function(){
          const i = itemsArr.indexOf(item)    // Extract actual index at runtime (might change in the future)
          itemsArr.splice(i, 1)
          jItem.off().remove()
        }),
      buildJqCheckBox(item, 'correct', {inputId:`correct-${ iId }`, noLabel:true, tipText:"Choix correct"}),
      buildJqTextArea(item, 'txt', {tagId: `txt-${ iId }`,
                                    noLabel: true,
                                    resize: 'vertical',
                                    placeholder: "(option)"}),
    ]},
  )
  return jItem
}
