---
args:
    IDE:
        TERM_H: 2
        TEST: skip
testing:
    empty_section_fallback: skip
---

Des éditeurs dans lesquels faire des essais quelconques...
{{pmt_note("Pour tout essai utilisant des figures, il est conseillé de passer l'IDE concerné en [mode 2 colonnes](--user-editors).")}}

<br>

=== "Bac à sable"

    `{% raw %}{{ IDE() }}{% endraw %}`

    {{ IDE('exemple/await_sandbox', MIN_SIZE=15, TERM_H=10, MERMAID=True, TEST="") }}


    ??? tip "Essais async"

        Si une fonction `#!py async def post_async()` est définie dans l'éditeur, elle sera automatiquement appelée depuis la section `post` de cet IDE (en effet, un fichier est en fait associé à cet IDE, contrairement à ce qui est annoncé plus haut. Mais en dehors de cela, cet IDE se comporte exactement comme si aucun fichier ne lui était associé).

        __Ceci permet d'essayer du code `async` dans le bac à sable.__

        _Rappel : il est aussi possible d'exécuter du code async depuis le terminal._


=== "Bac à sable `p5`"

    Il est possible d'utiliser la seconde figure en bas de la page pour une seconde animation, si on le souhaite.

    Utiliser dans ce cas `#!py target="figure1"` et `#!py stop_others=False` pour les arguments de l'appel à `run(...)`.

    <br>

    {{ IDE('p5_processing/exemples/exo_1', MAX_SIZE=35, ID=1) }}

    {{ figure("figure_p5", admo_title="Passer la souris au-dessus du canevas", inner_text="(exécuter l'IDE)") }}


=== "`p5.Sketch`"

    Il est possible d'utiliser la seconde figure en bas de la page pour une second animation, si on le souhaite.

    Utiliser dans ce cas `#!py target="figure1"` et `#!py stop_others=False` pour les arguments de l'appel à `run(...)`.

    <br>

    {{ IDE('exemple/p5_sketch_sandbox', MAX_SIZE=40) }}

    {{ figure("figure_p5_sketch", admo_title="Cliquer sur le canevas...", inner_text="(exécuter l'IDE)") }}


=== "`mermaid`"

    {{ IDE_py('custom/exemples/mermaid', ID=1) }}

=== "`matplotlib`"

    {{ IDE_py('custom/exemples/plot_1', ID=1) }}


{{ figure(inner_text='', admo_kind="") }}


??? tip "Essais figures"

    La page contient une `<div>` cachée, ayant l'id html par défaut, `#!py "figure1"` de la macro `{% raw %}{{ figure() }}{% endraw %}`.

    L'IDE est également configuré avec l'argument [`MERMAID=True`](--IDE-MERMAID).

    Ceci permet de faire des essais concernant les fonctionnalités liées à [`matplotlib`](--custom/matplotlib/), [`p5`](--p5_processing/how_to/) et [`mermaid`](--custom/mermaid/).
