À partir de la version 3.2.14, il est {{orange("possible de lire et stocker des informations dans le localStorage, concernant l'IDE en cours d'exécution")}} (moyennant certaines restrictions concernant l'ajout de données).


### Exemple

Les fonctions peuvent être utilisées directement depuis pyodide :

```python
# --- PYODIDE:env --- #
use_this = get_storage('last_time')

# --- PYODIDE:code --- #
# Mise à jour de `use_this` à un moment où un autre... (par l'utilisateur
# ou le code du rédacteur, dans la section secrets, par exemple)

# --- PYODIDE:post --- #
set_storage('last_time', use_this)
```



### `get_storage(key)` { #get-storage }

* La fonction n'est {{red("_pas_ utilisable depuis le terminal")}} d'un IDE.

* Toutes les propriétés stockées pour l'IDE en cours sont lisibles.

* Accéder à une valeur non définie dans le localStorage renvoie `None`.



### `set_storage(key, value)` { #set-storage }

* La fonction n'est {{red("_pas_ utilisable depuis le terminal")}} d'un IDE.

* {{red("Il n'est pas possible de modifier les propriétés définies par le thème")}} lui-même.
<br>Une erreur est levée dans ce cas (de type `pyodide.ffi.JsException`).

* Pour éviter de se retrouver avec des données inutilisables, des fuites de mémoire, et pour éviter tous problèmes de conversion de données, les valeurs utilisées pour le second argument ne peuvent être {{red("que des nombres ou des chaînes de caractères")}}. Une erreur est levée dans le cas contraire (de type `pyodide.ffi.JsException`).
<br>Pour utiliser des objets plus complexes, il faudra passer par des conversions `objet -> str -> localStorage` puis `localStorage -> str -> objet`, gérées depuis les codes python.



### `del_storage(key)` { #del-storage }

* La fonction n'est {{red("_pas_ utilisable depuis le terminal")}} d'un IDE.

* {{red("Il n'est pas possible de supprimer les propriétés définies par le thème")}} lui-même.
<br>Une erreur est levée dans ce cas (de type `pyodide.ffi.JsException`).




### `keys_storage()` { #keys-storage }

* La fonction n'est {{red("_pas_ utilisable depuis le terminal")}} d'un IDE.

* Renvoie une liste python de toutes les clefs définies pour l'IDE en cours dans le localStorage.


<br>
<br>


??? note "Versions 3.2.7+"

    Les fonctionnalités initiales ont en fait été ajoutées depuis la version 3.2.7, mais uniquement avec des fonctions accédées depuis le module `js`, et pour les fonctions `getStorage` et `setStorage`.