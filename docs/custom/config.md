Le plugin `pyodide_macros` vient avec le thème et permet de configurer les comportements de celui-ci.




## Architecture globale { #global-architecture }

Le plugin `pyodide_macros` peut être configuré via le fichier `mkdocs.yml`, les options disponibles étant les suivantes :

<br>

--8<-- "docs_tools/inclusions/config_plugin.md"




## Détails

<br INSERTION_TOKEN>


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.ArgsConfig') }}`args` { #pyodide_macros.args .doc .doc-heading }

_Classe : `#!py ArgsConfig`_ | _Étend : `#!py CopyableConfig, Config`_

Réglages des arguments par défaut accessibles pour les différentes macros du thème. Explications détaillées dans la page [Aide rédacteurs/Résumé](--redactors/resume/).



<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig') }}`build` { #pyodide_macros.build .doc .doc-heading }

_Classe : `#!py BuildConfig`_ | _Étend : `#!py CopyableConfig, Config`_

Réglages concernant la construction de la documentation ou qui impactent la façon dont le contenu des pages est construit.
<br><br><br>

| Options disponibles | Valeur par défaut |
|-|:-:|
| [`deprecation_level`](#pyodide_macros.build.deprecation_level) | `#!yaml "error"` |
    | [`encrypted_js_data`](#pyodide_macros.build.encrypted_js_data) | `#!yaml true` |
    | [`forbid_macros_override`](#pyodide_macros.build.forbid_macros_override) | `#!yaml true` |
    | [`ignore_macros_plugin_diffs`](#pyodide_macros.build.ignore_macros_plugin_diffs) | `#!yaml false` |
    | [`limit_pypi_install_to`](#pyodide_macros.build.limit_pypi_install_to) | `#!yaml null` |
    | [`load_yaml_encoding`](#pyodide_macros.build.load_yaml_encoding) | `#!yaml "utf-8"` |
    | [`macros_with_indents`](#pyodide_macros.build.macros_with_indents) | `#!yaml []` |
    | [`meta_yaml_allow_extras`](#pyodide_macros.build.meta_yaml_allow_extras) | `#!yaml false` |
    | [`meta_yaml_encoding`](#pyodide_macros.build.meta_yaml_encoding) | `#!yaml "utf-8"` |
    | [`python_libs`](#pyodide_macros.build.python_libs) | `#!yaml ["py_libs"]` |
    | [`skip_py_md_paths_names_validation`](#pyodide_macros.build.skip_py_md_paths_names_validation) | `#!yaml false` |
    | [`tab_to_spaces`](#pyodide_macros.build.tab_to_spaces) | `#!yaml -1` |

---

<br>






#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.deprecation_level') }}`#!py deprecation_level = C.Choice(('error', 'warn'), default='error')` { #pyodide_macros.build.deprecation_level .doc .doc-heading .fake-h4 }

Comportement utilisé lors d'un build/serve lorsqu'une option obsolète est utilisée.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.encrypted_js_data') }}`#!py encrypted_js_data = C.Type(bool, default=True)` { #pyodide_macros.build.encrypted_js_data .doc .doc-heading .fake-h4 }

Si `True`, les données de configuration des IDEs, terminaux et py_btns sont encodées. Si des problèmes de décompression des données sont rencontrés, cette option peut être désactivée.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.forbid_macros_override') }}`#!py forbid_macros_override = C.Type(bool, default=True)` { #pyodide_macros.build.forbid_macros_override .doc .doc-heading .fake-h4 }

Si `True`, `PyodideMacrosError` est levée lorsque deux macros du même nom sont enregistrées par le plugin.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.ignore_macros_plugin_diffs') }}`#!py ignore_macros_plugin_diffs = C.Type(bool, default=False)` { #pyodide_macros.build.ignore_macros_plugin_diffs .doc .doc-heading .fake-h4 }

Passer à `#!py True` pour éviter la vérification de compatibilité de la configuration du plugin `PyodideMacroPlugin` avec celle du plugin original des macros, `MacrosPlugin`.

??? note "Raisons de cette vérification"

    Le plugin du thème hérite de celui de la bibliothèque `mkdocs-macros-plugin`, `PyodideMacros`.

    Or, la configuration du plugin `MacrosPlugin` est faite "à l'ancienne", avec `config_scheme`, alors que celle de `PyodideMacroPlugin` utilise les classes `Config` disponibles à partir de mkdocs `1.5+`. Les deux étant incompatibles, cela à imposé de reporter en dur la configuration du plugin d'origine dans celle du thème. Ceci fait qu'une modification de la configuration du plugin d'origine pourrait rendre celle du thème inopérante et ceci sans préavis.

    Cette vérification permet donc d'assurer que le comportement des objets `MacrosPlugin` sera celui attendu. Si une différence est constatée entre les deux configurations, le build est donc avorté car il n'y a aucune garantie que le site construit puisse encore être correct.

    Si les modifications de `MacrosPlugin` sont mineures, il est possible qu'un build puisse tout de même fonctionner, et passer cette option à `#!py True` permettra donc de faire l'essai. À tenter à vos risques et périls...



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.limit_pypi_install_to') }}`#!py limit_pypi_install_to = C.Optional(C.ListOfItems(C.Type(str)))` { #pyodide_macros.build.limit_pypi_install_to .doc .doc-heading .fake-h4 }

Si cette liste est définie, seules les imports dont le nom de bibliothèque figure dans cette liste seront autorisés à déclencher une installation automatique depuis PyPI. Noter que :

* C'est le nom de l'import dans le code python qui doit être renseigné (ex : `PIL` pour interdire l'installation de `pillow`).
* Utiliser `[]` interdit toutes les installations automatiques depuis PyPI.
* Mettre cette option à `null` (valeur par défaut) autorise toutes les requêtes vers PyPI.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.load_yaml_encoding') }}`#!py load_yaml_encoding = C.Type(str, default='utf-8')` { #pyodide_macros.build.load_yaml_encoding .doc .doc-heading .fake-h4 }

Encodage à utiliser lors du chargement de données YAML avec les fonctionnalités originales de MacrosPlugin :

La méthode d'origine n'utilise aucun argument d'encodage, ce qui peut entraîner des comportements différents entre Windows et Linux (typiquement : lors de l'exécution d'un pipeline sur la forge EN par rapport au travail local sous Windows).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.macros_with_indents') }}`#!py macros_with_indents = C.ListOfItems(C.Type(str), default=[])` { #pyodide_macros.build.macros_with_indents .doc .doc-heading .fake-h4 }

Permet d'enregistrer des macros personnalisées (liste de chaînes de caractères), qui insèrent du contenu markdown multilignes, pour pouvoir indenter  correctement le contenu dans la page :

Une fois qu'une macro est enregistrée dans cette liste, elle peut appeler la méthode `env.indent_macro(markdown)` durant son exécution pour que le contenu généré soit indenté correctement par le plugin.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.meta_yaml_allow_extras') }}`#!py meta_yaml_allow_extras = C.Type(bool, default=False)` { #pyodide_macros.build.meta_yaml_allow_extras .doc .doc-heading .fake-h4 }

Définit s'il est possible d'ajouter dans les fichiers {{meta()}} des données autres que celles relatives au plugin lui-même.

Lorsque cette valeur est à `#!yaml false`, seules des options du plugin `pyodide_macros` sont autorisées, ce qui permet de valider l'intégralité du contenu du fichier, mais empêche par exemple de définir des variables pour les macros dans ces fichiers.<br>Si la valeur est à `#!yaml true`, il est alors possible d'ajouter d'autres variables, mais les fautes de frappes dans les premiers niveaux ne peuvent plus être identifiées (exemple : `temrs.cut_feedback` au lieu de `terms.cut_feedback`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.meta_yaml_encoding') }}`#!py meta_yaml_encoding = C.Type(str, default='utf-8')` { #pyodide_macros.build.meta_yaml_encoding .doc .doc-heading .fake-h4 }

Encodage utilisé pour charger les [fichiers `.meta.pmt.yml`](--custom/metadata/).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.python_libs') }}`#!py python_libs = C.ListOfItems(C.Type(str), default=['py_libs'])` { #pyodide_macros.build.python_libs .doc .doc-heading .fake-h4 }

Liste de répertoires de [bibliothèques python](--custom-libs) qui doivent être importables dans Pyodide.

Une erreur est levée si :

* Le nom donné ne correspond pas à un répertoire existant (sauf s'il s'agit de la valeur par défaut, `#!py "py_libs"`).
* Le répertoire n'est pas situé à la racine du projet.
* Le répertoire n'est pas une bibliothèque Python (c'est-à-dire qu'il ne contient pas de fichier `__init__.py`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.skip_py_md_paths_names_validation') }}`#!py skip_py_md_paths_names_validation = C.Type(bool, default=False)` { #pyodide_macros.build.skip_py_md_paths_names_validation .doc .doc-heading .fake-h4 }

Par défaut, les noms de chemin de tous les fichiers `.py` et `.md` présents dans le `docs_dir` sont vérifiés pour s'assurer qu'ils ne contiennent aucun caractère autre que des lettres, des chiffres, des points ou des tirets. Cela garantit le bon fonctionnement des macros liées aux IDEs.

Si des caractères indésirables sont détectés, une erreur de type `BuildError` est levée. Cependant, cette vérification peut être désactivée en assignant `True` à ce paramètre. ... À Utiliser  à vos risques et périls.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.BuildConfig.tab_to_spaces') }}`#!py tab_to_spaces = C.Type(int, default=-1)` { #pyodide_macros.build.tab_to_spaces .doc .doc-heading .fake-h4 }

Si cette option est définie avec une valeur positive (ou nulle), les tabulations trouvées avant un appel à une macro multiligne (voir l'option [`macros_with_indent`](--pyodide_macros_build_macros_with_indents)) seront automatiquement converties en utilisant ce nombre d'espaces.

__Aucune garantie n'est alors donnée quant à la correction du résultat__. <br>Si une conversion est effectuée, un avertissement sera affiché dans la console pour faciliter la localisation et la modification des appels de macros responsables du warning.

!!! warning "Éviter les caractères de tabulation dans la documentation"

    Régler votre éditeur de code de manière à ce qu'il remplace automatiquement les tabulations par des espaces.

    Les caractères de tabulation ne sont pas toujours interprétés de la même façon selon le contexte d'utilisation du fichier, tandis que les fichiers markdown reposent en bonne partie sur les indentations pour définir la mise en page des rendus. <br>Les tabulations sont donc à proscrire.


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig') }}`ides` { #pyodide_macros.ides .doc .doc-heading }

_Classe : `#!py IdesConfig`_ | _Étend : `#!py CopyableConfig, Config`_

Réglages spécifiques aux IDEs (comportements impactant l'utilisateur et les exécutions).
<br><br><br>

| Options disponibles | Valeur par défaut |
|-|:-:|
| [`ace_style_dark`](#pyodide_macros.ides.ace_style_dark) | `#!yaml "tomorrow_night_bright"` |
    | [`ace_style_light`](#pyodide_macros.ides.ace_style_light) | `#!yaml "crimson_editor"` |
    | [`deactivate_stdout_for_secrets`](#pyodide_macros.ides.deactivate_stdout_for_secrets) | `#!yaml true` |
    | [`decrease_attempts_on_user_code_failure`](#pyodide_macros.ides.decrease_attempts_on_user_code_failure) | `#!yaml "editor"` |
    | [`editor_font_family`](#pyodide_macros.ides.editor_font_family) | `#!yaml "monospace"` |
    | [`editor_font_size`](#pyodide_macros.ides.editor_font_size) | `#!yaml 15` |
    | [`encrypt_alpha_mode`](#pyodide_macros.ides.encrypt_alpha_mode) | `#!yaml "direct"` |
    | [`encrypt_corrections_and_rems`](#pyodide_macros.ides.encrypt_corrections_and_rems) | `#!yaml true` |
    | [`export_zip_prefix`](#pyodide_macros.ides.export_zip_prefix) | `#!yaml ""` |
    | [`export_zip_with_names`](#pyodide_macros.ides.export_zip_with_names) | `#!yaml false` |
    | [`forbid_corr_and_REMs_with_infinite_attempts`](#pyodide_macros.ides.forbid_corr_and_REMs_with_infinite_attempts) | `#!yaml true` |
    | [`forbid_hidden_corr_and_REMs_without_secrets`](#pyodide_macros.ides.forbid_hidden_corr_and_REMs_without_secrets) | `#!yaml true` |
    | [`forbid_secrets_without_corr_or_REMs`](#pyodide_macros.ides.forbid_secrets_without_corr_or_REMs) | `#!yaml true` |
    | [`show_only_assertion_errors_for_secrets`](#pyodide_macros.ides.show_only_assertion_errors_for_secrets) | `#!yaml false` |

---

<br>






#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.ace_style_dark') }}`#!py ace_style_dark = C.Type(str, default='tomorrow_night_bright')` { #pyodide_macros.ides.ace_style_dark .doc .doc-heading .fake-h4 }

Thème de couleur utilisé pour les éditeurs des IDEs en mode sombre ([liste des thèmes disponibles][ace-themes]: utiliser le noms des fichiers `js` sans l'extension).{{ pmt_note("Ce réglage est écrasé par l'ancienne façon de modifier le thème, en définissant `extra.ace_style.slate` dans le fichier mkdocs.yml.")}} )}}



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.ace_style_light') }}`#!py ace_style_light = C.Type(str, default='crimson_editor')` { #pyodide_macros.ides.ace_style_light .doc .doc-heading .fake-h4 }

Thème de couleur utilisé pour les éditeurs des IDEs en mode clair ([liste des thèmes disponibles][ace-themes]: utiliser le noms des fichiers `js` sans l'extension).{{ pmt_note("Ce réglage est écrasé par l'ancienne façon de modifier le thème, en définissant `extra.ace_style.default` dans le fichier mkdocs.yml.")}}



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.deactivate_stdout_for_secrets') }}`#!py deactivate_stdout_for_secrets = C.Type(bool, default=True)` { #pyodide_macros.ides.deactivate_stdout_for_secrets .doc .doc-heading .fake-h4 }

Détermine si la sortie standard (stdout) sera visible dans les terminaux lors des tests secrets ou non.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.decrease_attempts_on_user_code_failure') }}`#!py decrease_attempts_on_user_code_failure = C.Choice(('editor', 'public', 'secrets', True, False), default='editor')` { #pyodide_macros.ides.decrease_attempts_on_user_code_failure .doc .doc-heading .fake-h4 }

Les validations sont grossièrement constituées de 4 étapes, exécutant les éléments suivants :

1. La section `env`, qui ne devrait pas lever d'erreur sauf `AssertionError`.
1. Le contenu de l'éditeur (y compris l'état actuel des tests publics).
1. La section `tests` du fichier python, assurant que la version __originale__ des tests publics est toujours exécutée.
1. La section `secrets` du fichier python.

Les exécutions étant stoppées à la première erreur rencontrée, cette option définit à partir de quelle étape une erreur doit consommer un essai :

1. `#!py "editor"` : Une erreur levée lors de l'exécution de la section `env` ou du contenu de l'éditeur sera comptée comme un essai consommé.
1. `#!py "public"` : seules les erreurs levées depuis les étapes 3 et 4 décompteront un essai.
1. `#!py "secrets"` : seules les erreurs levées depuis la section `secrets` décompteront un essai.

--8<-- "docs_tools/inclusions/decrease_attempts_on_user_code_failure.md"

??? warning "Options booléennes"

    Les valeurs booléennes sont là uniquement pour la rétrocompatibilité et un warning apparaîtra dans la console si elles sont utilisées.

    * `True` correspond à `#!py "editor"`
    * `False` correspond à `#!py "secrets"`



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.editor_font_family') }}`#!py editor_font_family = C.Type(str, default='monospace')` { #pyodide_macros.ides.editor_font_family .doc .doc-heading .fake-h4 }

Police de caractère à utiliser pour les éditeurs des IDEs.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.editor_font_size') }}`#!py editor_font_size = C.Type(int, default=15)` { #pyodide_macros.ides.editor_font_size .doc .doc-heading .fake-h4 }

Taille de la police de caractère les éditeurs des IDEs.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.encrypt_alpha_mode') }}`#!py encrypt_alpha_mode = C.Choice(('direct', 'shuffle', 'sort'), default='direct')` { #pyodide_macros.ides.encrypt_alpha_mode .doc .doc-heading .fake-h4 }

Les contenus (codes, corrections & remarques) sont transmis de mkdocs aux pages html en utilisant des données compressées. L'encodage est réalisé avec l'algorithme LZW, et cette option contrôle la manière dont l'alphabet/la table initiale est construit à partir du contenu à encoder :

- `#!py "direct"` : l'alphabet utilise les symboles dans l'ordre où ils sont trouvés dans le contenu à compresser (utilisé par défaut).
- `#!py "shuffle"` : l'alphabet est mélangé aléatoirement.
- `#!py "sort"` : les symboles sont triés dans l'ordre naturel.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.encrypt_corrections_and_rems') }}`#!py encrypt_corrections_and_rems = C.Type(bool, default=True)` { #pyodide_macros.ides.encrypt_corrections_and_rems .doc .doc-heading .fake-h4 }

Si activé, le contenu de la div HTML de la correction et des remarques, sous les IDEs, sera compressé lors de la construction du site.

Désactiver ceci peut être utile durant le développement, mais {{ red("cette option doit toujours être activée pour le site déployé") }}, sans quoi la barre de recherche pourraient suggérer le contenu des corrections et des remarques à l'utilisateur.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.export_zip_prefix') }}`#!py export_zip_prefix = C.Type(str, default='')` { #pyodide_macros.ides.export_zip_prefix .doc .doc-heading .fake-h4 }

Préfixe ajouté au début du nom des archives zip créées avec les contenus des éditeurs des IDEs configurés comme exportable (argument `EXPORT=True`). Si `{{config_validator('ides.export_zip_prefix',tail=1)}}` n'est pas une chaîne vide, un trait d'union sera ajouté automatiquement entre le préfixe et le reste du nom de l'archive.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.export_zip_with_names') }}`#!py export_zip_with_names = C.Type(bool, default=False)` { #pyodide_macros.ides.export_zip_with_names .doc .doc-heading .fake-h4 }

Si `#!py True`, au moment où un utilisateur demandera de créer l'archive zip avec tous les codes des IDEs de la page, une fenêtre s'ouvrira lui demandant d'indiquer son nom. Une fois le nom renseigné, il sera ajouté entre l'éventuel préfixe (voir {{config_link( 'ides.export_zip_prefix', tail=1)}}) et le nom normal de l'archive zip, entouré par des traits d'union.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.forbid_corr_and_REMs_with_infinite_attempts') }}`#!py forbid_corr_and_REMs_with_infinite_attempts = C.Type(bool, default=True)` { #pyodide_macros.ides.forbid_corr_and_REMs_with_infinite_attempts .doc .doc-heading .fake-h4 }

Lors de la construction des IDEs, si une section `corr`, un fichier `REM` ou ` VIS_REM` existent et que le nombre de tentatives est illimité, ce contenu ne sera jamais accessible à l'utilisateur, sauf s'il réussit les tests.

Par défaut, cette situation est considérée comme invalide et `BuildError` sera levée. Si ce comportement est souhaité, passer cette option à `False`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.forbid_hidden_corr_and_REMs_without_secrets') }}`#!py forbid_hidden_corr_and_REMs_without_secrets = C.Type(bool, default=True)` { #pyodide_macros.ides.forbid_hidden_corr_and_REMs_without_secrets .doc .doc-heading .fake-h4 }

Lors de la construction des IDEs, le bouton de validation n'apparaît que si une section `secrets` existe. <br>Si des sections `corr` ou des fichiers `REM` existent alors qu'aucune section `secrets` n'est présente, leur contenu ne sera jamais disponible pour l'utilisateur en raison de l'absence de bouton de validation dans l'interface.

Par défaut, cette situation est considérée comme invalide et `BuildError` sera levée. Si ce comportement est souhaité, passer cette option à `False`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.forbid_secrets_without_corr_or_REMs') }}`#!py forbid_secrets_without_corr_or_REMs = C.Type(bool, default=True)` { #pyodide_macros.ides.forbid_secrets_without_corr_or_REMs .doc .doc-heading .fake-h4 }

Par défaut, cette situation est considérée comme invalide et `BuildError` sera levée. Si ce comportement est souhaité, passer cette option à `False`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.IdesConfig.show_only_assertion_errors_for_secrets') }}`#!py show_only_assertion_errors_for_secrets = C.Type(bool, default=False)` { #pyodide_macros.ides.show_only_assertion_errors_for_secrets .doc .doc-heading .fake-h4 }

Si activé (`True`), la stacktrace des messages d'erreur sera supprimée et seuls les messages des assertions resteront inchangées lorsqu'une erreur sera levée pendant les tests secrets.

| `AssertionError` | Pour les autres erreurs |
|:-:|:-:|
| {{ pmt_note("Option à `false`",0) }}<br>![AssertionError: message normal](!!show_assertions_msg_only__assert_full_png) | {{ pmt_note("Option à `false`",0) }}<br>![Autres erreurs: message normal](!!show_assertions_msg_only__error_full_png) |
| ![AssertionError: sans stacktrace](!!show_assertions_msg_only_assert_no_stack_png){{ pmt_note("Option à `true`") }} | ![Autres erreurs sans stacktrace ni message](!!show_assertions_msg_only_error_no_stack_png){{ pmt_note("Option à `true`") }} |


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.QcmsConfig') }}`qcms` { #pyodide_macros.qcms .doc .doc-heading }

_Classe : `#!py QcmsConfig`_ | _Étend : `#!py CopyableConfig, Config`_

Réglages spécifiques aux QCMs.
<br><br><br>

| Options disponibles | Valeur par défaut |
|-|:-:|
| [`forbid_no_correct_answers_with_multi`](#pyodide_macros.qcms.forbid_no_correct_answers_with_multi) | `#!yaml true` |

---

<br>






#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.QcmsConfig.forbid_no_correct_answers_with_multi') }}`#!py forbid_no_correct_answers_with_multi = C.Type(bool, default=True)` { #pyodide_macros.qcms.forbid_no_correct_answers_with_multi .doc .doc-heading .fake-h4 }

Si désactivé (`False`), une question sans réponse correcte fournie, mais marquée comme `multi=True`, est considérée comme valide. Si cette option est réglée à `True`, cette situation lèvera une erreur.


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TermsConfig') }}`terms` { #pyodide_macros.terms .doc .doc-heading }

_Classe : `#!py TermsConfig`_ | _Étend : `#!py CopyableConfig, Config`_

Réglages spécifiques aux terminaux.
<br><br><br>

| Options disponibles | Valeur par défaut |
|-|:-:|
| [`cut_feedback`](#pyodide_macros.terms.cut_feedback) | `#!yaml true` |
    | [`stdout_cut_off`](#pyodide_macros.terms.stdout_cut_off) | `#!yaml 200` |

---

<br>






#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TermsConfig.cut_feedback') }}`#!py cut_feedback = C.Type(bool, default=True)` { #pyodide_macros.terms.cut_feedback .doc .doc-heading .fake-h4 }

Si activé (`True`), les entrées affichées dans les terminaux sont tronquées si elles sont trop longues, afin d'éviter des problèmes de performances d'affichage des outils `jQuery.terminal`.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TermsConfig.stdout_cut_off') }}`#!py stdout_cut_off = C.Type(int, default=200)` { #pyodide_macros.terms.stdout_cut_off .doc .doc-heading .fake-h4 }

Nombre maximal de lignes restant affichées dans un terminal : si de nouvelles lignes sont ajoutées, les plus anciennes sont éliminées au fur et à mesure.

??? note "Performances d'affichage des terminaux"

    ___Les éléments `jQuery.terminal` deviennent horriblement lents lorsque le nombre de caractères affichés est important.___

    Cette option permet de limiter ces problèmes de performance lorsque la sortie standard n'est pas tronquée (voir le bouton en haut à droite du terminal).

    Noter par contre que cette option _ne limite pas_ le nombre de caractères dans une seule ligne, ce qui veut dire qu'une page figée est toujours possible, tandis que l'option de troncature, `cut_feedback` évitera ce problème aussi.


<br><br>

{{sep()}}


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TestingConfig') }}`testing` { #pyodide_macros.testing .doc .doc-heading }

_Classe : `#!py TestingConfig`_ | _Étend : `#!py CopyableConfig, Config`_

Permet de paramétrer la page pour tester automatiquement tous les IDEs de la documentation.
<br><br><br>

| Options disponibles | Valeur par défaut |
|-|:-:|
| [`empty_section_fallback`](#pyodide_macros.testing.empty_section_fallback) | `#!yaml "skip"` |
    | [`include`](#pyodide_macros.testing.include) | `#!yaml "null"` |
    | [`load_buttons`](#pyodide_macros.testing.load_buttons) | `#!yaml null` |
    | [`page`](#pyodide_macros.testing.page) | `#!yaml "test_ides"` |

---

<br>






#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TestingConfig.empty_section_fallback') }}`#!py empty_section_fallback = C.Choice(('', 'skip', 'fail', 'code', 'human', 'no_clear'), default='skip')` { #pyodide_macros.testing.empty_section_fallback .doc .doc-heading .fake-h4 }

Lorsque la page des tests des IDEs est construite et que la section à tester pour un IDE donné ne contient pas de code et que `{{config_validator("testing.empty_section_fallback", 1)}}` est définie, c'est cette "stratégie" qui sera utilisée à la place.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TestingConfig.include') }}`#!py include = C.Choice(('null', 'serve', 'site'), default='null')` { #pyodide_macros.testing.include .doc .doc-heading .fake-h4 }

Définit si la page de tests des IDEs doit être générée et de quelle façon. {{ul_li([ "`#!py 'null'` : la page de tests n'est pas générée.", "`#!py 'serve'` : la page de tests est générée pendant `mkdocs serve`, et est ajoutée automatiquement à la navigation.", "`#!py 'site'` : la page de tests est ajoutée sur le site construit, mais n'apparaît pas dans la navigation.", ])}}



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TestingConfig.load_buttons') }}`#!py load_buttons = C.Optional(C.Type(bool))` { #pyodide_macros.testing.load_buttons .doc .doc-heading .fake-h4 }

Définit si le bouton pour charger l'ensemble des codes associés à un IDE de la page des tests sera présent ou non. <br>Le comportement par défaut dépend de la valeur de l'option {{ config_link( 'testing.include') }} :

* Pour {{ config_validator("testing.include", val="serve") }}, le bouton est présent par défaut.
* Pour {{ config_validator("testing.include", val="site") }}, le bouton est absent par défaut.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.TestingConfig.page') }}`#!py page = C.Type(str, default='test_ides')` { #pyodide_macros.testing.page .doc .doc-heading .fake-h4 }

Nom de fichier markdown (avec ou sans l'extension `.md`) utilisé pour générer une page contenant le nécessaire pour tester de manière semi-automatisée tous les IDEs de la documentation.

* La page n'est créée que si l'option `{{config_validator("testing.include")}}` n'est pas à `#!yaml null`.
* Une erreur est levée si un fichier du même nom existe déjà.
* Une erreur est levée si le fichier n'est pas à la racine de la documentation.


<br><br>

{{sep()}}

<br INSERTION_TOKEN_2>


### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.PyodideMacrosConfig') }} {{ anchor_redirect(id='pyodide_macros', mk_addr_skip=False) }} `#!py pyodide_macros` {  .doc .doc-heading }

_Classe : `#!py PyodideMacroConfig`_ | _Étend : `#!py CopyableConfig, Config`_

La configuration du plugin, `PyodideMacrosConfig`, reprend également toutes les options du plugin original `MacrosPlugin`, ce qui permet d'en réutiliser toutes les fonctionnalités. <br>Ces options, décrites succinctement ci-dessous, sont disponibles à la racine de la configuration du plugin, dans `mkdocs.yml:plugins.pyodide_macros` (voir [en haut de cette page](--global-architecture)).

Pour plus d'informations à leur sujet ou concernant le fonctionnement general des macros :

- [GitHub repository][mkdocs-macros]{: target=_blank }
- [Help page](https://mkdocs-macros-plugin.readthedocs.io/en/latest/){: target=_blank }
- [Configuration information](https://mkdocs-macros-plugin.readthedocs.io/en/latest/# configuration-of-the-plugin){ target=_blank }




#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.force_render_paths') }}`#!py force_render_paths = C.Type(str, default='')` { #pyodide_macros.force_render_paths .doc .doc-heading .fake-h4 }

Force le rendu des fichiers et dossiers indiqués (utilise des [syntaxes Pathspec](https://python-path-specification.readthedocs.io/en/stable/readme.html#tutorial) ).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.include_dir') }}`#!py include_dir = C.Type(str, default='')` { #pyodide_macros.include_dir .doc .doc-heading .fake-h4 }

Répertoire de [fichiers externes à inclure][macros-include_dir]{: target=_blank }.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.include_yaml') }}`#!py include_yaml = C.ListOfItems(C.Type(str), default=[])` { #pyodide_macros.include_yaml .doc .doc-heading .fake-h4 }

Pour inclure des [fichiers de données externes][macros-include_yaml]{: target=_blank }.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_block_end_string') }}`#!py j2_block_end_string = C.Type(str, default='')` { #pyodide_macros.j2_block_end_string .doc .doc-heading .fake-h4 }

Pour changer la syntaxe des fermetures de blocs Jinja2 (défaut: {% raw %}`%}`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_block_start_string') }}`#!py j2_block_start_string = C.Type(str, default='')` { #pyodide_macros.j2_block_start_string .doc .doc-heading .fake-h4 }

Pour changer la syntaxe des ouvertures de blocs Jinja2 (défaut: {% raw %}`{%`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_comment_end_string') }}`#!py j2_comment_end_string = C.Type(str, default='')` { #pyodide_macros.j2_comment_end_string .doc .doc-heading .fake-h4 }

Pour changer la syntaxe des fermetures de commentaires Jinja2 (défaut: {% raw %}`#}`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_comment_start_string') }}`#!py j2_comment_start_string = C.Type(str, default='')` { #pyodide_macros.j2_comment_start_string .doc .doc-heading .fake-h4 }

Pour changer la syntaxe des ouvertures de commentaires Jinja2 (défaut: {% raw %}`{#`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_variable_end_string') }}`#!py j2_variable_end_string = C.Type(str, default='')` { #pyodide_macros.j2_variable_end_string .doc .doc-heading .fake-h4 }

Pour changer la syntaxe des fermetures de variables Jinja2 (défaut: {% raw %}`}}`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.j2_variable_start_string') }}`#!py j2_variable_start_string = C.Type(str, default='')` { #pyodide_macros.j2_variable_start_string .doc .doc-heading .fake-h4 }

Pour changer la syntaxe des ouvertures de variables Jinja2 (défaut: {% raw %}`{{`{% endraw %}).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.module_name') }}`#!py module_name = C.Type(str, default='main')` { #pyodide_macros.module_name .doc .doc-heading .fake-h4 }

Nom du module/package python contenant vos macros personnalisées, filtres et variables. Utiliser un nom de fichier (sans extension), un nom de dossier, ou un chemin relatif (dossiers séparés par des slashes : `dossier/module`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.modules') }}`#!py modules = C.ListOfItems(C.Type(str), default=[])` { #pyodide_macros.modules .doc .doc-heading .fake-h4 }

Liste de [pluglets][macros-pluglets]{ target=_blank } à ajouter aux macros (= modules de macros qui peuvent être installés puis listés  avec `pip list`).



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.on_error_fail') }}`#!py on_error_fail = C.Type(bool, default=False)` { #pyodide_macros.on_error_fail .doc .doc-heading .fake-h4 }

Interrompt le `build` si une erreur est levée durant l'exécution d'une macro.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.on_undefined') }}`#!py on_undefined = C.Type(str, default='keep')` { #pyodide_macros.on_undefined .doc .doc-heading .fake-h4 }

Comportement à adopter quand une macro rencontre une variable non définie lors des rendus. Par défaut, les expressions Jinja ne sont alors pas modifiées dans la page markdown. Utiliser `'strict'` pour provoquer une erreur.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.render_by_default') }}`#!py render_by_default = C.Type(bool, default=True)` { #pyodide_macros.render_by_default .doc .doc-heading .fake-h4 }

Exécute les macros dans toutes les pages ou non.



#### {{ anchor_redirect(id='pyodide_mkdocs_theme.pyodide_macros.plugin.config.verbose') }}`#!py verbose = C.Type(bool, default=False)` { #pyodide_macros.verbose .doc .doc-heading .fake-h4 }

Affiche plus d'informations dans le terminal sur les étapes de rendu des macros si passé à `True` lors d'un build/serve.


<br INSERTION_TOKEN_3>