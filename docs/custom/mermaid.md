Le thème propose des fonctionnalités pour faciliter l'intégration dans la documentation de graphiques [`mermaid`][mermaid]{: target=_blank } créés dynamiquement.


<br>


??? help "Graphes `mermaid` dans `pyodide-mkdocs-theme`"

    Pour pouvoir utiliser des graphes `mermaid` dans la documentation, il faut dans un premier temps configurer l'extension markdown `pymdownx.superfences` dans le fichier `mkdocs.yml`, si cela n'est pas déjà fait :

    ```yaml
    markdown_extensions:
      - pymdownx.superfences:
          custom_fences:
            - name: mermaid
              class: mermaid
              format: !!python/name:pymdownx.superfences.fence_code_format
    ```

    Il est alors possible de déclarer des graphes `mermaid` simplement en déclarant des blocs de code spécifiant `mermaid` comme langage :

        ```mermaid
        graph TB
            A --- B
        ```

    ```mermaid
    graph TB
        A --- B
    ```



## Principe général { #mermaid-generalities }

L'idée générale pour que cela fonctionne est la suivante :

1. Insérer dans le fichier markdown un élément html (une `<div>`) qui accueillera la figure une fois qu'elle sera créée.
1. Signifier au thème que la page va devoir gérer du contenu dynamique `mermaid` (= "marquer la page").
1. Dans pyodide, préparer une fonction que l'utilisateur pourra appeler pour générer la figure avec mermaid.
1. Construire les données et appeler la fonction qui va mettre à jour le contenu de la `div` créée au point 1.

<br>

* L'étape 1 est facilitée par l'utilisation de la macro [`{% raw %}{{ figure(...) }}{% endraw %}`](--redactors/figures/).
* L'étape 2 est réalisée en passant `MERMAID=True` en argument de l'une des macros `IDE`, `IDEv`, `terminal`, `py_btn` ou `run` de la page (v. plus bas).
* L'étape 3 est facilitée par la fonction `mermaid_figure(figure_id)`, qui renvoie une fonction pour modifier le contenu de la balise `div` d'identifiant `figure_id`.
* L'utilisateur n'a plus qu'à appeler la fonction avec un code mermaid valide, à l'étape 4.





## Exemple simple { #mermaid-simple-example}

Concrètement, pour un IDE qui devrait générer du code mermaid, puis afficher le graphe correspondant dans la page, les codes markdown et python suivant suffisent pour intégrer une première figure dans la page :

<br>

!!! note "Fichier markdown"

    Dans la page markdown, on insère:

    * La balise qui accueillera la figure.
    * L'IDE qui générera le code mermaid, en n'oubliant pas d'utiliser l'argument `MERMAID`.

    ```markdown
    {% raw %}
    {{ figure() }}

    {{ IDE('exemples/mermaid', MERMAID=True) }}
    {% endraw %}
    ```

!!! note "Fichier python"

    {{ py('exemples/mermaid') }}

    La fonction `mermaid_figure` est définie par le thème. C'est une "function factory", qui crée une fonction qui insèrera le code fourni par l'utilisateur dans la bonne balise html.

    _Voir plus bas pour [plus d'informations sur la fonction `mermaid_figure`](--mermaid_figure-function)._


!!! note "Résultat"

    {{figure()}}

    {{IDE('exemples/mermaid', MERMAID=True)}}






## Utilisation dans pyodide



### La fonction `mermaid_figure` { #mermaid_figure-function }

Cette fonction est similaire aux objets [`PyodidePlot`](--pyodide-plot) :

* Elle prend un argument optionnel, qui est l'identifiant html (`id`) de la balise `div` qui devra accueillir la figure.
* Si cet argument n'est pas fourni, c'est [la valeur par défaut pour l'argument `div_id` de la macro `figure`](--redactors/figures/) qui est utilisé.
* Tout ceci est compatible avec les configurations via les[ fichiers {{meta()}} et les entêtes des fichiers markdown](--custom/metadata/).

<br>

La fonction `mermaid_figure` est une "function factory", et renvoie une nouvelle fonction que l'utilisateur pourra appeler pour insérer le graphe mermaid à l'endroit voulu dans la page.
<br>Cette seconde fonction prend deux arguments :

```python
mermaid_figure(id:str) -> (code:str, debug:bool=False) -> None
```

<br>

| Argument | Type |  Résumé |
|-|-|-|
| `code` | `#!py str` | Code du graphe mermaid à insérer. |
| `debug` | `#!py bool` | [Défaut: `#!py False`] Si l'argument `debug` vaut `#!py True`, le code passé en argument est affiché dans le terminal avant traitement. |

<br>

!!! warning "Disponibilité de la fonction `mermaid_figure`"

    Si elle est toujours définie dans l'environnement, {{red('la fonction `mermaid_figure`{.red} lèvera une erreur si la page de la documentation n\'a pas été "marquée" comme devant accueillir du contenu mermaid dynamique ([argument `MERMAID=True`](--mermaid-simple-example))')}}.

    Cette contrainte permet de garantir que les différents éléments de logique nécessaires sont présents lors des exécutions.

<br>

??? tip "Exécution locale de fichiers python utilisant la fonction `mermaid_figure`"

    ___Méthode 1 :___

    La ["boîte à outil"](--py-local-toolbox) du thème permet "d'utiliser" la fonction en local. Il suffit de mettre en place le fichier `toolbox.py` et de l'importer dans le fichier python:

    ```python title="Snippet à ajouter en haut du fichier python"
    # --- PYODIDE:ignore --- #
    from toolbox import *
    ```

    <br>

    ___Méthode 2 :___

    Il est aussi possible d'ajouter le code suivant au tout début du fichier python de la documentation :

    ```python title="Snippet à ajouter en haut du fichier python"
    # --- PYODIDE:ignore --- #
    mermaid_figure = lambda _: print
    ```

    <br>

    {{ sep() }}

    {{ pmt_note("_Les codes des sections `ignore` ne sont exécutés qu'en local: ils ne sont pas utilisés pour construire le site final avec mkdocs._", 0) }}



### Mise à jour du DOM

- Aucune action de l'utilisateur n'est requise, le thème gère la mise à jour du contenu de la page automatiquement.
- Pour information, la mise à jour du DOM est faite en mode [`async`](--async), l'appel se situant après l'exécution des sections `post` et/ou `post_term`.






## Autres exemples


??? help "Contenu mermaid et macro `py_btn`"

    Si tout le code nécessaire est placé dns la [section `env`](--ide-sections), une simple macro `{% raw %}{{ py_btn(...) }}{% endraw %}` peut suffire :


    {{ figure_macro_md(
        "exemples/mermaid_btn", "
{{ figure('btn-fig-id') }}

{{ py_btn('exemples/mermaid_btn', MERMAID=True) }}
") }}



??? help "Contenu mermaid et macro `terminal`"

    !!! tip ""
        Noter que l'argument `#!py MERMAID=True` a été ignoré dans cet exemple, car d'autres macros ont déjà été utilisées pour "marquer" cette page.

    {{ figure_macro_md(
        "exemples/mermaid_term", "
{{ terminal('exemples/mermaid_term', FILL='generate()', TERM_H=3) }}

{{ figure('term-fig-id') }}
") }}
