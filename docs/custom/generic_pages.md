# Construire des pages à partir d'informations globales sur tous les appels de macros du thème

<br><br>

!!! warning "Utilisation _très_ avancée"

    Le contenu de cette page ne sera utile qu'à des personnes étant un minimum familières avec [les évènements utilisées pour les plugins mkdocs][mkdocs-events], ou les ["fichiers de crochets"][mkdocs-hooks].


<br><br>

Le thème stocke des informations détaillées sur les appels de macros effectués dans les différentes pages de la documentation. Ces informations peuvent être ensuite utilisées pour construire des pages avec des données plus génériques concernant tout ou partie des pages de la documentation ou des exercices, comme par exemple la [page de recherche de CodEx](https://codex.forge.apps.education.fr/recherche/), ou gérer les boutons de suivi des IDEs présents dans la page de recherche et [celle des parcours](https://codex.forge.apps.education.fr/parcours/) (toujours sur CodEx).




## Vue d'ensemble

- Le thème stocke les informations pour tous les appels des macros du thème, effectués durant les évènements `on_page_markdown`, dans une liste d'objets `MacroData`.
<br><br>

- Les données complètes sont donc accessibles à partir du crochet `on_env`, une fois que toutes les pages ont été traitées.
<br><br>

- Il est possible d'accéder à cette liste d'objets en l'extrayant du plugin `pyodide_macros`, via l'argument `config` de l'un des crochets mkdocs en utilisant le code `config.plugins['pyodide_macros'].all_macros_data` (un exemple est disponible plus bas dans cette page).
<br><br>

- Il est conseillé de formatter les données à votre convenance depuis le crochet `on_env`, en stockant les données dans une variable globale.
<br><br>

- Le crochet `on_page_context` peut ensuite être utilisé pour modifier le contenu html des pages souhaitées, en mutant `page.content` à partir des données extraites depuis le crochet `on_env`.

    !!! warning "Utilisable uniquement pour créer du contenu html !"

        Les contenus générés à cette étape ne doivent effectivement être que des contenus html, et non des contenus markdown : la conversion de markdown vers html a en effet déjà été effectuée par mkdocs.

    <br><br>




## Mise en place

1. Définir un [fichier de crochets][mkdocs-hooks] dans le projet (en dehors du `docs_dir`). Typiquement, `hooks.py`, à la racine du projet.
<br><br>

1. Référencer ce fichier dans le `mkdocs.yml` :

    ```yaml
    hooks:
        - hooks.py
    ```
    <br><br>

1. Coller dans le fichier de crochets le code ci-dessous, qui peut servir d'architecture de base :

    ```python
    from typing import Any
    from jinja2 import Environment
    from mkdocs.config.defaults import MkDocsConfig
    from mkdocs.structure.nav import Navigation
    from mkdocs.structure.files import Files
    from mkdocs.structure.pages import Page
    from pyodide_mkdocs_theme.pyodide_macros import *


    MY_DATA: Any = {}

    def on_env(env:Environment, config:MkDocsConfig, files:Files):
        """
        Fonction exécutée une seule fois durant le build.

        ATTENTION:
            * L'argument `env` n'a rien à voir avec l'argument utilisé pour définir les macros.
            * La variable `plugin` ci-dessous, elle, correspond bien à l'argument `env` des
              fonctions `define_env` utilisées pour déclarer les macros.
            * Les signatures des fonctions crochets mkdocs sont très contraintes et ne permettent
              généralement pas de renommer les arguments...
        """
        plugin: PyodideMacroPlugin = config.plugins['pyodide_macros'])
        lst: List[MacroData] = plugin.all_macros_data
        # Extraire/formatter ensuite les données nécessaires à votre convenance et les rendre
        # disponibles dans une variable globale (ex: MY_DATA).
        # Il est recommander de typer les objets autant que possible, de manière à profiter
        # ensuite de l'auto-complétion.


    def on_page_context(ctx, page:Page, *, config:MkDocsConfig, nav:Navigation):
        """
        Fonction exécutée une fois par page de la documentation.
        Muter `page.content:str`, en y ajoutant le code html voulu.
        """
        # Utiliser MY_DATA pour construire le code html voulu, lorsque la page passée en argument
        # est celle désirée.
        if page.url == 'custom/generic_pages/':
            page.content += '<a href="here">Whatever you want</a>'
    ```
    <br><br>

1. Modifier ensuite le code pour customiser les pages voulues.

<br>

Un exemple d'utilisation de cette fonctionnalité est disponible sur le dépôt de CodEx ([`codex_hooks.py`](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr/-/blob/main/codex_hooks.py?ref_type=heads)).





## Données disponibles


### Interface `MacroData`

La classe `MacroData` regroupe l'interface commune des données fournies dans la liste `plugin.all_macros_data`.
<br>Ces objets comportent une interface générique, et une partie spécifique à la macro utilisée.

<br><br><br>

???+ help "Interface générique"
    ::: pyodide_mkdocs_theme.pyodide_macros.plugin.tools.macros_data.MacroDataGeneric
        options:
            show_bases: false
            heading_level: 3
            members_order: alphabetical

<br><br><br>

???+ help "Interface spécifique"
    ::: pyodide_mkdocs_theme.pyodide_macros.plugin.tools.macros_data.MacroDataSpecific
            options:
                show_bases: false
                heading_level: 3
                members_order: alphabetical




### Sous classes de `MacroData`

Afin de profiter de l'auto-complétion, le thème met à disposition les variantes des classes `MacroData` correspondant à chaque type de macros.

Il est donc fortement conseillé de réarranger à votre convenance les objets de `plugin.all_macros_data` depuis le crochet `on_env`, en créant d'autres structures de données qui utiliseront un et un seul type de sous-classe. Ainsi, l'IDE proposera les suggestions d'auto-complétion.


???+ help "Exemple d'utilisation d'une sous classe de `MacroData`"

    ```python
    from typing import Dict
    from jinja2 import Environment
    from mkdocs.config.defaults import MkDocsConfig
    from mkdocs.structure.files import Files

    from pyodide_mkdocs_theme.pyodide_macros import PyodideMacroPlugin, MacroDataIDE


    # Déclaration avec typage, pour profiter de l'auto-complétion dans `on_page_context`:
    IDES: Dict[str,MacroDataIDE] = {}


    def on_env(env:Environment, config:MkDocsConfig, files:Files):

        plugin: PyodideMacroPlugin = config.plugins['pyodide_macros'])
        for macro_data in plugin.all_macros_data:
            if macro_data.macro == 'IDE':
                IDES[ macro_data.page_url ] = macro_data
    ```

<br><br>

<span GENERATED_TOKEN></span>



??? note "`#!py MacroDataIDE`"
    ```python
    class MacroDataIDE(MacroData):
        """ Runtime data object for the MacroDataIDE macro. """

        args: MacroArgsDataIDE


    class MacroArgsDataIDE(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataIDE macro. """

        py_name: str
        ID: int
        SANS: str
        WHITE: str
        REC_LIMIT: int
        MERMAID: bool
        AUTO_RUN: bool
        MAX: int
        LOGS: bool
        MODE: str
        MIN_SIZE: int
        MAX_SIZE: int
        TERM_H: int
        TEST: str
        TWO_COLS: bool
        STD_KEY: str
        EXPORT: bool
    ```






??? note "`#!py MacroDataTerminal`"
    ```python
    class MacroDataTerminal(MacroData):
        """ Runtime data object for the MacroDataTerminal macro. """

        args: MacroArgsDataTerminal


    class MacroArgsDataTerminal(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataTerminal macro. """

        py_name: str
        ID: int
        SANS: str
        WHITE: str
        REC_LIMIT: int
        MERMAID: bool
        AUTO_RUN: bool
        TERM_H: int
        FILL: str
    ```






??? note "`#!py MacroDataPy_btn`"
    ```python
    class MacroDataPy_btn(MacroData):
        """ Runtime data object for the MacroDataPy_btn macro. """

        args: MacroArgsDataPy_btn


    class MacroArgsDataPy_btn(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataPy_btn macro. """

        py_name: str
        ID: int
        SANS: str
        WHITE: str
        REC_LIMIT: int
        ICON: str
        HEIGHT: int
        WIDTH: int
        SIZE: int
        TIP: str
        TIP_SHIFT: int
        TIP_WIDTH: float
        WRAPPER: str
        MERMAID: bool
        AUTO_RUN: bool
    ```






??? note "`#!py MacroDataRun`"
    ```python
    class MacroDataRun(MacroData):
        """ Runtime data object for the MacroDataRun macro. """

        args: MacroArgsDataRun


    class MacroArgsDataRun(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataRun macro. """

        py_name: str
        ID: int
        SANS: str
        WHITE: str
        REC_LIMIT: int
        MERMAID: bool
    ```






??? note "`#!py MacroDataSection`"
    ```python
    class MacroDataSection(MacroData):
        """ Runtime data object for the MacroDataSection macro. """

        args: MacroArgsDataSection


    class MacroArgsDataSection(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataSection macro. """

        py_name: str
        section: str
    ```






??? note "`#!py MacroDataMulti_qcm`"
    ```python
    class MacroDataMulti_qcm(MacroData):
        """ Runtime data object for the MacroDataMulti_qcm macro. """

        args: MacroArgsDataMulti_qcm


    class MacroArgsDataMulti_qcm(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataMulti_qcm macro. """

        questions: Any
        description: str
        hide: bool
        multi: bool
        shuffle: bool
        shuffle_questions: bool
        shuffle_items: bool
        admo_kind: str
        admo_class: str
        qcm_title: str
        tag_list_of_qs: str
        DEBUG: bool
    ```






??? note "`#!py MacroDataPy`"
    ```python
    class MacroDataPy(MacroData):
        """ Runtime data object for the MacroDataPy macro. """

        args: MacroArgsDataPy


    class MacroArgsDataPy(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataPy macro. """

        py_name: str
    ```






??? note "`#!py MacroDataFigure`"
    ```python
    class MacroDataFigure(MacroData):
        """ Runtime data object for the MacroDataFigure macro. """

        args: MacroArgsDataFigure


    class MacroArgsDataFigure(ObjectWithDataGetters):
        """ Runtime arguments object for the MacroDataFigure macro. """

        div_id: str
        div_class: str
        inner_text: str
        admo_kind: str
        admo_class: str
        admo_title: str
        p5_buttons: str
    ```





### `Content` et `HasContent`

Si définies, les propriétés `content` et `has` des objets `MacroData` offrent les interfaces suivantes :






!!! note "`#!py Content`"
    ```python
    class Content:
        """ Content of each section (or absolute Path on the disk for REMs files). """

        env: str = ""
        env_term: str = ""
        code: str = ""
        corr: str = ""
        tests: str = ""
        secrets: str = ""
        post_term: str = ""
        post: str = ""
        REM: Optional[Path] = None
        VIS_REM: Optional[Path] = None
        check_btn: bool = False
    ```






!!! note "`#!py HasContent`"
    ```python
    class HasContent:
        """ Flags to know what sections/elements are defined or not. """

        env: bool = False
        env_term: bool = False
        code: bool = False
        corr: bool = False
        tests: bool = False
        secrets: bool = False
        post_term: bool = False
        post: bool = False
        REM: bool = False
        VIS_REM: bool = False
        check_btn: bool = False
    ```


<span GENERATED_TOKEN></span>
