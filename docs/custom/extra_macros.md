

Le plugin `pyodide_macros` hérite de la classe `MacrosPlugin` du plugin [`mkdocs-macros-plugin`][mkdocs-macros]{: target=_blank }, et les différentes façons de déclarer les macros pour celui-ci restent donc toutes applicables avec le thème.

Il est donc toujours possible d'ajouter vos propres macros en plus de celles définies par le thème :

* Soit via un fichier [`main.py` à la racine du dépôt][pmt-macros-main-py], comme celui à la racine de la version 1.5.0 de pyodide-mkdocs-theme.
* Soit [avec un paquet python](--macros-module) déclaré à la racine du projet, comme le dossier `pmt_macros` à la racine de la version actuelle du thème.


!!! help "Contraintes sur les noms des macros personnalisées"

    Les macros personnalisées ne doivent pas avoir le même nom que l'une de celles définies par le thème ([voir ici](--macros_table)).


## {{anchor_redirect("Fichier-mainpy")}} Fichier `main.py` { #macros-main-py }

C'est la méthode la plus simple :

--8<-- "docs_tools/inclusions/macros_main.md"


## {{anchor_redirect("module-de-macros")}}Module de macros { #macros-module }

Si vous avez beaucoup de code/macros personnalisées, il peut être intéressant d'utiliser un package python plutôt qu'un simple fichier `main.py`, pour obtenir une meilleure organisation du projet :

--8<-- "docs_tools/inclusions/macros_package.md"



## Exemples { #macros-examples }

Comme dit précédemment, il y a de nombreuses stratégies utilisables pour intégrer ses propres macros à un projet PMT.

L'exemple qui vient naturellement à l'esprit est le module de macros personnalisées du thème lui-même, [`pmt_macros`][pmt-macros]{: target=_blank }, mais il a atteint un niveau de complexité telle, que je déconseille de commencer par là.

<br>

Voici l'évolution des codes déclarant des macros personnalisées dans la documentation de PMT ou sur CodEx, illustrant différentes stratégies.

| Projet {{width(11)}} | Stratégie/intérêt |
|-|-|
| [`PMT 1.5.0`][pmt-macros-main-py]{: target=_blank }  | Fichier `main.py` unique.<br>Simple d'utilisation. |
| [`PMT 2.0.0`][pmt-macros-simple]{: target=_blank } | Module `pmt_macros`.<br>Correspond à la "méthode 2" décrite ci-dessus. |
| [`CodEx (ancien)`][codex_macros_old]{: target=_blank } | Module `codex_macros`.<br>Correspond à la "méthode 2" décrite ci-dessus. |
| [`CodEx (actuel)`][codex_macros]{: target=_blank } | Module `codex_macros`.<br>Utilise les deux techniques décrites ci-dessus pour enregistrer les macros, avec le fichier `__init__.py` appelant la fonction `define_env` d'un sous-module (méthode 1). Cette version est sans doute plus naturelle, venant d'un fichier `main.py` unique. |
| [`pmt_macros`][pmt-macros]{: target=_blank } | Module `pmt_macros`.<br>Utilise une logistique entièrement différente, afin de déclarer certaines macros globalement pour une "session" `mkdocs serve` entière (ceci permet de mettre en cache certains résultats). Pour programmeurs avertis seulement, car il est très facile de se retrouver avec des problèmes de données invalides en cache. |





## Configuration

Toutes les options du plugin original des macros sont disponibles directement sur le plugin `pyodide_macros`.<br>Pour plus de détails, voir :

* La [page de configuration](--pyodide_macros) du plugin `pyodide_macros` du thème.
* La [documentation du plugin original][mkdocs-macros]{: target=_blank } de `mkdocs-macros-plugin`.



## Indentation et macros multilignes {{anchor_redirect(id="indentation-et-macros-multi-lignes")}} { #multiline-macros-vs-indentations }

Pour les macros qui doivent insérer du contenu multilignes, il est important de pouvoir savoir à quel niveau d'indentation l'appel de la macro en cours a été fait.

Par exemple, pour une macro `#!py admo(lst:List[str])` qui générerait une liste de puces dans une admonition, on souhaite obtenir le résultat suivant :

!!! note inline end w45 "Rendu markdown souhaité"

    ```markdown
    !!! tip ""
        * chat
        * chien
        * chaud

    ??? tip "indenté"
        !!! tip ""
            * chat
            * chien
            * chaud
    ```

!!! note no-margin-top "Fichier markdown source"

    ```markdown
    {% raw %}{{ admo(["chat", "chien", "chaud"]) }} {% endraw %}

    ??? tip "indenté"
        {% raw %}{{ admo(["chat", "chien", "chaud"]) }} {% endraw %}

    {{insecable()}}
    {{insecable()}}
    {{insecable()}}
    {{insecable()}}
    {{insecable()}}
    ```

Pour réaliser cela, il faut que la macro sache combien d'espaces elle doit ajouter au début de chaque ligne.

<br>

__`Pyodide-mkdocs-theme` propose une fonctionnalité permettant d'indenter automatiquement le contenu généré par votre macro avec le bon niveau d'indentation : `env.indent_macro(contenu)`.__

Cette fonctionnalité peut-être étendue aux macros personnalisées, en procédant de la façon suivante :

1. Ajouter le nom de la macro dans la configuration du plugin du thème, dans [`build.macros_with_indents`](--pyodide_macros_build_macros_with_indents) :

    ```yaml
    plugins:
        - pyodide_macros:
            build:
                {{ config_validator("build.macros_with_indents",1)}}:
                    - admo
    ```

    <br>

1. Implanter la macro dans votre fichier `main.py` ou votre module de macros personnalisées (voir en haut de cette page), en omettant la logique d'indentation dans un premier temps :

    ```python
    from typing import List
    from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

    def define_env(env:PyodideMacrosPlugin):

        @env.macro
        def admo(lst:List[str]):
            list_items = '!!! tip ""' + ''.join( f'\n    * { item }' for item in lst )
            return list_items
    ```

    <br>

1. Intégrer au code de la macro l'appel à la méthode `#!py env.indent_macro(content:str)`, qui prend en argument le contenu markdown non indenté, et renvoie le contenu indenté de manière appropriée. :

    ```python
        @env.macro
        def admo(lst:List[str]):
            list_items = '!!! tip ""' + ''.join( f'\n    * { item }' for item in lst )
            indented   = env.indent_macro(list_items)
            return indented
    ```

    <br>

Les appels de macro dans l'exemple [ci-dessus](--multiline-macros-vs-indentations) générerait en l'occurrence les chaînes suivantes :

* Appel 1 :
    ```
    '!!! tip ""\n    * chat\n    * chien\n    * chaud'
    ```

* Appel 2 :
    ```
    '!!! tip ""\n        * chat\n        * chien\n        * chaud'
    ```

Notez que dans la sortie indentée, la première ligne ne doit contenir aucune indentation, puisque l'appel de macro lui-même est déjà indenté dans le fichier markdown source.


!!! warning "Utilisation des macros référencées dans [`build.macros_with_indents`](--pyodide_macros_build_macros_with_indents)"

    * Il ne doit pas y avoir d'autres caractères que des espaces à gauche de ces appels de macros, dans les fichiers markdown.

    * `#!py PyodideMacrosTabulationError` est levée si des caractères de tabulation sont trouvés dans les indentations à gauche de ces appels de macros.

        Il est possible de contourner cette erreur en utilisant l'option {{ config_link('build.tab_to_spaces') }} du plugin, pour remplacer automatiquement les caractères de tabulation.
        <br>{{orange("Garder en tête que la validité des indentations obtenues n'est alors plus garantie")}}.


??? note "Contrôle "plus fin" (?) de l'ajout des indentations"

    S'il s'avérait nécessaire d'exercer un contrôle plus poussé sur l'insertion des indentations, ou de stocker l'information pour une utilisation ultérieure lors du processus de construction du site, la méthode `env.get_macro_indent()`  renvoie la chaîne de caractères avec le bon nombre d'espaces à utiliser au début de chaque ligne, _sauf la première_, pour indenter les contenus multilignes.

    La macro ci-dessus pourrait alors être écrite de la façon suivante :

    ```python
        def admo(lst:List[str]):
            list_items = '!!! tip ""' + ''.join( f'\n    * { item }' for item in lst )
            indent     = env.get_macro_indent()
            return list_items.replace('\n', '\n'+indent)
    ```