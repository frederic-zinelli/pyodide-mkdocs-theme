# Exécutions / Runtime



Cette page contient des informations supplémentaires concernant la gestion des IDEs ou des terminaux durant le "runtime", ainsi que ce qui touche aux restrictions (de près ou de loin).

La structure et l'agencement des fichiers pythons {{annexes()}} eux-mêmes sont discutés [ici pour les IDEs](--redactors/IDE-details/) et [là pour les terminaux](--macro-terminal).

Pour des gestions de fichiers plus complexes (bibliothèques, extraction de fichiers distants, ...) voir la [page dédiée](--custom/files/).














## Spécificités liées à Pyodide { #pyodide-specials }

Sont décrites ici quelques différences observables entre le fonctionnement original d'un moteur Python et l'environnement Pyodide tel que disponible via Pyodide-Mkdocs-Theme.

### Généralités sur les remplacements de builtins

Certaines fonctions builtins sont remplacées à la volée durant les exécutions, pour faciliter l'articulation avec la partie JavaScript/le DOM du site construit.

Les fonctions builtins remplacées utilisent des objets "wrappers", afin de les ramener au plus proche des comportements des objets d'origine :

* Appeler les fonctions `str` ou `repr` avec ces objets renverra le résultat de la fonction d'origine.
* La fonction `help` affichera le docstring d'origine.
* Il est cependant possible d'identifier les fonctions remplacées en demandant leur `#!py type`.

<br>
Concrètement :

```pycon
>>> help(input)
Python Library Documentation: built-in function input in module builtins

input(prompt='', /)
    Read a string from standard input.  The trailing newline is stripped.

    The prompt string, if given, is printed to standard output without a
    trailing newline before reading input.

    If the user hits EOF (*nix: Ctrl-D, Windows: Ctrl-Z+Return), raise EOFError.
    On *nix systems, readline is used if available.

>>> input
<built-in function input>
>>> type(input)
<class '__main__.BuiltinWrapperInput'>
```


### `print` { #pyodide-print }

La fonction `print` disponible dans l'environnement utilise différents objets pour gérer le feedback donné à l'utilisateur via les terminaux.

Concernant l'objet `sys.stdout`, il est à noter que:

* Il est renouvelé à chaque exécution de [section](--ide-sections).
* Il est de type `io.String`, ce qui permet d'en récupérer le contenu après affichage dans le terminal, via `sys.stdout.getvalue()`
* Cet objet n'est en fait pas utilisé par le thème lui-même et peut donc être muté par le rédacteur à volonté, notamment pour faciliter des tests vérifiant le contenu de la sortie standard en le vidant juste avant un test ([explications plus détaillées ici](--tests-test-print)).



### `input` { #pyodide-input }

La fonction `input` est remplacée de manière à ouvrir une fenêtre `window.prompt` dans le navigateur.
<br>Le message passé à la fonction `input` ainsi que la réponse de l'utilisateur sont ensuite affichés dans le terminal (même comportement que la fonction originale dans un terminal python).

<br>

L'affichage de ces messages dans le terminal n'est cependant visible qu'une fois le code de la [section](--ide-sections) en cours exécuté, ce qui peut s'avérer ennuyeux si l'utilisateur doit faire plusieurs choix successifs (typiquement: deviner un nombre par dichotomie, pierre-feuille-ciseaux, ...).
<br>Pour palier à cela, la fonction mise à disposition par le thème possède un second argument optionnel, _non documenté_ (au sens de la fonction `help`) :

```python
input(question:str="", beginning:str="") -> str
```

Cet argument permet d'ajouter du texte au début de la fenêtre de prompt, sans en afficher le contenu dans le terminal.

??? help "Un exemple valant mieux qu'un long discours..."

    {{ IDE("exemples/input_second_arg", TEST="human") }}



### `help` { #pyodide-help }

La fonction builtin ne marche pas de manière simple dans l'environnement de Pyodide car c'est une fonctionnalité interactive. Elle a donc été remplacée par une fonction affichant directement la totalité du docstring de l'argument dans le terminal (via la fonction `print`).










## Exécutions des codes python { #runtime }

### Déroulements

{{ md_include("docs_tools/inclusions/runtime.md") }}


### Connaître le type d'exécution en cours ? { #running-kind }

Il y a différentes façons de savoir depuis la couche python/pyodide quel est le type d'exécution qui a déclenché le code.

<br>

Le plus simple, pour différencier une exécution via le terminal d'une via l'éditeur de code, est de regarder laquelle des variables globales `__USER_CODE__` ou `__USER_CMD__` n'est pas vide (voir ci-dessous).
<br>Ceci en supposant évidemment que l'utilisateur n'exécute pas une ligne de commande vide ou un éditeur vide...

<br>

S'il est nécessaire d'avoir un niveau d'information plus fin ou plus fiable, il est également possible d'accéder au profil des exécutions telles que vues dans la couche JS en procédant comme suit :

!!! tip "Savoir quel type d'exécution est en cours depuis la couche python"


    ```python
    # --- PYODIDE:env --- #

    @auto_run
    def _hide_all_this():
        import js

        currently_running = js.config().running
        if "Validate" in currently_running:
            do_something()
        else:
            do_something_else()
    ```

    !!! danger "Toujours utiliser `"..." in running` pour l'identification !"

        Il y a différentes façons de lancer les mêmes types d'opérations, notamment lors de l'utilisation de la [page des tests de tous les IDEs](--redactors/IDE-tests-page/) du site.

        Les noms des "profiles d'exécution" sont des compositions des chaînes de caractères appropriées, donc une validation peut par exemple être identifiée par :

        * `#!py "Validate"` : validation normale, par action de l'utilisateur.
        * `#!py "ValidateCorr"` : validation avec le bouton de test de la correction, lors d'un `mkdocs serve`.
        * `#!py "TestingValidate"` : validation lancée depuis la page des tests des IDEs.
        * ...


    Les valeurs pouvant être utiles sont les suivantes :

    | `running` | Déclenché par... |
    |-|:-:|
    | `#!py "Command"`  | ...une commande tapée dans un terminal |
    | `#!py "Play"`     | ...le bouton {{ btn('play', in_tag='span') }} d'un IDE |
    | `#!py "Validate"` | ...le bouton {{ btn('check', in_tag='span') }} d'un IDE |
    | `#!py "PyBtn"`    | ...un [`py_btn`](--redactors/py_btns/) ou la macro [`run`](--redactors/run_macro/) |


<br>

De manière similaire, il est également possible d'accéder à l'id html de l'IDE (ou terminal, py_btn, ...) en cours d'exécution, avec `js.config().runningId`.
<br>Concernant les IDE, cet id est aussi l'identifiant de cet IDE dans le [localStorage](--custom/local-storage/) du navigateur.










## Extraction codes et commandes utilisateur { #user_code }

Il est possible d'accéder depuis le code python au code de l'utilisateur au moment où les exécutions sont lancées (contenu de l'éditeur quand il existe, commande exécutée depuis un terminal).

* Le code de l'éditeur est contenu dans une variable cachée nommée `__USER_CODE__` et est accessible depuis n'importe quelle étape des exécutions. S'il n'y a pas d'éditeur associé (ex: terminal isolé), la variable contient une chaîne vide.
* Si les exécutions sont lancées depuis un terminal, la commande utilisée est stockée dans une variable cachée nommée `__USER_CMD__`. Lorsque le terminal n'est pas utilisé, la variable contient une chaîne vide.

Ces variables peuvent être utilisées pour faire des vérifications additionnelles, en plus des fonctionnalités proposées avec les restrictions classiques (voir [argument `{% raw %}{{ IDE(... SANS=...) }}{% endraw %}`](--IDE-SANS)).

<br>

=== "Exemple terminal"

    À titre d'exemple, voici une utilisation de `__USER_CMD__`, pour empêcher l'utilisateur d'exécuter des commandes dans le terminal :

    {{ IDE_py(
        'exemples/user_cmd', MAX_SIZE=3, TERM_H=8, before='!!!', TEST=Case(subcases=[
            Case(term_cmd="45", in_error_msg='AssertionError: Ne pas utiliser le terminal...'),
            Case(code=1),
            Case(term_cmd="45", in_error_msg='AssertionError: Ne pas utiliser le terminal...'),
        ]
    )) }}

=== "Exemple éditeur"

    Le but est cette fois d'avoir un fichier python qui puisse tourner aussi en local, en dehors de l'environnement de pyodide (à supposer que le reste du fichier le permette également).

    {{ IDE_py(
        'exemples/user_code', MAX_SIZE=3, TERM_H=8, before='!!!', TEST=Case(
            code=True, subcases=[
                Case(in_error_msg="AssertionError: ~~Code~~ Français non conforme"),
                Case(term_cmd="45"),
            ]
    )) }}

<br>

???+ danger "Important"

    * L'utilisateur a aussi accès à ces variables, s'il en connaît l'existence, donc un rédacteur devrait y faire appel uniquement depuis la [section `env`](--ide-sections) pour `__USER_CODE__` et depuis la [section `env_term`](--ide-sections) pour `__USER_CMD__`, c'est-à-dire avant que le code de l'utilisateur ne puisse lui-même y accéder (et donc potentiellement les modifier).

    * Si le but est de connaître le nombre de caractères du code de l'utilisateur, ne pas oublier que les [tests publics](--ide-sections) _font partie du contenu de l'éditeur_.

    * Si des restrictions sont appliquées sur le code de l'éditeur [sans utiliser les arguments `SANS`](--IDE-SANS) des IDEs et terminaux, garder en tête que si les commandes du terminal ne sont pas également analysées, l'utilisateur pourrait mettre ce dernier à profit pour y définir différentes choses, et ainsi rendre des données disponibles dans l'environnement de l'éditeur sans taper de code dans l'éditeur lui-même (voir aussi la fonction `clear_scope`, dans l'admonition ci-dessous).

    * {{orange("_Il est très difficile d'écrire de bonnes restrictions !_")}} (surtout en python...)

        De bonnes restrictions devraient :

        1. Être efficaces (au sens de _contraignantes_).
        1. Ne pas provoquer de faux positifs (ou alors le moins possible).
        1. Ne pas rendre l'expérience de l'utilisateur horrible, de par leur fonctionnement (mauvais feedback, erreurs incompréhensibles, comportements inattendus d'objets ou fonctions classiques sans avertissement ou feedback, ...).

        <br>D'expérience, si vous vous lancez dans ce genre de choses, il y a fort à parier que vous n'arriviez pas à un résultat satisfaisant les trois points à la fois.
        <br>Les restrictions déjà proposées via l'argument [`SANS`](--IDE-SANS) des macros sont ce qui s'en rapproche le plus et je déconseille fortement d'en mettre d'autres en place. Par ailleurs, il est probable que vous ne puissiez pas mélanger facilement votre propre logique à celle issue de l'utilisation de l'argument `SANS`, sauf à explorer le code du thème pour voir comment vous y adapter...



{{anchor_redirect("clear_scope", mk_addr_skip=False)}}

??? danger "`clear_scope(keep=())`"

    _À but de complétude uniquement_, mais encore une fois, ___l'utilisation de tout ceci est déconseillée___.

    Une fonction `clear_scope`, masquée mais accessible depuis n'importe où, permet de supprimer tout ce qui a été défini dans l'environnement de l'utilisateur depuis le démarrage de Pyodide.<br>
    Elle peut permettre de s'assurer que l'utilisateur ne définit pas des choses qu'il n'est pas sensé définir via un terminal, pour contourner certaines restrictions. Il est également possible de passer un itérable en argument, contenant les noms d'objets ou de variables à ne pas effacer de l'environnement.


    Si cette fonction est utilisée, elle devrait l'être depuis la section `env`, pour les mêmes raisons que précédemment (et également pour que cela n'affecte pas les comportements liées aux restrictions de codes).















## Fonctionnement des restrictions { #restrictions }

Voici quelques informations supplémentaires sur la façon dont les restrictions de code fonctionnent ([argument `SANS`](--IDE-SANS) des macros `IDE`, `IDEv` et `terminal`).



### Philosophie { #restrictions-core-ideas }

!!! warning "But recherché avec les restrictions"

    {{ md_include("docs_tools/inclusions/restrictions_goals.md") }}

!!! warning "Efficacité des interdictions"

    {{ md_include("docs_tools/inclusions/restrictions_efficiency.md") }}




### Codes concernés { #restrictions-what-codes }

Les restrictions ne sont appliquées qu'à certaines sections/situations :

{{ md_include("docs_tools/inclusions/restrictions_where.md") }}


### Ordre et méthodes d'application { #restrictions-how }

Lorsqu'une section ("code cible") concernée par des restrictions est exécutée, le thème applique les opérations décrites ci-dessous. Si une erreur est levée à une de ces étapes, les étapes suivantes ne sont pas exécutées (sauf la dernière, si nécessaire).

1. Si le code exécuté ne contient pas d'erreur de syntaxes, vérifie les exclusions de méthodes et de mot clefs.
{{pmt_note("Cette vérification est faite via une analyse de l'AST représentant le \"code cible\", ce qui évite d'éventuels faux positifs avec le contenu des commentaires ou des docstrings.")}}

1. Le code à exécuter est analysé pour installer d'éventuels modules/bibliothèques manquants. Les modules/bibliothèques interdits d'utilisation ne sont normalement pas installés durant cette étape.

1. Les restrictions pour les fonctions et les modules/bibliothèques sont mises en place dans l'environnement.
{{pmt_note("Ces restrictions sont directement liées au code exécuté car elles reposent sur des redéfinitions de fonctions disponibles dans dans l'environnement de l'utilisateur. Le contenu des commentaires/docstrings ne peut donc pas déclencher de faux positifs.")}}

1. Le "code cible" est exécuté (contenu de l'IDE, tests de validation, ou commande du terminal).

1. Si des restrictions sur les fonctions ou les imports ont été mises en place, le code vérifie qu'elles n'ont pas été altérées par l'utilisateur. Si c'est le cas, une erreur sera levée après avoir supprimé les restrictions.
{{pmt_note("Des restrictions qui ont été mises en place seront systématiquement retirées, quoi qu'il se soit passé dans l'intervalle.")}}



### {{anchor_redirect(id="interdictions-de-methodes")}}Méthodes & attributs { #restrictions-methods }

!!! tip "Interdictions de méthodes ou attributs"

    |||
    |-|-|
    | Syntaxe | `SANS=".method1 .method2 .attribut"`<br>Soit plus généralement : `.identifiant` |
    | Mode d'application | Recherche d'accès à des attributs dans l'AST du code. |

Exemple :

```python title="Code python dans un IDE utilisant SANS='.sort'"
meme_si_ce_n_est_pas_une_liste.sort()  # Lève ExclusionError

meme_si_pas appelée.sort               # Lève ExclusionError

# lst.sort() dans un commentaire n'a pas d'effet.
```



### {{anchor_redirect(id="autres-interdictions")}}Fonctions { #restrictions-functions }

!!! tip "Interdictions de fonctions"

    |||
    |-|-|
    | Syntaxe | `SANS="min sorted"`<br>Soit plus généralement : `identifiant` |
    | Mode d'application | Ces fonctions lèvent une erreur lorsqu'elles sont appelées. |

Les interdictions de fonctions sont mises en place en remplaçant les fonctions d'origine dans l'environnement de l'utilisateur par des versions qui lèvent une erreur lorsqu'elles sont appelées.

Ceci présente des avantages et des inconvénients :

* Point positif: les éléments interdits mis dans des commentaires ne vont pas déclencher de faux positifs.
* Contrainte : ces interdictions concernent tous les codes susceptibles d'utiliser la fonction de l'utilisateur, donc elles s'appliquent aux codes des [sections](--ide-sections) `code`, `tests` et `secrets`. Il n'est donc "pas possible" d'utiliser les functions ou modules interdits depuis les tests pour valider les réponses de l'utilisateur...

Exemple :

```python title="SANS='sorted'"
# sorted(arr)               # Ce commentaire ne lève pas d'erreur

def func1(arr:list):
    return sorted(arr)      # Lève une erreur si func1 est appelée

f = sorted                  # Pas d'erreur car pas d'appel !

def func2(arr:list):
    return f(arr)           # Lève une erreur si func2 est appelée !
```



### Modules { #restrictions-packages }

!!! tip "Interdictions de modules / bibliothèques"

    |||
    |-|-|
    | Syntaxe | `SANS="numpy heapq"`<br>Soit plus généralement : `nom_module` |
    | Mode d'application | Ces modules/bibliothèques lèvent une erreur lorsqu'ils sont importés. |

Les interdictions de modules/bibliothèques fonctionnent de manière similaire aux interdictions de fonctions : la fonction d'import originale est remplacée par une autre, qui se comporte normalement pour les modules autorisés, mais qui lève une erreur si l'utilisateur tente d'importer un module interdit.

Exemple :

```python title="SANS='numpy'"
# Chacune de ces instructions lèverait une erreur :
import numpy
import numpy as np
from numpy import array
```

<br>

!!! warning "Ne pas importer de module interdits directement dans le scope global, depuis les sections d'environnement !"

    Les restrictions ne sont pas appliquées aux [sections](--ide-sections) dites "d'environnement" (`env`, `env_term`, `post_term`, `post`), donc il est possible d'y importer des modules interdits à l'utilisateur. Il ne faut cependant pas oublier qu'une fois un import effectué, le module est ensuite disponible dans le scope dans lequel il a été importé.

    Si des modules interdits à l'utilisateur doivent être utilisés depuis ces sections, il faut donc les importer depuis l'intérieur d'une fonction, afin éviter que l'utilisateur ne puisse ensuite directement y accéder.
    <br>En effet, bien que le module ait déjà été chargé dans l'environnement python, l'utilisateur devrait tout de même utiliser la fonction d'importation pour pouvoir utiliser le module, et c'est cette fonction qui lèvera une erreur pour un module interdit.

    <br>

    !!! help "`SANS='numpy'`"

        !!! tip inline end w45 "ok"

            ```python
            # --- PYODIDE:env --- #
            @auto_run
            def _prepare():
                import numpy
                # Utiliser numpy ici...
            ```

        !!! danger w45 "NON !"

            ```python
            # --- PYODIDE:env --- #
            import numpy

            # numpy est ensuite accessible à
            # l'utilisateur depuis l'IDE !
            ```



### Mots clefs & opérateurs { #restrictions-keywords }

"Mot clef" est ici à prendre au sens large. Cela recouvre évidemment les mots clefs du langage, mais aussi quelques constantes (`True`, `False` et `None`) ainsi que l'utilisation de `f-strings` et des opérateurs mathématiques.

Ces restrictions doivent être indiquées après le séparateur `{{AST()}}` dans l'argument `SANS` :


!!! tip "Interdictions de mots clefs"

    |||
    |-|-|
    | Syntaxe | `SANS="... AST: mot_clef1 mot_clef2"`<br>Soit plus généralement : `... AST: identifiants` |
    | Mode d'application | Recherche des mots clefs ou combinaisons de mots clefs dans l'AST du code. |


Exemple :

```python title="SANS='AST: for'"
# Les boucles for sont interdites...
# Le commentaire précédent ne lève pas d'erreur !

for _ in (): pass        # Lève ExclusionError
```

<br>

Concernant les mots clefs, il y a quelques subtilités qu'il faut garder à l'esprit. Notamment :

| Concerne... {{width(12)}} | Particularité |
|-|-|
| `#!py in` | Affecte uniquement les instructions de vérification de contenu, pas les boucles `#!py for`. |
| Restrictions larges<br>(ex: `#!py else`) | Les mots clefs autres que `in` sont recherchés partout où il peuvent être trouvés.<br>Par exemple, dans le cas de `#!py else`:<br>{{ul_li([
    "Conditions",
    "Clauses terminales  des boucles `#!py for` ou `#!py while`",
    "Dans les blocs `#!py try ... except ... else`"
])}} |
| Restrictions ciblées<br>(ex: `#!py for_else`) | Il est possible de trouver une version composée de certains mot clefs, qui permet de cibler des utilisations particulières afin d'obtenir un meilleur contrôle sur ce qui est interdit ou pas.<br>Par exemple :<br>{{ul_li([
    "`#!py for_else` pour interdire uniquement le `#!py else` des boucles `#!py for`",
    "`#!py while_else`",
    "`#!py try_else`",
    "...",
])}} |
| Boucles classiques<br>vs compréhensions | Il est possible d'interdire spécifiquement l'une ou l'autre, en utilisant :<br>{{ul_li([
    "`for_comp` pour interdire les compréhensions.",
    "`for_inline` pour interdire les boucles classiques.",
])}} |
| Constantes | `#!py True`, `#!py False` et `#!py None` sont considérés comme des mots clefs par le language lui-mème, donc ils ont été ajoutés à la liste des exclusions possibles. |
| `#!py f"{ ... }"` | Un "bonus" proposé par le thème, car il est difficile d'interdire les conversions en chaînes de caractères sans cette restriction.<br>Utiliser `f_str` ou `f_string` en tant que mot clef pour les interdire. |
| `#!py as` | Le mot clef `#!py as` n'est pas couvert par le thème (intérêt trop limité). |

<br>

{{ kws_exclusions() }}

<br>

!!! help "Exemples d'utilisation des règles d'exclusions"

    === "Interdire une méthode"

        Si on va au plus simple, il suffit d'indiquer le nom de la méthode à interdire avec un point devant dans l'argument `SANS` :

        {% raw %}
        ```markdown
        {{ IDE(..., SANS=".count") }}
        ```
        {% endraw %}

        Ceci n'empêche cependant pas un utilisateur de passer par `getattr`. On peut alors ajouter cette fonction en tant que builtin interdit, et interdire également les méthodes `.__getattr__` :


        {% raw %}
        ```markdown
        {{ IDE(..., SANS=".count .__getattr__ getattr") }}
        ```
        {% endraw %}

        S'il reste nécessaire d'utiliser `getattr` dans l'exercice et qu'on souhaite que la fonction laisse passer certains appels, il faut:

        - Ne pas renseigner la fonction dans l'argument `SANS`
        - En construire une version personnalisée dans la section `env`, levant `ExclusionError` aux moments appropriés, et qui est ensuite supprimée de l'environnement depuis la section `post` de l'exercice.
        {{pmt_note("La même logique est applicable aux terminaux, en travaillant cette fois avec les sections `env_term` et `post_term`.")}}



    === "Interdire un module"

        Les interdictions de modules sont les plus simples à mettre en place : il suffit de donner le nom du module dans l'argument `SANS`.

        {% raw %}
        ```markdown
        {{ IDE(..., SANS="numpy") }}
        ```
        {% endraw %}

        Ceci couvre tous les cas/syntaxes d'imports envisageables, et fonctionne aussi bien sur des modules internes, des `python_libs` venant avec le site construit/PMT, ou des bibliothèques externes à installer durant les exécutions.


    === "Interdire un opérateur"

        {{pmt_note("Il est à noter qu'interdire un opérateur et les fonctions faisant le même travail peut très vite se révéler très fastidieux et nécessite une bonne connaissance de python pour couvrir le maximum de cas.", 0)}}

        Par exemple, pour interdire la multiplication, il faut :

        * Interdire `*`
        * Interdire le module `operator`
        * Interdire le module `math` (à cause de la fonction `prod`)
        * Interdire les méthodes `.__mul__`, `.__rmul__` et `.__imul__` (accessibles sur les objets `int` et `float`)
        * Si on va au bout de la logique, interdire `getattr`, `.__getattribute__` et `.__getattr__` à cause du point précédent.
        * Potentiellement interdire le module `fractions`, selon ce que l'on veut faire ou pas dans l'exercice
        * Potentiellement interdire l'opérateur `/` également, selon ce que l'on veut faire ou pas dans l'exercice (à cause des opérations du type `a / (1/b)`, notamment avec des fractions).
        {{pmt_note("Noter que cette nouvelle interdiction implique également de bloquer de nouvelles méthodes concernant les soustractions...")}}

        Au final :

        {% raw %}
        ```markdown
        {{ IDE(...,
            SANS = "operator math getattr
                    .__getattr__ .__getattribute__ .__mul__ .__rmul__ .__imul__
                    AST: *
           ") }}
        ```
        {% endraw %}

        <br>

        {{ IDE(
            "exemples/restrictions_mul",
            SANS="
                operator math getattr
                .__getattr__ .__getattribute__ .__mul__ .__rmul__ .__imul__
                AST: *
            ",
            TEST = Case(code=1, subcases=[
                Case(term_cmd="2 * 3",                                       in_error_msg="FORBIDDEN: don't use *"),
                Case(term_cmd="(2).__mul__(3)",                              in_error_msg="FORBIDDEN: don't use .__mul__"),
                Case(term_cmd="int.__mul__(2,3)",                            in_error_msg="FORBIDDEN: don't use .__mul__"),
                Case(term_cmd="getattr(2, '__mu'+'l__')(3)",                 in_error_msg="FORBIDDEN: don't use getattr"),
                Case(term_cmd="3 . __getattribute__('__rmul__')(2)",         in_error_msg="FORBIDDEN: don't use .__getattribute__"),
                Case(term_cmd="from math import prod ; prod((2,3))",         in_error_msg="FORBIDDEN: don't use math"),
                Case(term_cmd="from operator import mul ; mul(2,3)",         in_error_msg="FORBIDDEN: don't use operator"),
                Case(term_cmd="from operator import __mul__ ; __mul__(2,3)", in_error_msg="FORBIDDEN: don't use operator"),
            ])
        ) }}


### Work around... { data-search-exclude }

Si vous souhaitez tout de même utiliser des fonctions interdites depuis les tests secrets, il est en fait possible de récupérer les fonctions originales et de les utiliser.

Notez cependant qu'il est alors indispensable d'exécuter les tests `secrets` dans une fonction afin de ne pas définir la fonction d'origine dans le scope de l'utilisateur, ce qui, outre le fait de la rendre disponible aussi pour lui, lèverait une erreur lors de la vérification finale des restrictions (voir point précédent).

<br>

=== "Avec `@auto_run`"

    Le [décorateur `auto_run`](--tests-auto_run) peut aider à simplifier l'écriture des tests, en évitant d'avoir à appeler les fonctions puis de les effacer à la main (onglet suivant) et en couvrant plus de cas.


    ```python title="Extraction des builtins d'origine, avec SANS='sorted'"
    @auto_run
    def test_anti_leak_function():
        sorted = __move_forward__('sorted')
        arr = [1, 3, 7, 1, 25, 8, 5, 2]
        exp = sorted(arr)
        assert func(arr) == exp, "some message"

    @auto_run
    def test_anti_leak_import():
        __import__ = __move_forward__('__import__')
        module = __import__("forbidden_module")
        assert ...
    ```

=== "À la main"

    ```python title="Extraction des builtins d'origine, avec SANS='sorted'"
    def test_anti_leak_function():
        sorted = __move_forward__('sorted')
        arr = [1, 3, 7, 1, 25, 8, 5, 2]
        exp = sorted(arr)
        assert func(arr) == exp, "some message"

    def test_anti_leak_import():
        __import__ = __move_forward__('__import__')
        module = __import__("forbidden_module")

        assert ...


    test_anti_leak_function() ; del test_anti_leak_function
    test_anti_leak_import() ; del test_anti_leak_import
    ```
