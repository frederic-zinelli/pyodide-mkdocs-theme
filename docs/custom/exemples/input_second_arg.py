# --- PYODIDE:code --- #
import sys

reminder = 'Réponses précédentes:\n'
txt = None
while txt!='stop':
    txt = input("Entrer du text (stop pour arrêter): ", reminder+'\n')
    reminder += txt+'\n'

print('Observer le contenu du terminal, pour comparaison')