# --- PYODIDE:env --- #
if 'ENC' not in globals():          # define once only
    ENC = "utf-16"

def upload(txt_as_bytes:bytes, name):
    print('Decoding', name, 'as', ENC)
    return txt_as_bytes.decode(ENC)[:50] + "[...]"

print("env!")
data = await pyodide_uploader_async(upload, read_as='bytes', multi=True, with_name=True)
# `data` is available NOW!

# --- PYODIDE:code --- #
print('---')
print(*(s[:50]+'...' for s in data), sep='\n')
print("\n(you can modify `ENC` from the terminal)")

# --- PYODIDE:post --- #
print("post!")
