# --- PYODIDE:env --- #

import matplotlib       # Indispensable (déclenche la déclaration de PyodidePlot)

pmt_plot = PyodidePlot('div_show')

# --- PYODIDE:code --- #

# pmt_plot: PyodidePlot
import matplotlib.pyplot as plt

pmt_plot.plot_func(lambda x:x**2, range(1,10), 'r-', show=False)

plt.title('plt.show() from outside')
plt.show()