# --- PYODIDE:code --- #
# (comment or uncomment lines)

2 * 3
# (2).__mul__(3)
# int.__mul__(2,3)
# getattr(2, '__mu'+'l__')(3)
# 3 . __getattribute__('__rmul__')(2)
# from math import prod ; prod((2,3))
# from operator import mul ; mul(2,3)
# from operator import __mul__ ; __mul__(2,3)