# --- PYODIDE:env --- #
def upload(content:str):
    print("This will show up in the terminal!")
    return content[:50]+' [...]'

print("env!")
data = await pyodide_uploader_async(upload)
# `data` available NOW!

# --- PYODIDE:code --- #
print("From `PYODIDE:code`, data is now:")
print(data)

# --- PYODIDE:post --- #
print("post!")
