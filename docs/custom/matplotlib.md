Le thème propose des fonctionnalités pour faciliter l'intégration de graphiques réalisés avec [`matplotlib`][matplotlib]{: target=_blank } dans la documentation, créés à partir de données/codes dans pyodide.

Ces outils sont une évolution d'un travail proposé initialement par Nicolas Révéret.





## Principe général { #matplotlib-generalities }

L'idée générale pour que cela fonctionne est la suivante :

1. Insérer dans le fichier markdown un élément html (une `<div>`) qui accueillera la figure une fois qu'elle sera créée.
1. Importer [`matplotlib`][matplotlib]{: target=_blank } depuis pyodide.
1. Configurer `matplotlib` pour interagir avec le DOM.
1. Construire les données et afficher les courbes souhaitées dans la `div` créée au point 1.

<br>

* L'étape 1 est facilitée par l'utilisation de la macro [`{% raw %}{{ figure(...) }}{% endraw %}`](--redactors/figures/).
* L'étape 2 est faite par l'utilisateur, normalement depuis la [section `env`](--ide-sections) du fichier python associé à une macro `IDE`, `terminal`, `py_btn` ou `run`.
* L'étape 3 est gérée automatiquement par le thème, lorsqu'un import de `matplotlib` est détecté (étape 2).
* L'étape 4 est facilitée par l'utilisation d'objets de [la classe `PyodidePlot`](--pyodide-plot) (déclarée automatiquement par le thème, lors de l'étape 2), qui permettent de tracer les figures directement au bon emplacement dans le DOM (la `div` du point 1, donc).






## Exemples

### Exemple simple

Concrètement, pour la situation la plus simple où ___une seule figure___ doit être affichée dans la page, il suffit d'insérer un appel de macro [figure](--redactors/figures/) et l'élément comportant le code python (`IDE`, `terminal`, `py_btn` ou `run`).

___Remarques :___

* Dans l'IDE ci-dessous, tous les codes sont groupés dans la section `code`, mais il est possible de mettre une partie de la logistique dans la section `env`, afin que l'utilisateur ne voit pas ce qui concerne les objets `PyodidePlot`.
* L'installation de `matplotlib` peut être ___très___ longue...

<br>

```markdown
{% raw %}{{ IDE('exemples/plot_1') }}

{{ figure() }}{% endraw %}
```

Permet d'obtenir :

{{ IDE('exemples/plot_1', TERM_H=3) }}

{{ figure() }}



### Plusieurs figures

Pour tracer plusieurs figures dans la même page, il faut utiliser des `id` html différents pour les deux figures, et il faut ensuite cibler chaque figure avant d'y démarrer les tracés :

<br>

```markdown
{% raw %}
{{ IDE('exemples/plot_2') }}

!!! help ""
    {{ figure('cible_1') }}

    {{ figure('cible_2') }}
{% endraw %}
```

Permet d'obtenir :

{{ IDE('exemples/plot_2', TERM_H=3) }}

!!! help ""
    {{ figure('cible_1') }}

    {{ figure('cible_2') }}




## La classe `PyodidePlot` { #pyodide-plot }

!!! warning "Disponibilité de la classe `PyodidePlot`"

    La classe `PyodidePlot` n'est déclarée dans l'environnement pyodide qu'_après_ que `matplotlib` ait été installé.
    <br>Cela signifie que la classe ne peut être utilisée dans une [section](--ide-sections) que si `matplotlib` y est également importé, ou s'il a déjà été importé précédemment.

<br>

!!! warning "L'utilisation des objets PyodidePlot a été simplifiée"

    Les objets de la classe PyodidePlot permettent en fait de faire plus que simplement pointer vers la bonne balise/figure, mais ces fonctionnalités se sont révélées trop spécifiques et la méthode `target` permet normalement de couvrir tous les besoins en restant au plus proche d'une utilisation normale de `matplotlib` dans l'environnement du thème.

    Les informations ci-dessous sont donc fournies à fins de complétude et restent d'actualité, même si elles ne sont à priori plus nécessaires.


### Généralités

Cette classe est déclarée automatiquement au premier import de `matplotlib` dans pyodide.

* Son constructeur prend un seul argument, optionnel, qui est l'identifiant html (`id`) de la balise `div` qui devra accueillir la figure.
* Si cet argument n'est pas fourni, c'est la valeur par défaut pour [l'argument `div_id` de la macro `figure`](--redactors/figures/) qui est utilisé.
* Ceci est compatible avec la configuration via les [fichiers {{meta()}} et les entêtes des fichiers markdown](--custom/metadata/).

L'objet `PyodidePlot` ainsi créé peut ensuite être utilisé en remplacement de `matplotlib.pyplot` : il relaie automatiquement tous les appels de méthodes à la fonction correspondant du module `pyplot`. Deux exceptions à cela :

* La méthode __`PyodidePlot.plot`{.green}__ met en place le tracé de la figure dans la `div` voulue, avant de relayer l'appel à `pyplot.plot`. Il faut donc _{{red("ne surtout pas utiliser")}} `pyplot.plot`{.red}_

* La méthode `PyodidePlot.plot_func` n'existe pas dans le module `pyplot`. C'est un ajout du thème, qui permet d'obtenir en un seul appel le tracé d'une fonction sur un domaine donné, en prenant en charge la construction des ordonnées, le titre éventuel de la figure et l'appel final à `pyplot.show()`.


??? tip "Accéder au code de la classe"

    Il est possible d'utiliser l'un des scripts du thème pour voir le code complet de la classe :

    ```
    python -m pyodide_mkdocs_theme --plot
    ```

    La classe utilisée dans le thème est rigoureusement identique à celle présente dans ce fichier.
    <br>Certaines instructions au tout début du fichier ne correspondent par contre pas au code utilisé dans le thème, car le but de ce fichier est de pouvoir exécuter les fichiers pythons de la documentation utilisant `matplotlib` en local (voir l'admonition suivante).


??? tip "Exécution locale de fichier python utilisant la classe `PyodidePlot`"

    Si vous souhaitez exécuter des fichiers python utilisant `matplotlib` en local, il faut "ruser" un peu, mais le thème fournit les outils nécessaires :

    1. Installer `matplotlib` sur la machine/dans l'environnement, si ce n'est pas déjà fait.

        !!! note "{{orange("Ne pas ajouter `matplotlib`{.orange} au fichier `requirements.txt`{.orange}")}}"
            La bibliothèque `matplotlib` est ici une _dépendance de développement_, et non une dépendance de la documentation.

    1. Utiliser la commande ci-dessous, pour récupérer le fichier `pyodide_plot.py` du thème et le placer à la racine du projet :

            python -m pyodide_mkdocs_theme --plot -F pyodide_plot.py

    1. Ajouter ceci au tout début du fichier python __de la documentation__ que vous voulez exécuter en local :

        ```python
        # --- PYODIDE:ignore --- #
        from pyodide_plot import PyodidePlot
        ```

        Ce code ne sera exécuté qu'en local, les sections `ignore` des fichiers n'étant pas récupérées pour le site construit avec mkdocs.


??? help "Documentation complète de la classe `PyodidePlot`"

    ::: pyodide_plot.PyodidePlot
        options:
            members_order: "source"
