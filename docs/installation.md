!!! warning "Mise à jour depuis [`pyodide-mkdocs`][pyodide-mkdocs]{: target=_blank }"

    Presque toutes les fonctionnalités de [pyodide-mkdocs][pyodide-mkdocs]{: target=_blank } restent présentes dans [pmt-pypi][pmt-pypi]{: target=_blank }, mais certaines modifications ont dû être apportées au projet.

    Ainsi, le passage à la version "thème" nécessitera d'appliquer quelques modifications à votre documentation, aussi bien du côté de l'environnement (packages installés) que de la documentation elle-même.<br>
    Voir la page concernant [la mise à jour vers pyodide-mkdocs-theme](--maj_pyodide_mkdocs/), si c'est ce que vous voulez faire.


## Installation via pip {{anchor_redirect(id="installation-via-pip")}} { #install-pip-install }


Le thème fonctionne avec [Python][python]{: target=_blank } 3.8^+^ et est [disponible sur PyPI][pmt-pypi]{: target=_blank }. Il peut donc être installé dans n'importe quel projet en exécutant cette commande dans un terminal, sur une machine avec une connexion internet :

```code
pip install pyodide-mkdocs-theme
```


## Démarrer un nouveau projet { #new-project-install }

Si le travail est effectué en local et une fois le thème installé, il est possible de démarrer un nouveau projet de documentation grâce à l'un des [scripts](--scripts/) disponibles avec le thème :

```code
python -m pyodide_mkdocs_theme --new PROJECT_NAME
```

<br>

Cette commande, exécutée dans un terminal, crée un nouveau dossier nommé `PROJECT_NAME` dans le répertoire en cours, et y ajoute les fichiers de base pour démarrer un projet avec le thème :

!!! quote inline end w30 "Fichiers & dossiers créés"

    ```
    ...
    └── PROJECT_NAME
        ├── docs
        │   ├── index.md
        │   └── exo.py
        ├── .gitignore
        ├── main.py
        ├── mkdocs.yml
        └── requirements.txt
    ```

* `mkdocs.yml`, avec une configuration minimale nécessaire pour le thème. Ne pas oublier d'y modifier ensuite les champs `site_name` et `site_url`.
* `requirements.txt`, qui contient toutes les dépendances du projet.
* `.gitignore`, avec des réglages génériques pour un projet de documentation avec python.
* `main.py`, avec la logistique pour modifier certains messages du thème en place. Ce fichier est optionnel et peut être supprimé ou modifié (ajout de macros personnalisées).
* `docs/index.md` et `docs/exo.py`, qui donnent des exemples d'utilisation des macros du thème.


##Dépendances {{anchor_redirect(id="dependances")}} { #dependencies-install }

Le thème est compatible [python 3.8^+^][python]{: target=_blank }.
<br>Les dépendances sont téléchargées et installées automatiquement lors de l'installation :

- [mkdocs 1.6^+^][mkdocs]{: target=_blank }
- [mkdocs-material 9.5^+^][mkdocs-material]{: target=_blank }
- [mkdocs-macros 1.0^+^][mkdocs-macros]{: target=_blank }

<br>

Comme le thème hérite de [`mkdocs-material`][mkdocs-material]{: target=_blank } toutes les fonctionnalités de ce dernier sont également disponibles.<br>


{{ md_include("docs_tools/inclusions/material_plugins_prefixes_note.md") }}



## `mkdocs.yml` : réglages {{anchor_redirect(id="activation-dans-mkdocsyml")}} { #mkdocs_yml-settings-install }


!!! danger inline end w40 margin-top-h4

    Si l'argument `--file` ou `-F` pointe vers un fichier existant, ce fichier sera écrasé sans demande de confirmation préalable.




### Configuration par défaut pour le thème { #mkdocs_yml-default-install }

Si le thème est déjà installé, il est possible de récupérer un fichier `mkdocs.yml` préconfiguré en exécutant le script ci-dessous.
<br>Il est également possible de simplement afficher le contenu du fichier modèle dans le terminal, si l'argument `--file` est omis :

```
python -m pyodide_mkdocs_theme --yml --file "mkdocs_.yml"
```





### Configuration du fichier { #mkdocs_yml-config-install }


Voici ci-dessous le contenu d'une configuration ___minimale requise___ pour utiliser le thème avec mkdocs.

!!! danger "Ne pas utiliser `navigation.instant`"

    Le thème n'est {{red("pas compatible avec l'outil `navigation.instant`{.red}")}} de [mkdocs-material][mkdocs-material]{: target=_blank }.
    <br>Si cette extension est activée par mégarde, une erreur sera levée.

<br>

```yaml
theme:
    name: pyodide-mkdocs-theme
#   features:
#       - navigation.instant   # /!\ Cette option est incompatible avec le thème !


plugins:
  - search
  - pyodide_macros
      on_error_fail: true   # Fortement conseillé...


# exclude_docs nécessite mkdocs 1.6+ pour fonctionner correctement. Sinon, vous pouvez utiliser
# le plugin mkdocs-exclude à la place.
# Ne surtout pas mettre de commentaires sur la ligne d'exclusion des fichiers python... !
exclude_docs: |
  **/*_REM.md
  **/*.py


markdown_extensions:
  - md_in_html              # !!REQUIS!!
  - admonition              # !!REQUIS!! Blocs colorés:  !!! info "ma remarque"
  - attr_list               # !!REQUIS!! Un peu de CSS et des attributs HTML, ex: { #id .class style="display:none" }
  - pymdownx.details        # !!REQUIS!! Admonition: ??? -> peuvent se déplier ; ???+ -> peuvent se replier.
  - pymdownx.emoji:         # !!REQUIS!! Émojis:  :boom:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
  - pymdownx.highlight      # !!REQUIS!! Coloration syntaxique du code
  - pymdownx.inlinehilite   # !!REQUIS!! Coloration syntaxique pour les "code spans": `#!python  code_python`
  - pymdownx.snippets:      # !!REQUIS!! Inclusion de fichiers
      check_paths: true     # Fortement conseillé...
  - pymdownx.superfences:   # !!REQUIS!!
      custom_fences:        # (custom_fences: uniquement si vous comptez utiliser mermaid)
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.arithmatex:    # !!REQUIS!! Pour LaTex
      generic: true
```



![alt](!!color_palette_png){: loading=lazy .w40 align=right .margin-top-h3 }

### Changer la palette de couleurs { #color-palette-install }

Il est possible de modifier la palette de couleurs utilisée pour les modes "jour" et "nuit".

Comme le thème vient avec un réglage par défaut pour les modes jour/nuit, il est nécessaire de redéfinir _l'intégralité_ de la palette de couleurs dans la section `theme`, pour que tout fonctionne correctement :

```yaml
theme:

  palette:        # Palettes de couleurs jour/nuit
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: orange
      accent: deep orange
      toggle:
        icon: material/weather-sunny
        name: Passer au mode nuit
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: orange
      accent: deep orange
      toggle:
        icon: material/weather-night
        name: Passer au mode jour
```

### Configuration du plugin du thème, `pyodide_macros`

Le thème vient avec son propre plugin, qui permet de mettre en place toutes les fonctionnalités nécessaires.

[De très nombreuses options peuvent y être configurées](--custom/config/) pour modifier le comportement du thème selon vos besoins.

??? tip "Hiérarchie complète de toutes les options du plugin"

    Les explications détaillées peuvent être trouvées dans [cette page](--custom/config/).

    --8<-- "docs_tools/inclusions/config_plugin.md"



### Plugins : pièges à éviter { #plugins-traps-install }

{{ md_include("docs_tools/inclusions/mkdocs_plugins_declaration.md") }}

{{ md_include("docs_tools/inclusions/material_plugins_prefixes_note.md") }}




### L'option `use_directory_urls` { #use_directory_urls-install }

Le thème fonctionne quel que soit le réglage utilisé pour l'option `use_directory_urls` (par défaut: `true`).

Le choix des noms de fichiers markdown de la documentation n'a pas non plus d'effet sur la façon de renseigner les fichiers python utilisés pour les macros du thème (`IDE`, `IDEv`, `terminal`, ...), quels que soient :

- La valeur de l'option `use_directory_urls`.
- Le nom du fichier markdown source: `index.md` ou `mon_fichier.md` (le second cas ayant un comportement à part pour certaines opérations, lorsque `use_directory_urls` vaut `true`).

<br>

!!! help "Construire une version locale de la documentation"

    Moyennant certaines contraintes à respecter, il est possible de [créer une version locale de la documentation][mkdocs-local], qui peut être ouverte directement dans un navigateur, après avoir fait un `mkdocs build`.
    <br>Les réglages à modifier dans le fichier `mkdocs.yml` avant le `build` sont :

    1. `site_url: ""`
    1. `use_directory_urls: false`
    1. Désactiver le plugin de recherche (en commentant la ligne / Nota: le plugin n'empêche pas le rendu, mais ne fonctionne pas normalement, dans cette situation).

    {{ orange("_Noter cependant qu'une connexion internet active reste indispensable au bon fonctionnement des pages : seul l'hébergement en ligne de la documentation est évité._") }}
