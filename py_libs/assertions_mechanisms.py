
from typing import Any, Callable
import py_libs.sequential_tests as seq


def ok(): pass
def fail(): assert False, "failure"

def equal(act,exp, msg=''):
    msg = f"{ msg }{ ': '*bool(msg) }{act!r} should be {exp!r}"
    assert act == exp, msg


def should_raise(func, kind=AssertionError, msg=""):
    print("Test against", kind.__name__, ': ', msg, end='\n\t')
    e = '"Did not raise..."'
    try:
        func()
    except kind:
        print('Raised', kind.__name__)
        return
    except Exception as exc:
        e = exc
    assert False, f'Should have raised {kind.__name__} but was {e} (type={type(e)})'


def not_defined(name:str, explain=""):
    assert name not in seq.GLOBS, f"{name!r} shouldn't be defined/accessible"
    if explain:
        print(explain)


def not_importable(name:str):
    should_raise(lambda: __import__(name), ExclusionError, name+" shouldn't be importable")


def truthy(that:Any, explain=""):
    """ Check that @that is truthy. """
    str_that = str(that)
    assert that, explain or str_that
    print('Available:', str_that[:80] + '...' * (len(str_that) > 80))


def test_stdout(
    func:Callable[[],None],
    *expected:str
):
    """
    Clear the current stdout content, then run the callable and check that the stdout contains
    the given chunks (in the given order).

    (NOTE: Only usable for python stdout, not for terminal_message testing).
    """
    import sys
    sys.stdout.truncate(0)
    sys.stdout.seek(0)
    func()
    out:str = sys.stdout.getvalue()
    i = 0
    for chunk in expected:
        i = out.find(chunk, i)
        assert i>=0, f"Couldn't find {chunk!r} in the output:\n\n{ out }"
