
from py_libs.sequential_tests import *
from py_libs.assertions_mechanisms import *

print('installed py_libs')



import sys

class StdOutAsserter:
    def __init__(self):
        self.store = []
    def gather(self):
        self.store.append(sys.stdout.getvalue())
    def check(self,exp, remove_spaces=False):
        act = ''.join(self.store)
        if remove_spaces:
            act = ''.join(c for c in act if not c.isspace())
        assert act==exp, f'{act!r} should be {exp!r}'
        print('StdOut test : OK!')