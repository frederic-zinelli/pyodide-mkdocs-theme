from typing import Callable


GLOBS = {}
local_N = VAR = None



def auto_N(globs=GLOBS, var='N', reset_on=None):
    """
    Generate then auto-update a global value in the given scope dict (@globs) after
    each run. Starts with 1, not 0.

    @var='N': name of the variable
    @reset_on: if given, the value will be set to 1 again when started with this
    value (applies right away).
    """
    global GLOBS, local_N, VAR
    if var not in globs:
        GLOBS = globs
        VAR   = var
        globs[VAR] = 0
    if reset_on is not None and globs[VAR] >= reset_on:
        globs[VAR] = 0
    globs[VAR] += 1
    local_N = globs[VAR]      # initiate a "global" counter (internal to the current module)




def do_it(ok=None, reset=False):
    """
    Boolean generator, returning True if @reset is True or if this call is the Nth one in
    the current run.
    Using this as a condition allows to run specifically some statements.
    """
    global local_N
    if reset: GLOBS[VAR] = 0
    if reset or not (local_N := local_N-1):
        if ok is not None:
            print(f"case {GLOBS[VAR]}: should { 'pass' if ok else 'FAIL' }")
        return True
    return False





def apply_Nth(*cbks:Callable, display=False, update=True):
    """
    On the N(th run, execute the Nth callback (ie. the one at index N-1).
    If @display is True, print "Case [i}" to the console (1-indexed), before the call.
    if a callback is falsy, just do nothing.
    """
    i = GLOBS[VAR] - 1
    if display:
        print('Case', i+1)
    return cbks[i] and cbks[i]()
