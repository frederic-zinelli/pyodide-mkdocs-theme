
from functools import lru_cache
from typing import Iterable, Literal, Optional

from pyodide_mkdocs_theme.__main__ import LANGS
from pyodide_mkdocs_theme.pyodide_macros.plugin import PLUGIN_CONFIG_SRC
from pyodide_mkdocs_theme.pyodide_macros.parsing import items_comma_joiner

from python_devops.config import TEMPLATES_PARTIALS, JS_CONFIG_LIBS

from pmt_macros._macros_logistic import MACROS, global_macro

# pylint: disable=missing-function-docstring, redefined-outer-name, multiple-statements, redefined-builtin
render_inner_macros = MACROS.render_inner_macros







def make_color_macro(color:str):

    @global_macro(color)
    def wrapper(msg:str, shadow:str=""):
        shadow = shadow and f'style="text-shadow: 0 0 0 { shadow }"'
                            # the 0 0 0 DOES have an effect... XD
        return f'<span class="{ color }" { shadow }>{ msg }</span>'



def macro_txt_with_color_factory(txt:str, color:str):
    """
    Build a macro with @txt name and taking no argument, putting that name in the given color.

    Note `annexe` macro takes an @extra="" argument
    """
    if txt == 'annexe':
        @global_macro(txt)
        @lru_cache(None)
        def wrapper(extra=""):
            msg = txt if not extra else f"{txt} {extra}"
            return f'<span class="{color}">{ msg }</span>'

    else:
        msg = f'<span class="{ color }">{ txt }</span>'
        @global_macro(txt)
        def wrapper():
            return msg




for color in 'red green orange blue gray cyan magenta yellow olive primary'.split():
    make_color_macro(color)

macro_txt_with_color_factory('source',  'orange')
macro_txt_with_color_factory('sources', 'orange')
macro_txt_with_color_factory('annexe',  'olive')
macro_txt_with_color_factory('annexes', 'olive')






@global_macro
def sep():            return '<hr class="sep"/>'


@global_macro
def fake_h3(txt:str): return f'<p class="rem_fake_h3" markdown>{txt}</p>'


@global_macro
def insecable(n=1):   return "\u202f" * n


@global_macro
def back_slash():     return "\\"


@global_macro
def raw(that:Literal['start','end']=None):
    start = '{% raw %}'
    end   = '{% endraw %}'
    if that is None:
        return f"{start} ... {end}"
    elif that=='start':
        return start
    return end




META_FILE      = PLUGIN_CONFIG_SRC.build._pmt_meta_filename.default      # pylint: disable=protected-access
META_FILE_CODE = f'`{ META_FILE }`'

@global_macro
def meta(wrap=1):
    return META_FILE_CODE if wrap else META_FILE


@global_macro
def get_langs_list(join:str='et'):
    return items_comma_joiner([*map("`{}`".format, LANGS)], join )





FILES_OVERRIDES = sorted(
    f'`{ file.name }`'  for file in TEMPLATES_PARTIALS.iterdir()
                        if file.suffix=='.html'
)

@global_macro
def partials_overrides():
    """ String (comma_join) of all the html files in templates/partials """
    return items_comma_joiner(FILES_OVERRIDES, 'et')


@global_macro
def partials_overrides_as_list():
    md_code = '\n'.join( f"* {file}" for file in FILES_OVERRIDES)
    return MACROS.env.indent_macro(md_code)



@global_macro
def config_lib_file():
    return str(JS_CONFIG_LIBS)



#-------------------------------------------------



@global_macro
def ul_li(items:Iterable[str], *, wrap='', tag='ul'):
    inner = ''.join( f'<li>{ wrap }{ s }{ wrap }</li>' for s in items )
    return f"<{tag}>{ inner }</{tag}>"

@global_macro
def ul_li_code(msg:str, sep=' '):
    return ul_li( msg.split(sep), wrap="`" )


@global_macro
def anchor_redirect(id:str='', *, kls:str='', mk_addr_skip=True):
    """
    @mk_addr_skip: if False, mkdocs_addresses will register this ID and check against it.
    """
    id  = id  and f' id="{ id }"'
    kls = f"{ 'anchor_redirect' * mk_addr_skip } { kls }".strip()
    kls = kls and f' class="{ kls }"'
    return f'<span{ id }{ kls }></span>'



@global_macro
def width(
    width:   Optional[float]=None,
    default: float = 0,
    *,
    left:    bool = False,
    center:  bool = False,
    right:   bool = False,
    justify: bool = False,
):

    s_style = ""
    align   = left + center*2 + right*4 + justify*8
    width   = default if width is None else width

    if align:
        s_style += f"text-align:{ TXT_ALGIN[align] };"

    if width:
        s_style += f"width:{ width }em;"

    return s_style and f'{ "{" }style="{ s_style }"{ "}" }'

TXT_ALGIN = {1:'left', 2:'center', 4:'right', 8:'justify'}




@global_macro
def pmt_note(txt:str, lf_location:Literal[-1,0,1]=-1):
    """
    Build a note (smaller + gray. See .pmt-note html class).

    * `lf_location ==-1`: add `<br>` before (default).
    * `lf_location == 0`: no line feed.
    * `lf_location == 1`: add `<br>` after.
    """
    location = MACROS.env.file_location()
    rendered = render_inner_macros(txt, location)
    lfl =  '<br>' * (lf_location < 0 )
    lfr =  '<br>' * (lf_location > 0 )
    return f"{ lfl }<span class=\"pmt-note\" markdown>{ rendered }</span>{ lfr }"




@global_macro
def annotate(annotation:str):
    return f'<span class="annotate">(1)</span><ol><li>{ annotation }</li></ol>'




def macro_py_factory(macro_name:str):
    """
    Macros factory of the "MACRO_py" kind, generating markdown like:

        ```markdown

        {{ MACRO(...) }}

        ??? tip {other_classes} "title"

            {{ py(sourcefile) }}

        ```

    With almost everything customizable:
        - admonition kind, classes, title,
        - admonition before or after the macro call,
        - include or not a "<br>' in between the two,
        - ...
    """

    @global_macro(f'{ macro_name }_py')
    def wrapper(
        exo:str,
        *section,               # For section_py only
        before:str=None,        # '!!!'
        after:str=None,         # '!!!'
        admo_kls:str='tip',
        py_title:str=None,
        add_empty_line:bool = False,
        **macro_args
    ):
        macro_args_as_str = ', '.join( f'{ k }={v!r}' for k,v in macro_args.items() )
        pos_args = ', '.join(map(repr, (exo,)+section ))

        admo_kind = before or after or MACROS.env.page.meta.get('ide_py_admo') or "???"

        split_exo_name = 'dev_docs' not in MACROS.env.page.file.src_uri
        source         = exo.split('/')[-1] if split_exo_name else exo
        py_title       = py_title or f"Contenu de `{ source }.py`"

        elements = [
            f"\n\n{ '{{' } { macro_name }({ pos_args }, { macro_args_as_str }) { '}}' }\n\n",
            "<br>" * add_empty_line,
            f"""\n\n{ admo_kind} { admo_kls } "{ py_title }"\n    { '{{' } py({exo!r}) { '}}' }\n\n""",
        ]
        ordered = reversed(elements) if before else elements

        out = render_inner_macros(''.join(ordered))
        return out

    wrapper.__doc__ = macro_py_factory.__doc__.replace('MACRO', macro_name)


macro_py_factory('IDE')
macro_py_factory('terminal')
macro_py_factory('py_btn')
macro_py_factory('section')
macro_py_factory('py')





def _fill_figure(
    title:str,
    args_as_str: str,
    figure_md_template: str = """
=== "TTTT"

    #### Syntaxe :

    ```markdown
    {% raw %}{{ figure(XXXX) }}{% endraw %}
    ```

    #### Résultat :

    {{ figure(XXXX) }}
""",
):
    s = figure_md_template.replace('TTTT', title)
    s = s.replace('XXXX', args_as_str.replace('\n', '\n        ') )

    # Fix closing `figure` macro call indentation (if needed)
    s = s.replace(      '\n        ) }}', '\n    ) }}')

    return s

@global_macro
def figure_md(*data):
    """
    Build as many {{ figure(..) }} as there are arguments.

    @*data: tuples (title, args_as_str)
    """
    code = '\n\n'.join(map(_fill_figure, *zip(*data)))
    out  = render_inner_macros(code)
    return out




@global_macro
def figure_macro_md(py:str, md_call:str):
    """
    Used in mermaid and matplotlib pages: build tabbed contents, macro vs sources
    """
    code   = md_call.replace('\n','\n    ')
    md_out = f"""
=== "`md` & `py`"

    ```markdown
     {'{% raw %}'}{ code }{'{% endraw %}'}
    ```

    <br>

    {'{{'} py('{ py }') {'}}'}

=== "Résultat"
    { code }
"""
    out = render_inner_macros(md_out)
    return out








@global_macro
def qcm_builder():
    out = f"""

<link href="{ MACROS.env.base_url }/extras/qcmBuilder.css" rel="stylesheet"/>
<fieldset id="qcm-builder-tool"></fieldset>
<script src="{ MACROS.env.base_url }/extras/qcmBuilder.js" type="module"></script>

"""
    return out
