


from typing import Set
from pyodide_mkdocs_theme.pyodide_macros.plugin.pyodide_macros_plugin import PyodideMacrosPlugin


MACROS: 'Macros' = None



def global_macro(name:str=None):

    def decorator(func):
        macro_name = name or func.__name__
        func.__qualname__ = func.__name__ = macro_name
        MACROS[macro_name] = func
        return func

    if callable(name):
        name, func = None, name
        return decorator(func)
    return decorator



class Macros(dict):

    md_include_path: Set[str]
    env: PyodideMacrosPlugin = None

    def __init__(self):
        super().__init__()
        self.md_include_path = set()

    def register_macros_in(self, env: PyodideMacrosPlugin):
        self.env = env
        for macro in self.values():
            env.macro(macro)


    def render_inner_macros(self, content, disk_location='<unknown>', src=None):

        env = self.env
        need_indents = env.is_macro_with_indent()

        if need_indents:
            indentations = env._parser.parse(content, disk_location)
            env._indents_store += [*reversed(indentations)]

        template = env.env.from_string(content)
        if src is not None:
            self.md_include_path.add(src)
        try:
            rendered = template.render(**env.variables)
        finally:
            if src is not None:
                self.md_include_path.remove(src)

        indent = need_indents and env.get_macro_indent()
        if indent:
            rendered = rendered.replace('\n', '\n' + indent)
        return rendered


MACROS = Macros()
