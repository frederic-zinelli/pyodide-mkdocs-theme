
from functools import lru_cache
import re
from typing import Any
from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros.parsing import items_comma_joiner
from pyodide_mkdocs_theme.pyodide_macros.plugin import (
    ARGS_MACRO_CONFIG,
    MacroConfigSrc,
    PLUGIN_CONFIG_SRC,
)
from pyodide_mkdocs_theme.pyodide_macros import MsgPlural
from pyodide_mkdocs_theme.pyodide_macros.plugin.config import TESTING_CONFIG
from pyodide_mkdocs_theme.pyodide_macros.plugin.tools.test_cases import CASES_OPTIONS
from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import KEYWORDS_SEPARATOR, PYTHON_KEYWORDS, IdeConstants

from pmt_macros.simple_md_or_html import ul_li
from pmt_macros._macros_logistic import MACROS, global_macro
from python_devops.docs_codes_updates.custom_config import ConfigTreeDocsYamlBlockDumper, MkdocstringsPageDocsDumper

# pylint: disable=missing-function-docstring, invalid-name, line-too-long


render_inner_macros = MACROS.render_inner_macros




@global_macro
def rec_limit():
    return IdeConstants.min_recursion_limit


@global_macro
def Env():
    return MACROS.env



@global_macro
def Lang(prop:str, plural=False):
    s: MsgPlural = getattr(MACROS.env.lang, prop)
    return s.plural if plural else s.msg





def _get_config_data(option_path:str, tail:int):
    yaml_path  = PLUGIN_CONFIG_SRC.get_plugin_path(option_path, no_deprecated=True)
    identifier = yaml_path.replace('.','_')
    if tail:
        option_path = ''.join( option_path.split('.')[-tail:] )
    return option_path, identifier


@global_macro
@lru_cache(None)
def config_link(option_path:str, tail:int=0, *, val:Any=None):
    """
    To make sure no outdated links/paths are used n the docs (get_plugin_path will implicitly
    validate de path).
    Declared here so that the cache is global to a serve and not just a build.

    @tail: takes the tail last elements of the option_path as text link (full it tail=0)
    """
    option_path, identifier = _get_config_data(option_path, tail)
    if val is not None:
        option_path = f"#!yaml { option_path }: {val}"
    return f"[`{ option_path }`](--{ identifier })"


@global_macro
@lru_cache(None)
def config_validator(option_path:str, tail:int=0, *, val:Any=None):
    """
    Same idea than config_link, but just validate the path and return the bare string
    (handling tail).
    If @val is given, add it a the end as value, and wraps the whole string in a yaml
    inline code span.
    """
    option_path, _ = _get_config_data(option_path, tail)
    if val is not None:
        option_path = f"`#!yaml { option_path }: {val}`"
    return option_path





@global_macro
def macro_signature(config_name):
    config: MacroConfigSrc = ARGS_MACRO_CONFIG.subs_dct[config_name]
    return config.signature_for_docs()




@global_macro
def macro_args_table(
    config_name,            # Class name
    with_headers=False,     # Add the headers on top of the table rows
    with_text=False,        # add generic text before the table (specific, see documentation... :p )
    only:str=None,          # Argument name: if given create a table with headers and only this argument
    width1:float = None,
    width2:float = None,
    with_meta_info: bool = True,
):
    width = MACROS.env.macros['width']     # accessed at runtime only, to avoid any import order troubles

    config: MacroConfigSrc = ARGS_MACRO_CONFIG.subs_dct[config_name]
    table  = config.as_docs_table() if only is None else config.subs_dct[only].as_table_row()

    if with_headers:
        op,clo  = '{}'
        content = """\
- Tous les arguments sont optionnels.
- À part pour le premier, déclarer les arguments en précisant leur nom (donc sous forme d'arguments nommés).

<br>

""" * with_text + f"""
| Argument { width(width1, 9) } | Type/défaut { width(width2, 9) } | Rôle |
|-|-|-|
{ table }
""" + f"""
<br>

{op+op} md_include("docs_tools/inclusions/config_defaults_info.md") {clo+clo}
""" * with_meta_info

    elif only:
        content = f"""\
| Type { width(width1, 8) } | Défaut { width(width2, 0) } | Résumé |
|-|-|-|
{ table }"""

    else:
        raise ValueError("invalid call...")

    rendered = render_inner_macros(content)
    return rendered




@global_macro
def testing_yaml():
    """
    Build the yaml tree hierarchy for the testing SubConfig object.
    """
    schema: str = ConfigTreeDocsYamlBlockDumper.apply(TESTING_CONFIG)
    schema = schema.replace('plugins', 'plugins:\n    - pyodide_macros')
    return schema


@global_macro
def testing_conf():
    """
    Render the docs config of the `testing` elements.
    """
    md = ''.join(
        MkdocstringsPageDocsDumper.apply(opt, header_lvl=4)
            for opt in TESTING_CONFIG.elements
    )
    md = re.sub(r"[{]. anchor_redirect[^}]+?[}].\s*", "", md, flags=re.M)
    md = re.sub(r"([{]\s*#[\w.-]+)", r"\1-tests", md, flags=re.M)
    return render_inner_macros(md)




@global_macro
def cases_options_as_yaml_str():
    """ Documentation purpose (macros config) """
    options = [ f'`#!py "{ opt }"`' for opt in CASES_OPTIONS ]
    return items_comma_joiner(options)



@global_macro
def cases_signature():
    """ Documentation purpose (macros config) """
    arguments = ''.join( f'\n    { opt }: bool = False,' for opt in CASES_OPTIONS if opt )
    return MACROS.env.indent_macro(f"""
```python
Case({ arguments }
    term_cmd: str="",
    description: str="",
)
```
""")







@global_macro
def meta_tests_config(**expected_dct):
    """
    For dev_docs "testing"
    """

    table = ['\n<br>\n\n## Configuration PMT vue depuis ce fichier','','| name | value | expected |', '|:-|:-:|:-:|']
    wrongs = []
    for name,exp in expected_dct.items():
        actual = getattr(MACROS.env, name)
        fmt = ''
        if exp!=actual:
            fmt = '{.red}'
            wrongs.append((actual,exp))
        table.append(f'| `{ name }` | `#!py { actual }`{fmt} |`#!py { exp }` |')
    table.append('\n<br>\n\n')

    if wrongs:
        msg = ''.join( f"\n    {actual} should be {exp}" for actual,exp in wrongs)
        raise BuildError(f"Wrong meta config in {MACROS.env.file_location()}:{msg}")
    else:
        print(f'Meta config test: \033[32mPASS\033[0m ({ MACROS.env.file_location() })')

    return '\n'.join(table)



def ast_link(kls:str):
    return f"[`{ kls }`](https://docs.python.org/3/library/ast.html#ast.{ kls })"

KW_KEYS  = sorted(PYTHON_KEYWORDS, key=lambda k: (not k.isidentifier(), k))
KW_LINES = [ f"`{ kw }` ({ ', '.join(map(ast_link, PYTHON_KEYWORDS[kw])) })" for kw in KW_KEYS ]
KW_LIST_AS_MD = f"""

??? tip "Liste des mots clefs et opérateurs et leurs combinaisons (`SANS='AST: ...'`)"

    Ci-dessous, la liste de tous les mot clefs ou opérateurs utilisable avec l'argument `SANS` des macros `IDE`, `IDEv` et `terminal`.
    <br>Les noms de classes entre parenthèses correspondent aux classes du module python `ast` qui sont concernés par le mot clef ou opérateur en question (liens vers la documentation python, si besoin de clarifications).

    { ul_li(KW_LINES) }

"""

@global_macro
def kws_exclusions(): return KW_LIST_AS_MD

@global_macro
def AST(): return KEYWORDS_SEPARATOR
