
from pmt_macros._macros_logistic import MACROS
from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin

# Import everything so that the macros are actually registered
from . import (
    config_inclusions,
    img_svgs,
    md_files_inclusions,
    simple_md_or_html,
)

def define_env(env:PyodideMacrosPlugin):
    MACROS.register_macros_in(env)
