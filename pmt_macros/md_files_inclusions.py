
from functools import wraps
import re
from pathlib import Path
from typing import Callable
from mkdocs.exceptions import BuildError

from pmt_macros._macros_logistic import MACROS, global_macro
from python_devops.system_updates.licences import LICENSE_COMMENTS
from python_devops import config as paths_config

# pylint: disable=missing-function-docstring, invalid-name

md_include_path     = MACROS.md_include_path
render_inner_macros = MACROS.render_inner_macros





def _include(
    *src:    str,
    before: Callable[[str],str] = None,
    after:  Callable[[str],str] = None,
):
    """
    Generic md_include logistic, with potential actions before and after the macro
    content has been rendered.
    """

    if src in md_include_path:
        raise BuildError(
            'Cyclic md_include on path:' + ''.join(f'\n   {p}' for p in md_include_path)
        )

    content = ""
    disk_loc = None
    for path in map(Path, src):
        disk_loc = str( MACROS.env.docs_dir_cwd_rel / path )
        content += Path(path).read_text(encoding='utf-8')

    if before:
        content = before(content)
    rendered = render_inner_macros(content, disk_loc, src)
    if after:
        rendered = after(rendered)

    return rendered


global_macro('md_include')(_include)






@global_macro
def md_include_rm_license(path_name:str):
    path    = getattr(paths_config, path_name)
    content = path.read_text(encoding='utf-8')

    license = LICENSE_COMMENTS[path.suffix]         # pylint: disable=redefined-builtin
    if content.startswith(license):
        content = content[len(license):]

    indent = MACROS.env.get_macro_indent()
    return content.replace('\n', '\n'+indent)




@global_macro
def file_path_to(path_name:str, rel_to_name:str=None):
    path:Path = getattr(paths_config, path_name)
    if rel_to_name:
        rel  = getattr(paths_config, rel_to_name)
        path = path.relative_to(rel)
    return str(path)




def _increase_title_lvl_after(n:int):
    """
    Add one '#' to each title in a markdown content.
    """
    @wraps(_increase_title_lvl_after)
    def wrapped(rendered:str) -> str:
        rendered = re.sub(r'^#+ ', lambda m: n*'#' + m[0], rendered, flags=re.M)
        return rendered
    return wrapped



def _remove_install_suffix_before(md):
    """
    Post process all html ids.
    """
    out = re.sub(r'(#[\w-]+?)-install', r'\1', md)
    return out


@global_macro
def include_installation():
    return _include(
        "docs/installation.md",
        before = _remove_install_suffix_before,
        after  = _increase_title_lvl_after(1)
    )




def _update_IDE_refs_and_increase_IDs_before(content:str) -> str:
    content = re.sub(r'\s*\{\s*#tests-', ' { #IDEs-', content)
    content = re.sub(r',\s*ID=\d+', lambda m:f'{m[0]}1', content)
    return content

@global_macro
def IDE_tests_to_details():
    return _include(
        "docs/redactors/IDE-tests.md",
        before = _update_IDE_refs_and_increase_IDs_before,
        after  = _increase_title_lvl_after(1)
    )




def _update_builder_ref_and_add_png(content:str) -> str:

    for token in ("#qcm-builder }", "<br><br>"):
        if token not in content:
            raise BuildError(f"Couldn't find the {token!r} token in { QCM_BUILDER_PATH }")

    content =  content.replace(
        "#qcm-builder }", f"""#qcms-qcm-builder {'}'}

![alt](!!qcm-json-builder_png){'{'}: loading=lazy .w30 align=right {'}'}

Le [constructeur de fichier Json pour QCM](--qcm-builder) permet de créer facilement des fichiers `json` pour définir des QCMs.
<br>""")

    content = content.split('<br SPLIT_TOKEN>')[0]      # get rid of the tool itself
    return content

QCM_BUILDER_PATH = "docs/redactors/qcm_builder.md"

@global_macro
def qcm_builder_to_qcms():
    return _include(
        QCM_BUILDER_PATH,
        before = _update_builder_ref_and_add_png,
        after  = _increase_title_lvl_after(2)
    )







@global_macro
def md_include_palette(name:str):
    files = (
        "docs_tools/inclusions/palette-intro.md",
       f"docs_tools/inclusions/palette-{ name }.md",
        "docs_tools/inclusions/palette-css-vars.md",
    )
    return _include(
        *files, after=lambda content: content.replace("#XXX", "#"+name)
    )