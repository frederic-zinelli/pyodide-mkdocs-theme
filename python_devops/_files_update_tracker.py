
from pathlib import Path
from mkdocs.exceptions import BuildError
from pyodide_mkdocs_theme.pyodide_macros.pyodide_logger import get_plugin_logger



RAISE = []


logger = get_plugin_logger("DEV-pyodide", color=31)   # in RED



def handle_files_updates_outcome(is_release_setup):

    if RAISE and not is_release_setup:
        raise BuildError(
            "You need to restart serving so that the runtime will actually be up to date:\n  "
            +'\n  '.join(RAISE)
        )
    elif RAISE:
        logger.info(
            "Files modified:\n  "
            +'\n  '.join(RAISE)
        )




def write_with_feedback_if_needed(
    path:Path,
    content:str,
    src_content:str=None,
    raise_on_update:bool=False
) -> bool:
    """
    Update the content of the given @path file if @content differs from @scr_content.

    If @scr_content isn't given, use the current content of the file as source instead.

    If @raise_on_update is True and the file is updated, BuildError will be raised (this is
    generally used when the updated content makes the current running implementation different
    from the written code).
    Otherwise, returns a bool telling if the file got updated or not.

    WARNING: raising won't be done right away, actually, so that all the critical updates can
    be done. So a flag is set, and the error will be raised only at the end of the hook.
    """
    if src_content is None:
        src_content = path.read_text(encoding='utf-8')

    if content != src_content:
        logger.info(f"[hooks] - { path } updated")
        path.write_text(content, encoding=('utf-8'))
        if raise_on_update:
            RAISE.append(f"{ path } code has been modified.")
        return True
    return False
