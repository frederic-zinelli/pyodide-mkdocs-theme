
import json
from collections import defaultdict
from typing import Dict, List

from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk

from python_devops.config import *
from python_devops._files_update_tracker import write_with_feedback_if_needed

# pylint: disable=all






def gather_all_scripts_and_css_kinds_and_rebuild_Deps():
    """
    Rebuild the Cdn and Css dependencies, based on the js and css files found in the custom_dir.
    These files must be organized in the following manner:

    1. The parent directory is named `xxx-block`, where block is either the "css" string or the
       main.html block where to insert the file.
    2. The file itself is named `[beginning-]html_dependency_name.suffix`, where:
        - `beginning` will define the order of the dependencies declarations. It may be
          `html_dependency_name` directly.
        - `html_dependency_name` is the property name of the givens Dep in the HtmlDependencies class
        - `suffix` is the file extension, and will determine the Dep class to use (Cdn or Css)
    3. The DepKind is determined this way:
        - DepKind.always for anything that is not in the `js-per-pages` directory.
        - Or the `html_dependency_name` for files in `js-per-pages.`
    """

    non_modules = (
        JS_CONFIG_LIBS,
        MATHJAX_LIBS,
        JS_LEGACY_SUBSCRIBER,
    )
    # Scripts loaded as "text/javascript" instead of "module"



    def handle_js_file_dependencies(dep_name):
        nonlocal got_js_config

        is_module = file not in non_modules
        dep_block = section
        dep_kind  = 'always'
        type_     = "module" if is_module else "text/javascript"
        extra     = ''
        justify   = 0

        if is_module:
            store_data_for_jsconfig_json(file)

        if section=='pages':
            dep_block = 'content'
            dep_kind  = dep_name
            dep_name  = file.stem.split('-')[-2]
            justify   = 10
            extra    += find_classes_pool_config(file)

        elif file==JS_CONFIG_LIBS:
            got_js_config = True
            extra += ', extra_dump="dump_to_js_config"'

        elif file==MATHJAX_LIBS:        # LEGACY/BACKWARD COMPATIBILITY about "mathjax-libs.js":
            dep_name = 'mathjax'        # the "-libs" part must be kept because already documented

        html_dep_code = (
            f'{ dep_name : <{justify_prop}} = Cdn(Block.{dep_block}, DepKind.{ dep_kind+"," : <{justify}} '
            f'"{url_template}", type="{type_}"{ extra })'
        )

        return html_dep_code


    def store_data_for_jsconfig_json(file:Path):
        data = (file.stem, f'{ file.relative_to(CUSTOM_DIR) }')
        jsconfig_paths.append(data)


    def find_classes_pool_config(file:Path):
        if file.stem.endswith('pyodide'): return ''

        pool_name = ''
        target = 'CONFIG.CLASSES_POOL.'
        tail   = file.read_text(encoding='utf-8')[-400:]

        # Gather all the pool properties, but only send one to the python Dep instance:
        i = 0
        while (i := tail.find(target, i)) >= 0:
            i += len(target)
            j  = tail.find('=', i)
            if j<0: j = None

            pool_name = tail[i:j].strip()
            pool_properties.append(pool_name)
            pool_name = f', pool="{ pool_name }"'

        return pool_name






    justify_prop     = 15                   # HtmlDep property name justification setting
    got_js_config    = False                # Make sure the CONFIG file/definition is found
    section_to_deps  = defaultdict(list)    # Data to build HtmlDependencies properties code
    jsconfig_paths   = []                   # Data to build jsconfig.json "paths" map
    pool_properties  = []                   # To check against CONFIG declaration

    css_and_js_files = [*CUSTOM_DIR.rglob('*.css')] + [*CUSTOM_DIR.rglob('*.js')]

    for file in css_and_js_files:

        is_css       = file.suffix=='.css'
        dep_name     = file.stem.split('-')[-1]
        section      = 'libs' if is_css else file.parent.name.split('-')[-1]
        url_template = "{base_url}/" + str(file.relative_to(CUSTOM_DIR))

        if is_css:
            dep_code = f'{ dep_name : <{justify_prop}} = Css(Block.libs, DepKind.always, "{url_template}")'
        else:
            dep_code = handle_js_file_dependencies(dep_name)

        section_to_deps[section].append(
            (is_css, file.name, dep_code)   # Ordering: css last + lexicographic
        )


    # Check JS config file exists
    if not got_js_config:
        raise BuildError(f"{JS_CONFIG_LIBS.stem}.js file not found during extractions.")

    rebuild_html_deps(section_to_deps)

    return pool_properties, jsconfig_paths






def rebuild_html_deps(section_to_deps:Dict[str,List[str]]):

    deps_src = deps_code = HTML_DEPENDENCIES.read_text(encoding='utf-8')
    for section, lst_deps in section_to_deps.items():
        lst_deps.sort()
        code  = '\n    '.join(['', *( dep[-1] for dep in lst_deps), ''])
        token = f"# GENERATED-{ section }"
        deps_code = replace_chunk(deps_code, token, token, code, keep_limiters=True)

    write_with_feedback_if_needed(HTML_DEPENDENCIES, deps_code, deps_src, raise_on_update=True)
