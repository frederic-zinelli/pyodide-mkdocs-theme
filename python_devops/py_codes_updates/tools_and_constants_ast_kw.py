
from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk

from python_devops.config import *
from python_devops._files_update_tracker import write_with_feedback_if_needed




def rebuild_python_ast_keywords_in_tools_and_constants():

    js_src  = JS_1_RUNTIME_MANAGER.read_text(encoding='utf-8')
    i = js_src.rfind('EXCLUSION_CLASSES')
    i = js_src.find('{', i)
    j = js_src.find('}', i)

    obj_str = js_src[i:j].rstrip("\n ,").replace("+","").replace('""','"+"')
    py_src  = PYM_TOOLS_AND_CTES.read_text(encoding='utf-8')
    repl    = replace_chunk(py_src, "PYTHON_KEYWORDS = ", ",\n}", obj_str, keep_limiters=True)

    write_with_feedback_if_needed(PYM_TOOLS_AND_CTES, repl, py_src, raise_on_update=True)
