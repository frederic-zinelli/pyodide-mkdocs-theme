
# pylint: disable=all

from typing import List


from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import ScriptSection
from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk
from pyodide_mkdocs_theme.pyodide_macros.plugin.config._string_tools import get_python_type_as_code
from pyodide_mkdocs_theme.pyodide_macros.plugin import (
    ARGS_MACRO_CONFIG,
    MacroConfigSrc,
    ConfigOptionSrc,
)

from .._files_update_tracker import write_with_feedback_if_needed
from ..config import MACRO_DATA_TOOLS



CONTENTS_PROPS = [*ScriptSection.sections(), *"REM VIS_REM".split()]







def rebuild_MacroData_classes_codes():
    """ Content, HasContent, MacroDataIDE, MacroDataArgsIDE, ... """

    source   = MACRO_DATA_TOOLS.read_text(encoding='utf-8')
    token    = '# GENERATED CLASSES'
    defs_lst = generate_macros_data_code_as_lst()
    modified = replace_chunk(source, token, token, ''.join(defs_lst), keep_limiters=True)

    write_with_feedback_if_needed(MACRO_DATA_TOOLS, modified, source, raise_on_update=True)








def generate_macros_data_code_as_lst(is_code=True):
    elements: List[MacroConfigSrc] = ARGS_MACRO_CONFIG.elements     # Typing...
    defs_lst = [
        generate_Content_code(is_code),
        generate_HasContent_code(is_code),
        *( generate_macro_data_classes_code(macro, is_code) for macro in elements )
    ]
    return defs_lst




def generate_Content_code(is_code=True):
    members = ''.join(
        f'''\n    { name }: str = { 'DataGetter(default="")' if is_code else '""' }'''
            if 'REM' not in name else
        f'''\n    { name }: Optional[Path] = { 'DataGetter(none_as_default=True)' if is_code else 'None' }'''
        for name in CONTENTS_PROPS
    )
    return f'''\n\n
class Content{ "(ObjectWithDataGetters)" * is_code }:
    """ Content of each section (or absolute Path on the disk for REMs files). """
{ members }
    check_btn: bool = { 'DataGetter(default=False)' if is_code else 'False' }
\n'''



def generate_HasContent_code(is_code=True):
    members = ''.join(
        f'\n    { name }: bool = { "DataGetter(default=False, wraps=bool)" if is_code else "False" }'
        for name in CONTENTS_PROPS
    )
    return f'''

class HasContent{ "(ObjectWithDataGetters)" * is_code }:
    """ Flags to know what sections/elements are defined or not. """
{ members }
    check_btn: bool = { 'DataGetter(default=False)' if is_code else 'False' }
\n\n
{ "HAS_CONTENT_PROPS = [p for p in dir(HasContent) if not p.startswith('_')]" * is_code }
\n\n\n\n\n\n'''



def generate_macro_data_classes_code(obj:MacroConfigSrc, is_code=True):
    """
    Generate the code for the related MarcoArgsDataXXX class, or the equivalent classes
    declaration for the docs, hiding some irrelevant declarations (constantes, ...).
    """

    def build_property_code(elt:ConfigOptionSrc):
        prop_type = 'Any' if elt.name=='questions' else get_python_type_as_code(elt.py_type)
        with_def  = 'none_as_default=True' if elt.name=='ID' else ""
        getter    = f" = DataGetter({ with_def })" if is_code else ""
        return f"\n    {elt.name}: { prop_type }{ getter }"

    members = ''.join( build_property_code(elt) for elt in obj.elements )

    kls_name, args_kls_name = obj.macro_data_class_names()
    def_after, def_before   = "", f'''

class { kls_name }(MacroData):
    """ Runtime data object for the { kls_name } macro. """

    args: { args_kls_name }'''

    if is_code:
        def_before, def_after = def_after, def_before
        def_after += f'''
    ARGS_CLASS: ClassVar[Type[ObjectWithDataGetters]] = { args_kls_name }'''

    return f'''{ def_before }\n\n
class { args_kls_name }(ObjectWithDataGetters):
    """ Runtime arguments object for the { kls_name } macro. """
{ members }{ def_after }
\n\n'''
