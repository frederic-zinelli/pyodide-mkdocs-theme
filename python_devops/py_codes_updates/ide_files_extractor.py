
from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk
from pyodide_mkdocs_theme.pyodide_macros.macros.ide_files_data import IdeFilesExtractor

from python_devops.config import *
from python_devops._files_update_tracker import write_with_feedback_if_needed





def rebuild_IdeFilesExtractor_sections_related_property_getters():

    # Validate all class level declarations for sections:
    missing = [ prop for prop in IdeFilesExtractor.PROPS_TO_CONTENTS.values()
                     if not hasattr(IdeFilesExtractor, prop) ]
    if missing:
        raise BuildError(
            "Missing properties declarations in IdeFilesExtractor:" + '\n\t'.join(missing)
        )

    # Build @property getters:
    defs = [
        f"""
    @property
    def has_{ section.lower() }(self): return bool(self.{ prop })\n"""
        for section,prop in IdeFilesExtractor.PROPS_TO_CONTENTS.items()
    ]
    defs.append('\n    ')         # for the "# GENERATED" token placement...

    source   = IDE_FILES_EXTRACTOR.read_text(encoding='utf-8')
    token    = '# GENERATED'
    modified = replace_chunk(source, token, token, ''.join(defs), keep_limiters=True)

    write_with_feedback_if_needed(IDE_FILES_EXTRACTOR, modified, source, raise_on_update=True)
