
# pylint: disable=signature-differs, invalid-name


from typing import List
from dataclasses import dataclass, field


from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk
from pyodide_mkdocs_theme.pyodide_macros.plugin.config import ConfOrOptSrc

from pyodide_mkdocs_theme.pyodide_macros.plugin.config._string_tools import get_python_type_as_code
from pyodide_mkdocs_theme.pyodide_macros.plugin.config.config_option_src import GETTERS_WITH_PATH
from pyodide_mkdocs_theme.pyodide_macros.plugin import PLUGIN_CONFIG_SRC
from pyodide_mkdocs_theme.pyodide_macros.plugin.config.dumpers import Dumper


from .._files_update_tracker import write_with_feedback_if_needed
from ..config import MAESTRO






def rebuild_base_maestro_ConfigExtractors_getters():
    """
    Rebuild all the plugin's getters in BaseMaestro.
    """

    code    = MAESTRO.read_text(encoding='utf-8')
    getters = BaseMaestroGettersDumper.apply(PLUGIN_CONFIG_SRC)
    token   = "\n    # ARGS EXTRACTOR TOKEN"
    updated = replace_chunk(code, token, token, getters, keep_limiters=True)

    write_with_feedback_if_needed(MAESTRO, updated, code, raise_on_update=True)








@dataclass
class BaseMaestroGettersDumper(Dumper):
    """
    Generate the code of all the ConfigExtractor getters for BaseMaestro.
    """

    code:  List[str] = field(default_factory=list)
    """ Lines of code for all getters (formatted and ordered) """

    stack: List[List[str]] = field(default_factory=list)
    """ Groups of ConfigOptionSrc being currently generated """


    def travel_with_dumper(self, obj:'ConfOrOptSrc'):

        # Enter:
        if obj.is_config:
            self.stack.append([])

        # Enter leaf node:
        elif obj.in_config:
            getter_code = self.to_base_maestro_getter_code(obj)
            self.stack[-1].append(getter_code)

        # Recurse:
        for child in self._ordered_iter(obj, sort_all=True):
            if child.in_config:
                self.travel_with_dumper(child)

        # Exit:
        if obj.is_config:
            group = self.stack.pop()
            if group:
                aligned = self._align_group(group)
                self.code.extend(aligned)
                self.code.append('\n')


    def finalize(self, _):
        self.code.pop()             # Suppress trailing empty line
        return ''.join(self.code)


    #-----------------------------------------------------------------------


    @staticmethod
    def to_base_maestro_getter_code(obj:'ConfOrOptSrc'):
        """
        Build the ConfigExtractor code for BaseMaestro class (used from mkdocs_hooks.on_config)
        """
        prop    = obj.maestro_extractor_getter_name
        py_type = get_python_type_as_code(obj.py_type)
        args    = [
            f"'{ '.'.join( obj.config_setter_path )}'",
        ]

        for special in GETTERS_WITH_PATH:
            if special in obj.config_setter_path:
                args.append(f"prop='{ obj.name }'")
                break

        if obj.is_deprecated:
            args.append("deprecated=True")

        if obj.value_transfer_processor:
            args.append(f"alteration={ obj.value_transfer_processor.__name__ }")

        return f"\n    { prop }: { py_type } = ConfigExtractor({ ', '.join(args) })"



    def _align_group(self, lst:List[str]):
        ij_s = [*map(self._chr_indices, lst)]
        right_most = max(j for _,j in ij_s)
        aligned = [ f"{ s[:i] }{ ' '*(right_most-j) }{ s[i:] }" for s,(i,j) in zip(lst, ij_s) ]
        return aligned

    @staticmethod
    def _chr_indices(getter:str):
        """ Finds the indices of `:` (+1) and `=` in the code of the getter """
        i = 1 + getter.find(':')
        j = getter.find('=', i)
        return i, j
