
# pylint: disable=signature-differs, invalid-name


import json
import re

from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import IDE_MODES
from pyodide_mkdocs_theme.pyodide_macros.parsing import camel, replace_chunk, eat
from pyodide_mkdocs_theme.pyodide_macros.plugin.tools.pages_and_macros_py_configs import MacroPyConfig
from pyodide_mkdocs_theme.pyodide_macros.plugin.pyodide_macros_plugin import PyodideMacrosPlugin

from .config import *
from ._files_update_tracker import write_with_feedback_if_needed



JS_CONFIG_DUMP  = "//JS_CONFIG_DUMP"




def rebuild_PMT_tolls_array_for_pyodide_installer():

    code     = JS_1_PKG_INSTALLER.read_text(encoding='utf-8')
    packages = [pkg.name for pkg in PMT_TOOLS.iterdir()]
    updated  = replace_chunk(
        code,
        "const PMT_TOOLS", "]",
        f"const PMT_TOOLS = [{ ', '.join(map(repr,packages)) }]"
    )
    write_with_feedback_if_needed(JS_1_PKG_INSTALLER, updated, code)




def rebuild_js_legacy_subscriber_libs():
    """
    Rebuild the generated legacy/sync script file (for backward compatibility).
    """
    code  = code_src = JS_LEGACY_SUBSCRIBER.read_text(encoding='utf-8')
    src   = JS_FUNCTOOLS_LIBS.read_text(encoding='utf-8')

    start = "function subscribeWhenReady("
    end   = "// TOKEN: end subscribeWhenReady"
    i     = src.index(start)
    j     = src.index(end, i)
    src   = src[ i+len(start) : j ]

    repl  = re.sub(r"jsLogger\((['\"]\[\w{1,3}ubscribing\])", r"console.log(\1 (legacy)", src)

    code  = replace_chunk(code, start, end, repl, keep_limiters=True)

    write_with_feedback_if_needed(JS_LEGACY_SUBSCRIBER, code, code_src)





def functools_transfer_lzw_data():
    """
    Transfer NO_HTML and TOME_BASE constants, used to compress LZW in python, to the
    JS/functools lib (as NO_HTML and ALPHA constants).
    """

    def extract(name):
        """ (Assumes the first occurrence is the wanted declaration) """

        _, i = eat(py_src, name)
        j, _ = eat(py_src, '\n', start=i)
        data = py_src[i:j].strip(' =')

        is_a_valid_string_code = data[0] != data[-1] or len(data)<2 or data[0] not in "\"'"
        if is_a_valid_string_code:
            raise ValueError(f"Invalid {name} string for LZW compression. Found:\n{data}")
        return data

    def replace(name, data):
        nonlocal changed
        changed = replace_chunk(changed, f"const {name} = ", "\n", data, keep_limiters=True)

    py_src  = PYM_PARSING.read_text(encoding='utf-8')
    js_src  = JS_FUNCTOOLS_LIBS.read_text(encoding='utf-8')
    changed = js_src

    no_html = extract('NO_HTML')
    replace("NO_HTML", no_html)

    alpha = extract('TOME_BASE')
    replace("ALPHA", alpha)

    write_with_feedback_if_needed(JS_FUNCTOOLS_LIBS, changed, js_src)





def rebuild_js_0_config_libs_CONFIG_model():
    """
    Creates the default substructure of the CONFIG object, so that it can be used to
    know what are all the properties that are defined there.
    """
    js_file = JS_CONFIG_LIBS
    code_js = js_file.read_text(encoding='utf-8')

    repl    = PyodideMacrosPlugin.dump_to_js_config(None)      # HACK!
    fresh   = replace_chunk(
        code_js, JS_CONFIG_DUMP, JS_CONFIG_DUMP, repl+"\n   ", keep_limiters=True
    )
    modes_dct = { camel(mode): mode for mode in IDE_MODES }
    json_dct  = json.dumps(modes_dct, indent=2).replace('\n', '\n    ')
    js_obj    = re.sub(r'"(\w+)":', r'\1:', json_dct)
    refresh   = replace_chunk(
        fresh, "PROFILES:{", "}", f"PROFILES:{ js_obj[:-1] }{'}'}"
    )
    write_with_feedback_if_needed(js_file, refresh, code_js)





def transfer_MacroPyConfig_properties_as_js_PyodideSectionsRunner_getters(is_pipeline:bool):
    """
    Rebuild the PyodideSectionRunner getters, for all the data coming from python/mkdocs,
    that are needed to handle an IDE/terminal/py_btn.

    @is_pipeline: If True, build "direct" getters, otherwise, wrap them with the `no_undefined`
                  logistic, to spot problems more easily during development.
    """

    js_file  = JS_2_PYTHON_RUNNER
    py_props = set(MacroPyConfig.__annotations__)       # pylint: disable=no-member

    # Update JS getters in PyodideSectionsRunner!
    getter_template = (
        "\n  get {call: <20}{op} return this.data.{prop} {clo}"
            if is_pipeline else
        "\n  get {call: <20}{op} return this.no_undefined('{camel}')(this.data.{prop}) {clo}"
    )
    getters = ''.join(
        getter_template.format(
            call  = camel(prop)+'()',
            camel = camel(prop),
            prop  = prop,
            op    = '{',
            clo   = '}',
        ) for prop in sorted(py_props)
    ) + "\n  "

    code_js = js_file.read_text(encoding='utf-8')
    fresh   = replace_chunk(code_js, JS_CONFIG_DUMP, JS_CONFIG_DUMP, getters, keep_limiters=True)
    write_with_feedback_if_needed(js_file, fresh, code_js)





def transfer_PyodidePlot_to_js_generics_snippets():
    """
    Recreate pyodideFeature.pyodidePlot python code, in the `0_GenericPyodideSnippets-pyodide.js`
    file, using the version currently in the root directory of the project.
    """

    mocking = 'from unittest.mock import Mock\njs = Mock()'
    pyodide = 'import js\nmatplotlib.use("module://matplotlib_pyodide.html5_canvas_backend")'
    token   = "# THIS IS A PLOT TOKEN"

    js_file = JS_0_PYTHON_SNIPPETS
    plot    = PYODIDE_PLOT.read_text(encoding='utf-8')

    if mocking not in plot:
        raise BuildError("Couldn't find the mocking jay in pyodide_plot.py")

    plot     = plot.replace(mocking, pyodide)
    plot     = f"\n{ plot }\n".replace("`", "\\`").replace('\n', '\n    ')
    plot     = re.sub(r'^ +$', '', plot, flags=re.MULTILINE)
    src_code = js_file.read_text(encoding='utf-8')
    fresh    = replace_chunk(src_code, token, token, plot,keep_limiters=True)

    write_with_feedback_if_needed(js_file, fresh, src_code)
