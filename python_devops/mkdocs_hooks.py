"""
mkdocs events:  https://www.mkdocs.org/dev-guide/plugins/#events

All the functions in this files involve using logistic coming from the PMT package, which imports
stuff from the .venv, so these operations CANNOT be done through the devops hooks (which work
from a global python interpreter).
"""
# pylint: disable=all


from mkdocs.exceptions import BuildError
from mkdocs.plugins import event_priority
from mkdocs.config.defaults import MkDocsConfig


from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import ICONS_IN_TEMPLATES_DIR
from pyodide_mkdocs_theme.pyodide_macros.html_dependencies.deps_class import Block, Cdn, Css, Dep, DepKind

from python_devops.config import *
from python_devops._files_update_tracker import (
    RAISE,
    logger,
    handle_files_updates_outcome,
)
from python_devops._checks_and_dev import (
    validate_all_config_paths,
    check_langs_consistencies,
    check_js_config_classes_pool_structure,
    rebuild_root_jsconfig_import_map,
)
from python_devops.docs_codes_updates.custom_config import add_PyodideMacrosConfig_to_docs_custom_config_md
from python_devops.docs_codes_updates.dev_docs_index import rebuild_dev_docs_index_summary
from python_devops.docs_codes_updates.custom_generic_pages import rebuild_maestro_tools_data_docs
from python_devops._yaml_codes_updates import rebuild_PyodideMacrosConfig_yaml_schemas
from python_devops._js_codes_updates import (
    rebuild_PMT_tolls_array_for_pyodide_installer,
    rebuild_js_legacy_subscriber_libs,
    functools_transfer_lzw_data,
    rebuild_js_0_config_libs_CONFIG_model,
    transfer_MacroPyConfig_properties_as_js_PyodideSectionsRunner_getters,
    transfer_PyodidePlot_to_js_generics_snippets,
)
from python_devops.py_codes_updates.html_deps_and_import_map import gather_all_scripts_and_css_kinds_and_rebuild_Deps
from python_devops.py_codes_updates.base_maestro_getters import rebuild_base_maestro_ConfigExtractors_getters
from python_devops.py_codes_updates.ide_files_extractor import rebuild_IdeFilesExtractor_sections_related_property_getters
from python_devops.py_codes_updates.macro_data_code import rebuild_MacroData_classes_codes
from python_devops.py_codes_updates.tools_and_constants_ast_kw import rebuild_python_ast_keywords_in_tools_and_constants





_ICONS = CUSTOM_DIR / ICONS_IN_TEMPLATES_DIR
if not _ICONS.is_dir():
    raise BuildError(f"{ _ICONS } does not exist.")






event_priority(3000)
def on_config(config:MkDocsConfig):
    # NOTE: this hook takes roughly 30ms => no need to "optimize" that with async stuff...

    # Never update the files during a pipeline (because files ordering differ on the GitLab
    # runner when gathering the scripts, and this already led to wrong JS scripts insertion
    # order on the built site, resulting in unusable IDEs and various other problems...).
    if config.extra['is_pipeline']:
        return

    logger.info("Refresh project")
    align_config_codes_and_docs(False, config.use_directory_urls)



# @event_priority(-2000)
# def on_page_context(ctx, page, **_):
#     print(page.content)


# def on_serve(*_,**__):
#     init,encoded = map(sum,zip(*MEAN))
#     print("\nMEAN:")
#     print(len(MEAN))
#     print(f"{init} -> {encoded} => x{encoded/init:.3f}")




def align_config_codes_and_docs(is_release_setup:bool, dir_urls=True):

    RAISE.clear()


    validate_all_config_paths()             # Always do first...

    # Update python & dev
    write_file(PYM_DOCS_DIRS_CONFIG, DEVOPS_DOCS_DIRS, as_bytes=1)
    rebuild_base_maestro_ConfigExtractors_getters()
    check_langs_consistencies()

    pool_properties, jsconfig_paths = gather_all_scripts_and_css_kinds_and_rebuild_Deps()
    check_js_config_classes_pool_structure(pool_properties)
    rebuild_root_jsconfig_import_map(jsconfig_paths)

    rebuild_IdeFilesExtractor_sections_related_property_getters()
    rebuild_MacroData_classes_codes()
    rebuild_python_ast_keywords_in_tools_and_constants()

    # Update templates & JS
    rebuild_js_legacy_subscriber_libs()
    functools_transfer_lzw_data()
    rebuild_js_0_config_libs_CONFIG_model()
    rebuild_PMT_tolls_array_for_pyodide_installer()
    transfer_MacroPyConfig_properties_as_js_PyodideSectionsRunner_getters(is_release_setup)
    transfer_PyodidePlot_to_js_generics_snippets()

    # Update docs/yaml schemas
    rebuild_dev_docs_index_summary(dir_urls)
    add_PyodideMacrosConfig_to_docs_custom_config_md()
    rebuild_maestro_tools_data_docs()
    rebuild_PyodideMacrosConfig_yaml_schemas()


    handle_files_updates_outcome(is_release_setup)
