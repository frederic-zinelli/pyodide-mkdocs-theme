
import re
from dataclasses import dataclass
from typing import Callable, ClassVar, List, Optional, Tuple
from contextlib import contextmanager

from myutils import CodeModifier, docstring_teller

from python_devops.config import *








@dataclass
class MultiLangScriptsBuilder:
    """
    Assumes the different version are always layered this way on the source side (docs):

        . (default: fr)
        ├── file1
        ├── file2
        ├── ...
        ├── de/
        |   ├── file1
        ├── en/
        |   ├── file1
        |   ├── file2
        |   └── ...
        └── .../

    If no `lang_and_fallbacks` provided, pick in the default directory only.
    Otherwise, try each fallback replacing the `lang` path with each possible element until
    the first valid file is found (or raise an error).
    """

    lang: Optional[str]
    lang_and_fallbacks: Tuple[str] = ()

    languages: ClassVar[List[str]] = []


    def __post_init__(self):

        self.languages.append(self.lang)

        self.lang_and_fallbacks = {
            'fr': ('.',),
            'en': ('en', '.',),
            'de': ('de', 'en', '.',),
        }[self.lang]


    @classmethod
    def update_LANGS_tuple_in_PMT___main___py(cls):
        print(f"Update LANGS tuple in { PMT__MAIN__ }")

        repl = f" = {tuple(sorted(cls.languages))!r}"
        CodeModifier(PMT__MAIN__).replace_chunk("LANGS", '\n', repl, keep_limiters=True).dump()



    def __get_file_with_fallbacks(self, path_with_lang_builder:Callable[[str],Path]):
        for lang in self.lang_and_fallbacks:
            target = path_with_lang_builder(lang)
            if target.exists():
                return target
        raise FileNotFoundError(target)


    def __scripts_file_builder(self, *tails:str):
        def builder(lang=self.lang):
            path = THEME_SCRIPTS / lang
            for tail in tails:
                path /= tail
            return path
        return builder



    def __get_files_and_data(self, sourcer, *tails:str, data_dct:dict=None):
        source   = (
            self.__get_file_with_fallbacks(sourcer)
                if callable(sourcer) else
            sourcer
        )
        targeter = self.__scripts_file_builder(*tails)
        target   = targeter(self.lang)
        print(f'    Update {target}')

        if not data_dct:
            return source, target

        dct = { targeter(lang): data for lang,data in data_dct.items() }
        return source, target, *dct[target]



    def recreate_scripts_lang_docs_exo_py(self):
        exo_py_src, scripts_target = self.__get_files_and_data(
            lambda lang: INCLUSIONS / lang / 'IDE_exo.py',
            'docs', 'exo.py'
        )
        write_file(scripts_target, exo_py_src, as_bytes=1)



    def recreate_scripts_lang_docs_index_md(self):
        qcm_src, scripts_target, start_token, legend = self.__get_files_and_data(
            lambda lang: INCLUSIONS / lang / 'qcms_working_ex.md',
            'docs', 'index.md',
            data_dct = {
                'fr': ("## Aperçu `multi_qcm`",  "Appel complet de la macro"),
                'en': ("## Preview `multi_qcm`", "Macro call, in the markdown file"),
                'de': ("## Preview `multi_qcm`", "Macro call, in the markdown file"),
            },
        )

        macro_call  = qcm_src.read_text(encoding='utf-8').replace('"""','\"\"\"')
        indented_md = macro_call.replace('\n', '\n    ')
        index_md    = scripts_target.read_text(encoding='utf-8')
        i           = index_md.index(start_token)

        write_file(scripts_target, index_md[:i] + f"""{ start_token }

??? help "{ legend }"

    ```markdown
    {'{%'} raw {'%}'}
    { indented_md }
    {'{%'} endraw {'%}'}
    ```

<br>

{macro_call}
""")




    def merge_models_into_lang(self):
        for file in SCRIPTS_MODELS.rglob('*.*'):
            tail = file.relative_to(SCRIPTS_MODELS)
            target = self.__scripts_file_builder(tail)(self.lang)
            write_file(target, file.read_bytes(), as_bytes=True)



    def update_scripts_lang_main_py(self):
        src_lang, main_target = self.__get_files_and_data(
            lambda lang: PYM_MESSAGES / f'{ "fr" if lang=="." else lang}_lang.py',
            'main.py'
        )

        token     = '# LANG_TOKEN'
        props_str = CodeModifier(src_lang).extract_between(token, token).strip()
        props_str = _cleanup_properties_docstrings(props_str)
        props_str = re.sub(r'Tr\s*=\s*(?=Msg|Tip|TestsToken)', '', props_str)   # To dict items
        props_str = re.sub(r'\)\n', r'),\n', props_str)                         # Add item trailing comma
        props_str = re.sub(r'(\n +)(\w+)\s*:', r'\1    "\2":', f"\n    { props_str }")

        lang_code = CodeModifier(
            main_target
        ).replace_chunk(
            'env.lang.overload({\n',  '\n    })', props_str, keep_limiters=True
        ).dump()


        if self.lang=='fr':
            print(f'*** Update {DOCS_MESSAGES}')
            doc_txt  = DOCS_MESSAGES.read_text(encoding='utf-8')
            i = doc_txt.index("#customization-code")
            i = doc_txt.index("\n", i)
            doc_txt = f"""{ doc_txt[:i] }

```python
{ lang_code.code }
```
"""
            write_file(DOCS_MESSAGES, doc_txt)




    def update_scripts_lang_mkdocs_yml(self):
        yml_src, scripts_target = self.__get_files_and_data(
            SCRIPTS_MODELS / 'mkdocs.yml',
            'mkdocs.yml',
        )

        replacement = CodeModifier(
            yml_src
        ).replace_once(
            'language: fr', 'language: '+self.lang if self.lang!='fr' else ''
        ).code
        write_file(scripts_target, replacement)







def _cleanup_properties_docstrings(props_str:str):
    out = []
    i = j = 0
    while ( j := props_str.find('###', i+3) ) >= 0:
        out.append(props_str[i:j].rstrip())
        i = props_str.index('###', j+3) + 3

    if not i and j<0:
        raise ValueError("Couldn't find any docstring...")

    out.append(props_str[i+3:])
    return ''.join(out)









@contextmanager
@docstring_teller
def common_models_scripts_context():
    """
    1. Remove the common files from the PMT/scripts/models directory.
    2. gives back control to the caller (handling the creation of all the lang directories).
    3. recreate the common files in the models.
    """

    targets_and_data = []
    # Tuples (file, content) of models to recreate after the scripts directory for the current
    # language has been updated.


    for fixed in '.gitignore requirements.txt'.split():
        target = SCRIPTS_MODELS / fixed
        targets_and_data.append( (target, target.read_bytes()) )
        target.unlink()
        print("Removed", target)

    for root in (TOOLBOX, PYODIDE_PLOT):
        target = SCRIPTS_MODELS / root.name
        targets_and_data.append( (target, root.read_bytes()) )
        target.unlink()
        print("Removed", target)

    yield

    for target,data in targets_and_data:
        write_file(target, data, as_bytes=1)
        print('Rebuilt', target)






@docstring_teller
def handle_Lang_related_logic_and_files_and_PMT_python_scripts():
    """ Handle PMT scripts and Lang logistic """

    with common_models_scripts_context():
        for lang_src_file in PYM_MESSAGES.glob('*_lang.py'):

            lang = lang_src_file.stem.split('_')[0]
            print(lang, ':')

            lang_dir = THEME_SCRIPTS / lang
            if not lang_dir.is_dir():
                raise FileNotFoundError(lang_dir)

            multi_lang = MultiLangScriptsBuilder(lang)
            multi_lang.recreate_scripts_lang_docs_exo_py()
            multi_lang.recreate_scripts_lang_docs_index_md()
            multi_lang.merge_models_into_lang()
            multi_lang.update_scripts_lang_main_py()
            multi_lang.update_scripts_lang_mkdocs_yml()
        print()

        MultiLangScriptsBuilder.update_LANGS_tuple_in_PMT___main___py()
