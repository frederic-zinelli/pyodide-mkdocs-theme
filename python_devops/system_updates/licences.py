
from pathlib import Path
from typing import Callable, Awaitable
from python_devops.config import *


LICENCE = """
pyodide-mkdocs-theme
Copyleft GNU GPLv3 🄯 2024 Frédéric Zinelli

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.
If not, see <https://www.gnu.org/licenses/>.
""".strip()




def build_short_licence(op:str=None, clos:str=None, per_line:str=None):
    if per_line is None:
        return f"{ op }\n{ LICENCE }\n{ clos }\n\n"

    return '\n'.join(
        f'{ per_line } { line }'.rstrip() for line in LICENCE.splitlines()
    ) + "\n\n"



LICENSE_COMMENTS = {
    '.py':   build_short_licence('"""', '"""'),
    '.css':  build_short_licence('/*', '*/'),
    '.js':   build_short_licence('/*', '*/'),
    '.html': build_short_licence('<!--', '-->'),
    '.yml':  build_short_licence(per_line='#'),
}



EXCLUDED_PATHS = (
    PMT / "__init__.py",
    PMT / "__version__.py",
    CUSTOM_DIR / "__init__.py",
    CUSTOM_DIR / "main.html",
)
EXCLUDED_PARTS = {
    'partials',
    PMT_SCRIPTS,
    'PMT_tools'
}

TODO_FILES = [
    file for file in PMT.rglob('*.*')
          if file.suffix in LICENSE_COMMENTS
             and not EXCLUDED_PARTS & set(file.parts)
             and file not in EXCLUDED_PATHS
]


def short_licence_adder(
    file: Path,
    coroutine_factory: Callable[ [Path, Callable, bool, str], Awaitable],
):
    def converter(code:str):
        """
        Add the appropriate version of the short licence at the beginning of the given file if:
            - It is not empty (__init__.py).
            - It doesn't start with the licence.
        """
        short = LICENSE_COMMENTS[file.suffix]

        # Do not add license to empty files:
        if code.strip() and not code.startswith(short.rstrip()):
            print('LICENCE UPDATE -', file)
            code = short+code

        return code
    return coroutine_factory(file, converter, False, 'licence')
