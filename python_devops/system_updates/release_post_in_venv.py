
from python_devops.mkdocs_hooks import transfer_MacroPyConfig_properties_as_js_PyodideSectionsRunner_getters


if __name__ == '__main__':
    # WARNING: this HAS to be run through os_system, so that the python interpreter will be
    #          the one in the .venv and the imports related to mkdocs will still work...
    transfer_MacroPyConfig_properties_as_js_PyodideSectionsRunner_getters(False)

else:
    raise ImportError(
        "This file should never be imported: it should be run through os_system only, "
        "using the .venv python interpreter."
    )
