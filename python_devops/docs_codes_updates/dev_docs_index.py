
from pathlib import Path

from ..config import DEV_DOCS_INDEX
from .._files_update_tracker import write_with_feedback_if_needed




def rebuild_dev_docs_index_summary(dir_urls):
    """
    Rebuild the dev_docs/index.md file, handling the `use_directory_urls` value
    (more or less...).
    """

    def build_link(file:Path):
        """
        Assuming use_directory_urls = True
        """
        rel_loc = file.relative_to(DEV_DOCS_INDEX.parent)
        color = '{ .orange }' * (file.stem == "dir_urls_false")

        if not dir_urls:
            link_uri = rel_loc.with_suffix('.html')

        elif rel_loc.stem == 'index':
            link_uri = f'{ rel_loc.parent }/'

        else:
            link_uri = f'{ rel_loc.with_suffix("") }/'

        link = f"* [{ link_uri }](./{ link_uri }){'{'} target=_blank { color } {'}'}"
        return link


    links = sorted(
        build_link(file) for file in DEV_DOCS_INDEX.parent.rglob("*.md")
                         if not file.stem.endswith('REM') and file != DEV_DOCS_INDEX
    )

    write_with_feedback_if_needed( DEV_DOCS_INDEX, "\n".join(links) )
