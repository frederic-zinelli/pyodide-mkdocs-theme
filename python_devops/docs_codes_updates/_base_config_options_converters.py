
from typing import ClassVar, Dict, Type

from mkdocs.config import config_options as C
from mkdocs.config.base import BaseConfigOption

from pyodide_mkdocs_theme.pyodide_macros.plugin.config._string_tools import (
    get_python_type_as_code,
    to_nice_yml_value_for_docs,
)







class ConfigOptionCode:
    """
    Convert a live BaseConfigOption instance to the equivalent string/code declaration.

    Note: not all the classes are implemented. See here to find how to add more:
        https://github.com/mkdocs/mkdocs/blob/master/mkdocs/config/config_options.py
    """

    DEPRECATION_TEMPLATE: ClassVar[str] = (
      "The configuration option '{}' has been deprecated and will be removed in a future release."
    )


    @classmethod
    def extract(cls, opt:BaseConfigOption):
        """
        Convert a live BaseConfigOption instance to the equivalent string/code declaration.
        Note: not all the classe are implemented.
        """
        name    = opt.__class__.__name__
        prop    = f"_{ name.lower() }"
        inner   = getattr(cls, prop)(opt)
        default = "" if opt.default is None else f', default={ opt.default !r}'
        return f'C.{ name }({ inner }{ default })'


    @classmethod
    def _optional(cls, opt:C.Optional):
        return cls.extract(opt.option)
    Deprecated = _optional

    @classmethod
    def _choice(cls, opt:C.Choice):
        return repr(opt.choices)

    @classmethod
    def _type(cls, opt:C.Type):
        return get_python_type_as_code(opt._type)       # pylint: disable=protected-access

    @classmethod
    def _dictofitems(cls, opt:C.DictOfItems):
        return cls.extract(opt.option_type)

    _listofitems = _dictofitems

    @classmethod
    def _deprecated(cls, opt:C.Deprecated):          # pylint: disable=E0102
        args = []
        for prop in 'moved_to removed option_type'.split():
            val = cls.extract(opt.option) if prop=='option_type' else getattr(opt,prop)
            if val and val != cls.DEPRECATION_TEMPLATE:
                args.append(f"{prop}={val}")
        return ', '.join(args)









class ConfigOptionYamlSchema:
    """
    Convert a live BaseConfigOption instance to the equivalent yaml schema dict.

    Note: not all the classes are implemented. See here to find how to add more:
        https://github.com/mkdocs/mkdocs/blob/master/mkdocs/config/config_options.py
    """

    TYPES: ClassVar[ Dict[Type,str] ] = {
        int:    'integer',
        float:  'number',
        str:    'string',
        bool:   'boolean',
    }

    @classmethod
    def get_schema(cls, opt: BaseConfigOption):
        kls_name = opt.__class__.__name__
        builder  = getattr(cls, '_'+kls_name.lower())
        sub_dct  = builder(opt)
        return sub_dct

    @classmethod
    def _type(cls, obj:C.Type):
        return {'type': cls.TYPES[obj._type] }

    @classmethod
    def _optional(cls, option:C.Optional):
        return cls.get_schema(option.option)

    @classmethod
    def _choice(cls, choice:C.Choice):
        dct  = {'enum': choice.choices }
        if choice.default is not None:
            dct['default'] = to_nice_yml_value_for_docs(choice.default)
        return dct

    @classmethod
    def _listofitems(cls, lst:C.ListOfItems):
        return {
            'type': 'array',
            'items':{
                'type': cls.TYPES[ lst.option_type._type ]
            }
        }

    @classmethod
    def _dictofitems(cls, dct:C.DictOfItems):
        raise AttributeError('not implemented')
        # See: https://json-schema.org/understanding-json-schema/reference/object
        # return {
        #     'type': 'object'
        # }









if __name__ == '__main__':

    print('\nTest ConfigOptionCode.extract:\n')
    TESTS = [
        (C.DictOfItems(C.Type(str)), 'C.DictOfItems(C.Type(str))'),
        (C.ListOfItems(C.Type(str)), 'C.ListOfItems(C.Type(str))'),
        (C.ListOfItems(C.Type(str), default=[]), 'C.ListOfItems(C.Type(str), default=[])'),
        (C.Optional(C.Type(bool) ), 'C.Optional(C.Type(bool))'),
        (
            C.Deprecated(option_type=C.Optional(C.Type(bool))),
           'C.Deprecated(option_type=C.Optional(C.Type(bool)))'
        ),
    ]
    for src,exp in TESTS:
        print(exp)
        actual = ConfigOptionCode.extract(src)
        assert actual == exp, f'{actual} should be {exp}'
