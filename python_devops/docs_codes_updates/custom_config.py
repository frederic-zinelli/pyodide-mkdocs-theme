
# pylint: disable=all


from typing import List, Optional, Union
from dataclasses import dataclass, field



from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk
from pyodide_mkdocs_theme.pyodide_macros.plugin.config._string_tools import to_nice_yml_value_for_docs
from pyodide_mkdocs_theme.pyodide_macros.plugin.config.dumpers import Dumper
from pyodide_mkdocs_theme.pyodide_macros.plugin.config import PluginConfigSrc, PLUGIN_CONFIG_SRC
from pyodide_mkdocs_theme.pyodide_macros.plugin.config.sub_config_src import ConfOrOptSrc

from ..config import *
from .._files_update_tracker import write_with_feedback_if_needed
from ._base_config_options_converters import ConfigOptionCode








def add_PyodideMacrosConfig_to_docs_custom_config_md():
    """
    Takes the plugin's config source object, `PLUGIN_CONFIG_SRC`, and recreates the whole
    custom/config.md content, as well as the config tree in docs_tools.
    """

    # Handle the docs_tools/inclusions file (config tree hierarchy of defaults)
    #--------------------------------------------------------------------------

    inclusion_file = DOCS_CONF_INCLUSION()
    output = ConfigTreeDocsYamlBlockDumper.apply(PLUGIN_CONFIG_SRC)
    write_with_feedback_if_needed(inclusion_file, output)



    # Update the entire config md page:
    #----------------------------------

    token1 = "<br INSERTION_TOKEN>"
    token2 = "<br INSERTION_TOKEN_2>"
    token3 = "<br INSERTION_TOKEN_3>"

    md_source = DOCS_CONFIG.read_text(encoding='utf-8')


    # Build all the sub-configs (not recursive, for `args`):
    markdown = []
    elements = sorted(PLUGIN_CONFIG_SRC.elements, key=MkdocstringsPageDocsDumper.ordering)
    for src in elements:
        if src.in_yaml_docs and src.is_config:
            recurse =  src.name != 'args'
            md = MkdocstringsPageDocsDumper.apply(src, recurse=recurse)
            markdown.append( md + "\n\n<br><br>\n\n{{sep()}}\n\n" )

    markdown  = "\n\n" + ''.join(markdown)
    md_config = replace_chunk(md_source, token1, token2, markdown, keep_limiters=True)


    # Build the source level, with the original macros-plugin properties:
    options = PluginConfigSrc(
        docs = PLUGIN_CONFIG_SRC.docs,
        elements = tuple(elt for elt in elements if not elt.is_config)
    )
    markdown2 = "\n\n" + MkdocstringsPageDocsDumper.apply(options, limit_depth=2) + '\n\n'
    md_config = replace_chunk(md_config, token2, token3, markdown2, keep_limiters=True)


    # Merge updates...
    write_with_feedback_if_needed(DOCS_CONFIG, md_config, md_source)









@dataclass
class ConfigTreeDocsYamlBlockDumper(Dumper):
    """
    Generate the complete tree of the plugin config for the docs (code block).
    """

    code:  List[str] = field(default_factory=list)
    """ Global lines of code for all getters (formatted and ordered) """


    @Dumper.spot_exiting_leaf_config
    def travel_with_dumper(self, obj:ConfOrOptSrc):

        # Enter:
        if obj.in_yaml_docs:

            line = self.as_mkdocs_line_in_yml_block(obj)
            self.code.append(line)

            # Recurse:
            for child in self._ordered_iter(obj):
                self.travel_with_dumper(child)

        # Exit:
        if self.is_closing_leaf_config(obj):
            self.code.append('')


    def finalize(self, _):
        joined    = '\n    '.join(self.code).rstrip()
        yml_block = f'```yaml\nplugins:\n    { joined }\n```\n'
        return yml_block


    #---------------------------------------------------------


    @staticmethod
    def as_mkdocs_line_in_yml_block(obj: Union['ConfOrOptSrc','PluginConfigSrc']):

        name  = obj.name
        head  = ""
        value = ""

        if obj.is_plugin_config:
            name = obj.PLUGIN_NAME
            head = "- "                 # using a md tick

        elif not obj.is_config:
            value = to_nice_yml_value_for_docs(obj.default)

        return f"{ obj.indent }{ head }{ name }: { value }".rstrip()







@dataclass
class MkdocstringsPageDocsDumper(Dumper):
    """
    Converts the tree to an equivalent of mkdocstrings markdown content/page.

    @header_lvl:  Starting point for the header level. The depth of the current element
                  is ignored and this value is increased by one at each recursive call.
    @recurse:     If False, render only the current element.
    @limit_depth: Alternative to @recurse, which allows to control the recursion depth
                  starting from the initial element.
    """

    header_lvl: int  = 3
    recurse:    bool = True
    limit_depth: int = -1

    markdown: List[str] = field(default_factory=list)
    """ Global content of the page (lines or §). """


    def travel_with_dumper(self, obj:'ConfOrOptSrc', header_lvl:Optional[int]=None):
        if not self.limit_depth:
            return
        if not obj.in_yaml_docs:
            raise ValueError("This config element has been deprecated!")

        if header_lvl is None:
            header_lvl = self.header_lvl

        # Enter
        self.as_mkdocstrings_content(obj, header_lvl)

        # Recurse
        if self.recurse:
            for child in self._ordered_iter(obj):
                if child.in_yaml_docs:
                    self.limit_depth -= 1
                    self.travel_with_dumper(child, header_lvl+1)
                    self.limit_depth += 1


    def finalize(self, _):
        return '\n\n'.join(self.markdown)


    #------------------------------------------------------------------------------------


    def as_mkdocstrings_content(self, obj:'ConfOrOptSrc', header_lvl:int):
        """
        Convert the current element (not recursively) to mkdocs documentation (mkdocstrings-like).
        """
        header  = self._to_docs_header_with_redirect(obj)
        h_class = '.fake-h4 ' if header_lvl==4 else ""
        md_id   = "" if obj.is_plugin_config else f'#{ obj.py_macros_path }'
                  # plugin: nothing because of mkdocs-adresses.

        markdown = f"""
{ '#' * header_lvl } { header } {'{'} { md_id } .doc .doc-heading { h_class }{'}'}

{ self._to_docs_page_content(obj) }
"""
        self.markdown.append(markdown)




    @staticmethod
    def _to_docs_header_with_redirect(obj:'ConfOrOptSrc'):
        """
        Create the code representation of the current element (used in the header).
        Note: the header level (hashtags) isn't part of the output (handled in the caller).
        """

        if obj.is_plugin_config:
            old_anchor = 'pyodide_mkdocs_theme.pyodide_macros.plugin.config.PyodideMacrosConfig'
            return (
                f"{'{{'} anchor_redirect(id='{ old_anchor }') {'}}'} "
                f"{'{{'} anchor_redirect(id='pyodide_macros', mk_addr_skip=False) {'}}'} "
                f"`#!py pyodide_macros`"
            )

        if obj.is_config:
            return (
                f"{'{{'} anchor_redirect(id={ obj.mkdocstrings_id !r}) {'}}'}"
                f"`{ obj.name }`"
            )

        type_value = ConfigOptionCode.extract(obj.conf_type)
        return (
            f"{'{{'} anchor_redirect(id={ obj.mkdocstrings_id !r}) {'}}'}"
            f"`#!py { obj.name } = { type_value }`"
        )




    @staticmethod
    def _to_docs_page_content(obj:'ConfOrOptSrc'):
        if not obj.is_config:
            return obj.docs

        class_name = obj.get_base_config_option_classname()
        no_summary = obj.name == 'args' or obj.is_macro or obj.is_plugin_config
        summary    = "" if no_summary else ConfigOptionsSummaryDumper.apply(obj)

        return f"""\
_Classe : `#!py { class_name }`_ | _Étend : `#!py { obj.DOCS_MRO_BASES }`_

{ obj.docs }{ summary }
"""






@dataclass
class ConfigOptionsSummaryDumper(Dumper):

    code: List[str] = field(default_factory=list)
    """ Lines of code for all getters (formatted and ordered) """


    def travel_with_dumper(self, obj:'ConfOrOptSrc', depth=0):
        if not obj.in_yaml_docs:
            return

        if not depth:
            # Recurse:
            for child in self._ordered_iter(obj):
                self.travel_with_dumper(child, 1)
        else:
            nicer_yml = to_nice_yml_value_for_docs(obj.default)
            line = f"| [`{ obj.name }`](#{ obj.py_macros_path }) | `#!yaml { nicer_yml }` |"
            self.code.append(line)


    def finalize(self, _):
        options = '\n    '.join(self.code)
        return f'''
<br><br><br>

| Options disponibles | Valeur par défaut |
|-|:-:|
{ options }

---

<br>

'''
