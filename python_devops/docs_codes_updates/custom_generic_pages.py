
import re

from pyodide_mkdocs_theme.pyodide_macros.parsing import add_indent, replace_chunk

from ..config import DOCS_MACRO_DATA
from .._files_update_tracker import write_with_feedback_if_needed
from ..py_codes_updates.macro_data_code import generate_macros_data_code_as_lst




def rebuild_maestro_tools_data_docs():

    def to_admo(code:str, admo:str='???'):
        code = code.strip()
        m = re.search(r"class (\w+)", code)
        return f"""
\n\n
{ admo } note "`#!py { m and m[1] }`"
    ```python
    { add_indent(code, '    ') }
    ```
\n\n"""

    source = DOCS_MACRO_DATA.read_text(encoding='utf-8')
    token  = '<span GENERATED_TOKEN></span>'

    content, has_content, *defs_lst = generate_macros_data_code_as_lst(False)

    defs_lst = [
        *map(to_admo, defs_lst),
        '''
\n\n### `Content` et `HasContent`

Si définies, les propriétés `content` et `has` des objets `MacroData` offrent les interfaces suivantes :
\n\n''',
        to_admo(content, '!!!'),
        to_admo(has_content, '!!!'),
    ]
    modified = replace_chunk(source, token, token, ''.join(defs_lst), keep_limiters=True)

    write_with_feedback_if_needed(DOCS_MACRO_DATA, modified, source)
