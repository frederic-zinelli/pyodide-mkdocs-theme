

import re
import json
from typing import List

from mkdocs.exceptions import BuildError

from pyodide_mkdocs_theme.pyodide_macros.tools_and_constants import JS_CONFIG_TEMPLATES
from pyodide_mkdocs_theme.pyodide_macros.parsing import replace_chunk
from pyodide_mkdocs_theme.pyodide_macros.messages import Lang

import python_devops.config as DEV_CONF

from python_devops.config import *
from ._files_update_tracker import write_with_feedback_if_needed


# pylint: disable=no-member










def validate_all_config_paths():
    """
    Check that all the files in the various config.py still exist oin the project.
    """
    invalid = []

    paths_to_check = [ (name, getattr(DEV_CONF, name)) for name in dir(DEV_CONF)]
    paths_to_check.append(
        ('JS_CONFIG_TEMPLATES', JS_CONFIG_TEMPLATES)
    )
    for data,path in paths_to_check:
        if isinstance(path, Path) and not path.exists():
            invalid.append(f'\n{data} = Path("{path}")')

    if invalid:
        raise FileNotFoundError("Missing files :" + "".join(invalid))





def check_js_config_classes_pool_structure(pool_properties:List[str]):
    """
    Check that all the CLASSES_POOL updates found in the scripts are defined in
    the JS_CONFIG_LIBS file.
    """

    # Check CONFIG.CLASSES_POOL initial declaration:
    js_config = JS_CONFIG_LIBS.read_text(encoding='utf-8')
    i = js_config.find('CLASSES_POOL:')
    i = js_config.find('{',i)
    j = js_config.find('}',i)+1

    pool_set = set(json.loads(js_config[i:j]))
    defined  = set(pool_properties)

    if len(defined) != len(pool_properties):
        raise BuildError(
            "Invalid CLASSES_POOL registrations. Some properties are registered several times:"
            + ''.join( "\n    "+name for name in pool_properties if pool_properties.count(name)>1 )
        )
    if defined != pool_set:
        raise BuildError(
            "Invalid CONFIG.CLASSES_POOL initial definition:"
            f"\n    Found in CONFIG:  { ', '.join(sorted(pool_set)) }"
            f"\n    Registered in JS: { ', '.join(sorted(defined))  }"
        )





def rebuild_root_jsconfig_import_map(jsconfig_paths:List[Path]):

    code = ',\n      '.join(
        f'"{ name }": ["./pyodide_mkdocs_theme/templates/{ path }"]' for name,path in jsconfig_paths
    )
    code = f"\n      { code }\n    "
    map_code = map_src = JS_CONFIG_JSON.read_text(encoding='utf-8')
    map_code = replace_chunk(map_code, '"paths": {', '}', code, keep_limiters=True)

    write_with_feedback_if_needed(JS_CONFIG_JSON, map_code, map_src)








def check_langs_consistencies():
    """
    * Make sure all the LangXx.py will be imported at runtime by rebuilding the function importing
      them (this enforces their availability).
    * Check that all the LangXx classes hold exactly the same properties.
    * Check that the dict used to redefine the custom messages also holds all the Lang properties.
    """
    __make_sure_all_xx_lang_files_are_imported_in_init()
    __check_all_lang_classes_properties_equal()
    __check_custom_messages_has_all_Lang_props()





def __make_sure_all_xx_lang_files_are_imported_in_init():
    """
    Logic:

    - The languages are seen in `Lang.get_lang_dct` only if the subclasses are created.
    - The classes are created only if the
    """

    langs = sorted(
        name.stem.split('_')[0] for name in PYM_MESSAGES.glob('*_lang.py')
                                if not name.stem.startswith('fr')  # already imported
    )
    imports = '\n    '.join(
        f"from .{ lang }_lang import Lang{ lang.title() }" for lang in langs
    )

    init = PYM_MESSAGES / '__init__.py'
    code = init.read_text(encoding='utf-8')
    i    = code.index('def __enforce_definitions_but_hidden_classes')
    repl = f"""\
def __enforce_definitions_but_hidden_classes():
    { imports }

__enforce_definitions_but_hidden_classes()
"""
    write_with_feedback_if_needed(init, code[:i]+repl, code, raise_on_update=True)






def __check_all_lang_classes_properties_equal():

    def diff(ref, other, lang):
        a = ref-other
        if a: bad.append(f"{lang} is missing: { ', '.join(a) }")

        b = other-ref
        if b: bad.append(f"{lang} shouldn't contain: { ', '.join(b) }")

    dct = Lang.get_langs_dct()
    ref = set(Lang.__annotations__)
    bad = []
    for lang,obj in dct.items():
        diff(ref, set(obj.__class__.__annotations__), lang)

    if bad:
        raise BuildError("Wrong LangXx declaration(s):\n  " + '\n  '.join(bad))





def __check_custom_messages_has_all_Lang_props():

    expected = set(Lang.__annotations__)
    done     = set()
    messages = DOCS_MESSAGES.read_text(encoding="utf-8")

    i = 0
    while (i:=messages.find("pyodide_mkdocs_theme.pyodide_macros.messages.Lang", i)) >= 0:
        i = messages.find('members', i)
        j = messages.find('\n\n', i)
        matches = re.findall(r"(?<=- )\w+", messages[i:j])
        done.update(matches)

    missing = expected - done
    if missing:
        missing_s = ''.join( f'\n    - {s}' for s in missing )
        raise BuildError(f'Missing Lang message properties in { DOCS_MESSAGES }:{ missing_s }')
