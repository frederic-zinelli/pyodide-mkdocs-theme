
# pylint: disable=all

import json
from typing import Dict
from pyodide_mkdocs_theme.pyodide_macros.plugin import (
    ConfigOptionSrc,
    PLUGIN_CONFIG_SRC,
    PluginConfigSrc,
    SubConfigSrc,
)
from pyodide_mkdocs_theme.pyodide_macros.plugin.config import ConfOrOptSrc
from pyodide_mkdocs_theme.pyodide_macros.plugin.config._string_tools import to_nice_yml_value_for_docs
from python_devops.docs_codes_updates._base_config_options_converters import ConfigOptionYamlSchema

from .config import *
from ._files_update_tracker import write_with_feedback_if_needed


YamlSchema = Dict[str, 'YamlSchema']





def rebuild_PyodideMacrosConfig_yaml_schemas():
    """
    Takes the plugin's config source object, `PLUGIN_CONFIG_SRC`, and recreates the whole
    YAML validation files needed to validate the plugin's config in mkdocs.yml.
    """
    yaml_dct  = plugin_yaml_schema(PLUGIN_CONFIG_SRC, GIT_LAB_PAGES)
    yaml_code = json.dumps(yaml_dct, indent=2)
    write_with_feedback_if_needed(PLUGIN_YAML_SCHEMA, yaml_code)






def plugin_yaml_schema(plugin_config:PluginConfigSrc, site_url:Path) -> YamlSchema:
    """
    Converts the current configSrc hierarchy to the equivalent json code to validate
    the yaml schema.
    """
    inner_schema = recursive_yaml_schema(plugin_config, site_url)
    plugin_name  = plugin_config.PLUGIN_NAME
    inner_schema['title'] = plugin_name

    python_schema = {
        "$schema": "http://json-schema.org/draft-07/schema",
        "title": plugin_name,
        "markdownDescription": inner_schema["markdownDescription"],
        "oneOf": [
            {"const": plugin_name},
            {
                "type": "object",
                "properties": {plugin_name: inner_schema},
                "additionalProperties": False
            }
        ]
    }
    return python_schema




def recursive_yaml_schema(obj:ConfOrOptSrc, site_url:Path):
    if obj.is_config:
        return sub_config_yaml_schema(obj, site_url)
    return option_yaml_schema(obj, site_url)




def sub_config_yaml_schema(obj:SubConfigSrc, site_url:Path) -> YamlSchema:
    props = {}
    for elt in obj.elements:
        if elt.in_yaml_docs or elt.name == '_dev_mode':
            schema = recursive_yaml_schema(elt, site_url)
            if schema:
                props[elt.name] = schema

    return props and {
        "markdownDescription": obj.get_yaml_schema_md_infos(site_url),
        "title": obj.name,
        "type": "object",
        "properties": props,
        "additionalProperties": False,
    }




def option_yaml_schema(obj:ConfigOptionSrc, site_url:Path) -> YamlSchema :

    sub_dct = ConfigOptionYamlSchema.get_schema(obj.conf_type)
    schema = {
        "markdownDescription": obj.get_yaml_schema_md_infos(site_url),
        "title": obj.name,
        **sub_dct,
    }

    if obj.name == 'decrease_attempts_on_user_code_failure':
        # Remove boolean values (deprecated)
        schema['enum'] = tuple( v for v in schema['enum'] if isinstance(v,str) )

    # NOTE: yaml options with null as default in the schema never show up => just ignore them
    if obj.default is not None and 'default' not in schema:
        schema['default'] = to_nice_yml_value_for_docs(obj.default)

    return schema
